#include "../include/Communicator.h"
#include <iostream>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>
#include <time.h>

Communicator::Communicator()
{
    cockpitWSServer.config.port = 8089;
    this->server.config.port = 8090;
    this->broadcast_delay_in_micro_sec = 500000;
    this->require_broadcast = true;
    this->require_private = false;
    this->DATAHANDLER_FLAG = true;
}

void Communicator::InitializeThreads()
{
    std::cout << "Creating Communicator Web Socket Thread" << std::endl;
    pthread_create(&cockpitDataServer_Thread, NULL, CockpitDataServer, this);

    std::cout << "Creating WebSocket Server Thread" << std::endl;
    pthread_create(&wsSocketServer_Thread, NULL, StartWsServer, this);

    std::cout << "Creating Message Set Thread" << std::endl;
    pthread_create(&messageSet_Thread, NULL, MessageSet, this);

    std::cout << "Creating Database update Thread" << std::endl;
    pthread_create(&databaseUpdate_Thread, NULL, DatabaseUpdate, this);
}

void Communicator::InitializeSharedMemory()
{
    // std::cout << "In SHM" << std::endl;

    //..... Device data shared memory locations....
    SMemory s_memory;
    s_memory.create("PressureSensor", 1024, 1);
    // std::cout << "After PS" << std::endl;
    s_memory.create("IMU", 1024, 12);
    // std::cout << "After TMU" << std::endl;
    s_memory.create("Altimeter", 1024, 1);
    // std::cout << "After Alt" << std::endl;
    s_memory.create("Joystick", 3072, 28);
    // std::cout << "After Joy" << std::endl;
    s_memory.create("Cygnus", 1024, 1);
    // std::cout << "After Cygn" << std::endl;
    s_memory.create("GPSRov", 1024, 5);
    // std::cout << "After GPS" << std::endl;
    s_memory.create("LeakSensor", 1024, 2);
    // std::cout << "After LS" << std::endl;
    s_memory.create("Cockpit", 1024, 2);
    // std::cout << "After Coc" << std::endl;
    s_memory.create_str("videoFolderPath", 1024);
    // std::cout << "After SHM" << std::endl;
    s_memory.create("frame_count", 1024, 2);
    // std::cout << "After SHM" << std::endl;
    s_memory.create_str("startTime", 1024);
    // std::cout << "After starttime" << std::endl;
    s_memory.create_str("endTime", 1024);
    // std::cout << "After endtime" << std::endl;
    s_memory.create_str("uniqueId", 1024);
    // std::cout << "After uniqueid" << std::endl;
    s_memory.create_bool("updateDbFlag", 1024);
    // std::cout << "After updateDbflag" << std::endl;
    // ..... Logger control by vision flag..............
    s_memory.create_bool("vision_status_flag", 1024);
    // std::cout<<"out shm"<<std::endl;
}

void *Communicator::CockpitDataServer(void *arguments)
{
    Communicator *Communicator_Instance = (Communicator *)arguments;
    SMemory shmData;

    auto &echo = Communicator_Instance->cockpitWSServer.endpoint["^/echo/?$"];

    echo.on_message = [&shmData, Communicator_Instance](std::shared_ptr<WsServer::Connection> connection, std::shared_ptr<WsServer::Message> message) {
        auto latitude_val = Communicator_Instance->to_string_with_precision(shmData.fetch("GPSRov", 1), 8);
        auto longitude_val = Communicator_Instance->to_string_with_precision(shmData.fetch("GPSRov", 2), 8);
        auto message_str =
            "{\"frontcameraFrameCount\":\"" + std::to_string((double)(shmData.fetch("frame_count", 1))) +
            "\"," + "\"bottomcameraFrameCount\":\"" + std::to_string((double)(shmData.fetch("frame_count", 2))) +
            "\"," + "\"depth\":\"" +
            std::to_string(
                ((int)(shmData.fetch("PressureSensor", 1) * 1000)) /
                1000.0) +
            "\"," + "\"altitude\":\"" +
            std::to_string(shmData.fetch("Altimeter", 1)) + "\"," +
            "\"thickness\":\"" + std::to_string(shmData.fetch("Cygnus", 1)) +
            "\"," + "\"heading\":\"" +
            std::to_string(((int)(shmData.fetch("IMU", 1) * 1000)) / 1000.0) +
            "\"," + "\"pitch\":\"" + std::to_string(shmData.fetch("IMU", 2)) +
            "\"," + "\"roll\":\"" + std::to_string(shmData.fetch("IMU", 3)) +
            "\"," + "\"gps_lat\":\"" + latitude_val + "\"," + "\"gps_lng\":\"" +
            longitude_val + "\"," + "\"gps_acc\":\"" +
            std::to_string(shmData.fetch("GPSRov", 3)) + "\"," +
            "\"gps_state\":\"" + std::to_string(shmData.fetch("GPSRov", 4)) +
            "\"," + "\"leak_sensor_1\":\"" +
            std::to_string(shmData.fetch("LeakSensor", 1)) + "\"," +
            "\"leak_sensor_2\":\"" +
            std::to_string(shmData.fetch("LeakSensor", 2)) + "\"," +
            "\"T_Depth\":\"" + std::to_string(shmData.fetch("Cockpit", 1)) +
            "\"," + "\"video_path\":\"" + shmData.fetch_str("videoFolderPath") +
            "\"," + "\"T_Heading\":\"" +
            std::to_string(shmData.fetch("Cockpit", 2)) + +"\"}";

        usleep(20000);

        auto send_stream = std::make_shared<WsServer::SendStream>();
        *send_stream << message_str;
        // connection->send is an asynchronous function
        connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
            if (ec)
            {
                std::cout << "Server: Error sending message. " <<
                    // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
                    "Error: " << ec << ", error message: " << ec.message() << std::endl;
            }
        });
    };

    echo.on_open = [](std::shared_ptr<WsServer::Connection> connection) {
        std::cout << "Server: Opened connection " << connection.get() << std::endl;
    };

    // See RFC 6455 7.4.1. for status codes
    echo.on_close = [](std::shared_ptr<WsServer::Connection> connection, int status, const std::string & /*reason*/) {
        std::cout << "Server: Closed connection " << connection.get() << " with status code " << status << std::endl;
    };

    // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
    echo.on_error = [](std::shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
        std::cout << "Server: Error in connection " << connection.get() << ". "
                  << "Error: " << ec << ", error message: " << ec.message() << std::endl;
    };

    std::thread server_thread([Communicator_Instance]() {
        // Start WS-server
        Communicator_Instance->cockpitWSServer.start();
    });

    // Wait for server to start so that the client can connect
    std::this_thread::sleep_for(std::chrono::seconds(1));
    server_thread.join();
}

void Communicator::exitCommunicator()
{
    this->cockpitWSServer.stop();
    this->StopServer();
}

std::string randomStringGenerator()
{
    srand(time(NULL));
    static const char alphanum[] =
        "0123456789"
        "!@#$^&*"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    std::string randomStr;
    for (unsigned int i = 0; i < 19; ++i)
    {
        randomStr += alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    return randomStr;
}

void *Communicator::StartWsServer(void *arguments)
{
    Communicator *Server_Instance = (Communicator *)arguments;
    Server_Instance->server.start();
    pthread_exit(NULL);
}

void *Communicator::DatabaseUpdate(void *arguments)
{
    Communicator *Database_Instance = (Communicator *)arguments;
    if (!(Database_Instance->server_endpoints_init_status))
    {
        if (Database_Instance->require_broadcast)
        {
            Database_Instance->BroadcastEndpoint();
        }
        Database_Instance->server_endpoints_init_status = true;
    }
    std::cout << "[WebSocketApi] Started broadcasting messages" << std::endl;
    while (!(Database_Instance->exit_broadcast_status))
    {
        if (Database_Instance->broadcast_data_status)
        {
            std::cout << Database_Instance->broadcast_msg << std::endl;
            Database_Instance->SendBroadcastMessage();
            usleep(10000);
            Database_Instance->StopBroadcast();
        }
        usleep(Database_Instance->broadcast_delay_in_micro_sec);
    }
    Database_Instance->exit_broadcast_status = true;
    pthread_exit(NULL);
}

void *Communicator::MessageSet(void *arguments)
{
    Communicator *Message_Instance = (Communicator *)arguments;
    SMemory shmData;
    while (Message_Instance->DATAHANDLER_FLAG)
    {
        if (!Message_Instance->broadcast_data_status)
        {
            // time_t my_time = time(NULL);
            // tm *curr_time = localtime(&my_time);
            // char buffer[80];
            // strftime(buffer, 80, "%T", curr_time);
            // shmData.update_str("startTime", buffer);
            // shmData.update_str("endTime", buffer);
            // shmData.update_bool("updateDbFlag", true);
            // shmData.update_str("uniqueId", randomStringGenerator());
            // auto startTime = shmData.fetch_str("startTime");
            // auto endTime = shmData.fetch_str("endTime");
            // auto updateDb = shmData.fetch_bool("updateDbFlag");
            auto videoPath = shmData.fetch_str("videoFolderPath");
            //std::cout<<"Video1: "<<videoPath<<std::endl;
            // auto message_str ="{\"startTime\":\"\'"+shmData.fetch_str("startTime") +"\'\" , "
            //    "\"endTime\":\"\'"+shmData.fetch_str("endTime")+"\'\" , "
            //    "\"updateDb\":" + std::to_string(shmData.fetch_bool("updateDb"))+" , "
            //    "\"uniqueId\":\"\'"+shmData.fetch_str("uniqueId")+ "\'\""
            //    "}";
            auto message_str = "{\"video_path\":\"" + videoPath + "\"}";
        
            // if(Message_Instance->broadcast_msg == message_str || videoPath.empty())
            // {
            //     //std::cout<<"Video: "<<videoPath<<std::endl;
            //     continue;
            // }
            Message_Instance->broadcast_msg = message_str;
            std::cout << "Message Set: " << Message_Instance->broadcast_msg << std::endl;
            Message_Instance->StartBroadcast();
        }
        usleep(500000);
    }
    pthread_exit(NULL);
}

bool Communicator::format_checker(std::string message_str)
{
    Json::Value root;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(message_str.c_str(), root);
    if (!parsingSuccessful)
    {
        return false;
    }
    return true;
}

std::string Communicator::to_string_with_precision(double value,
                                                   const int n = 6)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(n) << value;
    return out.str();
}