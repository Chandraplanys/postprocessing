

**libudev functions** -


1. Initialize the library, getting handle to a struct udev.
2. Enumerate the devices
3. For each device:
  ->Print its node name (eg: /dev/hidraw0).
  ->Find the ancestor node which represents the actual USB device (as opposed to the device's HID interface).
  ->Print the USB device information (IDs, serial number, etc).
  ->Unreference the device object.
4. Unreference the enumeration object.
5. Unreference the udev object..
	   
