#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H
#include <iostream>
#include<bits/stdc++.h>
#include<bits/stdc++.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

class ConfigParser
{
private:
  std:: string path;

public:

  ConfigParser();
  ConfigParser(std::string path);
  ~ConfigParser();

  std::map<std::string, std::string> configvaluemapping;

  bool parseFile();
  bool parseFile(std::string path);
  std::string getvalue(std::string searchingvalue);
  void addinmap(std::string first, std::string last);
  //bool checkDevice(std::string idProduct, std::string , std::string);

  std::string findDevice(std::string searchKey);
  std::string getFileCreationTime(void); 

};



#endif