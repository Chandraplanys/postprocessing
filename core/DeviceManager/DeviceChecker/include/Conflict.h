#ifndef CONFLICT_H
#define CONFLICT_H
//#include "../include/Main.h"
#include "../include/Support_DC.h"
#include "../include/ConfigParser.h"
class SupportDC;
class Conflict
{
private:
protected:
public:
  Conflict();
  ~Conflict();

  void conflictResolve(std::string key, std::string deviceType, SupportDC *support);
  bool reWriteLog(std::string idProduct, std::string idVendor, std::string serial);
  std::string alreadyInLog(std::string search, SupportDC *support);
};

#endif