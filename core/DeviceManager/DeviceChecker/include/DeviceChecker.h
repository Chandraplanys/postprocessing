#ifndef DEVICECHECKER_H
#define DEVICECHECKER_H
#include "../include/Main.h"
#include "../include/Support_DC.h"
#include "../include/Symlink.h"
#include "../include/ConfigParser.h"
#include "../include/DeviceScanner.h"
#include <unistd.h>
#include <libudev.h>
#define SUBSYSTEM "usb"
#include "Conflict.h"
#include <pthread.h>
#include <exception>
#include "boost/algorithm/string.hpp"

//TO DO automatic search for this directory
//static std::string deviceManagerPath = "/home/shan/Documents/Codes/PlanysDevelopment/core/DeviceManager";
//static std::string deviceManagerPath = "";
//std::string serachKey = "find $HOME -name \"99-usb-serial.rules\""; 

extern std::string USBrulePath;
extern std::string DCConfigPath;
extern std::string logFilePath;



class DeviceChecker
{
private:
  struct udev_monitor *mon;
  int fd;

  

  std::string oldConfigFileCreationTime;
  std::string newConfigFileCreationTime;

  bool parseConfigStatus;
  std::pair<std::string, std::string> keys;
  std::vector<std::string> v;
  std::vector<std::string> nonSymlinkDeviceUnique;

protected:
public:
  DeviceScanner deviceScanner;
  Symlink symlink;
  SupportDC support;
  ConfigParser configParser;
  //ConfigParser configParser;

  pthread_t configUpdatedThread;
  pthread_t monitorThread;

  bool configModificationFlag;
  bool monitorFlag;
  DeviceChecker();
  ~DeviceChecker();
  void createThreads();
  std::vector<std::string> print_device(struct udev_device *dev);
  void process_device(struct udev_device *dev);
  void enumerate_devices(struct udev *udev);
  void monitor_devices(struct udev *udev);
  bool checkSymlinkPresent(std::string Symlink);
  void uniqueDevice(std::vector<std::string> v);
  static void *checkConfigUpdated(void *arguments);
  static void *monitorDevicesThread(void *arguments);
  std::string searchFile(std::string cmd, std::string match);
  void updateLogFile();
};

#endif