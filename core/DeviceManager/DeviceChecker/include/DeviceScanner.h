#ifndef DEVICESCANNER_H
#define DEVICESCANNER_H

//#include "Main.h"
#include "Conflict.h"
#include "Symlink.h"
#include "Support_DC.h"
#include "ConfigParser.h"

class SupportDC;
class Conflict;
class DeviceScanner
{
private:
protected:
public:
  std::multimap<std::string, std::string> deviceInfo;
  Conflict *conflict;

  void removeDevice(std::string sysPath);

  bool conflictState(std::string key, std::string sysPath, std::string keySearch);

  void addDevice(std::string key, std::string value);
  bool rebootDevice(std::string key, std::string sysPath /*, std::string keySearch*/);
  bool checkDevice(std::string symLink, ConfigParser *PtrConfigParser, SupportDC *PtrSupport);
  void conflictStateCond(std::vector<std::string> v, std::vector<std::string> &device, SupportDC *PtrSupport);
  std::string getDeviceName(std::string key);
};

#endif