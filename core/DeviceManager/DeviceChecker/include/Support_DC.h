#ifndef SUPPORT_DC_H
#define SUPPORT_DC_H

//#include "../include/Main.h"
#include "../include/ConfigParser.h"
#include "../include/Symlink.h"

class Symlink;
class SupportDC
{
  private:
  protected:
  public:
    SupportDC();
    ~SupportDC();

    void moveFileFromUdev(void);
    void moveFileToUdev(std::string Path);
    void rebootDevice(std::string moduleName);
    void reloadRules(void);
    bool deviceTypeCheck(std::string Path);
    std::string exec(std::string cmd);
    std::string rovDevice(std::string dev);
    std::string serialID(std::string Path);
    std::string vendorID(std::string Path);
    std::string productID(std::string Path);
    std::string symLink(std::string Path);
    std::string productName(std::string Path);
    std::pair<std::string, std::string> generateKey(std::string, std::string, std::string);
    void SearchDirectories(std::string directory, std::string target_File_Name);
    void generateSymlink(std::string LastModifiedTime, std::string searchKey, ConfigParser **configParser, Symlink **symlink);
};

#endif