#ifndef SYMLINK_H
#define SYMLINK_H
//#include "../include/Main.h"
#include "../include/Support_DC.h"
#include "../include/DeviceScanner.h"
class SupportDC;
class Symlink
{
  private:
    std::vector<std::string> nonSymlinkDevice;
    std::map<std::string, std::string> symLinkDevice;

  protected:
  public:
    Symlink();
    ~Symlink();
    bool ifSymlink(std::string serial, SupportDC *support);
    void createSymlink(std::string serial, std::string idProduct, std::string idVendor, std::string SymlinkName);
    void createNewIncludedSymlink(ConfigParser *PtrConfigParser, std::vector<std::string> &v, SupportDC *PtrSupport);
    void getNonsymLinkDevice(ConfigParser *PtrConfigParser, std::vector<std::string> &v, SupportDC *PtrSupport);
    bool findInRules(std::string);
    bool isDevice(std::pair<std::string, std::string> p);
};

#endif