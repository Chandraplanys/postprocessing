#include "../include/ConfigParser.h"
#include "../include/DeviceChecker.h"
#include <iostream>
#include <fstream>
#include <bits/stdc++.h>

ConfigParser::ConfigParser()
{
}

ConfigParser::ConfigParser(std::string path)
{
    this->path = path;
}

ConfigParser::~ConfigParser()
{
}

bool ConfigParser::parseFile()
{
    return this->parseFile(this->path);
}

bool ConfigParser::parseFile(std::string path)
{
    //std::cout<<"path:::: "<<path<<std::endl;
    std::ifstream filestream(path.c_str());
    std::string buffer;
    //std::vector<std::string> v;
    int lineCount = 1;

    if (filestream)
    {

        while (getline(filestream, buffer))
        {
            // if (!buffer.length())
            // {
            //     lineCount++;
            //     continue;
            // }

            if (buffer[0] == '#' || !buffer.length())
            {
                lineCount++;
                continue;
            }

            //std::istringstream ss(buffer);
            std::string token;

            std::vector<std::string> tmpVector;

            boost::algorithm::split(tmpVector, buffer, boost::is_any_of("="));

            if (tmpVector.size() != 2)
            {
                std::cout<<"[DeviceManager][DeviceChecker]config.config file does not contain device info correctly after or before ... = ... "<<std::endl;
                filestream.close();
                return false;
            }
            else
            {
                std::string key = tmpVector[0];
                boost::trim(key);
                std::string value = tmpVector[1];
                boost::trim(key);


                std::vector<std::string> tmpVector1;

                boost::algorithm::split(tmpVector1, tmpVector[0], boost::is_any_of(","));

                if (tmpVector1.size() != 3)
                {
                    tmpVector.clear();
                    filestream.close();
                    std::cout<<"[DeviceManager][DeviceChecker]config.config file does not contain device info correctly"<<std::endl;
                    return false;
                }
                else
                {
                    // std::cout<<"key: "<<key<<std::endl;
                    // std::cout<<"value: "<<value<<std::endl;
                    // std::string key = v[0];
                    // std::string value = v[1];
                    addinmap(key, value);
                    tmpVector.clear();
                    
                }
                
            }







            // while (std::getline(ss, token, '='))
            // {
            //     //std::cout<<"token: "<<token<<std::endl;
            //     v.push_back(token);
            // }
            // std::string key = v[0];
            // std::string value = v[1];

            // // std::cout<<"key: "<<key<<std::endl;
            // // std::cout<<"value: "<<value<<std::endl;
            // addinmap(key, value);
            // v.clear();
        }
        filestream.close();
        return true;
    }
    else
    {
        std::cout << "[Device Checker][Config Parser][Parse File]could not open config file" << std::endl;
        filestream.close();
        return false;
    }
}

std::string ConfigParser::getvalue(std::string searchingvalue)
{
    std::map<std::string, std::string>::iterator ir = configvaluemapping.find(searchingvalue);

    if (ir != configvaluemapping.end())
    {
        return ir->second;
    }
    else
    {
        return "notfound";
    }
}

void ConfigParser::addinmap(std::string first, std::string last)
{
    configvaluemapping[first] = last;
}

std::string ConfigParser::findDevice(std::string serachKey)
{
    //std::ifstream filestream(deviceManagerPath + "/DeviceChecker/config.config");
    std::ifstream filestream(DCConfigPath);
    std::string buffer;
    if (filestream)
    {
        while (getline(filestream, buffer))
        {
            if (buffer.find(serachKey) != std::string::npos)
            {
                filestream.close();
                return buffer;
            }
        }
        filestream.close();
        return "notfound";
    }
    else
    {
        std::cout << "[Device Checker][Config Parser][Find Devices]could not open config file" << std::endl;
        return "notfound";
    }
}

std::string ConfigParser::getFileCreationTime(void)
{
    struct stat attr;
    stat(DCConfigPath.c_str(), &attr);
    return ctime(&attr.st_mtime);
}