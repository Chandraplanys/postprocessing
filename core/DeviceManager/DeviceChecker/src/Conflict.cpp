#include "../include/Conflict.h"
#include "../include/DeviceChecker.h"
#include <fstream>

Conflict::Conflict()
{
}

Conflict::~Conflict()
{
}

void Conflict::conflictResolve(std::string ConflictDevice, std::string deviceType, SupportDC *support)
{
    std::string file = logFilePath;
    // std::string file = deviceManagerPath + "/DeviceChecker/nonSymlinkDeviceLog";
    std::ofstream outfile(file.c_str(), std::ios_base::app);
    if (outfile)
    {
        std::string symlink = support->symLink(ConflictDevice);
        std::string idProduct = support->productID(ConflictDevice);
        std::string idVendor = support->vendorID(ConflictDevice);
        std::string serial = support->serialID(ConflictDevice);
        std::string pName = support->productName(ConflictDevice);
        std::string copy = idProduct + "," + idVendor + "," + serial + "=";
        outfile << "DEVICEPATH: " << deviceType << std::endl;
        outfile << "SYMLINK: " << symlink << std::endl;
        outfile << "IDPRODUCT: " << idProduct << std::endl;
        outfile << "IDVENDOR: " << idVendor << std::endl;
        outfile << "IDSERIAL: " << serial << std::endl;
        outfile << "PRODUCTNAME: " << pName << std::endl;

        outfile << "COPY THIS TO CONFIG FILE -> " << copy << std::endl;
        outfile << std::endl
                << "......................................................." << std::endl;
    }
    else
    {
        std::cout << "[Conflict][conflictResolve]problem creating conflictLog file...plzz correct file path" << std::endl;
        return;
    }
}

bool Conflict::reWriteLog(std::string idProduct, std::string idVendor, std::string serial)
{
    //std::string file = deviceManagerPath + "/DeviceChecker/nonSymlinkDeviceLog";
    std::string file = logFilePath;
    std::ofstream outfile(file.c_str(), std::ios_base::app);
    if (outfile)
    {
        outfile << "THIS DEVICE IS NOT CURRENTLY RUNNING IN SYSTEM" << std::endl;
        outfile << "idProduct: " << idProduct << std::endl;
        outfile << "idVendor: " << idVendor << std::endl;
        outfile << "serial: " << serial << std::endl;
        outfile << "COPY THIS TO CONFIG FILE -> ";
        outfile << idProduct << "," << idVendor << "," << serial << "=" << std::endl;
        outfile << std::endl
                << "......................................................." << std::endl;
        return true;
    }
    else
    {
        std::cout << "[Conflict][reWriteLog] problem creating nonSymlinkDeviceLog file" << std::endl;
        return false;
    }
}

std::string Conflict::alreadyInLog(std::string ConflictDevice, SupportDC *support)
{
    std::string key = support->productID(ConflictDevice) + "," + support->vendorID(ConflictDevice) + "," + support->serialID(ConflictDevice);
    //std::cout<<"key : "<<key<<std::endl;
    return key;
}
