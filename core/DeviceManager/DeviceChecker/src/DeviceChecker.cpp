#include "../include/DeviceChecker.h"
#include "../include/Symlink.h"
#include <stdlib.h>

std::string USBrulePath;
std::string DCConfigPath;
std::string logFilePath;

DeviceChecker::DeviceChecker()
{
    try
    {
        logFilePath = searchFile("find $HOME/Documents -name \"nonSymlinkDeviceLog\"", "/core/DeviceManager/DeviceChecker/nonSymlinkDeviceLog");
        std::cout << "log: " << logFilePath << std::endl;
        if (logFilePath != "")
        {

            //logFilePath = deviceManagerPath + "/DeviceChecker/nonSymlinkDeviceLog";
            remove(logFilePath.c_str());
        }
        monitorFlag = true;

        //check if udev rule file is in local path or not
        //USBrulePath = deviceManagerPath + "/DeviceChecker/99-usb-serial.rules";
        USBrulePath = searchFile("find $HOME/Documents -name \"DeviceChecker\"", "/core/DeviceManager/DeviceChecker");
        std::cout << "USB: " << USBrulePath << std::endl;
        if (USBrulePath != "")
        {
            std::ifstream fileStream(USBrulePath + "/99-usb-serial.rules");
            if (fileStream.is_open())
            {
                std::cout << "udev rule File is already Present" << std::endl;
                fileStream.close();
            }
            else
            {

                //create new udev rule local file
                std::ofstream infile(USBrulePath + "/99-usb-serial.rules", std::ofstream::out);
                if (infile.is_open())
                {
                    std::cout << "creating new udev rule file in local path" << std::endl;
                    infile.close();
                }
                else
                {
                    //std::cout << "[DeviceChecker] [deviceCheker] problem creating udev file" << std::endl;
                    throw;
                    //exit(1);
                }
            }
        }
        else
        {
            std::cout << "can't find directory /core/DeviceManager/DeviceChecker" << std::endl;
        }

        //DCConfigPath = deviceManagerPath + "/DeviceChecker/config.config";

        DCConfigPath = searchFile("find $HOME/Documents -name \"config.config\"", "/core/DeviceManager/DeviceChecker/config.config");
        std::cout << "config: " << DCConfigPath << std::endl;
        if (DCConfigPath != "")
        {
            parseConfigStatus = configParser.parseFile(DCConfigPath);
            if (parseConfigStatus == false)
            {
                std::cout << "[DeviceManager][DeviceChecker] ile config.config could not be parsed" << std::endl;
            }
        }
        else
        {
            std::cout << "can't search config file in directory /core/DeviceManager/DeviceChecker/" << std::endl;
        }
    }
    catch (std::exception &e)
    {
        std::cout << e.what() << std::endl;
        std::cout << "[DeviceChecker] [deviceCheker] problem creating udev file" << std::endl;
        exit(1);
    }

    //create symlink for all devices include in config
    symlink.getNonsymLinkDevice(&configParser, nonSymlinkDeviceUnique, &support);

    this->oldConfigFileCreationTime = configParser.getFileCreationTime();
    this->configModificationFlag = true;
}

void DeviceChecker::createThreads()
{

    int ret = pthread_create(&this->configUpdatedThread, NULL, this->checkConfigUpdated, this);
    usleep(1000);
    ret = pthread_create(&this->monitorThread, NULL, this->monitorDevicesThread, this);
    usleep(1000);
}

DeviceChecker::~DeviceChecker()
{
}

std::vector<std::string> DeviceChecker::print_device(struct udev_device *dev)
{
    std::cout << "........................." << std::endl;
    //get usb port path as a key to store in map for all added devices info
    std::string sysPath = udev_device_get_syspath(dev);
    // std::cout<<"sysPath = "<<sysPath<<std::endl;
    const char *subsystem = udev_device_get_subsystem(dev);
    if (!subsystem)
        subsystem = "unknown";

    const char *action = udev_device_get_action(dev);
    if (!action)
        action = "Already Connected";

    const char *vendor = udev_device_get_sysattr_value(dev, "idVendor");
    if (!vendor)
        vendor = "0000";

    const char *idProduct = udev_device_get_sysattr_value(dev, "idProduct");
    if (!idProduct)
        idProduct = "0000";

    const char *product = udev_device_get_sysattr_value(dev, "product");
    if (!product)
        product = "0000";

    const char *serial = udev_device_get_sysattr_value(dev, "serial");
    if (!serial)
        serial = "0000";

    std::string value = udev_device_get_devnode(dev);

    std::cout << product << "," << vendor << "," << serial << "," << std::endl;

    /* returning vector will consist following values at perticular indexes
    v[0] = switch case number
    v[1] = syspath (key to find device in map)
    v[2] = Mapkey (product+vendor)
    v[3] = configFileKey (product+vendor+serial)
    v[4] = serial
    v[5] = idProduct
    v[6] = vendor
    v[7] = deviceType
    it is not necessary every return vector will consist all of these values*/
    std::string deviceAction(action);
    std::string productString(product);
    if (deviceAction == "remove")
    {
        deviceScanner.removeDevice(sysPath);
        std::vector<std::string> v;
        v.push_back("0");
        v.push_back(sysPath);
        return v;
    }
    else
    {
        //is device internal or external
        bool deviceType = support.deviceTypeCheck(sysPath);

        if (productString.find("Hub") != std::string::npos || productString.find("xHCI") != std::string::npos)
            deviceType = false;

        if (deviceType == false)
        {
            //std::cout<<"internal found...."<<std::endl;
            std::vector<std::string> v;
            v.push_back("1");
            v.push_back(sysPath);
            return v;
        }

        //is device ttyUSB*, ttyACM*, video*
        std::string devType = support.rovDevice(sysPath.substr(4, sysPath.length() - 3));
        if (devType.length() == 0)
        {
            //std::cout<<"NOT ROV found...."<<std::endl;
            std::vector<std::string> v;
            v.push_back("2");
            v.push_back(sysPath);
            return v;
        }

        keys = support.generateKey(idProduct, vendor, serial);
        std::string Mapkey = keys.first;
        std::string ConfigFileKey = keys.second;

        //udev rules are reloaded
        if (deviceScanner.rebootDevice(Mapkey, sysPath /*, ConfigFileKey*/))
        {
            //std::cout<<"rebooting found...."<<std::endl;
            std::vector<std::string> v;
            v.push_back("3");
            v.push_back(sysPath);
            v.push_back(Mapkey);
            v.push_back(ConfigFileKey);
            return v;
        }

        //similar devices are there
        else if (deviceScanner.conflictState(Mapkey, sysPath, ConfigFileKey))
        {
            //std::cout<<"conflict found...."<<std::endl;
            std::vector<std::string> v;
            v.push_back("4");
            v.push_back(sysPath);
            v.push_back(Mapkey);
            v.push_back(ConfigFileKey);
            v.push_back(serial);
            v.push_back(idProduct);
            v.push_back(vendor);
            v.push_back(devType);
            return v;
        }

        //unique devices
        else
        {
            //std::cout<<"unique found...."<<std::endl;
            std::vector<std::string> v;
            v.push_back("5");
            v.push_back(sysPath);
            v.push_back(Mapkey);
            v.push_back(ConfigFileKey);
            v.push_back(serial);
            v.push_back(idProduct);
            v.push_back(vendor);
            v.push_back(devType);
            return v;
        }
    }
}

void DeviceChecker::process_device(struct udev_device *dev)
{
    if (dev)
    {
        if (udev_device_get_devnode(dev))
        {
            v = print_device(dev);
            //int i = deviceAction.first;
            std::string c = v[0];
            //std::cout<<c<<std::endl;
            switch (stoi(c))
            {
            case 0: //remove device
                break;
            case 1: //internal device
                break;
            case 2: //not a ROV device
                break;
            case 3: //rebooting devices
                break;
            case 4: //same devices are in conflict
                if (symlink.ifSymlink(v[1], &support) == false)
                    deviceScanner.conflictStateCond(v, nonSymlinkDeviceUnique, &support);
                else
                {
                    std::cout << "......................." << std::endl;
                }
                break;
            case 5: //unique device
                uniqueDevice(v);
                break;
            default:
                std::cout << "ERROR:- [DeviceChecker][printdevice] is not returning any approriate value..look switch case in processDevice()" << std::endl;
                break;
            }
        }
        udev_device_unref(dev);
    }
}

void DeviceChecker::enumerate_devices(struct udev *udev)
{
    struct udev_enumerate *enumerate = udev_enumerate_new(udev);

    udev_enumerate_add_match_subsystem(enumerate, SUBSYSTEM);
    udev_enumerate_scan_devices(enumerate);

    struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
    struct udev_list_entry *entry;

    udev_list_entry_foreach(entry, devices)
    {
        const char *path = udev_list_entry_get_name(entry);
        struct udev_device *dev = udev_device_new_from_syspath(udev, path);
        process_device(dev);
    }

    udev_enumerate_unref(enumerate);
}

void DeviceChecker::monitor_devices(struct udev *udev)
{
    mon = udev_monitor_new_from_netlink(udev, "udev");

    udev_monitor_filter_add_match_subsystem_devtype(mon, SUBSYSTEM, NULL);
    udev_monitor_enable_receiving(mon);

    fd = udev_monitor_get_fd(mon);
}

//get device name(if running) from symlink name
bool DeviceChecker::checkSymlinkPresent(std::string symLink)
{
    struct stat p_statbuf;
    if (lstat(symLink.c_str(), &p_statbuf) < 0)
    { /* if error occured */
        //perror("[deviceChecker][is_symlink] calling stat()");
        return false;
    }

    if (S_ISLNK(p_statbuf.st_mode) == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void DeviceChecker::uniqueDevice(std::vector<std::string> v)
{
    deviceScanner.addDevice(v[2], v[1]);

    //std::string symLinkName = configParser.findDevice(v[3]);
    std::string symLinkName = configParser.getvalue(v[3]);
    //std::cout<<"symlinkNameUnique: "<<symLinkName<<std::endl;
    if (symLinkName == "notfound")
    {
        SupportDC *Ptrsupport = &support;
        if (std::find(nonSymlinkDeviceUnique.begin(), nonSymlinkDeviceUnique.end(), v[3]) != nonSymlinkDeviceUnique.end())
        {
            std::cout << "New ROV Device " << v[2] << " is detected" << std::endl
                      << "Check DeviceUnique File and add device symlink name in config to create symlink" << std::endl;
            std::cout << "........................." << std::endl;
            return;
        }
        else
        {
            nonSymlinkDeviceUnique.push_back(v[3]);
            deviceScanner.conflict->conflictResolve(v[1], v[7], Ptrsupport);
            std::cout << "New ROV Device " << v[2] << " is detected" << std::endl
                      << "Check DeviceUnique File and add device symlink name in config to create symlink" << std::endl;
            std::cout << "........................." << std::endl;
            return;
        }
    }
    else
    {
        if (symlink.ifSymlink(v[1], &support))
        {
            std::cout << "symlink already created for the device" << std::endl;
            std::cout << ".........................." << std::endl;
            return;
        }

        symlink.createSymlink(v[4], v[5], v[6], symLinkName);
        support.moveFileToUdev(USBrulePath);
        support.reloadRules();
        std::cout << "Symlink Created for the device" << std::endl;
        std::cout << ".........................." << std::endl;
        return;
    }
}

void *DeviceChecker::checkConfigUpdated(void *arguments)
{
    DeviceChecker *DCInstance = (DeviceChecker *)arguments;
    DCInstance->oldConfigFileCreationTime = DCInstance->configParser.getFileCreationTime();
    std::cout << "ConfigFile Last Update: " << DCInstance->oldConfigFileCreationTime << std::endl;
    while (DCInstance->configModificationFlag)
    {
        DCInstance->newConfigFileCreationTime = DCInstance->configParser.getFileCreationTime();
        if (DCInstance->newConfigFileCreationTime != DCInstance->oldConfigFileCreationTime)
        {
            DCInstance->symlink.createNewIncludedSymlink(&(DCInstance->configParser), DCInstance->nonSymlinkDeviceUnique, &(DCInstance->support));
            DCInstance->oldConfigFileCreationTime = DCInstance->newConfigFileCreationTime;
            DCInstance->updateLogFile();
        }
        usleep(20000);
    }
    std::cout << "exiting config thread...." << std::endl;
}

void *DeviceChecker::monitorDevicesThread(void *arguments)
{
    struct timeval tv;
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    DeviceChecker *DCInstance = (DeviceChecker *)arguments;

    while (DCInstance->monitorFlag)
    {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(DCInstance->fd, &fds);
        int ret = select(DCInstance->fd + 1, &fds, NULL, NULL, &tv);
        if (ret < 0)
            break;
        if (FD_ISSET(DCInstance->fd, &fds))
        {
            struct udev_device *dev = udev_monitor_receive_device(DCInstance->mon);
            DCInstance->process_device(dev);
        }
        usleep(100000);
    }
    std::cout << "exiting montior device thread...." << std::endl;
}

void DeviceChecker::updateLogFile()
{
    remove(logFilePath.c_str());
    std::sort(this->nonSymlinkDeviceUnique.begin(), this->nonSymlinkDeviceUnique.end());
    for (std::vector<std::string>::iterator ir = this->nonSymlinkDeviceUnique.begin(); ir != this->nonSymlinkDeviceUnique.end(); ++ir)
    {
        /* std::string idProduct = (*ir).substr(0,4);
        std::string idVendor = (*ir).substr(5,4);
        std::string serial = (*ir).substr(10, 8);*/
        std::vector<std::string> vi;
        std::istringstream ss(*ir);
        std::string token;
        while (std::getline(ss, token, ','))
        {
            //std::cout<<"tokenizedddd: "<<token<<std::endl;
            vi.push_back(token);
        }
        if (vi.size() == 3)
        {
            std::string idProduct = vi[0];
            std::string idVendor = vi[1];
            std::string serial = vi[2];
            vi.clear();

            std::string sysPath = deviceScanner.getDeviceName(idProduct + idVendor);
            if (sysPath != "notfound")
            {
                deviceScanner.conflict->conflictResolve(sysPath, vi[7], &support);
            }
            else
            {
                bool st = deviceScanner.conflict->reWriteLog(idProduct, idVendor, serial);
                if (st == false)
                {
                    std::cout << "[DeviceChecker][updateLogFile] device serial is not included in log file" << std::endl;
                }
            }
        }
        else
        {
            std::cout << "[DeviceChecker][updateLogFile] non symlink device in vector is not valid" << std::endl;
            vi.clear();
        }
    }
}

std::string DeviceChecker::searchFile(std::string cmd, std::string match)
{
    char buffer[128];
    std::string result = "";
    FILE *file;
    file = popen(cmd.c_str(), "r");

    if (!file)
    {
        throw std::runtime_error("[DeviceChecker][FindFile] popen() failed");
    }

    try
    {
        while (!feof(file))
        {
            if (fgets(buffer, 128, file) != NULL)
            {
                result += buffer;
            }
        }
    }
    catch (...)
    {
        pclose(file);
        throw;
    }
    pclose(file);

    //std::cout<<"result: "<<result<<std::endl;

    std::istringstream op(result);
    std::string line;
    while (std::getline(op, line))
    {
        if (boost::contains(line, match))
        {
            return line;
        }
    }
    return "";
}