#include "../include/DeviceScanner.h"

void DeviceScanner::removeDevice(std::string sysPath)
{
    std::multimap<std::string, std::string>::iterator itr = deviceInfo.begin();

    for (itr; itr != deviceInfo.end(); ++itr)
    {
        if (itr->second == sysPath)
        {
            std::cout << itr->first << " is disconnected" << std::endl;
            deviceInfo.erase(itr);
            std::cout << ".................." << std::endl;
            break;
        }
    }
}

bool DeviceScanner::conflictState(std::string key, std::string sysPath, std::string keySearch)
{
    if (deviceInfo.find(key) != deviceInfo.end())
    {
        deviceInfo.insert(std::pair<std::string, std::string>(key, sysPath));
        std::cout << deviceInfo.count(key) << " similar devices are there" << std::endl;
        return true;
    }
    return false;
}

void DeviceScanner::addDevice(std::string key, std::string value)
{
    deviceInfo.insert(std::pair<std::string, std::string>(key, value));
}

bool DeviceScanner::rebootDevice(std::string key, std::string sysPath /*, std::string keySearch*/)
{
    if (deviceInfo.find(key) != deviceInfo.end())
    {

        typedef std::multimap<std::string, std::string>::iterator iterator1;

        std::pair<iterator1, iterator1> iterpair1 = deviceInfo.equal_range(key);

        for (iterator1 it = iterpair1.first; it != iterpair1.second; ++it)
        {
            if (it->second == sysPath)
                return true;
        }
    }
    return false;
}

/*bool DeviceScanner::checkDevice(std::string symLink, ConfigParser *PtrConfigParser, SupportDC *PtrSupport)
{
    std::string output = PtrConfigParser->findDevice(symLink);
    if (output != "notfound")
    {
        size_t i = output.find("=");
        if (i != std::string::npos)
        {
            std::vector<std::string> vi;
            std::istringstream ss(output);
            std::string token;
            while (std::getline(ss, token, ','))
            {
                //std::cout<<"tokenizetocheck: "<<token<<std::endl;
                vi.push_back(token);
            }
            if (vi.size() == 3)
            {
                std::string idProduct = vi[0];
                std::string idVendor = vi[1];
                std::string serial = vi[2];
                vi.clear();

                std::string key = idProduct + idVendor;
                if (deviceInfo.find(key) != deviceInfo.end())
                {

                    typedef std::multimap<std::string, std::string>::iterator iterator1;

                    std::pair<iterator1, iterator1> iterpair1 = deviceInfo.equal_range(key);

                    std::string idSerial = output.substr(10, i - 10);
                    //std::cout<<"idSerial: "<<idSerial<<std::endl;

                    for (iterator1 it = iterpair1.first; it != iterpair1.second; ++it)
                    {
                        std::string getSerial = PtrSupport->serialID(it->second);
                        //std::cout<<"getSerial: "<<getSerial<<std::endl;
                        if (getSerial == idSerial)
                        {
                            // std::cout << "device: " << key << " " << it->second << " is present" << std::endl;
                            return true;
                        }
                    }
                    std::cout << "symlink device not present but similar device with another symlink name running" << std::endl;
                    return false;
                }
                else
                {
                    // std::cout << "symlink device not Found" << std::endl;
                    return false;
                }
            }
            else
            {
                std::cout << "symlink is in config but device is not valid" << std::endl;
                vi.clear();
                return false;
            }
        }
    }
    else
    {
        // std::cout << "symlink name is not in config file" << std::endl;
        return false;
    }
}*/

void DeviceScanner::conflictStateCond(std::vector<std::string> v, std::vector<std::string> &device, SupportDC *PtrSupport)
{
    typedef std::multimap<std::string, std::string>::iterator iterator;
    std::pair<iterator, iterator> iterpair = deviceInfo.equal_range(v[2]);

    for (iterator it = iterpair.first; it != iterpair.second; ++it)
    {
        std::string conflictDevice = it->second;
        //std::cout<<"v[3]- "<<v[3]<<std::endl;
        if (std::find(device.begin(), device.end(), v[3]) != device.end())
        {
            //std::cout<<"found........\n";
            //std::cout<<"....................."<<std::endl;
            //return;
            continue;
        }
        else
        {
            std::string s = conflict->alreadyInLog(conflictDevice, PtrSupport);

            if (std::find(device.begin(), device.end(), s) != device.end())
            {
                continue;
            }
            conflict->conflictResolve(conflictDevice, v[7], PtrSupport); //  conflictResolve(conflictDevice, "Conflict", &PtrSupport);
            std::cout << "Conflict Device " << v[2] << " is detected" << std::endl
                      << "Check DeviceConflict File and add device symlink name in config to create symlink" << std::endl;
            std::cout << "........................." << std::endl;
        }
    }
    device.push_back(v[3]);
}

std::string DeviceScanner::getDeviceName(std::string key)
{
    std::map<std::string, std::string>::iterator ir = this->deviceInfo.find(key);

    if (ir != this->deviceInfo.end())
    {
        return ir->second;
    }
    else
    {
        return "notfound";
    }
}