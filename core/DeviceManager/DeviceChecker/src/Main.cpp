#include "../include/Main.h"
#include "../include/Symlink.h"
#include "../include/DeviceChecker.h"
#include "../include/Support_DC.h"
#include "../include/ConfigParser.h"
#include <pthread.h>
#include <bits/pthreadtypes.h>
#include <signal.h>
//bool exitProgram = true;

void shutDown()
{
    //exitProgram = false;
}

void handle_signal(int s)
{

    printf("Interupt signal Received  %d  symlink setter", s);

    // Broken pipe from a client
    if (s == SIGPIPE)
    {

        // Just ignore, sock class will close the client the next time
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM)
    {
        std::cout << "triggering shut down symlink setter...." << std::endl;
        shutDown();
    }
}

void initSignal()
{
    struct sigaction sa;

    //setup the sighub handler
    sa.sa_handler = &handle_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
    sigaction(SIGSEGV, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
}

int main(int argsc, char **arbv)
{
    //initialize signal handler
    initSignal();
    DeviceChecker deviceChecker;

    //Create the udev object
    struct udev *udev = udev_new();
    if (!udev)
    {
        -fprintf(stderr, "[main] udev_new() failed\n");
        return 1;
    }

    deviceChecker.enumerate_devices(udev);
    deviceChecker.monitor_devices(udev);
    deviceChecker.createThreads();
    // std::stack<std::string> s;
    // s.push("/dev/IMU");
    // s.push("/dev/shghsg");
    // s.push("/dev/Altimeter");
    // s.push("/dev/Cygnus");
    // s.push("/dev/kjsd");

    // while (exitProgram)
    // {
    //     if (!s.empty())
    //     {
    //         if (deviceChecker.checkSymlinkPresent(s.top()))
    //         {
    //             std::cout << "symlink:present " << std::endl;
    //         }
    //         else
    //         {
    //             std::cout << "symlink:not present " << std::endl;
    //         }
    //         s.pop();
    //     }
    //     usleep(100000);
    // }
    deviceChecker.configModificationFlag = false;
    std::cout << "triggering internal threads shut down" << std::endl;
    pthread_join(deviceChecker.configUpdatedThread, NULL);
    deviceChecker.monitorFlag = false;
    pthread_join(deviceChecker.monitorThread, NULL);
    udev_unref(udev);
    std::cout << "exiting Program...." << std::endl;
    return 0;
}
