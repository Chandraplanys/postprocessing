#include "../include/Support_DC.h"
#include "../include/ConfigParser.h"
#include "../include/Symlink.h"
#include <fstream>
//#include <chrono>
//#include <thread>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>
#include <locale.h>
#include <langinfo.h>
#include <fcntl.h>
SupportDC::SupportDC()
{
}

SupportDC::~SupportDC()
{
}

void SupportDC::moveFileToUdev(std::string Path)
{
  std::string system_cmd = "cp " + Path + " /etc/udev/rules.d";
  system(system_cmd.c_str());
}

void SupportDC::rebootDevice(std::string moduleName)
{
  std::string cmd1 = "modprobe -r " + moduleName;
  std::string cmd2 = "modprobe " + moduleName;

  system(cmd1.c_str());
  system(cmd2.c_str());
}

void SupportDC::reloadRules(void)
{
  std::string cmd = "udevadm control --reload-rules && udevadm trigger";
  system(cmd.c_str());
}

bool SupportDC::deviceTypeCheck(std::string Path)
{
  std::string relativePath = Path + "/removable";
  std::ifstream myfile;
  myfile.open(relativePath.c_str());
  if (!myfile)
  {
    std::cout << "[support_DC][deviceTypeCheck]Unable to check removable Device" << std::endl;
    return false;
  }
  size_t pos1, pos2;
  std::string line;
  while (myfile.good())
  {
    getline(myfile, line);
    pos1 = line.find("removable");
    pos2 = line.find("unknown");

    if (pos1 != std::string::npos || pos2 != std::string::npos) // string::npos is returned if string is not found
    {
      return true;
    }
  }

  myfile.close();
  return false;
}

std::string SupportDC::exec(std::string command)
{
  const char *cmd = command.c_str();
  char buffer[128];
  std::string result = "";
  FILE *pipe = popen(cmd, "r");
  if (!pipe)
    throw std::runtime_error("[support_DC][exec]popen() failed!");
  try
  {
    while (!feof(pipe))
    {
      if (fgets(buffer, 128, pipe) != NULL)
        result += buffer;
    }
  }
  catch (...)
  {
    pclose(pipe);
    throw;
  }
  pclose(pipe);
  return result;
}

std::string SupportDC::rovDevice(std::string dev)
{
  std::string retV = "";
  std::string cmd1 = "ls -l /sys/class/tty/ttyUSB*";
  std::string cmd2 = "ls -l /sys/class/video*";
  std::string cmd3 = "ls -l /sys/class/tty/ttyACM*";

  std::string result1 = exec(cmd1);
  std::string result2 = exec(cmd2);
  std::string result3 = exec(cmd3);

  //std::cout<<result3<<std::endl;

  std::size_t i1 = result1.find(dev);
  std::size_t i2 = result2.find(dev);
  std::size_t i3 = result3.find(dev);

  if (i1 != std::string::npos)
  {
    std::string usbType = "";
    std::size_t j1 = result1.find("ttyUSB", i1);
    if (j1 != std::string::npos)
    {
      usbType = result1.substr(j1, 7);
    }
    std::cout << "usb: " << usbType << std::endl;
    std::cout << "ttyUSB* device" << std::endl;
    return usbType;
  }
  else if (i2 != std::string::npos)
  {
    std::cout << "video* device" << std::endl;
    std::string videoType = "";
    std::size_t j2 = result2.find("video", i2);
    if (j2 != std::string::npos)
    {
      videoType = result2.substr(j2, 6);
    }
    std::cout << "usb: " << videoType << std::endl;
    std::cout << "video* device" << std::endl;
    return videoType;
  }
  else if (i3 != std::string::npos)
  {
    std::cout << "ttyACM* device" << std::endl;
    std::string acmType = "";
    std::size_t j3 = result3.find("ttyACM", i3);
    //std::cout<<result3[j3]<<result3[j3+1]<<result3[j3+2]<<std::endl;
    if (j3 != std::string::npos)
    {

      acmType = result3.substr(j3, 7);
    }
    std::cout << "usb: " << acmType << std::endl;
    std::cout << "ttyACM* device" << std::endl;
    return acmType;
  }
  return retV;
}

std::string SupportDC::serialID(std::string Path)
{
  std::string cmd = "udevadm info --query=all --path=" + Path + " | grep ID_SERIAL_SHORT";
  std::string serial = exec(cmd.c_str());

  if (serial != "")
  {
    serial = serial.substr(19, serial.length() - 19);
    int j = 0;
    for (int i = 0; i < serial.length(); ++i)
    {
      j++;
      if (serial[i] == ' ')
        break;
    }
    serial = serial.substr(0, j - 1);
  }
  return serial;
}

std::string SupportDC::vendorID(std::string Path)
{

  std::string cmd = "udevadm info --query=all --path=" + Path + " | grep ID_VENDOR_ID";
  std::string vendor = exec(cmd.c_str());

  if (vendor != "")
  {
    vendor = vendor.substr(16, vendor.length() - 16);
    int j = 0;
    for (int i = 0; i < vendor.length(); ++i)
    {
      j++;
      if (vendor[i] == ' ')
        break;
    }
    vendor = vendor.substr(0, j - 1);
  }
  return vendor;
}

std::string SupportDC::productID(std::string Path)
{
  std::string cmd = "udevadm info --query=all --path=" + Path + " | grep ID_MODEL_ID";
  std::string product = exec(cmd.c_str());

  if (product != "")
  {
    product = product.substr(15, product.length() - 15);
    int j = 0;
    for (int i = 0; i < product.length(); ++i)
    {
      j++;
      if (product[i] == ' ')
        break;
    }
    product = product.substr(0, j - 1);
  }

  return product;
}

std::string SupportDC::symLink(std::string Path)
{
  std::string cmd = "udevadm info --query=all --path=" + Path + " | grep DEVLINKS";
  std::string symlink = exec(cmd.c_str());

  if (symlink != "")
    symlink = symlink.substr(17, symlink.length() - 17);
  return symlink;
}

std::string SupportDC::productName(std::string Path)
{
  std::string cmd = "udevadm info --query=all --path=" + Path + " | grep ID_VENDOR_FROM_DATABASE";
  std::string pName = exec(cmd.c_str());

  if (pName != "")
    pName = pName.substr(27, pName.length() - 27);
  return pName;
}

std::pair<std::string, std::string> SupportDC::generateKey(std::string idProduct, std::string idVendor, std::string serial)
{
  std::string Mapkey = idProduct;
  Mapkey += idVendor;

  std::string ConfigkeyFileKey = idProduct;
  ConfigkeyFileKey += ",";
  ConfigkeyFileKey += idVendor;
  ConfigkeyFileKey += ",";
  ConfigkeyFileKey += serial;

  return std::make_pair(Mapkey, ConfigkeyFileKey);
}

void SupportDC::SearchDirectories(std::string directory, std::string target_File_Name)
{
    DIR *dirp = opendir(directory.c_str());
    if (!dirp)
    {
        perror(("opendir " + directory).c_str());
        return;
    }

    struct dirent *dptr;
    while(dptr = readdir(dirp))
    {
        std::string file_Name = dptr->d_name;
        std::string file_Path = directory + "/" + file_Name;

        struct stat statStruct; 
        stat(file_Path.c_str(), &statStruct); 
        if( S_ISDIR(statStruct.st_mode) )
        {

	    	
            if ( file_Name.compare(".") == 0 || file_Name.compare("..") == 0 )
	          {
                continue; 
            }

            SearchDirectories(file_Path, target_File_Name);
        }
        else if( S_ISREG(statStruct.st_mode))
        {
            if( file_Name.compare(target_File_Name) == 0)
            {
                std::cout <<"found: "<< file_Path << std::endl;
            }
        }   
    }

    closedir(dirp);
}
