#include "../include/Symlink.h"
//#include "../include/Main.h"
#include "../include/DeviceChecker.h"
Symlink::Symlink()
{
}

Symlink::~Symlink()
{
}

bool Symlink::ifSymlink(std::string key, SupportDC *support)
{
  std::string SymLinkName = support->symLink(key);
  if (SymLinkName.length())
  {
    //std::cout<<"Symlink: "<<SymLinkName<<std::endl;
    return true;
  }
  return false;
}

void Symlink::createSymlink(std::string serial, std::string idProduct, std::string idVendor, std::string SymlinkName)
{
  //creating symlink by writing in 99-usb-serial.rules
  //std::ofstream rulefile( deviceManagerPath + "/DeviceChecker/99-usb-serial.rules", std::ios::app);
  std::ofstream rulefile(USBrulePath + "/99-usb-serial.rules", std::ios::app);
  if (rulefile.is_open())
  {
    if (rulefile.good())
    {
      std::string lines = "ATTRS{idVendor}==\"" + idVendor + "\", ATTRS{idProduct}==\"" + idProduct + "\", ATTRS{serial}==\"" + serial + "\", SYMLINK+=\"" + SymlinkName + "\"";
      rulefile << lines << std::endl;
      rulefile.close();
    }
    else
    {
      std::cout << "[symlink][createSymlink] unable to write rule correctly" << std::endl;
    }
    return;
  }

  std::cout << "[symlink][createSymlink] can't open 99-usb-serial rule file" << std::endl;
  rulefile.close();
  return;
}

void Symlink::createNewIncludedSymlink(ConfigParser *PtrConfigParser, std::vector<std::string> &v, SupportDC *PtrSupport)
{
  //std::cout<<"again reading"<<std::endl;
  //PtrConfigParser->parseFile(deviceManagerPath + "/DeviceChecker/config.config");
  PtrConfigParser->parseFile(DCConfigPath);
  getNonsymLinkDevice(PtrConfigParser, v, PtrSupport);
}

void Symlink::getNonsymLinkDevice(ConfigParser *PtrConfigParser, std::vector<std::string> &v, SupportDC *PtrSupport)
{
  for (std::map<std::string, std::string>::iterator ir = PtrConfigParser->configvaluemapping.begin(); ir != PtrConfigParser->configvaluemapping.end(); ++ir)
  {
    /*if(isDevice(*ir) == false)
    {
      //std::cout<<ir->first<<" not a device"<<std::endl;
      continue;
    }*/

    if (findInRules(ir->second) == false)
    {
      std::cout << "creating symlink for device: " << ir->first << "=" << ir->second << std::endl;
      /*std::string idProduct = ir->first.substr(0, 4);
      std::string idVendor= ir->first.substr(5, 4);
      std::string serial = ir->first.substr(10, 8);*/
      std::vector<std::string> vi;
      std::istringstream ss(ir->first);
      std::string token;
      while (std::getline(ss, token, ','))
      {
        //std::cout<<"tokenize: "<<token<<std::endl;
        vi.push_back(token);
      }
      if (vi.size() == 3)
      {
        std::string idProduct = vi[0];
        std::string idVendor = vi[1];
        std::string serial = vi[2];
        vi.clear();

        std::string SymlinkName = ir->second;
        std::cout << "symlink Name: " << SymlinkName << std::endl;
        createSymlink(serial, idProduct, idVendor, SymlinkName);

        //PtrSupport->moveFileToUdev(deviceManagerPath + "/DeviceChecker/99-usb-serial.rules");
        PtrSupport->moveFileToUdev(USBrulePath + "/99-usb-serial.rules");
        PtrSupport->reloadRules();
        /*std::string system_cmd = "cp /home/mikros/Documents/CodeBase/core/DeviceManager/DeviceChecker/99-usb-serial.rules /etc/udev/rules.d";
        system(system_cmd.c_str());
        std::string cmd1 = "udevadm control --reload-rules && udevadm trigger";
        system(cmd1.c_str());*/
        std::cout << SymlinkName << " created for serial" << serial << " device" << std::endl;
        std::string key = idProduct + "," + idVendor + "," + serial;
        std::cout << key << std::endl;
        v.erase(std::remove(v.begin(), v.end(), key), v.end());
      }
      else
      {
        std::cout << "[Symlink][getNonsymLinkDevice]one of device is not copied correctly in config.config for symlink" << std::endl;
        vi.clear();
      }
    }
  }
}

bool Symlink::findInRules(std::string symLink)
{
  //std::ifstream inFile(deviceManagerPath + "/DeviceChecker/99-usb-serial.rules");
  std::ifstream inFile(USBrulePath + "/99-usb-serial.rules");
  std::string line;
  std::vector<std::string> v;

  if (inFile)
  {
    while (inFile.good())
    {
      getline(inFile, line);                // get line from file
      std::size_t pos = line.find(symLink); // search
      // std::cout<<"lol: "<<pos<<std::endl;
      if (pos != std::string::npos) // string::npos is returned if string is not found
      {
        //  std::cout<<"found"<<std::endl;
        inFile.close();
        return true;
      }
    }
    inFile.close();
    return false;
  }
  else
  {
    std::cout << "[Device Checker][Symlink]could not open config file" << std::endl;
    return false;
    inFile.close();
  }
}

bool Symlink::isDevice(std::pair<std::string, std::string> p)
{
  std::string attr = p.first;
  if (attr[4] == ',' && attr[9] == ',' && (p.second) != "")
    return true;
  else
    return false;
}
