#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    //if (!pipe) throw std::runtime_error("popen() failed!");
    //try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    //} //catch (...) {
        //pclose(pipe);
        //throw;
   // }
    pclose(pipe);
    return result;
}

int main()
{
  std::string cmd1 = "ls -l /sys/class/tty/ttyUSB*";
  std::string cmd2 = "ls -l /sys/class/video*";
  std::string cmd3 = "ls -l /sys/class/tty/ttyACM*";

  std::string result1 = exec(cmd1.c_str());
  std::string result2 = exec(cmd2.c_str());
  std::string result3 = exec(cmd3.c_str());

  std::cout<<"result1: "<<result1<<std::endl;
  std::cout<<"result2: "<<result2<<std::endl;
  std::cout<<"result3: "<<result3<<std::endl;
  return 0;
}
