
#include <sys/types.h>
#include <stdio.h>  /* defines FILENAME_MAX */
// #define WINDOWS  /* uncomment this line to use it for windows.*/ 
#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif
#include<iostream>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>
#include <locale.h>
#include <langinfo.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std; 

void SearchDirectory(string directory, string target_File_Name)
{
    DIR *dirp = opendir(directory.c_str());
    if (!dirp)
    {
        perror(("opendir " + directory).c_str());
        return;
    }

    struct dirent *dptr;
    while(dptr = readdir(dirp))
    {
        string file_Name = dptr->d_name;
        string file_Path = directory + "/" + file_Name;

        struct stat statStruct; 
        stat(file_Path.c_str(), &statStruct); 
        if( S_ISDIR(statStruct.st_mode) )
        {

	    	
            if ( file_Name.compare(".") == 0 || file_Name.compare("..") == 0 )
	    {
                continue; 
            }

            SearchDirectory(file_Path, target_File_Name);
        }
        else if( S_ISREG(statStruct.st_mode)){
            if( file_Name.compare(target_File_Name) == 0)
            {
                cout <<"found: "<< file_Path << endl;
            }
        }   
    }

    closedir(dirp);
}

std::string GetCurrentWorkingDir( void ) {
  char buff[FILENAME_MAX];
  GetCurrentDir( buff, FILENAME_MAX );
  std::string current_working_dir(buff);
  return current_working_dir;
}

int main(int argc, char *argv[])
{
    string file_Name = "cwd.cpp"; 
    string path = GetCurrentWorkingDir();
    std::cout<<path<<endl;
    int n = chdir(path.c_str());
    string path1 = GetCurrentWorkingDir();
    std::cout<<n<<endl;			
    //SearchDirectory(path, file_Name); 
    return 0; 
}
