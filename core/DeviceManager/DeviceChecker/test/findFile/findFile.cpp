#include <sys/types.h>
#include <stdio.h>  /* defines FILENAME_MAX */
// #define WINDOWS  /* uncomment this line to use it for windows.*/ 
#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif
#include<bits/stdc++.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>
#include <locale.h>
#include <langinfo.h>
#include <fcntl.h>
using namespace std; 

// void SearchDirectory(string directory, string target_File_Name)
// {
//     DIR *dirp = opendir(directory.c_str());
//     if (!dirp)
//     {
//         perror(("opendir " + directory).c_str());
//         return;
//     }

//     struct dirent *dptr;
//     while(dptr = readdir(dirp))
//     {
//         string file_Name = dptr->d_name;
//         string file_Path = directory + "/" + file_Name;

//         struct stat statStruct; 
//         stat(file_Path.c_str(), &statStruct); 
//         if( S_ISDIR(statStruct.st_mode) )
//         {

	    	
//             if ( file_Name.compare(".") == 0 || file_Name.compare("..") == 0 )
// 	        {
//                 continue; 
//             }

//             SearchDirectory(file_Path, target_File_Name);
//         }
//         else if( S_ISREG(statStruct.st_mode)){
//             if( file_Name.compare(target_File_Name) == 0/* && file_Path.compare(directory) == 0*/)
//             {
//                 cout <<"found: "<< file_Path << endl;
//             }
//         }   
//     }

//     closedir(dirp);
// }



void SearchDirectory(string directory, string file_Path)
{
    DIR *dirp = opendir(file_Path.c_str());
    if (!dirp)
    {
        perror(("opendir " + file_Path).c_str());
        return;
    }

    struct dirent *dptr;
    while(dptr = readdir(dirp))
    {
        if( file_Path.compare(directory) == 0/* && file_Path.compare(directory) == 0*/)
            {
                cout <<"found: "<< file_Path << endl;
                return ;
            }
        if(dptr->d_type == DT_DIR)
        {
            file_Path = file_Path + "/" + dptr->d_name;
            SearchDirectory(directory, file_Path);
        }
        
        else
        {
            continue;
        }
    }

    closedir(dirp);
}

std::string GetCurrentWorkingDir( void ) {
  char buff[FILENAME_MAX];
  GetCurrentDir( buff, FILENAME_MAX );
  std::string current_working_dir(buff);
  return current_working_dir;
}



int main()
{
    std::string folderName;
    std::string fileName;
    std::cout<<"folder Name: ";
    cin>>folderName;
    // std::cout<<"file Name: ";
    // cin>>fileName;
   string cwd = GetCurrentWorkingDir();
   //cout<<cwd<<endl;
   
    vector<std::string> v;
    int i = 1;
    
    //vector<std::string>::iterator ir, it;
    string token;
    std::istringstream ss(cwd);
    while (std::getline(ss, token, '/'))
    {
       // cout<<token<<endl;
        v.push_back(token);
    }
    int l = v.size();
    string path = "";
    while(i <= l-4)
    {
        path =  path + "/" + v[i];
        i++;
    }
    cout<<path<<endl;
    SearchDirectory(folderName, path);
    

    return 0; 
}