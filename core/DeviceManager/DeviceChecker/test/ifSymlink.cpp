#include <bits/stdc++.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAXFILENAMELEN 100

/* Prototype for function */
bool is_symlink(const char *filename);

int main()
{
    std::string filename = "/dev/CAMERA1";
    bool symlink = is_symlink(filename.c_str());
    std::cout<<symlink;	
    return 0;
}

bool is_symlink(const char *filename)
{
    struct stat p_statbuf;	
    if (lstat(filename, &p_statbuf) < 0) {  /* if error occured */
        perror("[deviceChecker][is_symlink] calling stat()");
        return false;  /* end progam here */
    }

    if (S_ISLNK(p_statbuf.st_mode) == 1) {
        return true;
    } else {
        return false;
    }
}
