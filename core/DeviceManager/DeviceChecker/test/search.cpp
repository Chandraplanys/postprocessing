#include <bits/stdc++.h>
using namespace std;
std::string serachKey = "find $HOME/Documents -name \"99-usb-serial.rules\"";

int main()
{
    char buffer[128];
    std::string result = "";
    FILE *file;
    file = popen(serachKey.c_str(), "r");

    if (!file)
    {
        throw std::runtime_error("[DeviceChecker][FindFile] popen() failed");
    }

    try
    {
        while (!feof(file))
        {
            if (fgets(buffer, 128, file) != NULL)
            {
                result += buffer;
            }
        }
    }
    catch (...)
    {
        pclose(file);
        throw;
    }
    pclose(file);

    //std::cout<<"result: "<<result<<std::endl;

    std::istringstream op(result);
    std::string line;

    while (std::getline(op, line))
    {
        if (strstr(line.c_str(), "/core/DeviceManager/DeviceChecker/99-usb-serial.rules") == NULL)
        {
            std::cout << line << std::endl;
        }
    }

    return 0;
}