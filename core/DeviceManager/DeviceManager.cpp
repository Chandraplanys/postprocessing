#include "DeviceManager.h"
#include "Device_Variables_Protocol.h"
#include <iostream>

#include <math.h>
#include <stdio.h>
#include <string.h> /* for strncpy */
#include <unistd.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "../SharedMemoryService/shared_memory.h"

// Thread Controller Flags

bool PRESSURE_SENSOR_THREAD_FLAG = true;
bool IMU_THREAD_FLAG = true;
bool REMOTE_CONTROLLER_THREAD_FLAG = true;
bool ALTIMETER_THREAD_FLAG = true;
bool ALTIMETER_NMEA_THREAD_FLAG = false;
bool CYGNUS_THREAD_FLAG = false;
bool GPSROV_THREAD_FLAG = false;
bool WEBSOCKET_THREAD_FLAG = false;
bool LOGGER_THREAD_FLAG = false;

bool DEVICE_MANAGER_FLAG = true;

/*** GPSRov for specially GPS values ****/
std::string latitude_val;
std::string longitude_val;

SMemory s_memory;

/*** IMU Object and Serial Initialization ***/

// ........... cygnus initialization...........
// Cygnus cygnus(CYGNUS_PORT, CYGNUS_BAUD_RATE);

//........... Ws server initialization...........
WsServer server(8086, 1);

using namespace std;

// ...............Shared Memory Creation ................

DeviceManager::DeviceManager()
{
  gamepadServer.setPortnIP(this->getIPAdrress(), TCP_PORT);
}
void DeviceManager::InitializeSharedMems()
{

  // std::cout << "In SHM" << std::endl;

  //..... Device data shared memory locations....

  s_memory.create("PressureSensor", 1024, 1);
  // std::cout << "After PS" << std::endl;
  s_memory.create("IMU", 1024, 12);
  // std::cout << "After TMU" << std::endl;
  s_memory.create("Altimeter", 1024, 1);
  // std::cout << "After Alt" << std::endl;
  s_memory.create("Joystick", 3072, 28);
  // std::cout << "After Joy" << std::endl;
  s_memory.create("Cygnus", 1024, 1);
  // std::cout << "After Cygn" << std::endl;
  s_memory.create("GPSRov", 1024, 5);
  // std::cout << "After GPS" << std::endl;
  s_memory.create("LeakSensor", 1024, 2);
  // std::cout << "After LS" << std::endl;
  s_memory.create("Cockpit", 1024, 2);
  // std::cout << "After Coc" << std::endl;
  s_memory.create_str("videoFolderPath", 1024);
  // std::cout << "After SHM" << std::endl;

  // ..... Logger control by vision flag..............
  s_memory.create_bool("vision_status_flag", 1024);
}

// .............DM Thread Monitor...........

void *DeviceManager::threadMonitor(void *argument)
{
  DeviceManager *DMInstance = (DeviceManager *)argument;
  bool devices = false;
  int threadChecker;

  //Object to check Devices connection status
  DeviceChecker deviceChecker;

  struct udev *udev = udev_new();
  if (!udev)
  {
    std::cout << "udev Failed " << std::endl;
    exit(0);
  }
  // std::cout << "1 " << std::endl;

  deviceChecker.enumerate_devices(udev);
  // std::cout << "2 " << std::endl;

  deviceChecker.monitor_devices(udev);
  // std::cout << "3 " << std::endl;

  deviceChecker.createThreads();
  // std::cout << "4 " << std::endl;

  while (true)
  {

    if (DEVICE_MANAGER_FLAG)
    {
      //IMU Thread Monitor
      if (deviceChecker.checkSymlinkPresent(IMU_PORT))
      {
        if (IMU_THREAD_FLAG)
        {
          pthread_create(&DMInstance->VN100_Thread, NULL, DMInstance->FetchAndUpdate_IMU, NULL);
          boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
      }
      else
      {
        DMInstance->stopThread(DMInstance->VN100_Thread, &IMU_THREAD_FLAG, "IMU");
      }

      // //Pressure Sensor Thread Monitor
      // if (deviceChecker.checkSymlinkPresent(PRESS_SENSOR_PORT))
      // {
      if (PRESSURE_SENSOR_THREAD_FLAG)
      {
        pthread_create(&DMInstance->PressureSensor_Thread, NULL, DMInstance->FetchAndUpdate_PressureSensor, NULL);
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
      }
      // }
      // else
      // {
      //   DMInstance->stopThread(DMInstance->PressureSensor_Thread, &PRESSURE_SENSOR_THREAD_FLAG, "Pressure Sensor");
      // }

      //Altimeter Sensor Thread Monitor
      if (deviceChecker.checkSymlinkPresent(ALTIMETER_PORT))
      {
        if (ALTIMETER_THREAD_FLAG)
        {
          pthread_create(&DMInstance->Altimeter_Thread, NULL, DMInstance->FetchAndUpdate_Altimeter, NULL);
          boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
      }
      else
      {
        DMInstance->stopThread(DMInstance->Altimeter_Thread, &ALTIMETER_THREAD_FLAG, "Altimeter");
      }

      //Cygnus Sensor Thread Monitor
      if (deviceChecker.checkSymlinkPresent(CYGNUS_PORT))
      {
        if (CYGNUS_THREAD_FLAG)
        {
          pthread_create(&DMInstance->Cygnus_Thread, NULL, DMInstance->FetchAndUpdate_Cygnus, NULL);
          boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
      }
      else
      {
        DMInstance->stopThread(DMInstance->Cygnus_Thread, &CYGNUS_THREAD_FLAG, "Cygnus");
      }

      // //GPSRov Sensor Thread Monitor
      if (deviceChecker.checkSymlinkPresent(GPS_PORT))
      {
        if (GPSROV_THREAD_FLAG)
        {
          pthread_create(&DMInstance->GPSRov_Thread, NULL, DMInstance->FetchAndUpdate_GPSRov, NULL);
          boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
      }
      else
      {
        DMInstance->stopThread(DMInstance->GPSRov_Thread, &GPSROV_THREAD_FLAG, "GPS");
      }

      // //REMOTE_CONTROLLER Thread Monitor
      if (REMOTE_CONTROLLER_THREAD_FLAG)
      {

        pthread_create(&DMInstance->RemoteController_Thread, NULL, DMInstance->FetchAndUpdate_RemoteController, argument);
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
      }

      // //Websocket Thread Monitor
      if (WEBSOCKET_THREAD_FLAG)
      {

        pthread_create(&DMInstance->websocket_Thread, NULL, DMInstance->websocket_communication, NULL);
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
      }

      // //Logger Thread Monitor
      if (LOGGER_THREAD_FLAG)
      {

        pthread_create(&DMInstance->Logger_Thread, NULL, DMInstance->StartLogger, NULL);
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
      }
    }
    else //Close all the threads
    {
      DMInstance->stopThread(DMInstance->VN100_Thread, &IMU_THREAD_FLAG, "IMU");

      DMInstance->stopThread(DMInstance->PressureSensor_Thread, &PRESSURE_SENSOR_THREAD_FLAG, "Pressure Sensor");

      DMInstance->stopThread(DMInstance->Altimeter_Thread, &ALTIMETER_THREAD_FLAG, "Altimeter");

      DMInstance->stopThread(DMInstance->Cygnus_Thread, &CYGNUS_THREAD_FLAG, "Cygnus");

      DMInstance->stopThread(DMInstance->GPSRov_Thread, &GPSROV_THREAD_FLAG, "GPS");

      DMInstance->stopThread(DMInstance->RemoteController_Thread, &REMOTE_CONTROLLER_THREAD_FLAG, "Remote Controller");

      DMInstance->stopThread(DMInstance->websocket_Thread, &WEBSOCKET_THREAD_FLAG, "Websocket");

      DMInstance->stopThread(DMInstance->Logger_Thread, &LOGGER_THREAD_FLAG, "Logger");

      deviceChecker.configModificationFlag = false;
      pthread_join(deviceChecker.configUpdatedThread, NULL);
      deviceChecker.monitorFlag = false;
      pthread_join(deviceChecker.monitorThread, NULL);
      udev_unref(udev);

      std::cout << "Exiting Thread Monitor Thread" << std::endl;
      break;
    }
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
  }
  pthread_exit(NULL);
}

//............. DM thread Intialization ......................

void DeviceManager::InitializeThreads(DeviceManager *argument)
{
  std::cout << "Starting Threads " << std::endl;

  pthread_create(&threadMonitor_Thread, NULL, threadMonitor, argument);
}

// .............. Sensor Data Collection threads .................

void *DeviceManager::FetchAndUpdate_IMU(void *arguments)
{
  try
  {
    VN100 IMU(IMU_PORT, IMU_BAUD_RATE);

    IMU_THREAD_FLAG = false;
    std::cout << "[Device Manager][IMU Thread][STARTING]Started IMU Thread. /n "
                 "Data is Being updated to Shared Memory."
              << std::endl;
    while (!IMU_THREAD_FLAG)
    {
      s_memory.update("IMU", 1, IMU.getHeading());
      s_memory.update("IMU", 2, IMU.getPitch());
      s_memory.update("IMU", 3, IMU.getRoll());
      s_memory.update("IMU", 4, IMU.magX);
      s_memory.update("IMU", 5, IMU.magY);
      s_memory.update("IMU", 6, IMU.magZ);
      s_memory.update("IMU", 7, IMU.aclX);
      s_memory.update("IMU", 8, IMU.aclY);
      s_memory.update("IMU", 9, IMU.aclZ);
      s_memory.update("IMU", 10, IMU.angX);
      s_memory.update("IMU", 11, IMU.angY);
      s_memory.update("IMU", 12, IMU.angZ);

      // std::cout << "IMU Var Heading: " << s_memory.fetch("IMU", HEADING) << std::endl;
      //       std::cout << "IMU Var Pitch: " << s_memory.fetch("IMU", PITCH) << std::endl;

      // boost::this_thread::sleep(boost::posix_time::milliseconds(1));
      boost::this_thread::sleep(boost::posix_time::microseconds(200000));
    }
    IMU_THREAD_FLAG = true;
    pthread_exit(NULL);
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread IMU][Error] " << e.what()
              << std::endl;
  }
}

void *DeviceManager::FetchAndUpdate_PressureSensor(void *arguments)
{
  try
  {
    PRESSURE_SENSOR_THREAD_FLAG = false;

    std::cout << "[Device Manager][PressureSensor Thread][STARTING] Started "
                 "PressureSensor Thread. /n Data is Being updated to Shared "
                 "Memory."
              << std::endl;
    PressureSensor pressureSensor(PRESS_SENSOR_PORT, 9600);
    float data = 0.0;
    while (!PRESSURE_SENSOR_THREAD_FLAG)
    {

      data = pressureSensor.get_Depth();
      // std::cout << "Depth: " << data << std::endl;
      s_memory.update("PressureSensor", PS_DEPTH, data);

      boost::this_thread::sleep(boost::posix_time::microseconds(20000));
    }

    PRESSURE_SENSOR_THREAD_FLAG = true;

    pthread_exit(NULL);
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread Pressure Sensor][Error] " << e.what()
              << std::endl;
  }
}

void *DeviceManager::FetchAndUpdate_Altimeter(void *arguments)
{
  try
  {
    ALTIMETER_THREAD_FLAG = false;
    std::cout << "[Device Manager][Altimeter Thread][STARTING]Started "
                 "Altimeter Thread. /n Data is Being updated to Shared Memory."
              << std::endl;
    Altimeter altimeter(ALTIMETER_PORT, ALTIMETER_BAUD_RATE);

    float data;

    while (!ALTIMETER_THREAD_FLAG)
    {
      data = altimeter.get_Altitude();
      // std::cout<<"Altimeter: "<<data<<std::endl;
      s_memory.update("Altimeter", ALTITUDE, data);
      boost::this_thread::sleep(boost::posix_time::microseconds(200000));
    }

    ALTIMETER_THREAD_FLAG = true;
    pthread_exit(NULL);
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread - altimeter][Error] " << e.what()
              << std::endl;
  }
}

void *DeviceManager::FetchAndUpdate_Cygnus(void *arguments)
{
  try
  {
    // CYGNUS_THREAD_FLAG = false;
    // std::cout << "[Device Manager][Cygnus Thread][STARTING]Started Cygnus "
    //              "Thread. /n Data is Being updated to Shared Memory."
    //           << std::endl;

    // cygnus.initialize();

    // while (!CYGNUS_THREAD_FLAG)
    // {
    //   if (!(cygnus.thickness_value < 0))
    //   {
    //     s_memory.update("Cygnus", 1, cygnus.thickness_value);
    //   }
    //   boost::this_thread::sleep(boost::posix_time::milliseconds(20));
    // }
    // cygnus.thread_status = false;
    // pthread_join(cygnus.thread, NULL);
    // while (!cygnus.thread_killed_status)
    // {
    //   usleep(100000);
    // }
    // CYGNUS_THREAD_FLAG = true;
    // pthread_exit(NULL);
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread - cygnus][Error] " << e.what()
              << std::endl;
  }
}

void *DeviceManager::FetchAndUpdate_GPSRov(void *arguments)
{
  try
  {
    // GPSROV_THREAD_FLAG = false;
    // std::cout << "[Device Manager][GPS Thread][STARTING]Started GPSRov Thread. "
    //              "/n Data is Being updated to Shared Memory."
    //           << std::endl;

    // GPS gps(GPS_PORT, GPS_BAUD_RATE, "../Logger/sub_log4cpp.properties",
    //         "../../../Logs/gps/");
    // if (gps.initialize())
    // {
    //   s_memory.update("GPSRov", 5, 1.0);
    //   gps.thread_status = true;

    //   double status_of_values;
    //   while ((!GPSROV_THREAD_FLAG) && gps.thread_status)
    //   {
    //     s_memory.update("GPSRov", 1, gps.latitude);
    //     latitude_val = ((DeviceManager *)arguments)
    //                        ->to_string_with_precision(gps.latitude, 8);
    //     s_memory.update("GPSRov", 2, gps.longitude);
    //     longitude_val = ((DeviceManager *)arguments)
    //                         ->to_string_with_precision(gps.longitude, 8);
    //     s_memory.update("GPSRov", 3, gps.data_accuracy * 1.0);
    //     gps.data_status ? s_memory.update("GPSRov", 4, 1.0)
    //                     : s_memory.update("GPSRov", 4, 0.0);
    //     boost::this_thread::sleep(boost::posix_time::milliseconds(20));
    //   }
    //   gps.thread_status = false;
    //   pthread_join(gps.thread, NULL);
    //   GPSROV_THREAD_FLAG = true;
    // }
    // else
    // {
    //   cout << "Unable to Start GPSRov Thread." << endl;
    // }
    // s_memory.update("GPSRov", 5, 0.0);
    // // pthread_exit(NULL);
    // std::cout << "[Device Manager][GPS Thread] Terminated GPSRov Thread." << std::endl;
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread - GPS ROV][Error] " << e.what()
              << std::endl;
  }
}

// ............ RC Data control and server thread ...................

void *DeviceManager::FetchAndUpdate_RemoteController(void *arguments)
{
  REMOTE_CONTROLLER_THREAD_FLAG = false;
  DeviceManager *RCInstance = (DeviceManager *)arguments;

  RCInstance->RemoteController_Thread_Restart = true;
  while (RCInstance->RemoteController_Thread_Restart)
  {
    RCInstance->RemoteController_Thread_Restart = false;
    try
    {
      std::cout << "[Device Manager][RemoteController Thread][STARTING]Started "
                   "RemoteController Thread. /n Joystick data is Being "
                   "updated to Shared Memory."
                << std::endl;

      //  boost::this_thread::sleep(boost::posix_time::milliseconds(1));

      while (!REMOTE_CONTROLLER_THREAD_FLAG)
      {
        std::cout << "In RC" << std::endl;
        RCInstance->gamepadServer.initialize(&RCInstance->gamepadServer);
        std::cout << "Before Controller Thread" << std::endl;

        while ((!REMOTE_CONTROLLER_THREAD_FLAG) &&
               RCInstance->gamepadServer.threadController)
        {

          for (int i = 1; i < 29; i++)
          {
            s_memory.update("Joystick", i, RCInstance->gamepadServer.joystick_data[i]);
            //std::cout<<s_memory.fetch("Joystick", i)<<std::endl;
          }
          boost::this_thread::sleep(boost::posix_time::microseconds(20000));
        }
        RCInstance->gamepadServer.controller_thread_flag = false;
        if (REMOTE_CONTROLLER_THREAD_FLAG)
        {

          RCInstance->gamepadServer.threadController = false;
          RCInstance->gamepadServer.connectionStatus = true;

          RCInstance->gamepadServer.closeServer();
        }

        (void)pthread_join(RCInstance->gamepadServer.thread, NULL);
        (void)pthread_join(RCInstance->gamepadServer.connect_thread, NULL);
        boost::this_thread::sleep(boost::posix_time::microseconds(20000));
      }
      REMOTE_CONTROLLER_THREAD_FLAG = true;
      std::cout << "Exiting Remote Controller" << std::endl;
      pthread_exit(NULL);
    }
    catch (exception &e)
    {
      std::cout << "[Device Manager][Thread - RC] [Error]" << e.what()
                << std::endl;
      ((DeviceManager *)arguments)->RemoteController_Thread_Restart = true;
    }
  }
}

std::string DeviceManager::getIPAdrress()
{
  int fd;
  struct ifreq ifr;

  fd = socket(AF_INET, SOCK_DGRAM, 0);

  /* I want to get an IPv4 IP address */
  ifr.ifr_addr.sa_family = AF_INET;

  /* I want IP address attached to "eth0" */
  strncpy(ifr.ifr_name, "eno1", IFNAMSIZ - 1);
  ioctl(fd, SIOCGIFADDR, &ifr);
  close(fd);
  std::string ipadd =
      inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);

  return ipadd;
}

// .............. Logger thread integrated with vision .............

void *DeviceManager::StartLogger(void *arguments)
{
  try
  {
    LOGGER_THREAD_FLAG = false;

    std::cout << "[Device Manager][Logger Thread][STARTING]Initiating Logger "
                 "with Vision Status Check"
              << std::endl;
    int frontCamFrameCount = 0, bottomCamFrameCount = 0;
    double heading = 0, pitch = 0, roll = 0, magX = 0, magY = 0, magZ = 0,
           aclX = 0, aclY = 0, aclZ = 0, angX = 0, angY = 0, angZ = 0;
    double depth = 0, altitude = 0, thickness = 0, gps_accuracy = 0, gps_state = 0;
    std::string latitude = "0", longitude = "0";

    std::string logvalues;

    //  InitLog("test.log");
    bool vision_status = true;

    s_memory.create_bool("vision_status_flag", 1024);

    while (!LOGGER_THREAD_FLAG)
    {
      vision_status = s_memory.fetch_bool("vision_status_flag");
      if (vision_status)
      {
        std::string logFlePath =
            s_memory.fetch_str("videoFolderPath") + "/Data.log";
        InitLog(logFlePath);
      }
      while (vision_status && (!LOGGER_THREAD_FLAG))
      {
        frontCamFrameCount = (int)s_memory.fetch("frame_count", 1);
        bottomCamFrameCount = (int)s_memory.fetch("frame_count", 2);

        heading = s_memory.fetch("IMU", 1);
        pitch = s_memory.fetch("IMU", 2);
        roll = s_memory.fetch("IMU", 3);
        magX = s_memory.fetch("IMU", 4);
        magY = s_memory.fetch("IMU", 5);
        magZ = s_memory.fetch("IMU", 6);
        aclX = s_memory.fetch("IMU", 7);
        aclY = s_memory.fetch("IMU", 8);
        aclZ = s_memory.fetch("IMU", 9);
        angX = s_memory.fetch("IMU", 10);
        angY = s_memory.fetch("IMU", 11);
        angZ = s_memory.fetch("IMU", 12);

        depth = s_memory.fetch("PressureSensor", PS_DEPTH);
        altitude = s_memory.fetch("Altimeter", ALTITUDE);
        thickness = s_memory.fetch("Cygnus", 1);
        gps_accuracy = s_memory.fetch("GPSRov", 3);
        gps_state = s_memory.fetch("GPSRov", 4);
        latitude = ((DeviceManager *)arguments)
                       ->to_string_with_precision(s_memory.fetch("GPSRov", 1), 8);
        longitude = ((DeviceManager *)arguments)
                        ->to_string_with_precision(s_memory.fetch("GPSRov", 2), 8);
        latitude_val = ((DeviceManager *)arguments)->to_string_with_precision(s_memory.fetch("GPSRov", 1), 8);
        longitude_val = ((DeviceManager *)arguments)->to_string_with_precision(s_memory.fetch("GPSRov", 2), 8);

        logvalues = std::to_string(frontCamFrameCount) + " " +
                    std::to_string(bottomCamFrameCount) + " " +
                    std::to_string(heading) + " " + std::to_string(pitch) +
                    " " + std::to_string(roll) + " " + std::to_string(magX) +
                    " " + std::to_string(magY) + " " + std::to_string(magZ) +
                    " " + std::to_string(aclX) + " " + std::to_string(aclY) +
                    " " + std::to_string(aclZ) + " " + std::to_string(angX) +
                    " " + std::to_string(angY) + " " + std::to_string(angZ) +
                    " " + std::to_string(depth) + " " +
                    std::to_string(altitude) + " " + std::to_string(thickness) +
                    " " + latitude + " " + longitude +
                    " " + std::to_string(gps_accuracy) + " " + std::to_string(gps_state);

        LOG_INFO(logvalues);
        vision_status = s_memory.fetch_bool("vision_status_flag");
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
      }
      boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }
    LOGGER_THREAD_FLAG = true;
    std::cout << "Exiting Logger" << std::endl;
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread - Logger][Error] " << e.what()
              << std::endl;
  }
}

// ................. WebSocket connection thread for GUI ...............

void *DeviceManager::websocket_communication(void *arguments)
{
  try
  {
    WEBSOCKET_THREAD_FLAG = false;
    std::cout << "[Device Manager][WebSocket Thread][STARTING]Initiating "
                 "WebSocket For Pilots WebPage"
              << std::endl;
    boost::this_thread::sleep(boost::posix_time::milliseconds(100));

    SMemory sh_memory;

    auto &echo = server.endpoint["^/echo/?$"];

    echo.onmessage = [&sh_memory](
                         std::shared_ptr<WsServer::Connection> connection,
                         std::shared_ptr<WsServer::Message> message) {
      // auto message_str=message->string();

      auto message_str =
          "{\"depth\":\"" +
          std::to_string(
              ((int)(sh_memory.fetch("PressureSensor", PS_DEPTH) * 1000)) /
              1000.0) +
          "\"," + "\"altitude\":\"" +
          std::to_string(sh_memory.fetch("Altimeter", ALTITUDE)) + "\"," +
          "\"thickness\":\"" + std::to_string(sh_memory.fetch("Cygnus", 1)) +
          "\"," + "\"heading\":\"" +
          std::to_string(((int)(sh_memory.fetch("IMU", 1) * 1000)) / 1000.0) +
          "\"," + "\"pitch\":\"" + std::to_string(sh_memory.fetch("IMU", 2)) +
          "\"," + "\"roll\":\"" + std::to_string(sh_memory.fetch("IMU", 3)) +
          "\"," + "\"gps_lat\":\"" + latitude_val + "\"," + "\"gps_lng\":\"" +
          longitude_val + "\"," + "\"gps_acc\":\"" +
          std::to_string(sh_memory.fetch("GPSRov", 3)) + "\"," +
          "\"gps_state\":\"" + std::to_string(sh_memory.fetch("GPSRov", 4)) +
          "\"," + "\"leak_sensor_1\":\"" +
          std::to_string(sh_memory.fetch("LeakSensor", 1)) + "\"," +
          "\"leak_sensor_2\":\"" +
          std::to_string(sh_memory.fetch("LeakSensor", 2)) + "\"," +
          "\"T_Depth\":\"" + std::to_string(sh_memory.fetch("Cockpit", 1)) +
          "\"," + "\"video_path\":\"" + sh_memory.fetch_str("videoFolderPath") +
          "\"," + "\"T_Heading\":\"" +
          std::to_string(sh_memory.fetch("Cockpit", 2)) + +"\"}";
      // auto message_str =std::to_string(test_int);

      // if (message_str == "1000") {
      //   server.stop();
      // }

      // std::cout << "Server: Message received: \"" << message->string() << "\"
      // from " << (size_t)connection.get() << std::endl;

      // std::cout << "Server: Sending message \"" << message_str <<  "\" to "
      // << (size_t)connection.get() << std::endl;

      auto send_stream = make_shared<WsServer::SendStream>();
      *send_stream << message_str;
      // server.send is an asynchronous function
      server.send(
          connection, send_stream, [](const boost::system::error_code &ec) {
            if (ec)
            {
              std::cout << "Server: Error sending message. " <<
                  // See
                  // http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html,
                  // Error Codes for error code meanings
                  "Error: " << ec << ", error message: " << ec.message()
                        << std::endl;
            }
          });
    };

    echo.onopen = [](std::shared_ptr<WsServer::Connection> connection) {
      std::cout << "Server: Opened connection " << (size_t)connection.get()
                << std::endl;
    };

    // See RFC 6455 7.4.1. for status codes
    echo.onclose = [](std::shared_ptr<WsServer::Connection> connection,
                      int status, const std::string & /*reason*/) {
      std::cout << "Server: Closed connection " << (size_t)connection.get()
                << " with status code " << status << std::endl;
    };

    // See
    // http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html,
    // Error Codes for error code meanings
    echo.onerror = [](std::shared_ptr<WsServer::Connection> connection,
                      const boost::system::error_code &ec) {
      std::cout << "Server: Error in connection " << (size_t)connection.get()
                << ". "
                << "Error: " << ec << ", error message: " << ec.message()
                << std::endl;
    };
    thread server_thread([]() {
      // Start WS-server
      std::cout << "Starting the server" << std::endl;
      server.start();
    });

    // Wait for server to start so that the client can connect
    this_thread::sleep_for(chrono::seconds(1));

    // server.stop();
    server_thread.join();

    std::cout << "Exiting the WebSocket Thread" << std::endl;
    WEBSOCKET_THREAD_FLAG = true;
    return 0;
  }
  catch (exception &e)
  {
    std::cout << "[Device Manager][Thread - websocket][Error] " << e.what()
              << std::endl;
  }
}

bool DeviceManager::stopThread(pthread_t threadToStop, bool *threadControlFlag, std::string deviceName)
{

  if (!(*threadControlFlag))
  {
    *threadControlFlag = true;
    std::cout << " Waiting for " << deviceName << " Device is Disconnection " << std::endl;
    (void)pthread_join(threadToStop, NULL);
    std::cout << deviceName << " Device Thread Stopped" << std::endl;

    return true;
  }
  return true;
}

std::string DeviceManager::to_string_with_precision(double value,
                                                    const int n = 6)
{
  std::ostringstream out;
  out << std::fixed << std::setprecision(n) << value;
  return out.str();
}
