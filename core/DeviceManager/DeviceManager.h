#ifndef DEVICE_MANAGER_H
#define DEVICE_MANAGER_H

#include "Devices/Support/client_ws.hpp"
#include "Devices/Support/server_ws.hpp"
#include <boost/thread.hpp>
#include <pthread.h>
#include <bits/stdc++.h>
#include <stdlib.h>

#include "../Logger/GlobalLogger.h"
#include "../SharedMemoryService/shared_memory.h"
#include "Devices/IMU/VN100.h"
#include "Devices/RemoteController_Server/BoostSyncTCPServer.h"
#include "../SerialCommunication/SerialConnection.h"
#include "Devices/Altimeter/Altimeter.h"
// #include "Devices/cygnus/cygnus.h"
// #include "Devices/gps/gps.h"
#include "Devices/PressureSensor/PressureSensor.h"
#include "DeviceChecker/include/DeviceChecker.h"
#include "DeviceChecker/include/Symlink.h"
#include "DeviceChecker/include/Support_DC.h"
#include "DeviceChecker/include/ConfigParser.h"

#define PRESS_SENSOR_PORT "/dev/ttyACM2"
#define PRESS_SENSOR_BAUD_RATE 9600
#define PS_DEPTH 1
#define PS_TEMPERATURE 2
#define IMU_PORT "/dev/IMU"
#define IMU_BAUD_RATE 230400
#define ALTIMETER_PORT "/dev/Altimeter"
#define ALTIMETER_BAUD_RATE 9600
#define CYGNUS_PORT "/dev/Cygnus"
#define CYGNUS_BAUD_RATE 2400
#define GPS_PORT "/dev/GPSRov"
#define GPS_BAUD_RATE 9600

// Thread Controller Flags

extern bool PRESSURE_SENSOR_THREAD_FLAG;
extern bool IMU_THREAD_FLAG;
extern bool REMOTE_CONTROLLER_THREAD_FLAG;
extern bool ALTIMETER_THREAD_FLAG;
extern bool ALTIMETER_NMEA_THREAD_FLAG;
extern bool CYGNUS_THREAD_FLAG;
extern bool GPSROV_THREAD_FLAG;
extern bool LOGGER_THREAD_FLAG;
extern bool DEVICE_MANAGER_FLAG;

/*** IMU Object and Serial Initialization ***/

//........... Ws server initialization...........
typedef SimpleWeb::SocketServer<SimpleWeb::WS> WsServer;
extern WsServer server;

class DeviceManager
{

private:
  // VN100 VN100IMU;
  // Altimeter Altimeter;
  // PressureSensor PressureSensor;

protected:
public:
  DeviceManager();
  BoostSyncTCPServer gamepadServer;

  pthread_t VN100_Thread;
  pthread_t PressureSensor_Thread;
  pthread_t Altimeter_Thread;
  pthread_t Altimeter_NMEA_Thread;
  pthread_t RemoteController_Thread;
  pthread_t Cygnus_Thread;
  pthread_t GPSRov_Thread;
  pthread_t Logger_Thread;
  pthread_t websocket_Thread;
  pthread_t threadMonitor_Thread;
  bool RemoteController_Thread_Restart;

  static void *threadMonitor(void *arguments);
  static void *FetchAndUpdate_IMU(void *arguments);
  static void *FetchAndUpdate_PressureSensor(void *arguments);
  static void *FetchAndUpdate_Altimeter(void *arguments);
  static void *FetchAndUpdate_Altimeter_NMEA(void *arguments);
  static void *FetchAndUpdate_RemoteController(void *arguments);
  static void *FetchAndUpdate_Cygnus(void *arguments);
  static void *FetchAndUpdate_GPSRov(void *arguments);
  static void *StartLogger(void *arguments);
  static void *websocket_communication(void *arguments);

  void InitializeThreads(DeviceManager *argument);
  void InitializeSharedMems();
  std::string getIPAdrress();
  bool stopThread(pthread_t threadToStop, bool *threadControlFlag, std::string deviceName);

  std::string to_string_with_precision(double value, int n);
};

#endif // end DEVICE_MANAGER_H
