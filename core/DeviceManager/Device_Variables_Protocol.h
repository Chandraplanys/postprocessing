#ifndef DEVICE_VARIABLES_PROTOCOLS_H
#define DEVICE_VARIABLES_PROTOCOLS_H

//--------------------- SENSOR Definations ------------------------

#define HEADING 1
#define PITCH 2
#define ROLL 3
#define MAGX 4
#define MAGY 5
#define MAGZ 6
#define ACLX 7
#define ACLY 8
#define ACLZ 9
#define ANGX 10
#define ANGY 11
#define ANGZ 12
#define PS_DEPTH 1
#define PS_TEMPERATURE 2
#define ALTITUDE 1
#define THICKNESS 1

//-----------------------Mission Control Definations----------------

// Note : ------ mission control status flags for control are:
//               "Mission_control_response_flag"
//               "Mission_control_status_flag"

//------- "XY_plane_control"----------
#define XYP_AUTO_HEADING 1
#define XYP_MANUAL 2

//------- "Diving_plane_control" -----
#define DP_AUTODEPTH 1
#define DP_AUTOVELOCITY 2
#define DP_MANUAL 3

//-------- "Pitch_control" -----------
#define P_ENABLE 1
#define P_DISABLE 2

//-------- "Lights_values"------------

#define FRONT_LIGHTS 1
#define BOTTOM_LIGHTS 2

// Note : ----- lights status changed in bool type shared memory locations
//              "Front_light_request_status"
//              "Bottom_light_request_status"

//------------------------------------------------------------------

#endif // End DEVICE_VARIABLES_PROTOCOLS_H
