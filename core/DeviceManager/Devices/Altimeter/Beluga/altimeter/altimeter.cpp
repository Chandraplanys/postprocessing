#include "altimeter.h"

#define REQEST_SEQ_SIZE 1
#define RESPONSE_SIZE 10

uint8_t *AltimeterProtocol::request_seq() { return req_seq; }

double AltimeterProtocol::value() {
  for (int i = 0; i < 7; i++) {
    number.push_back(recData[i]);
  }
  result = std::stod(number);
  return result;
}

bool Altimeter::initialize(std::string serial_port, int baud_rate) {
  if (this->serial.open(serial_port, baud_rate, 8,
                        boost::asio::serial_port::parity::none,
                        boost::asio::serial_port_base::stop_bits::one,
                        boost::asio::serial_port::flow_control::none)) {
    pthread_create(&thread, NULL, Altimeter::data_update_thread, this);
    return true;
  } else {
    return false;
  }
}

void *Altimeter::data_update_thread(void *c) {
  serialPortwReadTimeout reader(((Altimeter *)c)->serial.m_serialPort,
                                500); // 500ms waiting period

  ((Altimeter *)c)->altimeter_thread_status = true;
  while (((Altimeter *)c)->altimeter_thread_status) {
    ((Altimeter *)c)->get_depth(reader, ((Altimeter *)c)->sensor_protocol);
  }
}

double Altimeter::fetch_altitude() { return altitude_value; }

void Altimeter::print_data() {
  while (1) {
    sleep(1);
    printf("Depth: %lf m\n", altitude_value);
  }
}

void Altimeter::get_depth(serialPortwReadTimeout &reader,
                          AltimeterProtocol sensor_protocol) {
  reader.writeString(sensor_protocol.request_seq(), REQEST_SEQ_SIZE);
  altitude_value = this->get_response_value(reader, sensor_protocol);
}

double Altimeter::get_response_value(serialPortwReadTimeout &reader,
                                     AltimeterProtocol sensor_protocol) {

  double value;
  reader.depth_sensor_response(RESPONSE_SIZE);
  char *result = reader.depth_rsp;
  if (result[0] != char(126)) {
    for (int i = 0; i < RESPONSE_SIZE; i++) {
      sensor_protocol.recData[i] = result[i];
    }
    sensor_protocol.size = RESPONSE_SIZE;
    value = sensor_protocol.value();
  } else {
    value = 0.0;
  }
  return value;
}
