#ifndef ALTIMETER_H
#define ALTIMETER_H

#include "../Support/boost_serial.h"
#include <iostream>
#include <string>
#include <time.h>

class AltimeterProtocol {

public:
  uint8_t *request_seq();
  double value();
  char recData[10];
  int size;

private:
  char command;
  uint8_t req_seq[1] = {0x5A};
  std::string number;
  double result;
};

class Altimeter {

public:
  Serial serial;
  AltimeterProtocol sensor_protocol;
  double altitude_value;
  bool initialize(std::string serial_port, int baud_rate);
  double fetch_altitude();
  void print_data();
  void get_depth(serialPortwReadTimeout &reader,
                 AltimeterProtocol sensor_protocol);
  double get_response_value(serialPortwReadTimeout &reader,
                            AltimeterProtocol sensor_protocol);
  static void *data_update_thread(void *c);
  pthread_t thread;
  bool altimeter_thread_status;
};

#endif
