#include "altimeter.h"

using namespace std;

int main(int argc, char *argv[]) {
  printf("Program Initiated....\n");
  Altimeter altimeter;
  altimeter.initialize("/dev/Altimeter", 9600);
  altimeter.print_data();
  return 0;
}
