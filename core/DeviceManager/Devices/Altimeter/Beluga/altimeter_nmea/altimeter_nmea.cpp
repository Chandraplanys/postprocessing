#include "altimeter_nmea.h"

AltimeterNMEA::AltimeterNMEA(std::string port, long baud_rate) {
  // comment color types
  this->red = "\033[0;31m";
  this->green = "\033[1;32m";
  this->yellow = "\033[1;33m";
  this->reset = "\033[0m";

  this->port = port;
  this->baud_rate = baud_rate;
  this->thread_status = false;
  this->thread_killed_status = false;
  this->dataPacketSize = 0;
  this->altitude = 0.0;
}

bool AltimeterNMEA::initialize() {
  try {
    std::cout << this->yellow << "[AltimeterNMEA] Initiating..." << this->reset
              << std::endl;
    if (this->serial.open(port, baud_rate, 8,
                          boost::asio::serial_port::parity::none,
                          boost::asio::serial_port_base::stop_bits::one,
                          boost::asio::serial_port::flow_control::none)) {
      pthread_create(&thread, NULL, AltimeterNMEA::data_update_thread, this);
      std::cout << this->green << "[AltimeterNMEA] Succesfully opened port"
                << this->reset << std::endl;
      return true;
    } else {
      std::cout << this->red << "[AltimeterNMEA] Unable to open port: " << port
                << this->reset << std::endl;
      return false;
      this->thread_killed_status = true;
    }
  } catch (exception &e) {
    this->thread_killed_status = true;
    std::cout << this->red << "[AltimeterNMEA][Error] Unable to open port -"
              << e.what() << this->reset << std::endl;
  }
}

void *AltimeterNMEA::data_update_thread(void *c) {
  try {
    std::cout << ((AltimeterNMEA *)c)->yellow
              << "[AltimeterNMEA] Data update thread initiated"
              << ((AltimeterNMEA *)c)->reset << std::endl;
    serialPortwReadTimeout reader(((AltimeterNMEA *)c)->serial.m_serialPort,
                                  500); // 500ms waiting period
    reader.queue_buffer_clear_size = 100;
    reader.thread_killed_status = false;

    std::cout << ((AltimeterNMEA *)c)->yellow
              << "[AltimeterNMEA] Initiating boost serial queue thread "
              << ((AltimeterNMEA *)c)->reset << std::endl;
    reader.sensor_name = "AltimeterNMEA";
    reader.initialize_data_thread();

    ((AltimeterNMEA *)c)->thread_status = true;
    while (((AltimeterNMEA *)c)->thread_status &&
           (!reader.thread_killed_status)) {
      ((AltimeterNMEA *)c)->read_altitude(reader);
      boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }
    // closing sequence
    reader.queue_thread_status = false;
    while (!reader.thread_killed_status) {
      usleep(10000);
    }
    std::cout << ((AltimeterNMEA *)c)->yellow
              << "[AltimeterNMEA] Closing Serial port"
              << ((AltimeterNMEA *)c)->reset << std::endl;
    ((AltimeterNMEA *)c)->serial.close();
    std::cout << ((AltimeterNMEA *)c)->red << "[AltimeterNMEA] Terminated"
              << ((AltimeterNMEA *)c)->reset << std::endl;
    ((AltimeterNMEA *)c)->thread_killed_status = true;
  } catch (std::exception &e) {
    std::cout << ((AltimeterNMEA *)c)->red
              << "[AltimeterNMEA][data_update_thread][Error] - " << e.what()
              << ((AltimeterNMEA *)c)->reset << std::endl;
  }
}

void AltimeterNMEA::read_altitude(serialPortwReadTimeout &reader) {
  try {
    int idx = 0;
    if (reader.dataQueue.size() > 25) {
      // check for data initiating identifier
      if (reader.dataQueue.front() == '$') {
        // Get the initial location identification string
        for (int i = 0; i < 6; i++) {
          this->dataPacket[i] = reader.dataQueue.front();
          this->dataCheck.push_back(this->dataPacket[i]);
          // this->dataCheck[i] = reader.dataQueue.front();
          this->wait(reader);
          reader.dataQueue.pop_front();
        }
        int i = 6;
        // compare the string to location identifier
        if (this->dataCheck == "$SDDBT") {
          // collect all data until next line "$"
          // std::cout << "getting data..";
          while (reader.dataQueue.front() != '$') {
            this->dataPacket[i] = reader.dataQueue.front();
            this->wait(reader);
            reader.dataQueue.pop_front();
            i++;
          }
          // store the next line char as indicator for data end
          // store dataPacketSize
          this->dataPacketSize = i;
          this->parseDataPacket();
        }
        this->dataCheck.clear();

      } else {
        this->wait(reader);
        reader.dataQueue.pop_front();
      }
    }
  } catch (exception &e) {
    std::cout << this->red
              << "[AltimeterNMEA][Error] - [Reading AltimeterNMEA value] - "
              << e.what() << this->reset << std::endl;
  }
}

void AltimeterNMEA::wait(serialPortwReadTimeout &reader) {
  while (reader.dataQueue.size() < 20) {
    usleep(100);
  }
}

void AltimeterNMEA::parseDataPacket() {
  try {
    if (this->dataPacketSize > 0) {
      // parse the data based on comma
      std::string dataPacket_str;
      for (int i = 0; i < dataPacketSize; i++) {
        dataPacket_str.push_back(dataPacket[i]);
      }
      // std::cout << dataPacket_str << std::endl;
      std::string data[10];
      std::string str_variable;
      int data_size = 0;
      for (unsigned i = 0; i < dataPacket_str.length(); ++i) {
        str_variable = dataPacket_str.at(i);
        if (str_variable != ",") {
          data[data_size].append(str_variable);
        } else {
          data_size++;
        }
      }

      if (!(data[1].empty())) {
        this->altitude = std::stod(data[1]);
      } else {
        this->altitude = 0.0;
        std::cout << this->red << "[AltimeterNMEA] - [Value is empty]"
                  << this->reset << std::endl;
      }
    }
  } catch (exception &e) {
    std::cout << this->red
              << "[AltimeterNMEA][Error] - [Parsing AltimeterNMEA value] - "
              << e.what() << this->reset << std::endl;
  }
}

void AltimeterNMEA::print_data() {
  while (1) {
    sleep(1);
    printf("altitude: %0.8f\n", this->altitude);
  }
}
