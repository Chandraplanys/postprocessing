#ifndef ALTIMETER_NMEA_H
#define ALTIMETER_NMEA_H

#include "../../../Logger/sub_logger.h"
#include "../../../SharedMemoryService/shared_memory.h"
#include "../Support/boost_serial.h"
#include <bitset>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/thread.hpp>
#include <deque>
#include <exception>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

class AltimeterNMEA {

public:
  // fetching data
  AltimeterNMEA(std::string port, long baud_rate);
  Serial serial;
  std::string port;
  long baud_rate;
  char dataPacket[100];
  std::string dataCheck;
  int dataPacketSize;

  // data control functions
  bool initialize();
  static void *data_update_thread(void *c);
  void read_altitude(serialPortwReadTimeout &reader);
  void parseDataPacket();
  void wait(serialPortwReadTimeout &reader);

  // thread and meamory manage
  bool thread_status;
  bool thread_killed_status;
  pthread_t thread;
  SMemory s_memory;

  void print_data();

  // variables of altitude
  double altitude;

  std::string red;
  std::string green;
  std::string yellow;
  std::string reset;
};

#endif
