#include "altimeter_nmea.h"

#define ALTIMETER_PORT "/dev/ttyUSB1"
#define ALTIMETER_BAUD_RATE 9600

AltimeterNMEA altimeter(ALTIMETER_PORT, ALTIMETER_BAUD_RATE);

void shutDown() {
  printf("Shutdown sequence initiated...\n");

  altimeter.thread_status = false;
  while (!altimeter.thread_killed_status) {
    usleep(100000);
  }
  exit(0);
}

void handler(int s) {
  printf("Interupt signal Received --->%d<---- Cygnus\n", s);
  // Broken pipe from a client
  if (s == SIGPIPE) {
    // Just ignore, sock class will close the client the next time
  }
  // Catch ctrl-c, segmentation fault, abort signal
  if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM) {
    shutDown();
  }
}

void initSigHandler() {

  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  sigaction(SIGPIPE, &sigIntHandler, NULL);
  sigaction(SIGABRT, &sigIntHandler, NULL);
  sigaction(SIGSEGV, &sigIntHandler, NULL);
  sigaction(SIGTERM, &sigIntHandler, NULL);
}

int main(int argc, char *argv[]) {
  // initialization
  initSigHandler();
  printf("Program Initiated....\n");
  altimeter.initialize();
  altimeter.thread_status = true;
  altimeter.print_data();
  return 0;
}
