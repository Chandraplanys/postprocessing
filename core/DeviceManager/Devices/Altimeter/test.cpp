#include "Altimeter.h"

using namespace std;

int main(int argc, char *argv[])
{
  printf("Program Initiated....\n");
  Altimeter altimeter("/dev/Altimeter", 9600);
  while (1)
  {
    std::cout << "Altitude: " << altimeter.get_Altitude() << std::endl;
    usleep(200000);
  }
  return 0;
}
