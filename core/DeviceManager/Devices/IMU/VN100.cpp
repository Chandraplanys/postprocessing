#include <iostream>
#include "VN100.h"

using namespace std;

VN100::VN100(std::string devicePort, long baudRate)
{
	this->minimum_queue_size = 242;
	this->devicePort = devicePort;
	this->baudRate = baudRate;
	this->DataResponse = false;
	this->DataRate = 0;

	this->yaw = 0;
	this->pitch = 0;
	this->roll = 0;

	this->magX = 0;
	this->magY = 0;
	this->magZ = 0;

	this->aclX = 0;
	this->aclY = 0;
	this->aclZ = 0;

	this->angX = 0;
	this->angY = 0;
	this->angZ = 0;

	this->Serial.Initialize(this->devicePort, this->baudRate, this->Callback, this);
}

VN100::~VN100()
{
}

void VN100::Callback(const char *dataString, size_t dataStringLength, void *thisClassObject)
{
	int idx = 0;
	// cout << "VN100 Data: " << string(data) << endl;
	while (idx < dataStringLength)
	{
		((VN100 *)thisClassObject)->dataQueue.push_back(dataString[idx++]);
	}

	if (((VN100 *)thisClassObject)->dataQueue.size() > ((VN100 *)thisClassObject)->minimum_queue_size)
	{
		((VN100 *)thisClassObject)->dataProcess();
	}
}

void VN100::dataProcess()
{
	std::string dataPacket = "";
	while (this->dataQueue.front() != '$')
	{
		this->dataQueue.pop_front();
	}
	while (this->dataQueue.front() != '\r')
	{
		dataPacket.push_back(this->dataQueue.front());
		this->dataQueue.pop_front();
	}
	this->parseDataPacket(dataPacket);
}

void VN100::parseDataPacket(std::string dataString)
{
	int idx = 0, sign = +1;
	const char *data = dataString.c_str();

	/* Yaw */
	idx = 7;
	sign = data[idx++] == '+' ? +1 : -1;
	this->yaw = ((int)data[idx++] - 48) * 100 + ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001;
	this->yaw = this->yaw * sign;

	/* Pitch */
	idx = 16;
	sign = data[idx++] == '+' ? +1 : -1;
	this->pitch = ((int)data[idx++] - 48) * 100 + ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001;
	this->pitch = this->pitch * sign;

	/* Roll */
	idx = 25;
	sign = data[idx++] == '+' ? +1 : -1;
	this->roll = ((int)data[idx++] - 48) * 100 + ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001;
	this->roll = this->roll * sign;

	/* Magnetic X */
	idx = 34;
	sign = data[idx++] == '+' ? +1 : -1;
	this->magX = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001 + ((int)data[idx++] - 48) * 0.0001;
	this->magX = this->magX * sign;

	/* Magnetic Y */
	idx = 43;
	sign = data[idx++] == '+' ? +1 : -1;
	this->magY = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001 + ((int)data[idx++] - 48) * 0.0001;
	this->magY = this->magY * sign;

	/* Magnetic Z */
	idx = 52;
	sign = data[idx++] == '+' ? +1 : -1;
	this->magZ = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001 + ((int)data[idx++] - 48) * 0.0001;
	this->magZ = this->magZ * sign;

	/* Acceleration X */
	idx = 61;
	sign = data[idx++] == '+' ? +1 : -1;
	this->aclX = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001;
	this->aclX = this->aclX * sign;

	/* Acceleration Y */
	idx = 69;
	sign = data[idx++] == '+' ? +1 : -1;
	this->aclY = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001;
	this->aclY = this->aclY * sign;

	/* Acceleration Z */
	idx = 77;
	sign = data[idx++] == '+' ? +1 : -1;
	this->aclZ = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001;
	this->aclZ = this->aclZ * sign;

	/* Angular Rate X */
	idx = 85;
	sign = data[idx++] == '+' ? +1 : -1;
	this->angX = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001 + ((int)data[idx++] - 48) * 0.0001 + ((int)data[idx++] - 48) * 0.00001 + ((int)data[idx++] - 48) * 0.000001;
	this->angX = this->angX * sign;

	/* Angular Rate Y */
	idx = 96;
	sign = data[idx++] == '+' ? +1 : -1;
	this->angY = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001 + ((int)data[idx++] - 48) * 0.0001 + ((int)data[idx++] - 48) * 0.00001 + ((int)data[idx++] - 48) * 0.000001;
	this->angY = this->angY * sign;

	/* Angular Rate Z */
	idx = 107;
	sign = data[idx++] == '+' ? +1 : -1;
	this->angZ = ((int)data[idx++] - 48) * 10 + ((int)data[idx++] - 48) + ((int)data[(++idx)++] - 48) * 0.1 + ((int)data[idx++] - 48) * 0.01 + ((int)data[idx++] - 48) * 0.001 + ((int)data[idx++] - 48) * 0.0001 + ((int)data[idx++] - 48) * 0.00001 + ((int)data[idx++] - 48) * 0.000001;
	this->angZ = this->angZ * sign;

	/* Update Shared Memory */
	//this->updateSharedMemory();

	//	cout << "Yaw -" <<this->yaw  << "Pitch- " << this->pitch << "Roll- " << this->roll << endl;
	//	cout << "MagX- " <<this->magX << "MagY- " << this->magY  << "MagZ- " << this->magZ << endl;
	//	cout <<"AclX- " <<this->aclX << "AclY- " << this->aclY  << "AclZ " << this->aclZ << endl;
	//	cout <<"AngX- " <<this->angX << "AngY- " << this->angY  << "AngZ- " << this->angZ << endl;
}

float VN100::getHeading()
{
	return (this->yaw + 179);
}

float VN100::getPitch()
{
	return (this->pitch);
}

float VN100::getRoll()
{
	return (this->roll);
}

//void VN100::updateSharedMemory()
//{
//	std::pair<double*, std::size_t> mem;
//	mem = this->sharedMemoryObject->find<double>("IMUVars");

//	*mem.first++ = this->getHeading();
//	*mem.first++ = this->getPitch();
//	*mem.first++ = this->getRoll();
//	*mem.first++ = this->magX;
//	*mem.first++ = this->magY;
//	*mem.first++ = this->magZ;
//	*mem.first++ = this->angX;
//	*mem.first++ = this->angY;
//	*mem.first++ = this->angZ;
//	*mem.first++ = this->aclX;
//	*mem.first++ = this->aclY;
//	*mem.first = this->aclZ;
//}

void VN100::update()
{
	// dummy update function
}
