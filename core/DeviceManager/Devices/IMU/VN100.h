#ifndef _VN100_H
#define _VN100_H

#include <boost/interprocess/managed_shared_memory.hpp>
#include <deque>

#include "../../../SerialCommunication/SerialConnection.h"

using namespace boost::interprocess;
using namespace std;

class VN100
{
  public:
	long baudRate;
	std::string devicePort;
	deque<char> dataQueue;
	int minimum_queue_size;
	bool DataResponse;
	int DataRate;

	float yaw, pitch, roll;
	float magX, magY, magZ;
	float aclX, aclY, aclZ;
	double angX, angY, angZ;
	//managed_shared_memory *sharedMemoryObject;

	VN100(std::string devicePort, long baudRate);
	~VN100();
	SerialConnection Serial;

	static void Callback(const char *dataString, size_t dataStringLength, void *thisClassObject);
	void dataProcess();

	void parseDataPacket(std::string dataString);

	float getHeading();

	float getPitch();

	float getRoll();

	//void updateSharedMemory();

	void update();
};

#endif
