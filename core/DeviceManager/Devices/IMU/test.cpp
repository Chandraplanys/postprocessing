#include <iostream>

#include "VN100.h"

using namespace std;

int main(int argc, char **argv)
{
	VN100 VN100IMU("/dev/IMU", 230400);
	while (1)
	{
		cout << "Data --";
		cout << VN100IMU.getHeading() << endl;
		cout << VN100IMU.getPitch() << endl;
		cout << VN100IMU.getRoll() << endl;
		cout << VN100IMU.magX << endl;
		cout << VN100IMU.magY << endl;
		cout << VN100IMU.magZ << endl;
		cout << VN100IMU.aclX << endl;
		cout << VN100IMU.aclY << endl;
		cout << VN100IMU.aclZ << endl;
		cout << VN100IMU.angX << endl;
		cout << VN100IMU.angY << endl;
		cout << VN100IMU.angZ << endl;
		usleep(1000000);
	}

	return 0;
}
