#include "PressureSensor.h"

PressureSensor::PressureSensor(std::string port, long baud_rate)
{
	this->minimum_queue_size = 25;
	this->Port = port;
	this->Baud_rate = baud_rate;
	this->DataResponse = false;
	this->DataRate = 0;
	this->depth = 0;

	this->Serial.Initialize(this->Port, this->Baud_rate, this->Callback, this);
}

PressureSensor::~PressureSensor()
{
}

void PressureSensor::Callback(const char *data, size_t len, void *c)
{

	int idx = 0;
	while (idx < len)
	{
		((PressureSensor *)c)->dataQueue.push_back(data[idx++]);
	}

	if (((PressureSensor *)c)->dataQueue.size() > ((PressureSensor *)c)->minimum_queue_size)
	{
		((PressureSensor *)c)->dataProcess();
	}
}

void PressureSensor::dataProcess()
{
	std::string dataPacket = "";
	while (this->dataQueue.front() != '$')
	{
		this->dataQueue.pop_front();
	}
	while (this->dataQueue.front() != 'M')
	{
		dataPacket.push_back(this->dataQueue.front());
		this->dataQueue.pop_front();
	}

	this->parseDataPacket(dataPacket);
}

void PressureSensor::parseDataPacket(std::string dataString)
{
	try
	{
		std::string value = "";

		if (dataString.size() >= 24)
		{
			for (int i = 0; i < 6; i++)
			{
				value.push_back(dataString.at(i + 17));
			}
			this->depth = std::stod(value);
		}
		else
		{
			std::cout << "Pressure Short String: " << dataString << "---" << dataString.size() << std::endl;
			// this->dataQueue.clear();
		}
	}
	catch (std::exception &e)
	{
		std::cout << "[Pressure Sensor][parseDataPacket][ERROR] - "
				  << e.what() << std::endl;
	}
}

float PressureSensor::get_Depth()
{
	return this->depth;
}