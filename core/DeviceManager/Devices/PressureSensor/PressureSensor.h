#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include <boost/interprocess/managed_shared_memory.hpp>
#include <deque>

#include "../../../SerialCommunication/SerialConnection.h"

class PressureSensor
{
  private:
	float depth;

  public:
	PressureSensor(std::string port, long baud_rate);
	~PressureSensor();

	std::string Port;
	long Baud_rate;
	deque<char> dataQueue;

	//` serial communication callback

	static void Callback(const char *dataString, size_t dataStringLength, void *thisClassObkect);
	void dataProcess();
	void parseDataPacket(std::string data);
	int minimum_queue_size;

	SerialConnection Serial;

	bool DataResponse;
	int DataRate;
	float get_Depth();
};

#endif