#include "PressureSensor.h"

int main(int argc, char *argv[])
{
  // initialization
  PressureSensor pressureSensor("/dev/ttyACM2", 9600);

  while (1)
  {
    std::cout << "Depth: " << pressureSensor.get_Depth() << std::endl;
    sleep(1);
  }
  return 0;
}