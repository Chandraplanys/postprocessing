#include "BoostSyncTCPServer.h"

BoostSyncTCPServer::~BoostSyncTCPServer()
{
}
BoostSyncTCPServer::BoostSyncTCPServer() : acceptor(io_service)
{
  active = false;
  this->controller_thread_flag = true;
  this->connectionStatus = false;

  // for (int i = 1; i < 29; i++)
  //   joystick_data[i] = 0;
}

BoostSyncTCPServer::BoostSyncTCPServer(int opt_port, std::string opt_hostname)
    : port(opt_port), hostname(opt_hostname), acceptor(io_service)
{
  active = false;
  this->controller_thread_flag = true;
  this->connectionStatus = false;

  for (int i = 1; i < 29; i++)
    joystick_data[i] = 0;

  joystick_data[21] = 128;
  joystick_data[22] = 128;
}

int BoostSyncTCPServer::initialize(BoostSyncTCPServer *server)
{
  // server->connectionStatus = true;
  pthread_create(&connect_thread, NULL, BoostSyncTCPServer::connectClient,
                 server);
  pthread_create(&thread, NULL, BoostSyncTCPServer::read_and_parse_string,
                 server);
}

void BoostSyncTCPServer::setPortnIP(std::string IP, int port)
{
  this->hostname = IP;
  this->port = port;
}

void *BoostSyncTCPServer::connectClient(void *c)
{

  try
  {

    if (((BoostSyncTCPServer *)c)->reconnect())
    {
      ((BoostSyncTCPServer *)c)->connectionStatus = true;
    }
    std::cout << "Exiting the Reconnect thread" << std::endl;
  }

  catch (std::exception &e)
  {
    std::cerr << "Connect Client thread: ERROR: " << e.what() << std::endl;
  }
}

void *BoostSyncTCPServer::read_and_parse_string(void *c)
{
  try
  {

    int receive_count = 0;
    string_parser string_parser_object;

    for (int i = 1; i < 29; i++)
    {
      ((BoostSyncTCPServer *)c)->joystick_data[i] = 0;
    }
    std::string dataFromController;
    std::string newrecieve;
    while (!(((BoostSyncTCPServer *)c)->connectionStatus))
    {
      boost::this_thread::sleep(boost::posix_time::microseconds(1000));
    }

    while ((((BoostSyncTCPServer *)c)->threadController) &&
           ((BoostSyncTCPServer *)c)->acceptor.is_open())
    {

      newrecieve = ((BoostSyncTCPServer *)c)->read(120);

      dataFromController = ((BoostSyncTCPServer *)c)->base64_decode(newrecieve);
      //std::cout << "recieve: " << dataFromController << std::endl;

      if (dataFromController == "NULL")
      {
        ((BoostSyncTCPServer *)c)->closeServer();
        ((BoostSyncTCPServer *)c)->connectionStatus = false;
        ((BoostSyncTCPServer *)c)->threadController = false;
        std::cout << "Server Closed" << std::endl;
      }
      else
      {
        if (!dataFromController.empty())
        {
          string_parser_object.parse_string(dataFromController);
          for (int i = 1; i < 29; i++)
          {
            ((BoostSyncTCPServer *)c)->joystick_data[i] = string_parser_object.data[i];
          }
        }
      }
      boost::this_thread::sleep(boost::posix_time::microseconds(1000));
    }

    std::cout << "[BoostSyncTCPServer][ReadAndParseString]Came out of while" << std::endl;
    ((BoostSyncTCPServer *)c)->connectionStatus = false;
    std::cout << "Exiting the Parsing thread in RemoteController BoostSyncTCPServer" << std::endl;
  }

  catch (std::exception &e)
  {
    std::cerr << "Read and parse: Error" << e.what() << std::endl;
  }
}

bool BoostSyncTCPServer::connect()
{
  try
  {
    tcp::endpoint endpoint(boost::asio::ip::address::from_string(hostname),
                           port);

    acceptor.open(endpoint.protocol());

    acceptor.bind(endpoint);
    acceptor.listen();
    std::cout << "Waiting for client..." << std::endl;

    tcp::acceptor::endpoint_type end_type;
    acceptor.accept(*stream.rdbuf(), end_type);
    cout << port << endl;
    std::cout << "Accepted client " << end_type.address().to_string() << ":"
              << end_type.port() << std::endl;

    active = true;

    return true;
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << std::endl;
    return false;
  }
}
bool BoostSyncTCPServer::reconnect()
{
  try
  {

    tcp::endpoint endpoint(boost::asio::ip::address::from_string(hostname),
                           port);
    this->acceptor.open(endpoint.protocol());

    this->acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    this->acceptor.bind(endpoint);

    this->acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    this->acceptor.listen();

    std::cout << "Waiting for client..." << std::endl;

    tcp::acceptor::endpoint_type end_type;
    this->acceptor.accept(*this->stream.rdbuf(), end_type);
    cout << port << endl;
    std::cout << "Accepted client " << end_type.address().to_string() << ":"
              << end_type.port() << std::endl;

    active = true;

    return true;
  }
  catch (std::exception &e)
  {

    std::cout << "Error in Reconnect function of BoostSyncTCPServer" << std::endl;
    std::cerr << "Error: " << e.what() << std::endl;
    return false;
  }
}

std::string BoostSyncTCPServer::read(int len)
{
  char data[len];
  this->stream.read(data, len);

  if (stream)
  {
    return std::string(data, len);
  }
  else
  {
    return "NULL";
  }
}

std::string BoostSyncTCPServer::base64_decode(const std::string &str)
{

  if (str == "NULL")
  {
    return "NULL";
  }
  else
  {
    BIO *bio, *base64_filter, *bio_out;
    char inbuf[512];
    int inlen;
    base64_filter = BIO_new(BIO_f_base64());
    BIO_set_flags(base64_filter, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new_mem_buf((void *)str.c_str(), str.length());
    bio = BIO_push(base64_filter, bio);
    bio_out = BIO_new(BIO_s_mem());
    while ((inlen = BIO_read(bio, inbuf, 512)) > 0)
    {
      BIO_write(bio_out, inbuf, inlen);
    }
    BIO_flush(bio_out);
    char *new_data;
    long bytes_written = BIO_get_mem_data(bio_out, &new_data);
    std::string result(new_data, bytes_written);
    BIO_free_all(bio);
    BIO_free_all(bio_out);
    return result;
  }
}

void BoostSyncTCPServer::closeServer()
{
  if (!this->io_service.stopped())
  {
    this->io_service.reset();
    this->io_service.stop();
    std::cout << "Closing IO service" << std::endl;
  }

  this->stream.close();
  std::cout << "Closed TCP Stream" << std::endl;

  this->acceptor.cancel();
  this->acceptor.close();

  pthread_cancel(this->connect_thread);

  if (this->acceptor.is_open())
  {
    std::cout << "Failed to close the Synchronous Acceptor" << std::endl;
  }
  else
  {
    printf("TCP Server is closed now\n");
  }

  active = false;
}
