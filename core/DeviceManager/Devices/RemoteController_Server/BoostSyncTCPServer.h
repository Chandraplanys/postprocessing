#ifndef _SERVER_H
#define _SERVER_H
#include <boost/circular_buffer.hpp>
#include "../../../SharedMemoryService/shared_memory.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "parser.h"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <netinet/in.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include<bits/stdc++.h>

#define TCP_PORT 5001

using boost::asio::ip::tcp;
using namespace boost::interprocess;
using namespace std;

void signal_handler();

class BoostSyncTCPServer
{
  int port;                           // host port number
  std::string hostname;               // host name
  boost::asio::io_service io_service; // boost asio service
  tcp::iostream stream;               // TCP i/o stream
  boost::system::error_code error;    // error code

public:
  
  tcp::acceptor acceptor; // TCP acceptor
  bool active;
  bool controller_thread_flag;
  char controllerInput[1000];
  //std::string controllerInput;
  int receive_count;
  double joystick_data[29];

  bool threadController = true;
  bool connectionStatus;
  pthread_t thread;
  pthread_t connect_thread;

  ~BoostSyncTCPServer();
  BoostSyncTCPServer();
  BoostSyncTCPServer(int, std::string);
  int initialize(BoostSyncTCPServer *server);
  void setPortnIP(std::string IP, int port);
  bool connect();
  bool reconnect();
  static void *read_and_parse_string(void *c);
  static void *connectClient(void *c);
  int fetch_data();
  void update_the_shared_memory(double[]);
  std::string read(int);
  void closeServer();
  std::string base64_decode(const std::string &);
  std::queue<char> dataPacket;
  std::queue<std::string> packets;
  boost::circular_buffer<char> cb;
};

#endif
