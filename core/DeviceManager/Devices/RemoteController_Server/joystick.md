# BelugaJoystickCodeIntegration

# About Version: 

* Buttons/Axis Mapping are done by changing in config file from client side, no need to change at server side    

* Every joystick has its own config which will be loaded by client when it will detect that joystick

* Client is exchanging data with server in a common BUtton/Axis name which is default mapped to buttons in each config file which can be interchanged

* Client sends data only when any event is occured at client side and last updated data is saved in a map structure 

* sent data is in this format (ButtonName$value) e.g. HMRL$1234 or TNU$1

* One type of joystick can be replaced by other type of joystick in between server running (if config file is present for that joystick)  
