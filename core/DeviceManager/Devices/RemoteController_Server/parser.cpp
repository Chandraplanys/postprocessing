#include "parser.h"

string_parser::string_parser()
{

    //this->fs.open ("test.txt", std::fstream::in | std::fstream::out | std::fstream::app);
    ConfigParserServer configParserServer;
    memset(this->data, 0, sizeof(this->data));
    data[22] = 128;
    data[21] = 128;
}

void string_parser::parse_string(std::string controllerInput)
{
    int count = 1;
    std::stringstream test(controllerInput);
    std::string segment;
    std::vector<std::string> seglist;
    std::vector<std::string> seglist1;
    std::vector<std::string> seglist2;
    //std::cout << controllerInput << std::endl;
    while (std::getline(test, segment, ';'))
    {
        seglist.push_back(segment);
    }

    for (std::vector<std::string>::iterator ir = seglist.begin(); ir != seglist.end(); ++ir)
    {
        std::stringstream test1(*ir);
        std::string segment1;
        while (std::getline(test1, segment1, ','))
        {
            seglist1.push_back(segment1);
        }
        for (std::vector<std::string>::iterator ir = seglist1.begin(); ir != seglist1.end(); ++ir)
        {

            std::stringstream test2(*ir);
            std::string segment2;
            //std::cout << "segmenting: " << *ir << "size: " << (*ir).length() << std::endl;
            count = 1;
            while (std::getline(test2, segment2, '$'))
            {
                //std::cout << "count: " << count << "  segment: " << segment2 << "   size: " << segment2.length() << std::endl;
                seglist2.push_back(segment2);
                count++;
            }
            count = 0;
            int n = getValueFirst(seglist2[0].substr(seglist2[0].length() - 3, 3));
            //std::cout<<n<<std::endl;
            if (n != INT_MAX)
            {

                this->data[n] = (double)stoi(seglist2[1]);
                //print_output();
            }

            seglist2.clear();
        }
        seglist1.clear();
    }
    seglist.clear();
}

void string_parser::print_output()
{
    //std::system("clear");
    int count = 1;
    std::cout << "\n";
    for (int i = 1; i <= 28; i++)
    {
        std::cout << "\t\t\t\t\t\t"
                  << this->print_map[i] << "\t\t"
                  << "----->" << this->data[i] << endl;
    }
}

ConfigParserServer::ConfigParserServer()
{
    this->delimeter = "=";
    //this->filePath = "serverButton.config";
    this->filePath = searchFile("find $HOME/Documents -name \"serverButton.config\"", "/core/DeviceManager/serverButton.config");
    if (this->filePath != "")
    {
        parseFile();
    }
    else
    {
        std::cout << "[DeviceManager][device][RemoteController][parser].... /core/DeviceManager/serverButton.config....  file not found" << std::endl;
    }
}

ConfigParserServer::ConfigParserServer(std::string filePath)
{
    this->delimeter = "=";
    this->filePath = filePath;
    ConfigParserServer(this->filePath, this->delimeter);
}

ConfigParserServer::ConfigParserServer(std::string filePath, std::string delimeter)
{
    this->delimeter = delimeter;
    this->filePath = filePath;
}

bool ConfigParserServer::parseFile()
{
    return this->parseFile(this->filePath);
}

bool ConfigParserServer::parseFile(std::string filePath)
{
    //std::cout<<"buttonServer file: "<<filePath<<std::endl;
    std::ifstream fileStream(filePath);
    std::string tmpLine;
    int lineCount = 1;

    if (!fileStream.is_open())
    {
        return false;
    }
    //std::cout<<"lol1111"<<std::endl;

    while (getline(fileStream, tmpLine))
    {

        /*** Ignore line with white spaces ***/
        if (!tmpLine.length())
        {
            lineCount++;
            continue;
        }
        else if (tmpLine[0] == '#') // Ignore Line starting with # which are comments in the file
        {
            lineCount++;
            continue;
        }
        else
        {
            bool isLineCorrupted;
            isLineCorrupted = this->parseAndAddToUmap(tmpLine);

            if (isLineCorrupted)
            {
                std::cout << "Please Check the Line Number: " << lineCount << ": " << tmpLine << std::endl;
                return false;
            }
        }
        lineCount++;
    }
    return true;
}

bool ConfigParserServer::parseAndAddToUmap(std::string line)
{
    std::vector<std::string> tmpVector;

    boost::algorithm::split(tmpVector, line, boost::is_any_of("="));

    if (tmpVector.size() == 1)
    {
        return true;
    }
    else
    {
        std::string key = tmpVector[0];
        boost::trim(key);
        std::vector<std::string> tmpVector1;

        boost::algorithm::split(tmpVector1, tmpVector[1], boost::is_any_of(","));
        if (tmpVector1.size() == 1)
        {
            return true;
        }
        else
        {
            int index = (int)stoi(tmpVector1[0]);
            std::string value = tmpVector1[1];
            boost::trim(value);
            //std::cout<<"lol222"<<std::endl;
            this->addToUMap(key, std::make_pair(index, value));
            this->print_map[index] = value;
        }
    }
    return false;
}

void ConfigParserServer::addToUMap(std::string key, std::pair<int, std::string> p)
{
    //std::cout<<"l:"<<key<<" "<<value<<std::endl;
    this->parameters[key] = p;
}

int ConfigParserServer::getValueFirst(std::string key)
{
    std::unordered_map<std::string, std::pair<int, std::string>>::iterator it;

    it = this->parameters.find(key);

    // Check if iterator points to end of map
    if (it != this->parameters.end())
    {
        return it->second.first;
    }
    else
    {
        return INT_MAX;
    }
}
std::string ConfigParserServer::getValueSecond(std::string key)
{
    std::unordered_map<std::string, std::pair<int, std::string>>::iterator it;

    it = this->parameters.find(key);

    // Check if iterator points to end of map
    if (it != this->parameters.end())
    {
        return it->second.second;
    }
    else
    {
        return "nf";
    }
}

std::string ConfigParserServer::searchFile(std::string cmd, std::string match)
{
    char buffer[128];
    std::string result = "";
    FILE *file;
    file = popen(cmd.c_str(), "r");

    if (!file)
    {
        throw std::runtime_error("[DeviceChecker][FindFile] popen() failed");
    }

    try
    {
        while (!feof(file))
        {
            if (fgets(buffer, 128, file) != NULL)
            {
                result += buffer;
            }
        }
    }
    catch (...)
    {
        pclose(file);
        throw;
    }
    pclose(file);

    //std::cout<<"result: "<<result<<std::endl;

    std::istringstream op(result);
    std::string line;
    while (std::getline(op, line))
    {
        if (boost::contains(line, match))
        {
            return line;
        }
    }
    return "";
}