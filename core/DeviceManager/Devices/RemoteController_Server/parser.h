#ifndef _STRING_PARSER_H_
#define _STRING_PARSER_H_

#include <bits/stdc++.h>
#include <stdio.h>
#include <sstream>	
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <boost/algorithm/string.hpp>

using namespace std;


class ConfigParserServer {

private:
std::string filePath;
std::string delimeter;
// std::unordered_map<std::string,std::string> parameters;

bool parseAndAddToUmap(std::string line);
void addToUMap(std::string key, std::pair<int, std::string> p);

public:
std::unordered_map<std::string,std::pair<int, std::string> > parameters;
std::unordered_map<int, std::string> print_map;

ConfigParserServer();
ConfigParserServer(std::string filePath);
ConfigParserServer(std::string filePath, std::string delimeter);

bool parseFile();
bool parseFile(std::string filePath);

int getValueFirst(std::string key);
std::string getValueSecond(std::string key);
std::string searchFile(std::string cmd, std::string match);

};

class string_parser:public ConfigParserServer
{

	public:
		// int index;
		// char button_name[29][10];
		// int button_value[29];
		// double data[29];	
		//ofstream myfile;
  		//std::fstream fs;	
		string_parser();
		double data[29];
		std::string buttonName, buttonValue;
		double value;
		std::unordered_map<std::string, double> buttons;
		
		void parse_string(std::string controllerInput);
		void print_output();
		void notify(int,char []);
};

#endif

