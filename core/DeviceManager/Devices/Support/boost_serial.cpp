#include "boost_serial.h"

Serial::Serial() : m_serialPort(m_io) { m_io.run(); }

Serial::~Serial() { this->m_serialPort.close(); }

bool Serial::open(
    const std::string &portname, int baudrate, int charactersize,
    boost::asio::serial_port_base::parity::type parity,
    boost::asio::serial_port_base::stop_bits::type stopbits,
    boost::asio::serial_port_base::flow_control::type flowcontrol)
{
  this->m_serialPort.open(portname.c_str());
  if (!this->m_serialPort.is_open())
  {
    return false;
  }
  this->m_serialPort.set_option(
      boost::asio::serial_port_base::baud_rate(baudrate));
  this->m_serialPort.set_option(
      boost::asio::serial_port_base::character_size(charactersize));
  this->m_serialPort.set_option(boost::asio::serial_port_base::parity(parity));
  this->m_serialPort.set_option(
      boost::asio::serial_port_base::stop_bits(stopbits));
  this->m_serialPort.set_option(
      boost::asio::serial_port_base::flow_control(flowcontrol));

  return true;
}

bool Serial::isOpen() { return this->m_serialPort.is_open(); }

void Serial::close() { this->m_serialPort.close(); }

std::string Serial::receive(int receive_length)
{
  using namespace boost;
  char c;
  std::string result;
  bool print = false;
  int char_count = 0;
  for (int i = 0; i < receive_length; i++)
  {
    asio::read(this->m_serialPort, asio::buffer(&c, 1));

    result += c;
  }

  return result;
}

std::string Serial::IMU_receive()
{

  using namespace boost;
  char c;
  std::string result;
  bool print = false;
  int char_count = 0;

  while (1)
  {
    c = '\0';
    asio::read(this->m_serialPort, asio::buffer(&c, 1));

    if (c == 'm')
    {
      if (print)
      {
        result += c;
        break;
      }
      else
      {
        print = true;
      }
    }

    if (print)
    {
      result += c;
      char_count++;

      // printf("%c",c);

      //			if(c=='\n')
      //			{
      //				printf("the Count=%d--",char_count);
      //			}
    }
  }

  return result;
}
size_t Serial::send(void *data, size_t length)
{
  return boost::asio::write(this->m_serialPort,
                            boost::asio::buffer(data, length));
}

void Serial::IMU_send_command(std::string command)
{
  char *dummy_command = (char *)command.c_str();
  this->send(dummy_command, strlen(dummy_command) - 1);
  this->receive(strlen(dummy_command));
}

void serialPortwReadTimeout::read_complete(
    const boost::system::error_code &error, size_t bytes_transferred)
{

  read_error = (error || bytes_transferred == 0);
  timer.cancel();
}

void serialPortwReadTimeout::time_out(const boost::system::error_code &error)
{
  if (error)
  {
    return;
  }
  port.cancel();
}

void serialPortwReadTimeout::writeString(void *s, unsigned int s_size)
{
  boost::asio::write(port, boost::asio::buffer(s, s_size));
}

bool serialPortwReadTimeout::read(uint8_t &val)
{

  val = c = '\0';
  port.get_io_service().reset();

  // Asynchronously read 1 character.
  boost::asio::async_read(
      port, boost::asio::buffer(&c, 1),
      boost::bind(&serialPortwReadTimeout::read_complete, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));

  // Setup a deadline time to implement our timeout.
  timer.expires_from_now(boost::posix_time::milliseconds(timeout));
  timer.async_wait(boost::bind(&serialPortwReadTimeout::time_out, this,
                               boost::asio::placeholders::error));

  // This will block until a character is read
  // or until the it is cancelled.
  port.get_io_service().run();

  if (!read_error)
    val = c;

  return !read_error;
}

void serialPortwReadTimeout::press_sensor_response(int size)
{

  uint8_t c;

  for (int i = 0; i < size; i++)
  {
    if (read(c))
      rsp[i] = c;
    else
    {
      // printf("timed out..");
      rsp[0] = 0xFF;
      break;
    }
  }
}

void serialPortwReadTimeout::depth_sensor_response(int size)
{

  uint8_t c;

  for (int i = 0; i < size; i++)
  {
    if (read(c))
      depth_rsp[i] = (char)c;
    else
    {
      printf("timed out..\n");
      depth_rsp[0] = char(126);
      break;
    }
  }
}

void serialPortwReadTimeout::initialize_data_thread()
{
  this->queue_thread_status = true;
  pthread_create(&queue_thread, NULL, serialPortwReadTimeout::data_thread,
                 this);
  std::cout << this->yellow << "[" << this->sensor_name
            << "][Serial] Sensor data_queue_thread Initiated" << this->reset
            << std::endl;
}

void *serialPortwReadTimeout::data_thread(void *c)
{
  try
  {
    int loop_breaker = 0;
    ((serialPortwReadTimeout *)c)->thread_killed_status = false;
    uint8_t data;
    if (((serialPortwReadTimeout *)c)->queue_buffer_clear_size == 0)
    {
      ((serialPortwReadTimeout *)c)->queue_buffer_clear_size = 1000;
    }

    while (((serialPortwReadTimeout *)c)->queue_thread_status)
    {
      if (((serialPortwReadTimeout *)c)->read(data))
      {
        ((serialPortwReadTimeout *)c)->dataQueue.push_back(data);
      }
      else
      {
        loop_breaker++;
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
        if (loop_breaker == 10000)
        {
          std::cout << ((serialPortwReadTimeout *)c)->red << "["
                    << ((serialPortwReadTimeout *)c)->sensor_name
                    << "][Serial] Terminating queue - device non responsive"
                    << ((serialPortwReadTimeout *)c)->reset << std::endl;
          break;
        }
      }
      // above else loop comment if there is any discrepancy
      if (((serialPortwReadTimeout *)c)->dataQueue.size() >
          ((serialPortwReadTimeout *)c)->queue_buffer_clear_size)
      {
        std::cout << ((serialPortwReadTimeout *)c)->red << "["
                  << ((serialPortwReadTimeout *)c)->sensor_name
                  << "][Serial] Sensor data_queue_thread Cleared.."
                  << ((serialPortwReadTimeout *)c)->reset << std::endl;
        ((serialPortwReadTimeout *)c)->dataQueue.clear();
        ((serialPortwReadTimeout *)c)->data_clear_flag = true;
      }
      boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }
    std::cout << ((serialPortwReadTimeout *)c)->red << "["
              << ((serialPortwReadTimeout *)c)->sensor_name
              << "][Serial] Sensor data_queue_thread Terminated"
              << ((serialPortwReadTimeout *)c)->reset << std::endl;
    ((serialPortwReadTimeout *)c)->thread_killed_status = true;
  }
  catch (std::exception &e)
  {
    std::cout << ((serialPortwReadTimeout *)c)->red << "["
              << ((serialPortwReadTimeout *)c)->sensor_name
              << "][Serial][Error] Sensor data_queue_thread Terminated - "
              << e.what() << ((serialPortwReadTimeout *)c)->reset << std::endl;
    ((serialPortwReadTimeout *)c)->thread_killed_status = true;
  }
}
