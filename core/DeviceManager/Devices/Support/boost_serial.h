#ifndef BOOST_SERIAL_H
#define BOOST_SERIAL_H

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <deque>
#include <iostream>
#include <pthread.h>
#include <string>

#define MAX_SIZE 20

class Serial
{
public:
  Serial();
  ~Serial();

  /// Opens a connection to the serial port.
  /**
   * The parity, stop bits and flowcontrol are types of the
   * boost::asio::serial_port_base.
   * @param portname The device name.
   * @param baudrate The Baudrate.
   * @param charactersize The Character size in bits.
   * @param parity The parity.
   * @param stopbits The stop bits.
   * @param flowcontrol The flowcontrol
  */
  bool open(const std::string &portname, int baudrate, int charactersize,
            boost::asio::serial_port_base::parity::type parity,
            boost::asio::serial_port_base::stop_bits::type stopbits,
            boost::asio::serial_port_base::flow_control::type flowcontrol);

  bool isOpen();

  /// Close the serial port.
  void close();

  /// Receive data from the serial port.
  /**
   * @param data The pointer to the data which will be filled.
   * @param length The number of data to read. (In Bytes)
   * @return The number of received data.
   */
  std::string receive(int receive_command_length);
  std::string IMU_receive();

  /// Send data to the serial port.
  /**
   * @param data The pointer to the data which will be send.
   * @param length The number of data to send. (In Bytes)
   * @return The number of transfered data.
   */
  size_t send(void *data, size_t length);
  void IMU_send_command(std::string command);

  boost::asio::io_service m_io;
  boost::asio::serial_port m_serialPort;
};

class serialPortwReadTimeout
{
private:
  boost::asio::serial_port &port;
  size_t timeout;
  uint8_t c;
  boost::asio::deadline_timer timer;
  bool read_error;

  void read_complete(const boost::system::error_code &error,
                     size_t bytes_transferred);

  // Called when the timer's deadline expires.
  void time_out(const boost::system::error_code &error);

public:
  serialPortwReadTimeout(boost::asio::serial_port &port, size_t timeout)
      : port(port), timeout(timeout), timer(port.get_io_service()),
        read_error(true)
  {
    this->queue_buffer_clear_size = 0;
    // comment color types
    this->red = "\033[0;31m";
    this->green = "\033[1;32m";
    this->yellow = "\033[1;33m";
    this->reset = "\033[0m";
    this->data_clear_flag = false;
  }

  void writeString(void *s, unsigned int s_size);

  // Reads as unit8_t or times out returns false if the read times out
  bool read(uint8_t &val);

  uint8_t rsp[MAX_SIZE];

  void press_sensor_response(int size);

  char depth_rsp[MAX_SIZE];

  void depth_sensor_response(int size);

  // continous data support
  int dataQueueSize;
  std::deque<uint8_t> dataQueue; // data queue 8-bit data
  int queue_buffer_clear_size;   // the max size of the data queue, if exceeds
                                 // gets buffer gets cleared
  pthread_t queue_thread;
  bool queue_thread_status;
  bool thread_killed_status;
  void initialize_data_thread();
  static void *data_thread(void *c);
  std::string sensor_name;
  bool data_clear_flag;

  std::string red;
  std::string green;
  std::string yellow;
  std::string reset;
};

#endif
