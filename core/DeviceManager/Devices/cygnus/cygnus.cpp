#include "cygnus.h"

using namespace std;

Cygnus::Cygnus(std::string port, long baud_rate) {
  s_memory.create("CygnusMaterialVelocity", 1024, 1);
  this->Port = port;
  this->baud_rate = baud_rate;
  this->packetCount = 0;
  this->thickness_value = 0;
  this->resolution = 0;
  this->range = 0;
  this->thread_status = false;
  this->thread_killed_status = true;

  // comment color types
  this->red = "\033[0;31m";
  this->green = "\033[1;32m";
  this->yellow = "\033[1;33m";
  this->reset = "\033[0m";
}

bool Cygnus::initialize() {
  try {
    std::cout << this->yellow << "[Cygnus] Initiating..." << this->reset
              << std::endl;
    if (this->serial.open(Port, baud_rate, 8,
                          boost::asio::serial_port::parity::none,
                          boost::asio::serial_port_base::stop_bits::one,
                          boost::asio::serial_port::flow_control::none)) {
      pthread_create(&thread, NULL, Cygnus::data_update_thread, this);
      std::cout << this->green << "[Cygnus] Succesfully opened serial port"
                << this->reset << std::endl;
      return true;
    } else {
      return false;
      std::cout << this->red << "[Cygnus] Initiate Failed - Failed to open port"
                << this->reset << std::endl;
      this->thread_killed_status = true;
    }
  } catch (exception &e) {
    this->thread_killed_status = true;
    std::cout << this->red << "[Cygnus][Error] Unable to open port - "
              << e.what() << this->reset << std::endl;
  }
}

void *Cygnus::data_update_thread(void *c) {
  std::cout << ((Cygnus *)c)->yellow << "[Cygnus] Data update thread initiated"
            << ((Cygnus *)c)->reset << std::endl;
  serialPortwReadTimeout reader(((Cygnus *)c)->serial.m_serialPort,
                                500); // 500ms waiting period
  reader.queue_buffer_clear_size = 20;
  reader.thread_killed_status = false;

  std::cout << ((Cygnus *)c)->yellow
            << "[Cygnus] Initiating boost serial queue thread "
            << ((Cygnus *)c)->reset << std::endl;
  reader.sensor_name = "Cygnus";
  reader.initialize_data_thread();

  ((Cygnus *)c)->thread_status = true;
  while (((Cygnus *)c)->thread_status && (!reader.thread_killed_status)) {
    ((Cygnus *)c)->read_thickness(reader);
    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
  }
  // closing sequence
  reader.queue_thread_status = false;
  while (!reader.thread_killed_status) {
    usleep(10000);
  }
  std::cout << ((Cygnus *)c)->yellow << "[Cygnus] Closing Serial port"
            << ((Cygnus *)c)->reset << std::endl;
  ((Cygnus *)c)->serial.close();

  std::cout << ((Cygnus *)c)->red << "[Cygnus] Terminated"
            << ((Cygnus *)c)->reset << std::endl;
  ((Cygnus *)c)->thread_killed_status = true;
}

void Cygnus::read_thickness(serialPortwReadTimeout &reader) {
  try {
    this->idx = 0;
    if (reader.dataQueue.size() > 10) {
      this->cygnus_data_size_status = true;

      while (reader.dataQueue.front() != (char)0x01) {
        if (reader.dataQueue.size() > 2)
          reader.dataQueue.pop_front();
      }
      while (reader.dataQueue.front() != (char)0x17) {
        if (this->idx < 6) {
          this->dataPacket[this->idx] = reader.dataQueue.front();
          if (reader.dataQueue.size() > 0) {
            reader.dataQueue.pop_front();
            this->idx++;
          } else {
            this->cygnus_data_size_status = false;
            break;
          }
        } else {
          this->cygnus_data_size_status = false;
          break;
        }
      }
      if (this->idx == 6) {
        this->dataPacket[6] = reader.dataQueue.front();
        if (reader.dataQueue.size() > 0)
          reader.dataQueue.pop_front();
      }

      if ((this->cygnus_data_size_status) && (this->idx > 2))
        this->parseDataPacket();
      else if ((this->cygnus_data_size_status) && (this->idx == 2))
        this->thickness_value = 0.0;
    } else {
      // cout << "required data not available: " << ((Cygnus
      // *)c)->dataQueue.size()
      //      << endl;
    }
  } catch (exception &e) {
    std::cout << this->red << "[Cygnus][Error]-[@ Accessing data from queue] "
              << e.what() << this->reset << std::endl;
  }
}

void Cygnus::parseDataPacket() {
  try {
    material_velocity = 0;
    material_velocity = s_memory.fetch("CygnusMaterialVelocity");

    // std::cout << std::hex << (int)dataPacket[0] << " " << (int)dataPacket[1]
    //           << " " << (int)dataPacket[2] << " " << (int)dataPacket[3] << "
    //           "
    //           << (int)dataPacket[4] << " " << (int)dataPacket[5] << " "
    //           << (int)dataPacket[6] << " " << std::endl;

    if (this->dataPacket[0] == (char)0x01) {
      bool reading_valid = sensor_protocol.dataCheck(dataPacket[1]);
      if (reading_valid) {
        bool positive_value = true;
        int first_digit = dataPacket[2] - '0';
        int second_digit = dataPacket[3] - '0';
        int third_digit = dataPacket[4] - '0';
        int fourth_digit = dataPacket[5] - '0';
        if (first_digit < 0 || second_digit < 0 || third_digit < 0 ||
            fourth_digit < 0) {
          positive_value = false;
        }
        resolution = sensor_protocol.resolution;
        range = sensor_protocol.range;

        if (material_velocity > 0)
          conversion_factor = material_velocity / (BASE_VELOCITY * 1.0);
        else
          conversion_factor = DEFAULT_MATERIAL_VELOCITY / (BASE_VELOCITY * 1.0);

        if (resolution == 0 && range == 0 && positive_value) {
          thickness_value = (first_digit * 100 + second_digit * 10 +
                             third_digit * 1 + fourth_digit * 0.1) *
                            conversion_factor;
        } else if (resolution == 0 && range == 1 && positive_value) {
          thickness_value = (first_digit * 100 + second_digit * 10 +
                             third_digit * 1 + fourth_digit * 0.1) *
                            conversion_factor;
        } else if (resolution == 1 && range == 0 && positive_value) {
          thickness_value = (first_digit * 10 + second_digit * 1 +
                             third_digit * 0.1 + fourth_digit * 0.01) *
                            conversion_factor;
        } else if (resolution == 1 && range == 1 && positive_value) {
          thickness_value = (first_digit * 100 + second_digit * 10 +
                             third_digit * 1 + fourth_digit * 0.1) *
                            conversion_factor;
        } else {
          thickness_value = 0.0;
          printf("%s\n", "Unable to Parse");
        }
        this->packetCount++;
      } else {
        thickness_value = 0.0;
        // printf("%s\n", "No Measuerment Value");
      }
    }
  } catch (exception &e) {
    std::cout << this->red << "[Cygnus][Error]-[@ Parsing the data] "
              << e.what() << this->reset << std::endl;
  }
}

double Cygnus::fetch_thickness() { return thickness_value; }

void Cygnus::print_data() {
  while (1) {
    usleep(500000);
    std::cout << "Thickness: " << thickness_value
              << " mm, Count: " << packetCount << std::endl;
  }
}

bool CygnusProtocol::dataCheck(char data) {
  try {
    // char buffer[10];
    // std::itoa((int)data, buffer, 2);
    std::string buffer = bitset<8>(data).to_string();
    // cout << data << " : " << buffer << endl;
    if (buffer.at(7) == '0') {
      this->resolution = buffer.at(1) - '0';
      this->range = buffer.at(3) - '0';
      return true;
    } else {
      return false;
    }
  } catch (exception &e) {
    std::cout << "[Cygnus][Error]-[@ Data check while Parsing] " << e.what()
              << std::endl;
  }
}
