#ifndef CYGNUS_H
#define CYGNUS_H

#include "../../../SharedMemoryService/shared_memory.h"
#include "../Support/boost_serial.h"
#include <bitset>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/thread.hpp>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/time.h>
#include <time.h>

#define BASE_VELOCITY 6400
#define DEFAULT_MATERIAL_VELOCITY 5750.0

using namespace boost::interprocess;
using namespace std;

class CygnusProtocol {

public:
  double value();
  bool dataCheck(char data);
  int resolution;
  int range;
};

class Cygnus {

public:
  Cygnus(std::string port, long baud_rate);

  Serial serial;

  std::string Port;
  long baud_rate;
  char dataPacket[7];
  deque<char> dataQueue;
  unsigned long packetCount;

  CygnusProtocol sensor_protocol;
  double thickness_value;
  int resolution;
  int range;
  double conversion_factor;
  double material_velocity;

  void parseDataPacket();
  int idx;
  static void *data_update_thread(void *c);
  bool initialize();

  void read_thickness(serialPortwReadTimeout &reader);
  double fetch_thickness();
  void print_data();

  bool cygnus_data_size_status;
  bool thread_status;
  bool thread_killed_status;
  pthread_t thread;
  SMemory s_memory;

  std::string red;
  std::string green;
  std::string yellow;
  std::string reset;
};

#endif
