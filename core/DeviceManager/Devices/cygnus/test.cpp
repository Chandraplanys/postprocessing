#include "cygnus.h"

#define CYGNUS_PORT "/dev/Cygnus"
#define CYGNUS_BAUD_RATE 2400
Cygnus cygnus(CYGNUS_PORT, CYGNUS_BAUD_RATE);

void shutDown() {
  printf("Shutdown sequence initiated...\n");

  cygnus.thread_status = false;
  while (!cygnus.thread_killed_status) {
    usleep(100000);
  }
  exit(0);
}

void handler(int s) {
  printf("Interupt signal Received --->%d<---- Cygnus\n", s);
  // Broken pipe from a client
  if (s == SIGPIPE) {
    // Just ignore, sock class will close the client the next time
  }
  // Catch ctrl-c, segmentation fault, abort signal
  if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM) {
    shutDown();
  }
}

void initSigHandler() {

  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  sigaction(SIGPIPE, &sigIntHandler, NULL);
  sigaction(SIGABRT, &sigIntHandler, NULL);
  sigaction(SIGSEGV, &sigIntHandler, NULL);
  sigaction(SIGTERM, &sigIntHandler, NULL);
}

int main(int argc, char *argv[]) {
  // initialization
  // SMemory s_memory;
  // s_memory.create("CygnusMaterialVelocity", 1024, 1);
  printf("Program Initiated....\n");
  initSigHandler();
  bool state = cygnus.initialize();
  cygnus.print_data();
  return 0;
}
