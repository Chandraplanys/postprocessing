#include "gps.h"

GPS::GPS(std::string port, long baud_rate, std::string log_properties_path,
         std::string log_location)
{
    // comment color types
    this->red = "\033[0;31m";
    this->green = "\033[1;32m";
    this->yellow = "\033[1;33m";
    this->reset = "\033[0m";

    this->port = port;
    this->baud_rate = baud_rate;
    this->thread_status = false;
    this->thread_killed_status = false;

    // create log file
    std::string curr_date = Processed_log.currentDate();
    std::string curr_time = Processed_log.currentTime();
    std::string log_file_path =
        log_location + "Processed/gps_" + curr_date + "_" + curr_time + ".log";
    ofstream outputFile(log_file_path);

    std::string raw_log_file_path =
        log_location + "Raw/gps_" + curr_date + "_" + curr_time + ".log";
    ofstream outputFile1(raw_log_file_path);
    // outputFile.open();
    // outputFile.close();
    std::cout << this->yellow << "[GPS] logfile path :" << log_file_path
              << this->reset << std::endl;
    // std::string log_properties_path = "../../../Logger/sub_log4cpp.properties";
    Processed_log.initialize(log_file_path, "gps", log_properties_path);
    Raw_log.initialize(raw_log_file_path, "raw_gps", log_properties_path);

    std::cout << this->yellow << "[GPS] sub logger initialized" << log_file_path
              << this->reset << std::endl;
}

bool GPS::initialize()
{
    try
    {
        if (this->serial.open(port, baud_rate, 8,
                              boost::asio::serial_port::parity::none,
                              boost::asio::serial_port_base::stop_bits::one,
                              boost::asio::serial_port::flow_control::none))
        {
            s_memory.create("GPSRov", 1024, 5);
            s_memory.create("PressureSensor", 1024, 2);
            s_memory.create("Altimeter", 1024, 1);
            pthread_create(&thread, NULL, GPS::data_update_thread, this);
            std::cout << this->green << "[GPS] Succesfully opened port" << this->reset
                      << std::endl;
            return true;
        }
        else
        {
            std::cout << this->red << "[GPS] Unable to open port: " << port
                      << this->reset << std::endl;
            std::string msg;
            msg = "Unable to open the port :" + port;
            Processed_log.error(msg);
            return false;
        }
    }
    catch (exception &e)
    {
        std::cout << this->red << "[GPS][Error] Unable to open port - " << e.what()
                  << this->reset << std::endl;
        std::string msg;
        msg = "Unable to connect to port -" + port + ", " + e.what();
        Processed_log.error(msg);
        return false;
    }
}

void *GPS::data_update_thread(void *c)
{
    try
    {

        std::cout << ((GPS *)c)->yellow << "[GPS] Data update thread initiated"
                  << ((GPS *)c)->reset << std::endl;
        serialPortwReadTimeout reader(((GPS *)c)->serial.m_serialPort,
                                      500); // 500ms waiting period
        reader.queue_buffer_clear_size = 5000;
        reader.thread_killed_status = false;

        std::cout << ((GPS *)c)->yellow
                  << "[GPS] Initiating boost serial queue thread "
                  << ((GPS *)c)->reset << std::endl;
        reader.sensor_name = "GPS";
        reader.initialize_data_thread();

        ((GPS *)c)->thread_status = true;
        while (((GPS *)c)->thread_status && (!reader.thread_killed_status))
        {
            ((GPS *)c)->read_gps(reader);
            boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
        // closing sequence
        reader.queue_thread_status = false;
        while (!reader.thread_killed_status)
        {
            usleep(10000);
        }
        std::cout << ((GPS *)c)->yellow << "[GPS] Closing Serial port"
                  << ((GPS *)c)->reset << std::endl;
        ((GPS *)c)->serial.close();
        std::cout << ((GPS *)c)->red << "[GPS] Terminated" << ((GPS *)c)->reset
                  << std::endl;
        ((GPS *)c)->thread_killed_status = true;
        ((GPS *)c)->thread_status = false;
    }
    catch (exception &e)
    {
        std::cout << ((GPS *)c)->red << "[GPS][Error] data_update_thread processing - "
                  << e.what() << ((GPS *)c)->reset << std::endl;
        std::string msg;
        msg = (std::string) "Issue in data_update_thread: Read Sensor While Loop" +
              e.what();
        ((GPS *)c)->Processed_log.error(msg);
        ((GPS *)c)->thread_status = false;
    }
}

void GPS::read_gps(serialPortwReadTimeout &reader)
{
    try
    {

        if (reader.dataQueue.size() > 1)
        {

            // while (true)
            // {
            //     std::cout << reader.dataQueue.front();
            //     reader.dataQueue.pop_front();
            //     usleep(10000);
            // }
            while (true)
            {

                if (reader.dataQueue.front() != '$')
                {
                    if (reader.dataQueue.size() > 50)
                    {
                        reader.dataQueue.pop_front();
                    }
                    else
                    {
                        usleep(10000);
                    }
                }
                else
                {
                    usleep(1000);
                    break;
                }
            }
            // check for data initiating identifier
            if (reader.dataQueue.front() == '$')
            {

                if (reader.dataQueue.size() > 0)
                {
                    gps_string.push_back(reader.dataQueue.front());
                    reader.dataQueue.pop_front();
                }
                while (reader.dataQueue.size() < 1)
                {
                    std::cout << "waiting ...";

                    usleep(100000);
                }

                while (reader.dataQueue.front() != '$')
                {
                    if (reader.dataQueue.size() > 50)
                    {
                        gps_string.push_back(reader.dataQueue.front());
                        reader.dataQueue.pop_front();
                    }
                    else
                    {
                        usleep(1000);
                    }
                }

                // std::cout << "GPS: " << gps_string << std::endl;
                gpsParser(gps_string); // Processing data - Please Remove for any Crashed
                gps_logging(gps_string);
                // std::cout << "Cleared String.........." << std::endl;
            }
            else
            {
                if (reader.dataQueue.size() > 0)
                {
                    reader.dataQueue.pop_front();
                }
            }
        }
    }
    catch (exception &e)
    {
        std::cout << this->red << "[GPS][Error] - [Reading/Parsing GPS value] - "
                  << e.what() << this->reset << std::endl;
    }
}

void GPS::wait(serialPortwReadTimeout &reader)
{
    while (reader.dataQueue.size() < 50)
    {
        usleep(100);
    }
}

void GPS::gps_logging(std::string gps_data)
{
    try
    {
        // gps_data.erase(std::remove(gps_data.begin(), gps_data.end(), '\n'), gps_data.end());
        // gps_data = gps_data.substr(0, gps_data.size() - 1);

        std::string values = std::to_string(sm.fetch("Altimeter", 1)) + "," +
                             std::to_string(sm.fetch("PressureSensor", 1)) + "," + gps_data;
        Raw_log.info(values);
        std::cout << "values: " << values << std::endl;
        gps_string = "";

        // printf("Value: %s\n", values);
    }
    catch (exception &e)
    {
        std::cout << this->red << "[GPS][Error] - [GPS Logging] - "
                  << e.what() << this->reset << std::endl;
    }
}

void GPS::gpsParser(std::string gpsDataString)
{
    std::string checkString;
    // std::cout << "String Size: " << gpsDataString.size() << std::endl;

    if (gpsDataString.size() > 6)
    {
        checkString = gpsDataString.substr(0, 6);
        if (checkString == "$GNGGA")
        {
            gpsParseData(gpsDataString);
        }
    }
}

void GPS::gpsParseData(std::string gpsDataString)
{
    vector<string> DataParts;
    boost::algorithm::split(DataParts, gpsDataString, boost::is_any_of(","));
    double latitude_value;
    double longitude_value;
    int latitude_sign;
    int longitude_sign;
    int accuracy_value;
    this->data_status = true;
    if (DataParts.size() > 1)
    {
        std::cout << "String GNGGA: " << DataParts[0] << std::endl;
    }
    else
    {
        this->data_status = false;
    }
    if (DataParts.size() > 2)
    {
        std::cout << "String UTC: " << DataParts[1] << std::endl;
    }
    else
    {
        this->data_status = false;
    }
    if (DataParts.size() > 3)
    {
        if (DataParts[2].size() > 8)
        {
            latitude_value = std::stod(DataParts[2].substr(0, 2)) + (std::stod(DataParts[2].substr(2)) / 60.0);
            std::cout << "String Lat: " << DataParts[2] << ',' << latitude_value << std::endl;
        }
    }
    else
    {
        this->data_status = false;
    }
    if (DataParts.size() > 4)
    {
        if ((DataParts[3] == "N") || (DataParts[3] == "E"))
            latitude_sign = 1.0;
        else if ((DataParts[3] == "S") || (DataParts[3] == "W"))
            latitude_sign = -1.0;
        else
            this->data_status = false;

        std::cout << "String Lat Direc: " << DataParts[3] << ',' << latitude_sign << std::endl;
    }
    else
    {
        this->data_status = false;
    }
    if (DataParts.size() > 5)
    {
        if (DataParts[4].size() > 8)
        {
            longitude_value = std::stod(DataParts[4].substr(0, 3)) + (std::stod(DataParts[4].substr(3)) / 60.0);
            std::cout << "String Long: " << DataParts[4] << ',' << longitude_value << std::endl;
        }
    }
    else
    {
        this->data_status = false;
    }
    if (DataParts.size() > 6)
    {
        if ((DataParts[3] == "N") || (DataParts[3] == "E"))
            longitude_sign = 1.0;
        else if ((DataParts[3] == "S") || (DataParts[3] == "W"))
            longitude_sign = -1.0;
        else
            this->data_status = false;
        std::cout << "String Long Direc: " << DataParts[5] << ',' << longitude_sign << std::endl;
    }
    else
    {
        this->data_status = false;
    }
    if (DataParts.size() > 7)
    {
        accuracy_value = std::stod(DataParts[6]);
        ((accuracy_value > -1) && (accuracy_value < 7)) ?: (this->data_status = false);
        std::cout << "String Accuracy: " << DataParts[6] << std::endl;
    }
    else
    {
        this->data_status = false;
    }

    if (this->data_status)
    {
        this->latitude = latitude_sign * latitude_value;
        this->longitude = longitude_sign * longitude_value;
        this->data_accuracy = accuracy_value;
        std::string values = std::to_string(sm.fetch("Altimeter", 1)) + "," +
                             std::to_string(sm.fetch("PressureSensor", 1)) + "," +
                             to_string_with_precision(this->latitude, 8) + "," +
                             to_string_with_precision(this->longitude, 8) + "," +
                             std::to_string(this->data_accuracy);
        std::cout << "Added to Log file" << std::endl;
        Processed_log.info(values);
        s_memory.update("GPSRov", 5, 1.0);
        s_memory.update("GPSRov", 1, this->latitude);
        s_memory.update("GPSRov", 2, this->longitude);
        s_memory.update("GPSRov", 3, this->data_accuracy * 1.0);
        this->data_status ? s_memory.update("GPSRov", 4, 1.0)
                          : s_memory.update("GPSRov", 4, 0.0);
        std::cout << "Updated Shared Memory" << std::endl;
        std::cout << "GPS Data: " << this->latitude << ',' << this->longitude << ',' << this->data_accuracy << std::endl;
        std::cout << "GPS Data Logged: " << values << std::endl;
    }
    else
    {
        std::cout << "[WARNING] Wrong Data" << std::endl;
        this->data_status ? s_memory.update("GPSRov", 4, 1.0)
                          : s_memory.update("GPSRov", 4, 0.0);
        std::cout << "Updated Shared Memory data status to false" << std::endl;
    }
}

void GPS::print_data()
{
    while (1)
    {
        sleep(1);
    }
}
std::string GPS::to_string_with_precision(double value, const int n = 6)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(n) << value;
    return out.str();
}