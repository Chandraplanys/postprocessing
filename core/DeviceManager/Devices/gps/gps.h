#ifndef GPS_H
#define GPS_H

#include "../../../Logger/Logger.h"
#include "../../../SharedMemoryService/shared_memory.h"
#include "../Support/boost_serial.h"
#include <bitset>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/fusion/algorithm/query/all.hpp>
#include <boost/fusion/include/all.hpp>
#include <boost/thread.hpp>
#include <deque>
#include <exception>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#define IDENTIFIER_SIZE 5

class GPS
{

public:
  // fetching data
  GPS(std::string port, long baud_rate, std::string log_properties_path,
      std::string log_location);
  Serial serial;
  Logger log;
  Logger Raw_log;
  std::string port;
  long baud_rate;

  // data control functions
  bool initialize();
  static void *data_update_thread(void *c);
  void read_gps(serialPortwReadTimeout &reader);
  void wait(serialPortwReadTimeout &reader);

  // thread and meamory manage
  bool thread_status;
  bool thread_killed_status;

  bool data_status;
  int data_accuracy;
  double latitude;
  double longitude;
  double latitude_direction;
  double longitude_direction;
  double latitude_degrees;
  double longitude_degrees;

  pthread_t thread;
  SMemory s_memory;
  SMemory sm;

  std::string Logging_Data;
  std::string gps_string;

  // parsing data for GPS location
  void gps_logging(std::string gps_data);
  void gpsParser(std::string gpsDataString);
  void gpsParseData(std::string gpsDataString);
  void print_data();
  std::string to_string_with_precision(double value, const int n);

  // logging data format
  // template <typename T>
  std::string red;
  std::string green;
  std::string yellow;
  std::string reset;
};

#endif
