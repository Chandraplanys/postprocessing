#include "gps.h"

GPS::GPS(std::string port, long baud_rate, std::string log_properties_path,
         std::string log_location)
{
  // comment color types
  this->red = "\033[0;31m";
  this->green = "\033[1;32m";
  this->yellow = "\033[1;33m";
  this->reset = "\033[0m";

  this->port = port;
  this->baud_rate = baud_rate;
  this->thread_status = false;
  this->thread_killed_status = false;
  this->dataPacketSize = 0;
  this->latitude = 111111.0;
  this->longitude = 111111.0;
  this->data_status = false;
  this->data_accuracy = 0;

  // create log file
  std::string curr_date = log.currentDate();
  std::string curr_time = log.currentTime();
  std::string log_file_path =
      log_location + "gps_" + curr_date + "_" + curr_time + ".log";
  ofstream outputFile(log_file_path);
  // outputFile.open();
  // outputFile.close();
  std::cout << this->yellow << "[GPS] logfile path :" << log_file_path
            << this->reset << std::endl;
  // std::string log_properties_path = "../../../Logger/sub_log4cpp.properties";
  log.initialize(log_file_path, "gps", log_properties_path);

  std::cout << this->yellow << "[GPS] sub logger initialized" << log_file_path
            << this->reset << std::endl;
}

bool GPS::initialize()
{
  try
  {
    if (this->serial.open(port, baud_rate, 8,
                          boost::asio::serial_port::parity::none,
                          boost::asio::serial_port_base::stop_bits::one,
                          boost::asio::serial_port::flow_control::none))
    {
      pthread_create(&thread, NULL, GPS::data_update_thread, this);
      std::cout << this->green << "[GPS] Succesfully opened port" << this->reset
                << std::endl;
      return true;
    }
    else
    {
      std::cout << this->red << "[GPS] Unable to open port: " << port
                << this->reset << std::endl;
      std::string msg;
      msg = "Unable to open the port :" + port;
      log.error(msg);
      return false;
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] Unable to open port - " << e.what()
              << this->reset << std::endl;
    std::string msg;
    msg = "Unable to connect to port -" + port + ", " + e.what();
    log.error(msg);
    return false;
  }
}

void *GPS::data_update_thread(void *c)
{
  try
  {

    std::cout << ((GPS *)c)->yellow << "[GPS] Data update thread initiated"
              << ((GPS *)c)->reset << std::endl;
    serialPortwReadTimeout reader(((GPS *)c)->serial.m_serialPort,
                                  500); // 500ms waiting period
    reader.queue_buffer_clear_size = 5000;
    reader.thread_killed_status = false;

    std::cout << ((GPS *)c)->yellow
              << "[GPS] Initiating boost serial queue thread "
              << ((GPS *)c)->reset << std::endl;
    reader.sensor_name = "GPS";
    reader.initialize_data_thread();

    ((GPS *)c)->thread_status = true;
    while (((GPS *)c)->thread_status && (!reader.thread_killed_status))
    {
      ((GPS *)c)->read_gps(reader);
      boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }
    // closing sequence
    reader.queue_thread_status = false;
    while (!reader.thread_killed_status)
    {
      usleep(10000);
    }
    std::cout << ((GPS *)c)->yellow << "[GPS] Closing Serial port"
              << ((GPS *)c)->reset << std::endl;
    ((GPS *)c)->serial.close();
    std::cout << ((GPS *)c)->red << "[GPS] Terminated" << ((GPS *)c)->reset
              << std::endl;
    ((GPS *)c)->thread_killed_status = true;
    ((GPS *)c)->thread_status = false;
  }
  catch (exception &e)
  {
    std::cout << ((GPS *)c)->red << "[GPS][Error] data_update_thread processing - "
              << e.what() << ((GPS *)c)->reset << std::endl;
    std::string msg;
    msg = (std::string) "Issue in data_update_thread: Read Sensor While Loop" +
          e.what();
    ((GPS *)c)->log.error(msg);
    ((GPS *)c)->thread_status = false;
  }
}

void GPS::read_gps(serialPortwReadTimeout &reader)
{
  try
  {
    int idx = 0;
    bool dataCollectStatus = true;
    if (this->dataCheck.size() > 0)
    {
      this->dataCheck.clear();
    }
    if (reader.dataQueue.size() > 100)
    {
      // check for data initiating identifier
      if (reader.dataQueue.front() == '$')
      {
        // Get the initial location identification string
        for (int i = 0; i < 6; i++)
        {
          if (reader.dataQueue.size() > 0)
          {
            this->dataPacket[i] = reader.dataQueue.front();
          }
          else
          {
            dataCollectStatus = false;
            std::cout << "[WARNING][GPS][read_gps] Data Collection for initial string -fetch" << std::endl;
          }
          this->dataCheck.push_back(this->dataPacket[i]);
          // this->dataCheck[i] = reader.dataQueue.front();
          this->wait(reader);
          if (reader.dataQueue.size() > 0)
          {
            reader.dataQueue.pop_front();
          }
          else
          {
            dataCollectStatus = false;
            std::cout << "[WARNING][GPS][read_gps] Data Collection for initial string - POP" << std::endl;
          }
        }
        int i = 6;
        if (dataCollectStatus)
        {
          // compare the string to location identifier
          if (this->dataCheck == "$GNGGA")
          {
            // collect all data until next line "$"
            // std::cout << "getting data..";
            while (reader.dataQueue.front() != '$')
            {
              if (reader.dataQueue.size() > 0)
              {
                this->dataPacket[i] = reader.dataQueue.front();
              }
              else
              {
                dataCollectStatus = false;
                std::cout << "[WARNING][GPS][read_gps] Fetching remaining data While Loop dataCollectionStatus is FALSE - Copy" << std::endl;

                break;
              }
              this->wait(reader);
              if (reader.dataQueue.size() > 0)
              {
                reader.dataQueue.pop_front();
              }
              else
              {
                dataCollectStatus = false;
                std::cout << "[WARNING][GPS][read_gps] Fetching remaining data While Loop dataCollectionStatus is FALSE - POP" << std::endl;

                break;
              }
              i++;
            }
            // store the next line char as indicator for data end
            // store dataPacketSize
            if (dataCollectStatus)
            {
              if (reader.dataQueue.size() > 0)
              {
                this->dataPacket[i] = reader.dataQueue.front();
              }
              else
              {
                dataCollectStatus = false;
                std::cout << "[WARNING][GPS][read_gps] Fetching Last String  dataCollectionStatus is FALSE - Copy" << std::endl;
              }
              this->wait(reader);
              if (reader.dataQueue.size() > 0)
              {
                reader.dataQueue.pop_front();
              }
              else
              {
                dataCollectStatus = false;
                std::cout << "[WARNING][GPS][read_gps] Fetching Last String  dataCollectionStatus is FALSE - POP" << std::endl;
              }

              this->dataPacketSize = i + 1;
              if (dataCollectStatus && !reader.data_clear_flag)
              {
                this->parseDataPacket();
              }
              else
              {
                if (reader.data_clear_flag)
                {
                  reader.data_clear_flag = false;
                  std::cout << "[WARNING][GPS][read_gps] Data queue has been cleared " << std::endl;
                }
              }
            }
          }
          if (this->dataCheck.size() > 0)
          {
            this->dataCheck.clear();
          }
        }
      }
      else
      {
        this->wait(reader);
        if (reader.dataQueue.size() > 0)
        {
          reader.dataQueue.pop_front();
        }
      }
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [Reading/Parsing GPS value] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::wait(serialPortwReadTimeout &reader)
{
  while (reader.dataQueue.size() < 50)
  {
    usleep(100);
  }
}

void GPS::parseDataPacket()
{
  try
  {
    if (this->dataPacketSize > 0)
    {
      // parse the data based on comma
      std::string dataPacket_str;
      for (int i = 0; i < dataPacketSize; i++)
      {
        dataPacket_str.push_back(dataPacket[i]);
      }
      // std::cout << dataPacket_str << std::endl;
      location_parse(dataPacket_str);
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [Parsing GPS Packet] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::location_parse(std::string str)
{
  try
  {
    std::string data[50];
    std::string str_variable;
    int data_size = 0;
    for (unsigned i = 0; i < str.length(); ++i)
    {
      str_variable = str.at(i);
      if (str_variable != ",")
      {
        data[data_size].append(str_variable);
      }
      else
      {
        data_size++;
      }
    }
    if (!(data[2].empty() || data[3].empty() || data[4].empty() ||
          data[5].empty() || data[6].empty()))
    {
      this->is_data_correct = true;
      this->degrees_protocol(data[2], "latitude");
      this->direction_protocol(data[3], "latitude");

      this->degrees_protocol(data[4], "longitude");
      this->direction_protocol(data[5], "longitude");

      this->accuracy_protocol(data[6]);
      this->gps_calculation();
    }
    else
    {
      // std::cout << "is latitude empty? : " << data[2].empty() << ":"
      //           << data[3].empty() << std::endl;
      // std::cout << "is longitude empty? : " << data[4].empty() << ":"
      //           << data[5].empty() << std::endl;
      // std::cout << "Accuracy value empty?" << data[6].empty() << std::endl;
      this->data_status = false;
      // loging the data
      std::string values;
      values.push_back(',');
      values.push_back(',');
      if (!data[6].empty())
      {
        int value = std::stod(data[6]);
        values;

        ((value > -1) && (value < 7)) ? values + std::to_string(value)
                                      : values + std::to_string(-2);
      }
      else
      {
        values = values + std::to_string(-1);
      }
      log.info(values);
      values.clear();
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [Location GPS Packet Parsing] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::degrees_protocol(std::string str, std::string type)
{
  try
  {
    if (type == "latitude")
    {
      this->latitude_degrees =
          std::stod(str.substr(0, 2)) + (std::stod(str.substr(2)) / 60.0);
    }
    else if (type == "longitude")
    {
      this->longitude_degrees =
          std::stod(str.substr(0, 3)) + (std::stod(str.substr(3)) / 60.0);
    }
    else
    {
      this->is_data_correct = false;
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [Degree Protocol] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::direction_protocol(std::string str, std::string type)
{
  try
  {
    int value;
    if ((str == "N") || (str == "E"))
      value = 1.0;
    else if ((str == "S") || (str == "W"))
      value = -1.0;
    else
      this->is_data_correct = false;

    if (type == "latitude")
    {
      this->latitude_direction = value;
    }
    else if (type == "longitude")
    {
      this->longitude_direction = value;
    }
    else
    {
      this->is_data_correct = false;
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [Direction Protocol] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::accuracy_protocol(std::string str)
{
  try
  {
    int value = std::stod(str);
    ((value > -1) && (value < 7)) ? (this->data_accuracy = value)
                                  : (this->is_data_correct = false);
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [Accuracy Protocol] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::gps_calculation()
{
  try
  {
    SMemory sm;
    if (this->is_data_correct)
    {
      this->latitude = this->latitude_direction * this->latitude_degrees;
      this->longitude = this->longitude_direction * this->longitude_degrees;
      this->data_status = true;
      std::string values = to_string_with_precision(this->latitude, 8) + "," +
                           to_string_with_precision(this->longitude, 8) + "," +
                           std::to_string(this->data_accuracy) + "," +
                           std::to_string(sm.fetch("Altimeter", 1)) + "," +
                           std::to_string(sm.fetch("PressureSensor", 1));
      log.info(values);
    }
  }
  catch (exception &e)
  {
    std::cout << this->red << "[GPS][Error] - [GPS Calculation] - "
              << e.what() << this->reset << std::endl;
  }
}

void GPS::print_data()
{
  while (1)
  {
    sleep(1);
    printf("latitude: %0.8f , longitude: %0.8f, accuracy: %d, status: %s\n",
           this->latitude, this->longitude, this->data_accuracy,
           this->data_status ? "true" : "false");
  }
}

std::string GPS::to_string_with_precision(double value, const int n = 6)
{
  std::ostringstream out;
  out << std::fixed << std::setprecision(n) << value;
  return out.str();
}
