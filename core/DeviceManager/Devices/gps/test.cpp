#include "gps.h"

#define GPS_PORT "/dev/ttyACM0"
#define GPS_BAUD_RATE 9600

int main(int argc, char *argv[])
{
  // initialization
  // SMemory s_memory;
  // s_memory.create("CygnusMaterialVelocity", 1024, 1);
  printf("Program Initiated....\n");
  GPS gps(GPS_PORT, GPS_BAUD_RATE, "../../../Logger/sub_log4cpp.properties",
          "../../logs/gps/test/");
  gps.initialize();
  gps.thread_status = true;
  gps.print_data();
  return 0;
}
