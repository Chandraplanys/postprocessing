#include <fstream>
#include <iostream>

#include "../Logger/DirectoryCreator.h"
#include "DeviceManager.h"

using namespace std;

DeviceManager DM;
SMemory s_memory_main;

DirectoryCreator logDirectory;

//..........Signal handler and Exit function...............

void shutDown()
{
  // s_memory_main.create_bool("DeviceManager_response_flag", true);
  s_memory_main.update_bool("DeviceManager_response_flag", false);

  s_memory_main.create("PressureSensor", 1024, 1);
  s_memory_main.create("IMU", 1024, 12);
  s_memory_main.create("Altimeter", 1024, 1);
  // s_memory_main.create("Joystick", 3072, 28);
  // s_memory_main.create("Cygnus", 1024, 1);
  // s_memory_main.create("GPSRov", 1024, 5);
  // s_memory_main.create("LeakSensor", 1024, 2);
  // s_memory_main.create("Cockpit", 1024, 2);
  // s_memory_main.create_str("videoFolderPath", 1024);
  std::cout << "[Device Manager] Terminating..." << std::endl;

  if (DEVICE_MANAGER_FLAG)
  {
    // DM.gamepadServer.closeServer();
    // server.stop();

    boost::this_thread::sleep(boost::posix_time::microseconds(1000));
    DEVICE_MANAGER_FLAG = false;
    (void)pthread_join(DM.threadMonitor_Thread, NULL);
  }
  boost::this_thread::sleep(boost::posix_time::microseconds(1000));

  s_memory_main.update_bool("DeviceManager_response_flag", true);
  s_memory_main.update_bool("DeviceManager_status_flag", false);

  s_memory_main.update("IMU", 1, 0.0);
  s_memory_main.update("IMU", 2, 0.0);
  s_memory_main.update("PressureSensor", 1, 0);
  s_memory_main.update("Altimeter", 1, 0);

  std::cout << "DeviceManager terminated \n"
            << std::endl;
  exit(0);
}

void handler(int s)
{

  printf("Interupt signal Received --->%d<---- Device Manager\n", s);

  // Broken pipe from a client
  if (s == SIGPIPE)
  {

    // Just ignore, sock class will close the client the next time
  }
  // Catch ctrl-c, segmentation fault, abort signal
  if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM)
  {

    shutDown();
  }
}

void initSigHandler()
{

  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  sigaction(SIGPIPE, &sigIntHandler, NULL);
  sigaction(SIGABRT, &sigIntHandler, NULL);
  sigaction(SIGSEGV, &sigIntHandler, NULL);
  sigaction(SIGTERM, &sigIntHandler, NULL);
}

void initSharedMem()
{
  //...... Device Manger control from GUI by flags ...............
  std::cout << "Creating Device Manger SHM" << std::endl;

  s_memory_main.create_bool("DeviceManager_status_flag", 1024);
  std::cout << "1" << std::endl;

  s_memory_main.create_bool("DeviceManager_response_flag", 1024);
  std::cout << "Created Device Manger SHM" << std::endl;
}

int main(int argc, char **argv)
{

  // std::string debugLogfolderPath = "";
  // /** backup std::cout buffer and redirect to debud file **/
  // // logDirectory.folder_file_maker("DeviceManager");
  // debugLogfolderPath = logDirectory.folder_file_maker("DeviceManager");
  // ofstream out(debugLogfolderPath + "DeviceManager_DEBUG.txt");

  // streambuf *stdcoutbuf = std::cout.rdbuf();
  // std::cout.rdbuf(out.rdbuf());

  initSigHandler();
  initSharedMem();

  s_memory_main.update_bool("DeviceManager_status_flag", true); // re-check with GUI
  DM.InitializeSharedMems();
  std::cout << "[DEVICE MANAGER MAIN CPP]Successfully intialized shared memory" << std::endl;
  DM.InitializeThreads(&DM);

  s_memory_main.update_bool("DeviceManager_response_flag", true);

  bool SERVICE_STATUS = true;

  while (SERVICE_STATUS)
  {
    SERVICE_STATUS = s_memory_main.fetch_bool("DeviceManager_status_flag");
    boost::this_thread::sleep(boost::posix_time::microseconds(500));
  }
  // out.close();
  // std::cout.rdbuf(stdcoutbuf);
  shutDown();

  return 0;
}
