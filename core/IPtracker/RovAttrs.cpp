#include <fstream>
#include <vector>
#include <iterator>
#include <bits/stdc++.h>
#include <string>
#include <boost/algorithm/string.hpp>
#include "RovAttrs.h"

ConfigParser::ConfigParser()
{
    this->delimeter = "=";
}

ConfigParser::ConfigParser(std::string filePath)
{
    this->delimeter = "=";
    this->filePath = filePath;
    ConfigParser(this->filePath, this->delimeter);
}

ConfigParser::ConfigParser(std::string filePath, std::string delimeter)
{
    this->delimeter = delimeter;
    this->filePath = filePath;
}

bool ConfigParser::parseFile()
{
    return this->parseFile(this->filePath);
}

bool ConfigParser::parseFile(std::string filePath)
{
    std::ifstream fileStream(filePath.c_str());
    std::string tmpLine;
    int lineCount = 1;
    int count = 0;

    if (!fileStream.is_open())
    {
        return false;
    }

    while (getline(fileStream, tmpLine))
    {
        if (tmpLine[0] == '#') // Ignore Line starting with # which are comments in the file
        {
            lineCount++;
            continue;
        }
        else
        {
            count++;
            this->parseAndAddToVector(tmpLine, count);
        }
        lineCount++;
    }
    return true;
}

void ConfigParser::parseAndAddToVector(std::string line, int count)
{
    std::vector<std::string> tmpVector1;

    boost::algorithm::split(tmpVector1, line, boost::is_any_of(this->delimeter));

    if (tmpVector1.size() < 1)
    {
        std::cout << "No ROV Mac Address in config.config";
    }
    else
    {
        if (tmpVector1[0] == "CM")
        {
            std::string value = tmpVector1[1];
            boost::trim(value);
            boost::algorithm::split(this->CM_mac, value, boost::is_any_of(","));
            for (int i = 0; i < this->CM_mac.size(); i++)
            {
                boost::trim(this->CM_mac[i]);
                std::cout << this->CM_mac[i] << std::endl;
            }
        }
        else if (tmpVector1[0] == "ROV")
        {
            std::string value = tmpVector1[1];
            boost::trim(value);
            boost::algorithm::split(this->tmpVector, value, boost::is_any_of(","));
            for (int i = 0; i < this->tmpVector.size(); i++)
            {
                boost::trim(this->tmpVector[i]);
                std::cout << this->tmpVector[i] << std::endl;
            }
        }
        else if (tmpVector1[0] == "PrimaryCamera")
        {
            std::string value = tmpVector1[1];
            boost::trim(value);
            boost::algorithm::split(this->primaryCamera, value, boost::is_any_of(","));
            for (int i = 0; i < this->primaryCamera.size(); i++)
            {
                boost::trim(this->primaryCamera[i]);
                std::cout << this->primaryCamera[i] << std::endl;
            }
        }
        else if (tmpVector1[0] == "SecondaryCamera")
        {
            std::string value = tmpVector1[1];
            boost::trim(value);
            boost::algorithm::split(this->secondaryCamera, value, boost::is_any_of(","));
            for (int i = 0; i < this->secondaryCamera.size(); i++)
            {
                boost::trim(this->secondaryCamera[i]);
                std::cout << this->secondaryCamera[i] << std::endl;
            }
        }
    }
}

RovAttrs::RovAttrs()
{
    this->pinged_Status = false;
}

std::vector<std::string> RovAttrs::GetIpAddresses(std::string cmd)
{
    const int max_buffer = 256;
    char buffer[max_buffer];
    std::vector<std::string> Ip_vector;
    FILE *pipe = popen(cmd.c_str(), "r");
    if (!pipe)
        throw std::runtime_error("popen() failed!");
    try
    {
        while (!feof(pipe))
            if (fgets(buffer, max_buffer, pipe) != NULL)
            {
                std::string output(buffer);
                output.erase(output.begin(), output.begin() + 1);
                output.erase(output.end() - 2, output.end());
                Ip_vector.push_back(output);
            }
    }
    catch (...)
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return Ip_vector;
}

bool RovAttrs::pingIpAddress(std::string ipAddress)
{
    if (ipAddress.empty())
        return false;
    std::string command = "ping " + ipAddress + " -c 1 -q | grep \"loss\"";
    std::string output = GetStdoutFromCommand(command);
    if (std::strstr(output.c_str(), "error"))
    {
        return false;
    }
    return true;
}

std::string RovAttrs ::getIPAddress(std::string macAddress)
{
    if (macAddress == "")
    {
        std::cout << "MAC Address is empty please check" << std::endl;
        return "";
    }
    else
    {

        // TO DO : Run ping for all IPs before arp -an command - ping -c 1
        // 192.168.0.X
        std::vector<std::string> IPAddresses;
        std::string getIPCommand =
            "arp -an | grep \"" + macAddress + "\" | cut -d ' ' -f 2";
        std::cout << "Get IP Command " << getIPCommand << std::endl;
        IPAddresses = GetIpAddresses(getIPCommand);
        if (IPAddresses.size() == 0)
        {
            std::cout << "No IP address found for the MAC address" << std::endl;
        }
        else
        {
            for (int i = 0; i < IPAddresses.size(); i++)
            {
                if (pingIpAddress(IPAddresses[i]))
                {
                    std::cout << "IP Address: " << IPAddresses[i] << std::endl;
                    return IPAddresses[i];
                }
            }
        }
    }
    return "";
}

bool RovAttrs::findCM(std::string CM_HWMMAC)
{
    std::string IPAddress = getIPAddress(CM_HWMMAC);
    int checkingCounter = 0;
    if (IPAddress == "")
    {
        if (pinged_Status == false)
        {
            pingNetwork();
            pinged_Status = true;
        }

        std::cout << "Waiting for ROV IP" << std::endl;
        while (IPAddress == "")
        {
            if (checkingCounter == 8)
            {
                std::cout << "[findROV][Debug] Dint find ROV exiting the process" << std::endl;
                break;
            }
            usleep(100000);
            IPAddress = getIPAddress(CM_HWMMAC);
            checkingCounter++;
        }
    }
    if (IPAddress != "")
    {
        std::cout << "Found IP without ping" << std::endl;
        this->CM_IP = IPAddress;
        this->CM_Mac = CM_HWMMAC;
        std::cout << "End of findCM" << std::endl;
        return true;
    }
    else
    {
        return false;
    }
}

bool RovAttrs::findROV(std::string ROV_HWMMAC)
{
    std::string IPAddress = getIPAddress(ROV_HWMMAC);
    int checkingCounter = 0;
    if (IPAddress == "")
    {
        if (pinged_Status == false)
        {
            pingNetwork();
            pinged_Status = true;
        }

        std::cout << "Waiting for ROV IP" << std::endl;
        while (IPAddress == "")
        {
            if (checkingCounter == 8)
            {
                std::cout << "[findROV][Debug] Dint find ROV exiting the process" << std::endl;
                break;
            }
            usleep(100000);
            IPAddress = getIPAddress(ROV_HWMMAC);
            checkingCounter++;
        }
    }
    if (IPAddress != "")
    {
        std::cout << "Found IP without ping" << std::endl;
        this->rovIP = IPAddress;
        this->rovMac = ROV_HWMMAC;
        std::cout << "End of findROV" << std::endl;

        return true;
    }
    else
    {
        return false;
    }
}

void RovAttrs::pingNetwork()
{

    std::string ping_cmd;

    std::string IPGateway = getGatewayDefault();
    if (IPGateway != "")
    {
        std::cout << "Pinging Network Hosts" << std::endl;

        // Clear all the arp cache before populating
        int ret = system("sudo ip -s -s neigh flush all >>/dev/null 2>>/dev/null");
        ping_cmd =
            "nmap -sP " + IPGateway + "/24 -n --send-ip >>/dev/null 2>>/dev/null";
        // std::cout << "Ping Network Command: " << ping_cmd << std::endl;
        ret = system(ping_cmd.c_str());
        // std::cout << "Arp Running" << std::endl;
        ret = system("arp -an  >>/dev/null 2>>/dev/null");
    }
}

std::string RovAttrs ::getGatewayDefault()
{
    std::string gatewayIP;
    // std::string gateway_cmd = "route | grep gateway  | grep 'UG[ \t]' | awk
    // '{print $2}'";
    std::string gateway_cmd = "ip route | awk '/default/ { print $3 }'";

    gatewayIP = removeNewLineTrails(GetStdoutFromCommand(gateway_cmd));
    std::cout << "Gateway: " << gatewayIP << std::endl;

    return gatewayIP;
}

std::string RovAttrs ::removeNewLineTrails(std::string line)
{
    line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
    return line;
}

std::string RovAttrs ::GetStdoutFromCommand(std::string cmd)
{
    std::string data;
    FILE *stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");
    if (stream)
    {
        while (!feof(stream))
            if (fgets(buffer, max_buffer, stream) != NULL)
                data.append(buffer);
        pclose(stream);
    }
    return data;
}

std::string RovAttrs::getROVMAC()
{
    return this->rovMac;
}

std::string RovAttrs::getROVIP()
{
    return this->rovIP;
}

std::string RovAttrs::getCM_MAC()
{
    return this->CM_Mac;
}

std::string RovAttrs::getCM_IP()
{
    return this->CM_IP;
}

bool RovAttrs::parseCMNetwork()
{
    bool status = false;
    this->pinged_Status = true;

    //std::cout<<"Vector Size: "<<this->tmpVector.size()<<std::endl;
    while ((status == false) && (!exitNetworkSearch))
    {
        for (int i = 0; i < this->CM_mac.size(); i++)
        {
            std::cout << "finding mac: " << this->CM_mac[i] << "........" << std::endl;
            status = findCM(this->CM_mac[i]);
            if (status)
            {
                break;
            }
        }
        this->pinged_Status = false;
        usleep(1000);
    }
    return status;
}
bool RovAttrs::parseROVNetwork()
{
    bool status = false;
    this->pinged_Status = true;

    //std::cout<<"Vector Size: "<<this->tmpVector.size()<<std::endl;
    while ((status == false) && (!exitNetworkSearch))
    {
        for (int i = 0; i < this->tmpVector.size(); i++)
        {
            std::cout << "finding mac: " << this->tmpVector[i] << "........" << std::endl;
            status = findROV(this->tmpVector[i]);
            if (status)
            {
                //std::cout << "3";
                this->ROV_index = i;
                //std::cout<<"mac: "<<this->tmpVector[i]<<" .....found"<<std::endl;
                break;
            }
            // else
            // {
            //     std::cout<<"mac: "<<this->tmpVector[i]" .....not found"<<std::endl;
            // }
        }
        this->pinged_Status = false;
        usleep(1000);
    }
    return status;
}

std::string RovAttrs::getPrimaryCamera()
{
    return this->primaryCamera[this->ROV_index];
}

std::string RovAttrs::getSecondaryCamera()
{
    return this->secondaryCamera[this->ROV_index];
}
