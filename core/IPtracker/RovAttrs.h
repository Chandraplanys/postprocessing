#ifndef ROVATTRS_H
#define ROVATTRS_H
#include <iostream>
#include <vector>

class ConfigParser
{
  private:
    std::string filePath;
    std::string delimeter;
    // std::unordered_map<std::string,std::string> parameters;

    void parseAndAddToVector(std::string line, int count);
    //void addToUMap(std::string key, std::string value);

  public:
    //std::unordered_map<std::string,std::string> parameters;
    std::vector<std::string> tmpVector;
    std::vector<std::string> primaryCamera;
    std::vector<std::string> secondaryCamera;
    std::vector<std::string> CM_mac;
    ConfigParser();
    ConfigParser(std::string filePath);
    ConfigParser(std::string filePath, std::string delimeter);

    bool parseFile();
    bool parseFile(std::string filePath);

    //    std::string getValue(std::string key);
};

class RovAttrs : public ConfigParser
{
  private:
    bool pinged_Status;

  protected:
  public:
    bool exitNetworkSearch = false;
    RovAttrs();
    //std::vector<std::string> rovMacs;
    std::string rovMac;
    std::string rovIP;
    std::string CM_Mac;
    std::string CM_IP;
    std::string GetIpAddress(std::string cmd);
    std::string getIPAddress(std::string macAddress);
    bool findCM(std::string ROV_HWMMAC);
    bool findROV(std::string ROV_HWMMAC);
    std::string getCM_MAC();
    std::string getCM_IP();
    void pingNetwork();
    std::string getGatewayDefault();
    std::string GetStdoutFromCommand(std::string cmd);
    std::string removeNewLineTrails(std::string line);
    std::string resolveIpAddress(std::string cmd);
    std::string getROVMAC();
    std::string getROVIP();
    std::string getPrimaryCamera();
    std::string getSecondaryCamera();
    std::vector<std::string> GetIpAddresses(std::string cmd);
    bool pingIpAddress(std::string ipAddress);
    bool parseCMNetwork();
    bool parseROVNetwork();
    int CM_index;
    int ROV_index;
};

#endif
