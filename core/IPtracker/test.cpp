#include"RovAttrs.h"
#include <iostream>
#include <string>
RovAttrs rovfind;
int main ()
{
    rovfind.parseFile("ROV_Cams_MACs.config");
    std::cout<<"Waiting to get IP of ROV"<<std::endl;
	bool status = rovfind.parseNetwork();
    if(status)
	{
        std::cout<<"IP of ROV found"<<std::endl;
        //std::string MAC = rovfind.getROVMAC();
        std::string IP = rovfind.getROVIP();
        if(IP == "")
        {
            std::cout<<"[main] MAC found but no IP attached with it"<<std::endl;
            return 1;
        }
    }
}