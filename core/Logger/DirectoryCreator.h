#ifndef SUPPORT_H
#define SUPPORT_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <signal.h>
#include <sstream>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

class DirectoryCreator
{

private:
public:
  /* global variables to make file name */
  char curr_time[80];
  char curr_file_bottom[100];
  char curr_file_front[100];

  /* global variables to make folder name */
  char ret_folder_path[512];
  char curr_date[80];
  char sub_folder_path_name[512];
  char final_folder_path[512];
  char current_folder_path[512];

  char glb_sub_folder_path_name[512];
  char path[512];

  DirectoryCreator();
  ~DirectoryCreator();

  /*  an utility function to replace a char from ':' to '_' */
  char *char_replace(char *ch);

  /* return system current time hh:mm:ss */
  char *currentTime();

  /* an utility function to get folder path without '\n' character */
  char *remove_nextLine(char *temp);

  /*  get current date, format is DD_MM_YYYY */
  char *currentDate();

  /* analyse the current working directory command and return the home and
   * current user directory */
  void popen_parser(char popen_buff[]);

  /* make sub folders range from 1 .. n  */
  void makeSubFolder();

  /* make a new main directory based on system date if directory not exist */
  void makeDateFolder(std::string deviceName);

  /* call fucntions which make files names and folders , returns
   * global_subfolder_path */
  std::string folder_file_maker(std::string deviceName);
};

#endif
