// logger.cpp

#include "Logger.h"

Logger::Logger()
{
}
Logger::~Logger()
{
    std::cout << "Shutting down Logger" << std::endl;

    debugAppender = NULL;
    debugLogger = NULL;

    delete debugAppender;

    delete debugLogger;

    log4cpp::Category::shutdown();
}
void Logger::initialize(std::string pathTologgerFile, std::string pathToProperties, std::string name)
{

    std::string tmpAppenderName = name + "appender";
    log4cpp::PropertyConfigurator::configure(pathToProperties);
    log4cpp::Category &log =
        log4cpp::Category::getInstance(string(tmpAppenderName));
    debugLogger = &log;

    std::string fileName = pathTologgerFile + "/Data.log";

    log4cpp::Appender *appender =
        new log4cpp::FileAppender("default", fileName);

    log4cpp::PatternLayout *Layout = new log4cpp::PatternLayout();
    Layout->setConversionPattern("%d [%p] %m%n");
    appender->setLayout(Layout);

    log.addAppender(appender);
}

void Logger::initialize(std::string deviceName, std::string pathToProperties)
{
    std::string directoryName = this->logDirectory.folder_file_maker(deviceName);
    std::string date = this->currentDate();
    std::string time_c = this->currentTime();
    std::string debugFile = directoryName + "/" + deviceName + "_" + date + "_" + time_c + ".log";

    // //Raw Logger Categorey Setup
    log4cpp::PropertyConfigurator::configure(pathToProperties);

    //Debug Appender Setup
    std::string tmpAppenderName_Debug = "Debug" + deviceName + "appender";
    log4cpp::Category &debugLoggerCategory =
        log4cpp::Category::getInstance(std::string(tmpAppenderName_Debug));
    debugLogger = &debugLoggerCategory;

    debugAppender =
        new log4cpp::FileAppender("default", debugFile);

    //Appender Layout for logging
    log4cpp::PatternLayout *Layout = new log4cpp::PatternLayout();
    Layout->setConversionPattern("%d [%p] %m%n");
    debugAppender->setLayout(Layout);

    //Appender Thresholds
    debugAppender->setThreshold(700);

    debugLoggerCategory.addAppender(debugAppender);
}

void Logger::exitLogger()
{
    try
    {
        std::cout << "1" << std::endl;
        // rawLogger->removeAllAppenders();
        std::cout << "2" << std::endl;
        debugLogger->removeAllAppenders();
        std::cout << "3" << std::endl;
    }
    catch (const std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}
/*
 	PriorityLevel { 
  EMERG = 0, 
  FATAL = 0,
  ALERT = 100,
  CRIT = 200, 
  ERROR = 300,
  WARN = 400, 
  NOTICE = 500, 
  INFO = 600, 
  DEBUG = 700, 
  NOTSET = 800 
}*/

void Logger::debug(std::string msg) { LOG4CPP_DEBUG_S((*debugLogger)) << msg; }
void Logger::info(std::string msg) { LOG4CPP_INFO_S((*debugLogger)) << msg; }
void Logger::warn(std::string msg) { LOG4CPP_WARN_S((*debugLogger)) << msg; }
void Logger::data(std::string msg) { LOG4CPP_ALERT_S((*debugLogger)) << msg; }
void Logger::error(std::string msg) { LOG4CPP_ERROR_S((*debugLogger)) << msg; }
void Logger::fatal(std::string msg) { LOG4CPP_FATAL_S((*debugLogger)) << msg; }

/*  get current date, format is DD_MM_YYYY */
std::string Logger::currentDate()
{
    time_t now = time(0);
    char curr_date[80];
    struct tm tstruct;
    tstruct = *localtime(&now);
    strftime(curr_date, sizeof(curr_date), "%d_%m_%Y", &tstruct);
    return std::string(curr_date);
}

/* return system current time hh:mm:ss */
std::string Logger::currentTime()
{
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    std::string _time;
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);
    _time = std::string(buf);
    std::replace(_time.begin(), _time.end(), ':', '_');

    return _time;
}
