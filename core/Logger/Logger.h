#ifndef _sub_loggerH
#define _sub_loggerH

#include "log4cpp/Appender.hh"
#include "log4cpp/Category.hh"
#include "log4cpp/CategoryStream.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/PatternLayout.hh"
#include "log4cpp/PropertyConfigurator.hh"

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>

#include "DirectoryCreator.h"

using namespace log4cpp;
using namespace std;

class Logger
{
public:
  Logger();
  ~Logger();
  DirectoryCreator logDirectory;
  // std::string appender_name, rawAppender, debugAppender;
  void initialize(std::string pathToLoggerFile, std::string pathToProperties, std::string name);
  void initialize(std::string name, std::string pathToProperties);
  void exitLogger();

  log4cpp::Category *debugLogger;

  log4cpp::Appender *debugAppender;

  void debug(std::string msg);
  void data(std::string msg);
  void info(std::string msg);
  void warn(std::string msg);
  void error(std::string msg);
  void fatal(std::string msg);
  std::string currentDate();
  std::string currentTime();
};

#endif /*_loggerH*/
