#include "Logger.h"
#include "DirectoryCreator.h"
#include <unistd.h>
#include <iostream>

bool stopVisionBool = true;
void closeProgram()
{
    std::cout << "[VideoServer][StopVision]" << std::endl;

    stopVisionBool = false;
}

void exitProgram()
{
    exit(0);
}
void recv_signal(int s)
{

    std::cout << "[Info][Vision][Signal Handler] Signal Recieved is " << s
              << std::endl;
    if (s == SIGPIPE)
    {
        // Just ignore, sock class will close the client the next time
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT || s == SIGSEGV || s == SIGABRT ||
        s == SIGKILL || s == SIGTERM || s == SIGHUP)
    {

        closeProgram();
    }
    if (s == SIGTSTP)
    {
        exitProgram();
    }
}

void initSigHandler()
{

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = recv_signal;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGTSTP, &sigIntHandler, NULL);
    sigaction(SIGPIPE, &sigIntHandler, NULL);
    sigaction(SIGABRT, &sigIntHandler, NULL);
    sigaction(SIGSEGV, &sigIntHandler, NULL);
    sigaction(SIGKILL, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);
    sigaction(SIGHUP, &sigIntHandler, NULL);
}

int main(int argc, char const *argv[])
{
    initSigHandler();
    Logger testLogger;
    testLogger.initialize("/home/venky/Videos/Debug_Logs/14_11_2018/test/2", "Logger.properties",
                          "test");
    int i = 0;
    while (stopVisionBool)
    {
        std::cout << "Test" << std::endl;
        // testLogger.warn("test " + std::to_string(i));
        testLogger.data("test " + std::to_string(i));
        i++;
        usleep(1000000);
    }
    // testLogger.exitLogger();
    // std::cout << "Exit 2" << std::endl;
    usleep(1000000);

    std::cout << "Exit" << std::endl;

    return 0;
}
