#include "DbUtils.h"
#include <postgresql/libpq-fe.h>
#include <stdlib.h>
#include "../../SupportLibs/JsonParser/jsonParser.h"
#include <string>
#include <unordered_set>
#include <stdarg.h>
#include <pthread.h>
#include "../../WebSocketCPP/client_ws.hpp"
#include "../../IPtracker/RovAttrs.h"

using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;
using WsConnect = WsClient::Connection;
static std::unordered_set<std::string> msgSet;
static std::unordered_set<std::string> videoSet;
class Database : public DbUtils
{
private:
  std::string rovIP;
  std::string port;
  std::string endpoint;
  std::string configFilePath;

public:
  Database();
  ~Database();
  RovAttrs ROVNetwork;
  bool dataHandler_Flag;
  WsClient *wsDbClient;
  std::shared_ptr<WsClient::Connection> connection;
  void createDiveTable();
  void InitializeThreads();
  static void *databaseClient(void *arguments);
  static void *databaseUpdate(void *arguments);
  std::string runCMD(std::string cmd);
  void DatabaseExecution(std::string videoPath, std::string UUID,std::string status);
  std::pair<double, double> logInformation(std::string);
  std::string parseLine(std::string line);
  std::pair<std::string, std::string> videoInformation(std::string);
  std::string getUTC();
};