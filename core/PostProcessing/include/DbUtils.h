#include <stdio.h>
#include <string>
#include <postgresql/libpq-fe.h>
#include <stdlib.h>
#include "../../SupportLibs/JsonParser/jsonParser.h"
#include <vector>
#include <stdarg.h>
#include <pthread.h>
#include <queue>
#include "../../WebSocketCPP/client_ws.hpp"

class DbUtils : public JsonParser
{
private:
  std::string userName;
  std::string databaseName;
  std::string password;

public:
  pthread_t databaseClient_Thread;
  pthread_t databaseUpdate_Thread;
  PGconn *conn;
  char curr_date[80];
  DbUtils();
  ~DbUtils();
  int getDbVersion();
  int getServerVersion();
  PGconn *dbConnect();
  bool createTable(std::string tablename, std::vector<std::string> &columnName);
  bool checkTableExits(std::string tablename);
  void insertRow(const char *tableName, ...);
  bool checkDbConnection();
  void deleteTable(std::string tablename);
  std::string getSchemaName();
  std::string getDb();
  void insertColumns(const char *tableName, ...);
  void deleteColumns(const char *tableName, const char *columnName);
  void execute(const char *query);
  void jsonToDb(std::string tablename, std::string, std::string ,int, int, std::pair<std::string, std::string>, std::pair<double, double>, std::string, std::string, std::string, std::string, std::string, std::string);
  void snapshot(std::string tablename);
  void do_exit();
  std::string randomStringGenerator();
  char *currentDate();
  int getSQLvalue(std::string Query);
};