#include <string>


class IPUtils
{
  private:
  public:
    bool stopBool = false;
    // std::string commandModule_HMAC = "94:c6:91:1b:8e:9a";
    std::string commandModule_HMAC;
    std::string commandModule_IP = "";
    IPUtils();
    ~IPUtils();
    std::string GetStdoutFromCommand(std::string cmd);
    std::string getIPAddress(std::string macAddress);
    std::string removeNewLineTrails(std::string line);
    std::string getGatewayDefault();
    void pingNetwork();
    bool findCommandModule(std::string commandModule_HMAC);
};
