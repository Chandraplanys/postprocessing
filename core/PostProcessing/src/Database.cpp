#include "Database.h"
#include <thread>
#include <boost/algorithm/string.hpp>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

std::map<std::string, int> months{
    {"Jan", 1},
    {"Feb", 2},
    {"Mar", 3},
    {"Apr", 4},
    {"May", 5},
    {"Jun", 6},
    {"Jul", 7},
    {"Aug", 8},
    {"Sep", 9},
    {"Oct", 10},
    {"Nov", 11},
    {"Dec", 12}};
std::string day = "";
Database::Database()
{
    this->dataHandler_Flag = true;
    rovIP = "";
    port = "8090";
    endpoint = "broadcast";
    configFilePath = "../IPtracker/MAC_Addresses.config";
}

Database::~Database()
{
}

void Database::createDiveTable()
{
    PGresult *res = PQexec(conn, "create table IF NOT EXISTS Dives(id SERIAL PRIMARY KEY,"
                                 "uniqueId VARCHAR(20) NOT NULL,dayId INTEGER,jobId INTEGER,reportId INTEGER,"
                                 "startTime TIME NOT NULL,endTime TIME NOT NULL,startDepth VARCHAR(20),endDepth VARCHAR(20),"
                                 "freeBoard VARCHAR(20),date DATE,comments VARCHAR(50),tidalLevel VARCHAR(20),"
                                 "locationLatitude VARCHAR(20),loationLongitude VARCHAR(20),locationdetails VARCHAR(20),"
                                 "locationX VARCHAR(20),locationY VARCHAR(20),locationZ VARCHAR(20),"
                                 "videoLocation VARCHAR(100),videoExtension VARCHAR(20),logLocation VARCHAR(100),"
                                 "logExtension VARCHAR(20),processedVideoLocation VARCHAR(100),"
                                 "processedVideoExtension VARCHAR(20),tag VARCHAR(20),"
                                 "active BOOLEAN,createdAt DATE,updatedAt DATE,deletedAt DATE)");
    if (PQresultStatus(res) == PGRES_COMMAND_OK)
    {
        PQclear(res);
    }
}

void Database::InitializeThreads()
{
    std::cout << "Creating Database WS client Thread" << std::endl;
    pthread_create(&databaseClient_Thread, NULL, databaseClient, this);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Creating Database update Thread" << std::endl;
    pthread_create(&databaseUpdate_Thread, NULL, databaseUpdate, this);
}

void *Database::databaseClient(void *arguments)
{

    Database *Database_Instance = (Database *)arguments;
    if (!Database_Instance->ROVNetwork.parseFile(Database_Instance->configFilePath))
    {
        std::cout << "[Error][databaseClient] Unable to parse \"" << Database_Instance->configFilePath << "\"" << std::endl;
        std::cout << "Exiting databaseClient thread...." << std::endl;
        pthread_exit(NULL);
    }
    while (Database_Instance->dataHandler_Flag)
    {
        bool status = Database_Instance->ROVNetwork.parseROVNetwork();
        if (status)
        {
            Database_Instance->rovIP = Database_Instance->ROVNetwork.getROVIP();
        }
        else
        {
            std::cout << "[Error][databaseClient] NO ROV IP Found" << std::endl;
            std::cout << "Exiting databaseClient thread...." << std::endl;
            pthread_exit(NULL);
        }
        WsClient databaseClientObject(Database_Instance->rovIP + ":" + Database_Instance->port + "/" + Database_Instance->endpoint);
        Database_Instance->wsDbClient = &databaseClientObject;
        Database_Instance->wsDbClient->on_message = [Database_Instance](std::shared_ptr<WsClient::Connection> conn_local, std::shared_ptr<WsClient::Message> message) {
            Database_Instance->connection = conn_local;
            std::string msg = message->string();

            std::istringstream f(msg);
            std::string videopath_stream;
            int count = 1;
            while (getline(f, videopath_stream))
            {
                if (videopath_stream.empty())
                {
                    count++;
                    continue;
                }
                if (count == 1)
                {
                    boost::trim(videopath_stream);
                    msgSet.insert(videopath_stream);
                }
                if (count == 2)
                {
                    boost::trim(videopath_stream);
                    videoSet.insert(videopath_stream);
                }
                count++;
            }
        };

        Database_Instance->wsDbClient->on_open = [](std::shared_ptr<WsClient::Connection> connection) {
            std::cout << "Client: Opened connection" << std::endl;
            std::string message = "Hello";
            auto send_stream = std::make_shared<WsClient::SendStream>();
            *send_stream << message;
            connection->send(send_stream);
        };

        Database_Instance->wsDbClient->on_close = [](std::shared_ptr<WsClient::Connection> /*connection*/, int status, const std::string & /*reason*/) {
            std::cout << "Client: Closed connection with status code " << status << std::endl;
        };

        // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
        Database_Instance->wsDbClient->on_error = [](std::shared_ptr<WsClient::Connection> connection, const SimpleWeb::error_code &ec) {
            std::cout << "Client: Error: " << ec << ", error message: " << ec.message() << std::endl;
            connection->send_close(1000);
        };
        Database_Instance->wsDbClient->start();
    }
    std::cout << "Exiting databaseClient thread...." << std::endl;
    pthread_exit(NULL);
}
void *Database::databaseUpdate(void *arguments)
{
    Database *Database_Update = (Database *)arguments;
    Database_Update->conn = Database_Update->dbConnect();
    std::string prevPath = "";
    std::string UUID = "";
    while (Database_Update->dataHandler_Flag)
    {
        std::string videoPath = "";
        std::string logPath = "";
        std::pair<double, double> logINFO;
        std::pair<std::string, std::string> videoInfo;
        std::string logExtension = "";
        std::string videoExtension = "";
        if (!videoSet.empty())
        {
            std::unordered_set<std::string>::iterator it = videoSet.begin();
            Database_Update->d.Parse((*it).c_str());
            videoPath = Database_Update->getvalue("video_path");
            if (videoPath != "" && UUID == "")
            {
                boost::uuids::uuid uuid = boost::uuids::random_generator()();
                UUID = boost::lexical_cast<std::string>(uuid);
                Database_Update->DatabaseExecution(videoPath, UUID, "NOTREADY");
                prevPath = videoPath;
            }
            const bool is_in = videoSet.find("{\"video_path\":\"\"}") != videoSet.end();
            if (is_in || videoSet.size() >= 2)
            {
                if (UUID != "")
                {
                    Database_Update->DatabaseExecution(prevPath, UUID, "TODO");
                }
                videoSet.erase("{\"video_path\":\"" + prevPath + "\"}");
                UUID = "";
                prevPath = "";
            }
        }
        if (!msgSet.empty())
        {
            std::unordered_set<std::string>::iterator it = msgSet.begin();
            std::string data = *it;
            Database_Update->FullMsg.Parse((*it).c_str());
            rapidjson::Value &results = Database_Update->FullMsg["state_variable"];
            std::string state_variable = Database_Update->GetJsonString(results);
            // std::cout << "state_variable: " << state_variable << std::endl;
            Database_Update->d.Parse(state_variable.c_str());
            std::string screenshot_path = Database_Update->getvalue("screenshot_path");
            Database_Update->FullMsg.Parse((*it).c_str());
            results = Database_Update->FullMsg["sensor_data"];
            std::string sensor_data = Database_Update->GetJsonString(results);
            Database_Update->d.Parse(sensor_data.c_str());
            if (screenshot_path != "")
            {
                if (Database_Update->checkDbConnection())
                {
                    Database_Update->snapshot("SensorSnapshots");
                    int snapshotId = Database_Update->getSQLvalue("SELECT id FROM \"SensorSnapshots\" ORDER BY id DESC LIMIT 1");
                    boost::uuids::uuid s = boost::uuids::random_generator()();
                    const std::string uuid = boost::lexical_cast<std::string>(s);
                    std::string date = Database_Update->currentDate();
                    int reportId = Database_Update->getSQLvalue("SELECT \"reportId\" FROM \"StateVariables\" where id = 1");
                    int jobId = Database_Update->getSQLvalue("SELECT \"jobId\" FROM \"StateVariables\" where id = 1");
                    std::string screenshot = std::string(std::strstr(screenshot_path.c_str(), "Pictures"));
                    screenshot.erase(screenshot.length() - 4);
                    std::string query = "INSERT INTO \"PreObservations\"(\"dataProcessingStatus\",\"date\",\"uuid\",\"fileExtension\",\"active\",\"createdAt\",\"updatedAt\",\"sensorId\",\"fileLocation\",\"timestamp\",\"jobId\",\"reportId\") VALUES(\'TODO\',\'" + date + "\',\'" + uuid + "\',\'.jpg\',\'TRUE\',\'" + date + "\',\'" + date + "\'," + std::to_string(snapshotId) + "," + "\'" + screenshot + "\',\'" + Database_Update->getUTC() + "\'," + std::to_string(jobId) + "," + std::to_string(reportId) + ")";
                    std::cout << query << std::endl;
                    Database_Update->execute(query.c_str());
                }
                else
                {
                    std::cout << "********************* DATABASE CONNECTION ERROR ***************************" << std::endl;
                }
            }
            msgSet.erase(data);
        }
        usleep(20000);
    }
    std::cout << "Database Update Thread Exiting" << std::endl;
    pthread_exit(NULL);
}

void Database::DatabaseExecution(std::string videoPath, std::string UUID, std::string status)
{
    std::string logExtension = "", videoExtension = "", logLocation = "", videoLocation = "", getCommand = "";
    std::pair<double, double> logINFO(0, 0);
    std::pair<std::string, std::string> videoInfo("", "");
    if (videoPath.empty() || UUID.empty())
        return;
    clock_t t = clock();
    while (logLocation == "" && (double)(clock() - t) / CLOCKS_PER_SEC < 5.0)
    {
        getCommand = "find " + videoPath + " -name Data.log";
        logLocation = runCMD(getCommand);
        boost::trim(logLocation);
        usleep(1000000);
    }
    if (!logLocation.empty())
    {
        logExtension = ".log";
        logINFO = logInformation(logLocation);
        std::cout << "logStart: " << logINFO.first << std::endl;
        std::cout << "logEnd: " << logINFO.second << std::endl;
    }
    else
    {
        std::cerr << "Data.log is missing in " << videoPath << std::endl;
        // return;
    }

    getCommand = "find " + videoPath + " -name bottom*";
    videoLocation = runCMD(getCommand);
    boost::trim(videoLocation);
    if (!videoLocation.empty())
    {
        std::cout << "videoPath: " << videoLocation << std::endl;

        videoInfo = videoInformation(videoLocation);
        std::cout << "videoStart: " << videoInfo.first << std::endl;
        std::cout << "videoEnd: " << videoInfo.second << std::endl;

        videoExtension = videoLocation.substr(videoLocation.find_last_of("."));
        std::cout << "videoExtension:" << videoExtension << std::endl;
    }
    else
    {
        std::cerr << "Front Camera Video is missing in " << videoPath << std::endl;
        if (status == "TODO")
            return;
    }
    try
    {
        boost::iterator_range<std::string::iterator> r = boost::find_nth(videoPath, "/", 2);
        size_t dis = (distance(videoPath.begin(), r.begin()));
        if (checkDbConnection())
        {
            std::cout << "Connection establised" << std::endl;
            int reportId = getSQLvalue("SELECT \"reportId\" FROM \"StateVariables\" where id = 1");
            int jobId = getSQLvalue("SELECT \"jobId\" FROM \"StateVariables\" where id = 1");
            std::string utcTime = getUTC();
            boost::trim(videoPath);
            boost::trim(logLocation);
            videoLocation.erase(videoLocation.length() - 4);
            logLocation.erase(logLocation.length() - 4);
            jsonToDb("Dives", UUID, status, reportId, jobId, videoInfo, logINFO, day, videoLocation.substr(dis + 1, videoLocation.length() - 3), logLocation.substr(dis + 1, logLocation.length() - 3), videoExtension, logExtension, utcTime);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
    }
    return;
}

std::string Database::runCMD(std::string cmd)
{
    char buffer[1024];
    std::string result = "";

    // if (!fp) throw std::runtime_error("popen() failed!");
    FILE *fp = popen(cmd.c_str(), "r");
    if (!fp)
        throw std::runtime_error("popen() failed!");

    try
    {
        while (fgets(buffer, sizeof buffer, fp) != NULL)
        {
            result += buffer;
        }
    }
    catch (...)
    {
        pclose(fp);
        throw;
    }

    pclose(fp);
    return result;
}

std::pair<double, double> Database::logInformation(std::string logPath)
{
    std::ifstream fin(logPath);
    std::string line = "", firstLine = "", lastLine = "";
    int i = 1;
    //getting first and last line of Data.log
    while (std::getline(fin, line))
    {
        if (i == 2)
        {

            firstLine = line;
        }
        i++;
        lastLine = line;
    }
    double startDepth = 0.0, endDepth = 0.0;
    //getting start Depth from first Line
    try
    {
        startDepth = stod(parseLine(firstLine));
        //getting end Depth from Last Line
        endDepth = stod(parseLine(lastLine));
    }
    catch (std::exception e)
    {
        std::cerr << "[Database][logInformation] " << e.what() << std::endl;
    }

    return std::make_pair(startDepth, endDepth);
}

std::string Database::parseLine(std::string line)
{
    std::vector<std::string> ValuesWithSpace;
    std::vector<std::string> ValuesWithComma;

    std::stringstream FirstLineStream(line);
    std::string spaceSegments = "";
    while (std::getline(FirstLineStream, spaceSegments, ' '))
    {
        ValuesWithSpace.push_back(spaceSegments);
    }
    std::stringstream depthStream(ValuesWithSpace.back());
    std::string commaSegments = "";
    while (std::getline(depthStream, commaSegments, ','))
    {
        //std::cout<<commaSegments<<std::endl;
        ValuesWithComma.push_back(commaSegments);
    }
    boost::trim(ValuesWithComma[2]);
    return ValuesWithComma[2];
}

std::pair<std::string, std::string> Database::videoInformation(std::string path)
{
    std::string videoName = path.substr(path.find_last_of("/") + 1);
    std::vector<std::string> vec;
    if (videoName.find("bottom") != std::string::npos)
    {
        videoName = videoName.substr(videoName.find_first_of("_") + 1);
        videoName = videoName.erase(8, 4);
        std::stringstream ss(videoName);
        while (ss.good())
        {
            std::string substr;
            getline(ss, substr, '_');
            vec.push_back(substr);
        }
    }
    if (vec.empty())
        return std::make_pair("0", "0");
    std::string startTime = vec[0] + ":" + vec[1] + ":" + vec[2];
    std::cout << "start: " << startTime << std::endl;
    std::string output = runCMD("date -r " + path);
    std::vector<std::string> spaceSegments;
    if (output.find("No such") == std::string::npos)
    {

        std::stringstream outputStream(output);
        std::string spaceWords = "";
        while (std::getline(outputStream, spaceWords, ' '))
        {
            if (!spaceWords.empty())
                spaceSegments.push_back(spaceWords);
        }
    }
    boost::trim(spaceSegments[3]);
    std::string endTime = spaceSegments[3];
    std::cout << "end: " << endTime << std::endl;
    boost::trim(spaceSegments[1]);
    const auto iter = months.find(spaceSegments[1]);
    int month = 0;
    if (iter != months.cend())
    {
        month = iter->second;
    }
    //std::cout << "end: ww" << std::endl;
    boost::trim(spaceSegments[2]);
    //std::cout << "end:qqq ww" << std::endl;
    boost::trim(spaceSegments[4]);
    //std::cout << "end: eeww" << std::endl;
    day = spaceSegments[2] + "-" + std::to_string(month) + "-" + spaceSegments[5];
    std::cout << "Day: " << day << std::endl;
    return std::make_pair(startTime, endTime);
}

std::string Database::getUTC()
{
    std::string output = runCMD("date");
    std::vector<std::string> spaceSegments;
    if (output.find("No such") == std::string::npos)
    {

        std::stringstream outputStream(output);
        std::string spaceWords = "";
        while (std::getline(outputStream, spaceWords, ' '))
        {
            // std::cout<<spaceWords<<std::endl;
            if (!spaceWords.empty())
                spaceSegments.push_back(spaceWords);
        }
    }
    boost::trim(spaceSegments[3]);
    std::string Time = spaceSegments[3];
    //std::cout<<"end: "<<endTime<<std::endl;
    boost::trim(spaceSegments[1]);
    const auto iter = months.find(spaceSegments[1]);
    int month = 0;
    if (iter != months.cend())
    {
        month = iter->second;
    }

    boost::trim(spaceSegments[2]);
    boost::trim(spaceSegments[5]);
    day = spaceSegments[5] + "-" + std::to_string(month) + "-" + spaceSegments[2];
    // std::cout<<"Day: "<<day<<std::endl;
    // std::cout<<"Time: "<<Time<<std::endl;
    return day + " " + Time;
}
