#include "../include/DbUtils.h"
#include <iostream>
#include <vector>
#include <stdarg.h>
#include <string>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>
//#include "../include/uuid.hpp"
//using namespace MathUtils;

DbUtils::DbUtils()
{
    userName = "postgres";
    databaseName = "webdesk_development";
    password = "ch4pn";
}

DbUtils::~DbUtils()
{
    if (checkDbConnection())
    {
        do_exit();
    }
}

int DbUtils::getDbVersion()
{
    return PQlibVersion();
}

int DbUtils::getServerVersion()
{
    return PQserverVersion(conn);
    ;
}

PGconn *DbUtils::dbConnect()
{
    std::string query = "user=" + userName + " password=" + password + " dbname=" + databaseName;
    PGconn *connection = PQconnectdb(query.c_str());

    if (PQstatus(connection) == CONNECTION_BAD)
    {

        fprintf(stderr, "Connection to database failed: %s\n",
                PQerrorMessage(connection));
        do_exit();
    }
    return connection;
}

bool DbUtils::checkDbConnection()
{
    return PQstatus(conn) == CONNECTION_OK;
}

bool DbUtils::createTable(std::string tablename, std::vector<std::string> &columnName)
{
    std::string query = "CREATE TABLE IF NOT EXISTS " + tablename + "(";
    for (int i = 0; i < columnName.size(); i++)
    {
        query += columnName[i];
        if (i != columnName.size() - 1)
            query += ",";
        else
            query += ")";
    }
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) == PGRES_COMMAND_OK)
    {
        PQclear(res);
        return true;
    }
    std::cerr << "Unable to create table" << std::endl;
    PQclear(res);
    do_exit();
    return false;
}

bool DbUtils::checkTableExits(std::string table)
{
    std::string query = " SELECT EXISTS ( SELECT 1 FROM information_schema.tables \
	                    WHERE table_schema = \'" +
                        getSchemaName() + "\' \
	                     AND table_name = \'" +
                        table + "\' \
                         )";
    PGresult *res = PQexec(conn, query.c_str());
    std::cout << query << " " << PQresultStatus(res) << " " << PQgetvalue(res, 0, 0) << std::endl;
    if (PQresultStatus(res) == PGRES_TUPLES_OK)
    {
        if (PQgetvalue(res, 0, 0))
        {
            PQclear(res);
            return true;
        }
    }
    PQclear(res);
    PQfinish(conn);
    return false;
}

void DbUtils::insertRow(const char *tableName, ...)
{
    std::string query = "INSERT INTO " + (std::string)tableName;
    va_list args;
    va_start(args, tableName);
    const char *val = va_arg(args, const char *);
    while (val)
    {
        query += " " + (std::string)val;
        val = va_arg(args, const char *);
    }
    va_end(args);
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        PQfinish(conn);
    }
    PQclear(res);
    return;
}

std::string DbUtils::getDb()
{
    PGresult *res = PQexec(conn, "SELECT current_database()");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        std::cerr << "No Schema Found" << std::endl;
        PQclear(res);
        do_exit();
    }
    PQclear(res);
    return PQgetvalue(res, 0, 0);
}

void DbUtils::deleteTable(std::string tablename)
{
    std::string query = "DROP TABLE IF EXISTS " + tablename;
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) == PGRES_COMMAND_OK)
    {
        PQclear(res);
        return;
    }
    std::cerr << "Unable to Delete table" << std::endl;
    PQclear(res);
    do_exit();
}

void DbUtils::insertColumns(const char *tableName, ...)
{
    va_list args;
    va_start(args, tableName);
    const char *columns = va_arg(args, const char *);
    std::cout << columns << std::endl;
    while (columns)
    {
        std::string query = "ALTER TABLE " + (std::string)tableName;
        query += " ADD COLUMN " + (std::string)columns;
        PGresult *res = PQexec(conn, query.c_str());
        if (PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            PQfinish(conn);
        }
        PQclear(res);
        columns = va_arg(args, const char *);
    }
    va_end(args);
    return;
}

void DbUtils::deleteColumns(const char *tableName, const char *columnName)
{
    std::string query = "ALTER TABLE " + (std::string)tableName + " DROP COLUMN " + (std::string)columnName;
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        PQfinish(conn);
    }
    PQclear(res);
}

std::string DbUtils::randomStringGenerator()
{
    srand(time(NULL));
    static const char alphanum[] =
        "0123456789"
        "!@#$^&*"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    std::string randomStr;
    for (unsigned int i = 0; i < 19; ++i)
    {
        randomStr += alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    return randomStr;
}
char *DbUtils::currentDate()
{

    time_t now = time(0);
    struct tm tstruct;

    tstruct = *localtime(&now);
    strftime(curr_date, sizeof(curr_date), "%Y/%m/%d", &tstruct);

    return curr_date;
}

std::string DbUtils::getSchemaName()
{
    if (!checkDbConnection())
        do_exit();
    std::string query = "SELECT current_schema()";
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        std::cerr << "No Schema Found" << std::endl;
        PQclear(res);
        do_exit();
    }
    return PQgetvalue(res, 0, 0);
}

void DbUtils::execute(const char *query)
{
    PGresult *res = PQexec(conn, query);
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        PQfinish(conn);
    }
    PQclear(res);
}

void DbUtils::jsonToDb(std::string tablename, std::string s,std::string status , int reportId, int jobId, std::pair<std::string, std::string> video, std::pair<double, double> log, std::string day, std::string vPath, std::string lPath, std::string extVideo, std::string extLog, std::string utc)
{
    std::string query = "select column_name from information_schema.columns \
                        where table_schema = \'" +
                        getSchemaName() + "\'" +
                        " and table_name = \'" + tablename + "\'";
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        std::cerr << "Schema doesn't exist" << std::endl;
        PQclear(res);
        do_exit();
    }
    int rows = PQntuples(res);
    std::vector<std::string> values;
    query = "INSERT INTO \"" + tablename + "\" (\"reportId\", \"jobId\", \"startTime\", \"endTime\", \"startDepth\", \"endDepth\", \"videoLocation\", \"videoExtension\", \"logLocation\", \"logExtension\", \"date\", \"uuid\", \"createdAt\", \"updatedAt\", \"active\", \"videoProcessingStatus\") VALUES (";
    query += std::to_string(reportId) + "," + std::to_string(jobId) + "," +
             "\'" + video.first + "\'" + "," +
             "\'" + video.second + "\'" + "," +
             std::to_string(log.first) + "," +
             std::to_string(log.second) + "," +
             "\'" + vPath + "\'" + "," +
             "\'" + extVideo + "\'" + "," +
             "\'" + lPath + "\'" + "," +
             "\'" + extLog + "\'" + "," +
             "\'" + day + "\'" + "," +
             "\'" + s + "\'" + "," +
             "\'" + utc + "\'" + "," +
             "\'" + utc + "\'" + "," +
             "\'" + "TRUE" + "\'" + "," +
             "\'" + status + "\'" +
             ");";
    std::cout << query << std::endl;
    PQclear(res);
    res = PQexec(conn, query.c_str());

    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        PQclear(res);
        do_exit();
    }
    std::cout << "Inserted into Database" << std::endl;
    PQclear(res);
}

void DbUtils::snapshot(std::string tablename)
{
    std::string query = "select column_name from information_schema.columns \
                        where table_schema = \'" +
                        getSchemaName() + "\'" +
                        " and table_name = \'" + tablename + "\'";
    PGresult *res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        std::cerr << "Schema doesn't exist" << std::endl;
        PQclear(res);
        do_exit();
    }
    int rows = PQntuples(res);
    std::vector<std::string> values;
    query = "INSERT INTO \"" + tablename + "\"(";
    for (int i = 0; i < rows; i++)
    {
        std::string columnName = PQgetvalue(res, i, 0);
        // std::cout << "columnName :" << columnName << std::endl;

        if (searchKey(columnName) && columnName != "gpsState")
        {
            if (values.size() != 0)
                query += ",";
            query += "\"" + columnName + "\"";
            std::cout << "columnName :" << columnName << std::endl;
            values.push_back(getvalue(columnName));
        }
    }
    query += ",\"gpsState\",\"active\",\"uuid\",\"createdAt\",\"updatedAt\"";
    query += ") VALUES(";
    boost::uuids::uuid s = boost::uuids::random_generator()();
    const std::string uuid = boost::lexical_cast<std::string>(s);
    std::string date = currentDate();

    std::string gpsState = "false";
    if (stod(getvalue("gpsState")) > 0)
    {
        gpsState = "true";
    }
    for (int i = 0; i < values.size(); i++)
    {

        query += values[i];
        if (i != values.size() - 1)
            query += ",";
        else
            query += "," + gpsState + ",\'TRUE\',\'" + uuid + "\',\'" + date + "\',\'" + date + "\')";
    }
    PQclear(res);
    std::cout << "query = " << query << std::endl;
    res = PQexec(conn, query.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        PQclear(res);
        do_exit();
    }
    std::cout << "Inserted into Database" << std::endl;
    PQclear(res);
}

void DbUtils::do_exit()
{
    if (conn)
    {
        PQfinish(conn);
    }
}

int DbUtils::getSQLvalue(std::string query)
{
    PGresult *res = PQexec(this->conn, query.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        PQclear(res);
        //do_exit();
    }
    //printf ("%s\n", PQgetvalue(res, 0, 0));
    return atoi(PQgetvalue(res, 0, 0));
}
