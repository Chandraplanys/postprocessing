#include <iostream>
#include <unistd.h>
#include <string>
#include <algorithm>
#include "IPUtils.h"

IPUtils::IPUtils()
{
    this->commandModule_HMAC = "20:e5:17:07:29:f9";
}

IPUtils::~IPUtils()
{
}

std::string IPUtils::GetStdoutFromCommand(std::string cmd)
{

    std::string data;
    FILE *stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");
    if (stream)
    {
        while (!feof(stream))
            if (fgets(buffer, max_buffer, stream) != NULL)
                data.append(buffer);
        pclose(stream);
    }
    return data;
}

std::string IPUtils::getIPAddress(std::string macAddress)
{
    if (macAddress == "")
    {
        std::cout << "MAC Address is empty please check" << std::endl;
        return "";
    }
    else
    {
        //TO DO : Run ping for all IPs before arp -an command - ping -c 1 192.168.0.X
        std::string IPAddress;
        std::string getIPCommand = "arp -an | grep \"" + macAddress + "\" | cut -d ' ' -f 2";
        // std::cout << "Get IP Command " << getIPCommand << std::endl;
        IPAddress = GetStdoutFromCommand(getIPCommand);
        if (IPAddress == "")
        {
            std::cout << "No IP address found for the MAC address" << std::endl;
        }
        else
        {
            IPAddress = IPAddress.substr(1, IPAddress.size() - 3);
            std::cout << "IP Address: " << IPAddress << std::endl;

            return IPAddress;
        }
    }
    return "";
}

std::string IPUtils::removeNewLineTrails(std::string line)
{
    line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
    return line;
}

std::string IPUtils::getGatewayDefault()
{
    std::string gatewayIP;
    // std::string gateway_cmd = "route | grep gateway  | grep 'UG[ \t]' | awk '{print $2}'";
    std::string gateway_cmd = "ip route | awk '/default/ { print $3 }'";

    gatewayIP = removeNewLineTrails(GetStdoutFromCommand(gateway_cmd));
    std::cout << "Gateway: " << gatewayIP << std::endl;

    return gatewayIP;
}

void IPUtils::pingNetwork()
{

    std::string ping_cmd;

    std::string IPGateway = getGatewayDefault();
    if (IPGateway != "")
    {
        std::cout << "Pinging Network Hosts" << std::endl;

        ping_cmd = "nmap -sP " + IPGateway + "/24 -n --send-ip >>/dev/null 2>>/dev/null";
        // std::cout << "Ping Network Command: " << ping_cmd << std::endl;
        int ret = system(ping_cmd.c_str());
        // std::cout << "Arp Running" << std::endl;
        ret = system("arp -an  >>/dev/null 2>>/dev/null");
    }
}

bool IPUtils::findCommandModule(std::string commandModule_HMAC)
{
    std::string IPAddress = getIPAddress(commandModule_HMAC);
    int checkingCounter = 0;
    if (IPAddress == "")
    {
        pingNetwork();

        std::cout << "Waiting for Command Module to get IP" << std::endl;
        while ((IPAddress == "") && (!stopBool))
        {
            if (checkingCounter == 8)
            {
                std::cout << "[findCommandModule][Debug] Dint find Server exiting the process" << std::endl;
                break;
            }
            usleep(1000000);
            IPAddress = getIPAddress(commandModule_HMAC);
            checkingCounter++;
        }
    }
    if (IPAddress != "")
    {
        commandModule_IP = IPAddress;
        return true;
    }
    else
    {
        return false;
    }
}
