#include "../include/Database.h"
#include <iostream>
Database Datahandler;
void shutDown()
{
    if (Datahandler.dataHandler_Flag)
    {
        Datahandler.ROVNetwork.exitNetworkSearch = true;
        Datahandler.dataHandler_Flag = false;
        Datahandler.connection->send_close(1000);
        pthread_join(Datahandler.databaseClient_Thread, NULL);
        std::cout << "Exiting Database Client Thread" << std::endl;

        pthread_join(Datahandler.databaseUpdate_Thread, NULL);
        std::cout << "Exiting Database Update Thread" << std::endl;
    }
}

void handler(int s)
{

    printf("Interupt signal Received --->%d<---- Communicator\n", s);

    // Broken pipe from a client
    if (s == SIGPIPE)
    {
        
        // Just ignore, sock class will close the client the next time
    }
    if(s == SIGSEGV)
    {
        exit(0);
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT  || s == SIGABRT || s == SIGTERM)
    {
        shutDown();
    }
}

void initSigHandler()
{

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGPIPE, &sigIntHandler, NULL);
    sigaction(SIGABRT, &sigIntHandler, NULL);
    sigaction(SIGSEGV, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);
}

int main()
{  
    initSigHandler();

    Datahandler.InitializeThreads();

    while (Datahandler.dataHandler_Flag)
    {
        sleep(1);
    }
    std::cout << "Exiting Main Function" << std::endl;
    return 0;
}
