#include "SerialConnection.h"
#include "AsyncSerial.h"

using namespace std;

SerialConnection::SerialConnection()
{

}

SerialConnection::SerialConnection(std::string opt_port, long opt_baudrate, const boost::function<void (const char*, size_t, void *)> &opt_callback, void *c)
{
	if(this->checkDevice(opt_port))
	{
		this->port     = opt_port;
		this->baudrate = opt_baudrate;
		this->serial.open(this->port, this->baudrate);
		this->serial.setCallback(opt_callback);
    this->serial.c=c;
	}

}

void SerialConnection::Initialize(std::string opt_port, long opt_baudrate, const boost::function<void (const char*, size_t, void *)> &opt_callback, void *c){
  if(this->checkDevice(opt_port))
  {
    this->port     = opt_port;
    this->baudrate = opt_baudrate;
    this->serial.open(this->port, this->baudrate);
    this->serial.setCallback(opt_callback);
    this->serial.c=c;
  }
}

bool SerialConnection::isOpen()
{
	return this->serial.isOpen();
}

void SerialConnection::write(char val)
{
	this->serial.write(&val, 1);
}

void SerialConnection::writeAll(const char* val, size_t size)
{
  this->serial.write(val, size);
}

void SerialConnection::close()
{
	this->serial.clearCallback();
	this->serial.close();
}

bool SerialConnection::checkDevice(std::string devicePort)
{
	char buf[512];
  std::string cmd = "ls "+devicePort+" 2>&1";
  std::string deiveString = devicePort;
  std::string returnString;
  FILE *cmd_pipe = popen(cmd.c_str(), "r");

  fgets(buf, 512, cmd_pipe);

  pclose( cmd_pipe );
returnString = std::string(buf).substr(0,std::string(buf).size()-1);
//std::cout<<returnString<<std::endl;
  if(strcmp(deiveString.c_str(),returnString.c_str())==0)
  {
    return true;
  }
  else{
      return false;
  }

}
