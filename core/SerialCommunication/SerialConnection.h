#ifndef _SERIALCONNECTION_H
#define _SERIALCONNECTION_H

#include "AsyncSerial.h"
#include<cstring>
#include<cstdio>

using namespace std;

class SerialConnection {
private:
	CallbackAsyncSerial serial;
	std::string port;
	long baudrate;
	bool active; 
public:
	SerialConnection();

	SerialConnection(std::string, long, const boost::function<void (const char *, size_t, void *)> &, void *c);

	void Initialize(std::string, long, const boost::function<void (const char*, size_t, void *)> &, void *c);

	bool isOpen();

	void write(char);
	void writeAll(const char*, size_t);

	void close();
	bool checkDevice(std::string devicePort);
};

#endif
