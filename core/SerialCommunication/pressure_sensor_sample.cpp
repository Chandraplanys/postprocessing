#include "pressure_sensor_sample.h"

void PressureSensor::Callback(const char *data, size_t len, void *c)
{

	int idx = 0;
	// cout << "VN100 Data: " << string(data) << endl;
	while (idx < len)
	{
		((PressureSensor *)c)->dataQueue.push_back(data[idx++]);
	}

	if (((PressureSensor *)c)->dataQueue.size() > ((PressureSensor *)c)->minimum_queue_size)
	{
		((PressureSensor *)c)->dataProcess();
	}
}

// void PressureSensor::

void PressureSensor::printData()
{
	std::string request = "SD";
	int n = 0;
	sleep(2);
	std::cout << "Program Initiated" << std::endl;
	while (1)
	{
		this->Serial.writeAll(request.c_str(), 2);
		std::cout << "Request Sent" << std::endl;
		while (n != 30)
		{
			std::cout << "Pressure Value: " << this->PressureSensorValue;
			std::cout << " Temperature Value: " << this->TemperatureValue;
			std::cout << std::endl;
			sleep(1);
			n++;
		}
		n = 0;
		// sleep(30);
	}
}

void PressureSensor::dataProcess()
{
	std::string dataPacket = "";
	while (this->dataQueue.front() != '$')
	{
		this->dataQueue.pop_front();
	}
	while (this->dataQueue.front() != 'm')
	{
		dataPacket.push_back(this->dataQueue.front());
		this->dataQueue.pop_front();
	}
	this->parseDataPacket(dataPacket);
}

void PressureSensor::parseDataPacket(std::string data)
{
	// std::cout << "parseDataPacket Now: " << data << std::endl;
	try
	{
		std::string value = "";
		for (int i = 0; i < 7; i++)
		{
			value.push_back(data.at(i + 1));
		}
		this->PressureSensorValue = std::stod(value);
	}
	catch (std::exception &e)
	{
		std::cout << "[Pressure Sensor][parseDataPacket][ERROR] - "
				  << e.what() << std::endl;
	}
}