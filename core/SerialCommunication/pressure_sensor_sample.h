#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/thread.hpp>
#include <sys/time.h>
#include <stdlib.h>
#include <string>
#include <deque>
#include "SerialConnection.h"

// deque<char> PressureSensor_dataQueue;

class PressureSensor
{
  public:
	PressureSensor(std::string port, long baud_rate)
	{
		this->minimum_queue_size = 100;
		this->Port = port;
		this->Baud_rate = baud_rate;
		this->DataResponse = false;
		this->DataRate = 0;
		this->PressureSensorValue = 0;
		this->TemperatureValue = 0;
		// SerialConnection Serial(this->Port, this->Baud_rate, recvFromVN100IMU);
		this->Serial.Initialize(this->Port, this->Baud_rate, this->Callback, this);
	}
	std::string Port;
	long Baud_rate;
	deque<char> dataQueue;

	//` serial communication callback
	// !Callback, dataProcess, minimum_queue_size and parseDataPacket are MANDATORY for serial to work
	static void Callback(const char *, size_t, void *c);
	void dataProcess();
	void parseDataPacket(std::string data);
	int minimum_queue_size;

	SerialConnection Serial;
	// SerialConnection Serial(this->Port, this->Baud_rate, recvFromVN100IMU);

	void printData();

	// Pressure sensor details
	bool DataResponse;
	int DataRate;
	double PressureSensorValue;
	double TemperatureValue;
};

#endif