#include "pressure_sensor_sample.h"

int main(int argc, char *argv[]) {
  // initialization
  PressureSensor sensor("/dev/ttyACM0", 9600);
  sensor.minimum_queue_size = 20;
  sensor.printData();
  return 0;
}