#include "shared_memory.h"

using namespace boost::interprocess;

void SMemory::create(std::string name, unsigned int mem_size) {
  managed_shared_memory managed_shm{open_or_create, name.c_str(), mem_size};
  if (managed_shm.find<double>("double").first) {
  } else {
    managed_shm.construct<double>("double")(0.0);
  }
}

void SMemory::create_bool(std::string name, unsigned int mem_size) {
  managed_shared_memory managed_shm{open_or_create, name.c_str(), mem_size};
  if (managed_shm.find<bool>("bool").first) {
  } else {
    managed_shm.construct<bool>("bool")(false);
  }
}

void SMemory::create(std::string name, unsigned int mem_size,
                     unsigned int data_size) {
  managed_shared_memory managed_shm{open_or_create, name.c_str(), mem_size};
  if (managed_shm.find<double>("double").first) {
  } else {
    managed_shm.construct<double>("double")[data_size](0.0);
  }
}

void SMemory::create_str(std::string name, unsigned int mem_size) {
  managed_shared_memory managed_shm{open_or_create, name.c_str(), mem_size};
  typedef allocator<char, managed_shared_memory::segment_manager> CharAllocator;
  typedef basic_string<char, std::char_traits<char>, CharAllocator> string;
  if (managed_shm.find<string>("String").first) {
  } else {
    managed_shm.find_or_construct<string>("String")(
        "", managed_shm.get_segment_manager());
  }
}

// void SMemory::create(std::string name, unsigned int mem_size,
//                      std::string data_type) {
//   managed_shared_memory managed_shm{open_or_create, name.c_str(), mem_size};
//   if (data_type == "integer")
//     managed_shm.construct<int>("integer")(0.0);
//   else
//     managed_shm.construct<double>("double")(0.0);
// }
//
// void SMemory::create(std::string name, unsigned int mem_size,
//                      unsigned int data_size, std::string data_type) {
//   managed_shared_memory managed_shm{open_or_create, name.c_str(), mem_size};
//   if (data_type == "integer")
//     managed_shm.construct<int>("integer")[data_size](0.0);
//   else
//     managed_shm.construct<double>("double")[data_size](0.0);
// }

double SMemory::fetch(std::string name) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  std::pair<double *, std::size_t> p = managed_shm.find<double>("double");
  if (p.first) {
    return *p.first;
  } else {
    return 0;
  }
}

bool SMemory::fetch_bool(std::string name) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  std::pair<bool *, std::size_t> p = managed_shm.find<bool>("bool");
  if (p.first) {
    return *p.first;
  } else {
    return 0;
  }
}

double SMemory::fetch(std::string name, unsigned int arr_location) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  std::pair<double *, std::size_t> p = managed_shm.find<double>("double");
  if (p.first) {
    return *(p.first + arr_location - 1);
  } else {
    return 0;
  }
}

std::string SMemory::fetch_str(std::string name) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  typedef allocator<char, managed_shared_memory::segment_manager> CharAllocator;
  typedef basic_string<char, std::char_traits<char>, CharAllocator> string;
  std::pair<string *, std::size_t> s = managed_shm.find<string>("String");
  if (s.first) {
    std::string act_s(s.first->data(), s.first->size());
    return act_s;
  } else {
    return 0;
  }
}

void SMemory::update(std::string name, double value) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  double *i = managed_shm.find_or_construct<double>("double")();
  *i = value;
}

void SMemory::update_bool(std::string name, bool value) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  bool *i = managed_shm.find_or_construct<bool>("bool")();
  *i = value;
}

void SMemory::update(std::string name, unsigned int arr_location,
                     double value) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  double *i = managed_shm.find_or_construct<double>("double")();
  *(i + arr_location - 1) = value;
}

void SMemory::update_str(std::string name, std::string message) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  typedef allocator<char, managed_shared_memory::segment_manager> CharAllocator;
  typedef basic_string<char, std::char_traits<char>, CharAllocator> string;
  string *s = managed_shm.find_or_construct<string>("String")(
      "", managed_shm.get_segment_manager());
  s->assign(message.c_str());
}

void SMemory::reset_sm_bool(std::string name) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  bool *i = managed_shm.find_or_construct<bool>("bool")();
  *i = false;
}

void SMemory::reset_sm(std::string name, unsigned int data_size) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  double *i = managed_shm.find_or_construct<double>("double")();
  for (unsigned int arr_location = 1; arr_location < data_size + 1;
       arr_location++)
    *(i + arr_location - 1) = 0.0;
}

bool SMemory::delete_obj(std::string name) {
  managed_shared_memory managed_shm{open_only, name.c_str()};
  return managed_shm.destroy<double>("double");
  // bool removed = shared_memory_object::remove(name.c_str());
  // std::cout << "Shared memory" << name << std::boolalpha << removed
  //          << std::endl;
}

bool SMemory::delete_sm(std::string name) {
  return shared_memory_object::remove(name.c_str());
}
