#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H
#define VALUE 0

//#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <iostream>
#include <string>

class SMemory {

public:
  void create(std::string name, unsigned int mem_size);
  void create(std::string name, unsigned int mem_size, unsigned int data_size);
  void create_bool(std::string name, unsigned int mem_size);
  void create_str(std::string name, unsigned int mem_size);
  // void create_string(std::string name, unsigned int mem_size);
  // void create(std::string name, unsigned int mem_size, std::string
  // data_type);
  // void create(std::string name, unsigned int mem_size, unsigned int
  // data_size,
  //            std::string data_type);

  double fetch(std::string name);
  double fetch(std::string name, unsigned int arr_location);
  bool fetch_bool(std::string name);
  std::string fetch_str(std::string name);
  // std::string fetch_string(std::string name);
  // void fetch(std::string name, std::string data_type);
  // void fetch(std::string name, unsigned int arr_location,
  //           std::string data_type);

  void update(std::string name, double value);
  void update(std::string name, unsigned int arr_location, double value);
  void update_bool(std::string name, bool value);
  void update_str(std::string name, std::string value);
  // void update_string(std::string name, std::string message);

  bool delete_obj(std::string name);
  bool delete_sm(std::string name);

  void reset_sm_bool(std::string name);
  void reset_sm(std::string name, unsigned int mem_size);
};

#endif
