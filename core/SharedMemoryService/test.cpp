#include "shared_memory.h"

using namespace boost::interprocess;

int main() {
  SMemory sms;
  sms.delete_sm("Boost");
  sms.delete_sm("_Bool");
  sms.create("Boost", 1024, 10);
  sms.create("Boost_obj", 1024, 5);
  sms.create_bool("_Bool", 1024);
  sms.update_bool("_Bool", true);

  std::cout << "Test 1 - Read Shared memory (non-functional approach)"
            << std::endl;
  managed_shared_memory managed_shm{open_only, "Boost"};
  std::pair<double *, std::size_t> p = managed_shm.find<double>("double");
  std::cout << "  first location value: " << *p.first << std::endl;
  std::cout << "  second location value: " << *(p.first + 1) << std::endl;
  std::cout << "  Shared memory size: " << p.second << std::endl;

  std::cout << std::endl;
  std::cout << "Test 2 - Read Shared Memory (functional approach)" << std::endl;
  double value = sms.fetch("Boost");
  std::cout << "  first location value: " << value << std::endl;
  value = sms.fetch("Boost", 2);
  std::cout << "  second locatoin value: " << value << std::endl;

  std::cout << std::endl;
  std::cout << "Test 3 - Update Share memory values" << std::endl;
  sms.update("Boost", 20.01);
  std::cout << "  Updated value in 1st location" << std::endl;
  value = sms.fetch("Boost");
  std::cout << "    first location value: " << value << std::endl;
  value = sms.fetch("Boost", 2);
  std::cout << "    second location value: " << value << std::endl;
  sms.update("Boost", 2, 10.01);
  std::cout << "  Updated value in 2nd location" << std::endl;
  value = sms.fetch("Boost");
  std::cout << "    first location value: " << value << std::endl;
  value = sms.fetch("Boost", 2);
  std::cout << "    second location value: " << value << std::endl;

  // Create and access bool locations
  std::cout << std::endl;
  std::cout << "Test 4 - Create Read and Update bool_obj shared memory"
            << std::endl;
  sms.create_bool("Bool", 1024);
  bool bool_value = sms.fetch_bool("_Bool");
  std::cout << "  bool value:" << bool_value << std::endl;
  sms.update_bool("_Bool", true);
  std::cout << "  updated bool value:" << bool_value << std::endl;

  //----------string testing ------------
  std::cout << std::endl;
  std::cout << "Test 5 - Create Update and Read string obj shared memory"
            << std::endl;
  sms.create_str("String", 1024);
  std::string sentence = sms.fetch_str("String");
  std::cout << "  string value: " << sentence << std::endl;
  sms.update_str("String", "Hello !!!");
  sentence = sms.fetch_str("String");
  std::cout << "  updated string value: " << sentence << std::endl;
  sms.update_str("String", "Yeah !!!");
  sentence = sms.fetch_str("String");
  std::cout << "  updated string value: " << sentence << std::endl;
  sms.delete_sm("String");

  //----------------Delete Shared Memory----------------------
  std::cout << std::endl;
  std::cout << "Test 5 - Delete shared Memory" << std::endl;
  bool state = sms.delete_sm("Boost");
  std::cout << "  delete shared mem type double :" << state << std::endl;
  std::cout << "  delete shared mem type double :" << sms.delete_sm("Boost_obj")
            << std::endl;
  std::cout << "  delete shared mem type bool :" << sms.delete_sm("_Bool")
            << std::endl;
  std::cout << "  delete unavailable shared mem:" << sms.delete_sm("Unknown")
            << std::endl;

  // ------------reset shared memory-------------
  std::cout << "Test 6 - Reset shared Memory" << std::endl;
  sms.delete_sm("Reset");
  sms.create("Reset", 1024, 10);
  sms.update("Reset", 2, 10.50);
  std::cout << "  Shared Location value before reset :" << sms.fetch("Reset", 2)
            << std::endl;
  sms.reset_sm("Reset", 10);
  std::cout << "  Shared Location value after reset :" << sms.fetch("Reset", 2)
            << std::endl;

  //-----------------------------------------
  return 0;
}
