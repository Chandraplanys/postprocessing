#include "jsonParser.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>
#include <sstream>
#include <string>
using namespace rapidjson;

JsonParser::JsonParser()
{
}

JsonParser::~JsonParser()
{
}

const char* JsonParser::GetJsonText(Document doc)
{
  rapidjson::StringBuffer buffer;

  buffer.Clear();

  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  doc.Accept(writer);

  return strdup( buffer.GetString() );
}

std::string JsonParser::GetJsonString(rapidjson::Value& result)
{
  rapidjson::StringBuffer buffer;

  buffer.Clear();

  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  result.Accept(writer);

  return buffer.GetString();
}


std::string JsonParser::getvalue(std::string key)
{
	std::string value = "";
	if (!searchKey(key))
	{
		return NULL;
	}
	else
	{

		if (d[key.c_str()].IsString() == 1)
		{
			value = d[key.c_str()].GetString();
		}
		else if (d[key.c_str()].IsInt() == 1)
		{
			std::ostringstream str1;
			str1 << d[key.c_str()].GetInt();
			std::string s = str1.str();
			value = s;
		}
		else if (d[key.c_str()].IsBool() == 1)
		{
			std::ostringstream str1;
			str1 << d[key.c_str()].GetBool();
			std::string s = str1.str();
			value = s;
		}
		else if (d[key.c_str()].IsDouble() == 1)
		{
			std::ostringstream str1;
			str1 << d[key.c_str()].GetDouble();
			std::string s = str1.str();
			value = s;
		}
		else if (d[key.c_str()].IsFloat() == 1)
		{
			std::ostringstream str1;
			str1 << d[key.c_str()].GetFloat();
			std::string s = str1.str();
			value = s;
		}
	}
	return value;
}
bool JsonParser::searchKey(std::string key)
{
	if (d.HasMember(key.c_str()) == 1)
	{
		return true;
	}
	return false;
}
std::string JsonParser::getcsv(std::string json, std::string key_arr[], int size)
{
	d.Parse(json.c_str());
	std::string st = "";
	for (int i = 0; i < size; ++i)
	{
		if (!getvalue(key_arr[i]).empty())
		{
			if (d[key_arr[i].c_str()].IsString() == 1)
			{
				std::string s = d[key_arr[i].c_str()].GetString();
				st += s;
			}
			else if (d[key_arr[i].c_str()].IsBool() == 1)
			{
				std::ostringstream str1;
				str1 << d[key_arr[i].c_str()].GetBool();
				std::string s = str1.str();
				st += s;
			}
			else if (d[key_arr[i].c_str()].IsInt() == 1)
			{
				std::ostringstream str1;
				str1 << d[key_arr[i].c_str()].GetInt();
				std::string s = str1.str();
				st += s;
			}
			else if (d[key_arr[i].c_str()].IsDouble() == 1)
			{
				std::ostringstream str1;
				str1 << d[key_arr[i].c_str()].GetDouble();
				std::string s = str1.str();
				st += s;
			}
			else if (d[key_arr[i].c_str()].IsFloat() == 1)
			{
				std::ostringstream str1;
				str1 << d[key_arr[i].c_str()].GetFloat();
				std::string s = str1.str();
				st += s;
			}
		}
		else
		{
			st += "0.00000";
			std::cout << key_arr[i] << " Not found" << std::endl;
		}
		if (i != size - 1)
			st += ",";
	}
	return st;
}