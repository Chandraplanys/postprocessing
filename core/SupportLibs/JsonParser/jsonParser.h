#ifndef JSONPARSER_H
#define JSONPARSER_H
#include "rapidjson/document.h"

using namespace rapidjson;

class JsonParser
{
private:
public:
	Document FullMsg,d;
	JsonParser();
	const char* GetJsonText(Document d);
	std::string GetJsonString(rapidjson::Value& result);
	std::string getvalue(std::string key);
	bool searchKey(std::string key);
	std::string getcsv(std::string json, std::string key_arr[], int size);
	~JsonParser();
};

#endif