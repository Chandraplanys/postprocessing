#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include "../../SharedMemoryService/shared_memory.h"
#include "../../WebSocketCPP/server_ws.hpp"
#include <pthread.h>

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;

class Communicator
{
private:

public:
  WsServer cockpitWSServer;

  pthread_t cockpitDataServer_Thread;

  Communicator();
  static void *CockpitDataServer(void *arguments);

  void InitializeThreads();
  void InitializeSharedMemory();

  std::string to_string_with_precision(double value, int n);

  void exitCommunicator();
};

#endif