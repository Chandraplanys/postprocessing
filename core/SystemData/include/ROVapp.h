#ifndef ROVAPP_H
#define ROVAPP_H

#include <stdlib.h>
#include <dirent.h>
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sysinfo.h>








/*
  get all Devices from 99-usb-rule files and check if symlink present or not for each Devices
*/
class ROVdevice
{
  private:
  public:
    std::vector<std::string> devices;
    void readDevices();
    void moveFileFromUdev();
    bool checkSymlinkPresent(std::string);
};

/*
  All Process are saved in a Global Vector String "Processing". For more processes checking, add that process into it.
*/


class ROVprocess
{
  private:
  public:
    pid_t proc_find(const char *);
};

/*
  CPU consumption, Network Bandwidth, Memory Consumtion etc..  
*/

class otherData
{
  private:
    //char buf[256];
    int size, prev_idle, prev_total, idle, total, i;

  public:
    float enoRxBytes;
    float enoTxBytes;
    float wloRxBytes;
    float wloTxBytes;
    float loTxBytes;
    float loRxBytes;

    int fd, *nums;
    char buf[256];
    otherData();
    int *parserResult(const char *, int);
    float cpuUsageCalc(void);

    //int getTotalPhysicalMemSize(void);
    float getCurrentPhysicalMemUsage(void);

    float cpuTemp(void);

    void netBandWidth(void);
};

#endif