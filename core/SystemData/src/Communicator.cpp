#include "../include/Communicator.h"
#include <iostream>


Communicator::Communicator()
{
    cockpitWSServer.config.port = 8095;
}

void Communicator::InitializeThreads()
{
    std::cout << "Creating Communicator Web Socket Thread" << std::endl;
    pthread_create(&cockpitDataServer_Thread, NULL, CockpitDataServer, this);
}

void Communicator::InitializeSharedMemory()
{
    // std::cout << "In SHM" << std::endl;

    //..... Device data shared memory locations....
    SMemory s_memory;
    
    s_memory.create_str("SystemData", 3072);

    //std::cout<<"1"<<std::endl;

    // ..... Logger control by vision flag..............
    //s_memory.create_bool("vision_status_flag", 1024);
}

void *Communicator::CockpitDataServer(void *arguments)
{
    
    Communicator *Communicator_Instance = (Communicator *)arguments;
    
    SMemory shmData;

    //std::cout<<"doneyrueyrueyu"<<std::endl;
    auto &echo = Communicator_Instance->cockpitWSServer.endpoint["^/echo/?$"];

    

    
    echo.on_message = [&shmData, Communicator_Instance](std::shared_ptr<WsServer::Connection> connection, std::shared_ptr<WsServer::Message> message) {


    
        auto message_str = shmData.fetch_str("SystemData");
        std::cout<<"sent: "<<message_str<<std::endl;
            

        usleep(20000);

        auto send_stream = std::make_shared<WsServer::SendStream>();
        *send_stream << message_str;
        //std::cout<<"lol: "<<std::endl;
        // connection->send is an asynchronous function
        connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
            if (ec)
            {
                std::cout << "Server: Error sending message. " <<
                    // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
                    "Error: " << ec << ", error message: " << ec.message() << std::endl;
            }
            
        });
        
    };

    echo.on_open = [](std::shared_ptr<WsServer::Connection> connection) {
        std::cout << "Server: Opened connection " << connection.get() << std::endl;
    };



    // See RFC 6455 7.4.1. for status codes
    echo.on_close = [](std::shared_ptr<WsServer::Connection> connection, int status, const std::string & /*reason*/) {
        std::cout << "Server: Closed connection " << connection.get() << " with status code " << status << std::endl;
    };

    // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
    echo.on_error = [](std::shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
        std::cout << "Server: Error in connection " << connection.get() << ". "
                  << "Error: " << ec << ", error message: " << ec.message() << std::endl;
    };

    std::thread server_thread([Communicator_Instance]() {
        // Start WS-server
        Communicator_Instance->cockpitWSServer.start();
    });

    // Wait for server to start so that the client can connect
    std::this_thread::sleep_for(std::chrono::seconds(1));
    server_thread.join();
    
}

void Communicator::exitCommunicator()
{
    this->cockpitWSServer.stop();
}

std::string Communicator::to_string_with_precision(double value,
                                                   const int n = 6)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(n) << value;
    return out.str();
}
