#include "../include/ROVapp.h"
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sstream>
#include <stdlib.h>
#include <dirent.h>
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sysinfo.h>
static std::string Path = "/home/shan/Documents/99-usb-serial.rules";



void ROVdevice::moveFileFromUdev()
{
    std::string system_cmd = "cp /etc/udev/rules.d/99-usb-serial.rules $HOME/Documents/ && chmod 777 $HOME/Documents/99-usb-serial.rules";
    system(system_cmd.c_str());
}

// Get all the Devices from udev rule file
void ROVdevice::readDevices()
{
    std::ifstream Symlinkfile(Path);
    if (!Symlinkfile.is_open())
    {
        std::cout << "could not open /home/usr/documents/99-usb-serial.rules" << std::endl;
    }
    else
    {
        std::string line;
        std::string device;
        while (Symlinkfile.good())
        {
            getline(Symlinkfile, line); // get line from file
            boost::trim(line);

            //std::cout<<"len: "<<line.length()<<std::endl;

            std::size_t pos = line.find("+="); // search
            // std::cout<<"lol: "<<pos<<std::endl;

            if (pos != std::string::npos) // string::npos is returned if string is not found
            {
                device = line.substr(pos + 3, line.length() - pos);

                //std::cout<<"len: "<<device.length()<<std::endl;

                device.pop_back();

                this->devices.push_back(device);
            }
        }
        Symlinkfile.close();
    }
}

//get device name(if running) from symlink name
bool ROVdevice::checkSymlinkPresent(std::string symLink)
{
    struct stat p_statbuf;
    if (lstat(symLink.c_str(), &p_statbuf) < 0)
    { /* if error occured */
        //perror("[deviceChecker][is_symlink] calling stat()");
        //std::cout<<symlink<<" has some other kind of error, please check manually...........  ls /dev/"<<std::endl;
        return false;
    }

    if (S_ISLNK(p_statbuf.st_mode) == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//test if process is running or not from /proc file
pid_t ROVprocess::proc_find(const char *name)
{
    DIR *dir;
    struct dirent *ent;
    char buf[512];

    long pid;
    char pname[100] = {
        0,
    };
    char state;
    FILE *fp = NULL;

    if (!(dir = opendir("/proc")))
    {
        perror("can't open /proc");
        return -1;
    }

    while ((ent = readdir(dir)) != NULL)
    {
        long lpid = atol(ent->d_name);
        if (lpid < 0)
            continue;
        snprintf(buf, sizeof(buf), "/proc/%ld/stat", lpid);
        fp = fopen(buf, "r");

        if (fp)
        {
            if ((fscanf(fp, "%ld (%[^)]) %c", &pid, pname, &state)) != 3)
            {
                printf("fscanf failed \n");
                fclose(fp);
                closedir(dir);
                return -1;
            }
            if (!strcmp(pname, name))
            {
                fclose(fp);
                closedir(dir);
                return (pid_t)lpid;
            }
            fclose(fp);
        }
    }

    closedir(dir);
    return -1;
}


//constructor 
otherData::otherData()
{
    this->prev_idle = 0;
    this->prev_total = 0;
    this->fd = open("/proc/stat", O_RDONLY);
}

// CPU consumtion from all cores(CPU1...CPU4)
int *otherData::parserResult(const char *buf, int size)
{
    static int ret[10];
    int i, j = 0, start = 0;

    for (i = 0; i < size; i++)
    {
        char c = buf[i];
        if (c >= '0' && c <= '9')
        {
            if (!start)
            {
                start = 1;
                ret[j] = c - '0';
            }
            else
            {
                ret[j] *= 10;
                ret[j] += c - '0';
            }
        }
        else if (c == '\n')
        {
            break;
        }
        else
        {
            if (start)
            {
                j++;
                start = 0;
            }
        }
    }

    return ret;
}

//computaion of cpu Usage 
float otherData::cpuUsageCalc(void)
{
    this->idle = this->nums[3];
    this->total = 0;

    for (int i = 0; i < 10; i++)
    {
        this->total += this->nums[i];
    }

    int diff_idle = this->idle - this->prev_idle;
    int diff_total = this->total - this->prev_total;
    float usage = (float)(((float)(1000 * (diff_total - diff_idle)) / (float)diff_total + 5) / (float)10);

    fflush(stdout);

    this->prev_total = this->total;
    this->prev_idle = this->idle;

    lseek(fd, 0, SEEK_SET);

    return usage;
}

// get Total free and used memory using free -h command
float otherData::getCurrentPhysicalMemUsage(void)
{
    char buffer[128];
    //std::string buffer;
    std::string result = "";
    FILE *pipe = popen("free -h", "r");
    if (!pipe)
        throw std::runtime_error("popen() failed!");
    try
    {
        int i = 1;
        while (!feof(pipe))
        {
            if (fgets(buffer, 128, pipe) != NULL)
            {
                result = buffer;
                std::istringstream iss(result);
                std::vector<std::string> v;
                do
                {
                    std::string sub;
                    iss >> sub;
                    v.push_back(sub);
                    //std::cout << sub << std::endl;

                } while (iss);
                if (i == 2)
                {
                    // float memAvail = (float)stoi(v[6]);
                    // float memTot = (float)stoi(v[1]);
                    float memAvail = strtof(v[6].c_str(), NULL);
                    float memTot = strtof(v[1].c_str(), NULL);
                    if(v[1].back() == 'G')
                    {
                        memTot = memTot * 1000;
                    }
                    if(v[6].back() == 'G')
                    {
                        memAvail = memAvail * 1000;
                    }
                    
                    float percMemUsed = (memTot - memAvail) / memTot;
                    //std::cout << memAvail << " " << memTot << std::endl;
                    return percMemUsed * 100;
                }
                v.clear();
                i++;
            }
        }
    }
    catch (...)
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return 0.0;
}

float otherData::cpuTemp(void)
{
    float systemp, milliDeg;
    FILE *thermal;
    int n;

    thermal = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
    n = fscanf(thermal, "%f", &milliDeg);

    fclose(thermal);

    systemp = milliDeg / 1000;
}

void otherData::netBandWidth(void)
{
    std::string s;
    std::ifstream myfile("/proc/net/dev");

    if (myfile.is_open())
    {

        int i = 1;

        while (getline(myfile, s))
        {
            if (i < 3)
            {
                i++;
                continue;
            }

            std::string token(s);
            std::istringstream iss(token);
            std::vector<std::string> v;

            do
            {
                std::string sub;
                iss >> sub;
                v.push_back(sub);

            } while (iss);

            // if(i > 2 && i < 6)
            // {

            // }

            if (v[0] == "lo:")
            {
                this->loRxBytes = strtof(v[1].c_str(), NULL) / 1000000;
                this->loTxBytes = strtof(v[9].c_str(), NULL) / 1000000;
            }
            else if (v[0] == "wlo1:")
            {
                this->wloRxBytes = strtof(v[1].c_str(), NULL) / 1000000;
                this->wloTxBytes = strtof(v[9].c_str(), NULL) / 1000000;
            }
            else if (v[0] == "eno1:")
            {
                this->enoRxBytes = strtof(v[1].c_str(), NULL) / 1000000;
                this->enoTxBytes = strtof(v[9].c_str(), NULL) / 1000000;
            }
            else
            {
                std::cout << "[ROVapp][netBandWidth] can't store NetBW bytes" << std::endl;
            }

            //std::cout<<v[0]<<v[1]<<"......"<<v[9]<<std::endl;
            v.clear();
            i++;
        }
        myfile.close();
    }
    else
    {
        std::cout << "[ROVapp][netBandWidth] error opening file " << std::endl;
        myfile.close();
    }
}