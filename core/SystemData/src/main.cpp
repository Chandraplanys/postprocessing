#include "../include/Communicator.h"
#include "../include/ROVapp.h"
#include <stdlib.h>
#include <fstream>
#include <iostream>

Communicator Datahandler;

std::vector<std::string> processing = {"mission", "client", "vision", "deviceManager"};

bool DATAHANDLER_FLAG = true;

void shutDown()
{
    if (DATAHANDLER_FLAG)
    {
        DATAHANDLER_FLAG = false;
        Datahandler.exitCommunicator();
        (void)pthread_join(Datahandler.cockpitDataServer_Thread, NULL);
        std::cout << "Exiting Data Handler" << std::endl;
    }
}
void handler(int s)
{

    printf("Interupt signal Received --->%d<---- Communicator\n", s);

    // Broken pipe from a client
    if (s == SIGPIPE)
    {

        // Just ignore, sock class will close the client the next time
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM)
    {

        shutDown();
    }
}
void initSigHandler()
{

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGPIPE, &sigIntHandler, NULL);
    sigaction(SIGABRT, &sigIntHandler, NULL);
    sigaction(SIGSEGV, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);
}

int main(int argc, char const *argv[])
{
    initSigHandler();

     SMemory s_memory;

    ROVdevice testDevice;
    ROVprocess testProcess;
    otherData data;

    std::string currentProcess;
    testDevice.moveFileFromUdev();
    testDevice.readDevices();
    float usage = 0, temp = 0, usedMem;
    int size, totalmem;

    Datahandler.InitializeSharedMemory();
    Datahandler.InitializeThreads();
    std::string updatedString = "";

    while (DATAHANDLER_FLAG)
    {
        updatedString = "{";
        for (int i = 0; i < processing.size(); i++)
        {
            currentProcess = processing[i];
            pid_t pid = testProcess.proc_find(currentProcess.c_str());
            if (pid == -1)
            {
                //std::cout << "process: " << currentProcess << " not running" << std::endl;
                updatedString += "\"" + currentProcess + "\"" + ":\"0\",";
            }
            else
            {
                //std::cout << "process: " << currentProcess << " running at id: " << pid << std::endl;
                updatedString += "\"" + currentProcess + "\"" + ":\"1\",";
            }
            currentProcess.erase();
        }

        std::string device;
        bool symStatus;
        
        //std::cout << std::endl;

        for (std::vector<std::string>::iterator ir = testDevice.devices.begin(); ir != testDevice.devices.end(); ++ir)
        {
            device = *ir;

            symStatus = testDevice.checkSymlinkPresent(device);

            if (symStatus == true)
            {
                //std::cout << "device: " << device << " running" << std::endl;
                updatedString += "\"" + device + "\"" + ":\"1\",";
            }
            else
            {
                //std::cout << "device: " << device << " not active " << std::endl;
                updatedString += "\"" + device + "\"" + ":\"0\",";
            }
            device.erase();
        }

        size = read(data.fd, data.buf, sizeof(data.buf));

        if (size <= 0)
        {
            std::cout << "CPU USAGE ERROR: /proc/stat file has no data" << std::endl;
        }

        data.nums = data.parserResult(data.buf, size);
        usage = data.cpuUsageCalc();

        //printf("CPU USAGE: %%%6.2f \n", usage);
        updatedString += "\"cpu\":\"" +  std::to_string(usage) + "\",";


        usedMem = data.getCurrentPhysicalMemUsage();
        //std::cout << "Consumed Memory: " << usedMem << std::endl;
        updatedString += "\"memory\":\"" +  std::to_string(usedMem) + "\",";

        temp = data.cpuTemp();
        //std::cout << "CPU Temp: " << temp << std::endl;
        updatedString += "\"temperature\":\"" +  std::to_string(temp) + "\",";

        data.netBandWidth();

        //std::cout << "lo: Rx:: " << data.loRxBytes << "\t Tx:: " << data.loTxBytes << std::endl;
        //std::cout << "wlo: Rx:: " << data.wloRxBytes << "\t Tx:: " << data.wloTxBytes << std::endl;
        //std::cout << "eno: Rx:: " << data.enoRxBytes << "\t Tx:: " << data.enoTxBytes << std::endl;
        updatedString += "\"netup\":\"" +  std::to_string(data.wloRxBytes) + "\",";
        updatedString += "\"netdown\":\"" +  std::to_string(data.wloTxBytes) + "\"}";
        

        //std::cout<<updatedString<<std::endl;
        // std::cout << std::endl
        //           << "......................." << std::endl
        //           << std::endl;

        s_memory.update_str("SystemData", updatedString);  

        usleep(1000000);
        
    }
    shutDown();

    return 0;
}
