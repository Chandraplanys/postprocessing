vision : (15-03-2018)
--------

delete temp files added


vision_0.17 (Current)(Stable)
-----------
- Stable version for Video Storing
- Temparary backup issue fixed
- Disabled development mode and ROV mode

vision_0.16 (vision_0.15 + vision_0.9.5)
-----------
- Classes seperated for Sony and logitech cameras.
- DEVMODE - normal mode implemented

vision_0.15 (vision_0.14 + vision_0.9.3)
-----------
- Added configure.config file for easy parameter retrivel.   
- fiexed bugs in configure feature.
- configure.config file existing validation.
- improved readability from file to code.
- fixed bug for logitech cam record timetrack.

vision_0.14
-----------
- server_vision.cpp modified accroding to the access of GUI. 
- implemented Start-Stop control vision through GUI.
- backup files for easy recovery : TEMP_front.mpg and TEMP_bottom.mpg added with new class file mpgvideo.cpp
- signal handler (SIGINT) implemented in dual_stream.js 

vision_0.13
-----------
- shared memory implemented with vision control in terminal.
- added some libraries in cmake for sharedmemory in vision module.
- pthread_kill() server_vision.cpp to stop the dual_stream_server.js 

vision_0.12
-----------
- implemented video_st->time_base = (AVRational){1 , 90000} for mp4 container to control video on SonyFCB.
- logitech : video_st->time_base = (AVRational){1 , 35000};

vision_0.11
-----------
- improved in breaking videos .

vision_0.10
----------
- implemented - video breaking at regular interval .
- bug identified and fixed for breaking video intervals .

vision_0.9
----------
- Support-class added to structure the code and easy maintaining of folder creation and making video files name.


vision_0.8
----------
- Individual streaming / recording control.
- seperate trailer dump function is introduced [writer.cpp] to fix segmentation fault.
- video storage folder moved to ubuntu's default Videos folder.
- code optimized for video storage name creation.

vision_0.7 (old version)(stable)
----------
- include folders added for .h files
- cmake added for easy compilation.
- disabled browser automatic launching from single click running.
- ctrl+z signal handler implemented.
- changed framecount variable from int to long long .


vision_0.6
----------
- check for connected cameras in the bot.
- can able to connect and play either single or dual camera
- small bugs fixed.
- storing video quality improved by Rescaling with swscale(): changed from  SWS_FAST_BILINEAR to SWS_BILINEAR on writer.cpp
 < http://www.mjbshaw.com/2012/10/rescaling-with-swscale-swsfastbilinear.html >

vision_0.5
----------
easylogging++ removed due to produing unexpected error when application running in multithreading.

vision_0.4
----------
create directory based on system date
create sub-directory to store the video with camera and system time as file name.
one click 'run' vision server added
vision module moved to 'src' folder.
file arrangement structured.
server included in same folder

vision_0.3
----------

easylogging++ with some bugs fixed.
easylogging++ with some bugs fixed.

vision_0.2
----------

-easylogging++ with some bugs fixed.
-can able to run on any ubuntu system

vision_0.1
----------
easylogging++ added

vision 0.0
----------
dual camera simeltaneous [multithreading] streamming and writing.
uses JSMPEG server for video streaming.
mp4 as container and mpeg4 as codec.



structure of vision module as follows [ vision 0.7 and above versions ]

.
|-- build
|   |-- camera.cpp
|   |-- include
|   |   |-- camera.h
|   |   |-- streamer.h
|   |   |-- Support.h
|   |   |-- vision.h
|   |   |-- writer.h
|   |-- jsmpeg
|   |   |-- backUp.html
|   |   |-- dualFeed.html
|   |   |-- dual_stream-server.js
|   |   |-- FRONT_CAMERA.html
|   |   |-- jsmpg.js
|   |   |-- node_modules
|   |   |   `-- ws
|   |   |       |-- index.js
|   |   |       |-- lib
|   |   |       |   |-- BufferPool.js
|   |   |       |   |-- BufferUtil.fallback.js
|   |   |       |   |-- BufferUtil.js
|   |   |       |   |-- ErrorCodes.js
|   |   |       |   |-- Extensions.js
|   |   |       |   |-- PerMessageDeflate.js
|   |   |       |   |-- Receiver.hixie.js
|   |   |       |   |-- Receiver.js
|   |   |       |   |-- Sender.hixie.js
|   |   |       |   |-- Sender.js
|   |   |       |   |-- Validation.fallback.js
|   |   |       |   |-- Validation.js
|   |   |       |   |-- WebSocket.js
|   |   |       |   `-- WebSocketServer.js
|   |   |       |-- Makefile
|   |   |       |-- node_modules
|   |   |       |   |-- options
|   |   |       |   |   |-- lib
|   |   |       |   |   |   `-- options.js
|   |   |       |   |   |-- Makefile
|   |   |       |   |   |-- package.json
|   |   |       |   |   `-- README.md
|   |   |       |   `-- ultron
|   |   |       |       |-- index.js
|   |   |       |       |-- LICENSE
|   |   |       |       |-- package.json
|   |   |       |       |-- README.md
|   |   |       |       `-- test.js
|   |   |       |-- package.json
|   |   |       |-- README.md
|   |   |       `-- SECURITY.md
|   |   |-- README.md
|   |   |-- single_stream-server.js
|   |   |-- test.html
|   |   |-- TOP_CAMERA.html
|   |   |-- top.html
|   |-- streamer.cpp
|   |-- Support.cpp
|   |-- vision.cpp
|   |-- vision.sh
|   |-- writer.cpp
|-- build.sh
|-- CMakeLists.txt
|-- README.md
|-- server_vision.cpp

