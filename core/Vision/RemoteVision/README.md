* To Clean the "arp" cache type the following command in terminal
    - sudo ip -s -s neigh flush all
* To check the MAC Address in the network
    - arp -an
* To populate "arp" cache 
    - nmap -sP 192.168.1.1/24
