#include <iostream>

#include "../../../../SharedMemoryService/shared_memory.h"

using namespace std;

int main(int argc, char const *argv[])
{
    std::string videoFolderPath;
    SMemory s_memory;
    s_memory.create_str("videoFolderPath", 1024);

    videoFolderPath = s_memory.fetch_str("videoFolderPath");

    if (videoFolderPath == "")
    {
        std::cout << "null" << std::endl;
    }
    else
    {
        std::cout << videoFolderPath << std::endl;
    }

    return 0;
}
