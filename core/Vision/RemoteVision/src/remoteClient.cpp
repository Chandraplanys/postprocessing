#include "remoteVision.hpp"

#include <remote/session.hpp>
#include <remote/make_tcp_binding.hpp>

#include <signal.h>
#include <algorithm>
#include <string>
#include <fstream>

#include "../../../SharedMemoryService/shared_memory.h"

#ifdef REMOTE_USER_REGISTER_ARCHIVE
#include <remote/register_archive.ipp>
#endif

bool stopVisionBool = false;
// std::string commandModule_HMAC = "94:c6:91:1b:8e:9a"; //Command Module MAC
std::string commandModule_HMAC = "88:d7:f6:e0:58:db"; //Venky Module MAC

std::string commandModule_IP = "";

void shutdown()
{
    std::cout << "[VideoServer][StopVision]" << std::endl;

    stopVisionBool = true;
}

void exitProgram()
{
    exit(0);
}
void recv_signal(int s)
{

    std::cout << "[Info][Vision][Signal Handler] Signal Recieved is " << s
              << std::endl;
    if (s == SIGPIPE)
    {
        // Just ignore, sock class will close the client the next time
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT || s == SIGSEGV || s == SIGABRT ||
        s == SIGKILL || s == SIGTERM || s == SIGHUP)
    {

        shutdown();
    }
    if (s == SIGTSTP)
    {
        exitProgram();
    }
}

void initSigHandler()
{

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = recv_signal;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGTSTP, &sigIntHandler, NULL);
    sigaction(SIGPIPE, &sigIntHandler, NULL);
    sigaction(SIGABRT, &sigIntHandler, NULL);
    sigaction(SIGSEGV, &sigIntHandler, NULL);
    sigaction(SIGKILL, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);
    sigaction(SIGHUP, &sigIntHandler, NULL);
}

std::string GetStdoutFromCommand(std::string cmd)
{

    std::string data;
    FILE *stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");
    if (stream)
    {
        while (!feof(stream))
            if (fgets(buffer, max_buffer, stream) != NULL)
                data.append(buffer);
        pclose(stream);
    }
    return data;
}

std::string getIPAddress(std::string macAddress)
{
    if (macAddress == "")
    {
        std::cout << "MAC Address is empty please check" << std::endl;
        return "";
    }
    else
    {
        //TO DO : Run ping for all IPs before arp -an command - ping -c 1 192.168.0.X
        std::string IPAddress;
        std::string getIPCommand = "arp -an | grep \"" + macAddress + "\" | cut -d ' ' -f 2";
        std::cout << "Get IP Command " << getIPCommand << std::endl;
        IPAddress = GetStdoutFromCommand(getIPCommand);
        if (IPAddress == "")
        {
            std::cout << "No IP address found for the MAC address" << std::endl;
        }
        else
        {
            IPAddress = IPAddress.substr(1, IPAddress.size() - 3);
            std::cout << "IP Address: " << IPAddress << std::endl;

            return IPAddress;
        }
    }
    return "";
}

std::string removeNewLineTrails(std::string line)
{
    line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
    return line;
}

std::string getGatewayDefault()
{
    std::string gatewayIP;
    // std::string gateway_cmd = "route | grep gateway  | grep 'UG[ \t]' | awk '{print $2}'";
    std::string gateway_cmd = "ip route | awk '/default/ { print $3 }'";

    gatewayIP = removeNewLineTrails(GetStdoutFromCommand(gateway_cmd));
    std::cout << "Gateway: " << gatewayIP << std::endl;

    return gatewayIP;
}

void pingNetwork()
{

    std::string ping_cmd;

    std::string IPGateway = getGatewayDefault();
    if (IPGateway != "")
    {
        std::cout << "Pinging Network Hosts" << std::endl;

        ping_cmd = "nmap -sP " + IPGateway + "/24 -n --send-ip >>/dev/null 2>>/dev/null";
        // std::cout << "Ping Network Command: " << ping_cmd << std::endl;
        int ret = system(ping_cmd.c_str());
        // std::cout << "Arp Running" << std::endl;
        ret = system("arp -an  >>/dev/null 2>>/dev/null");
    }
}

bool findCommandModule(std::string commandModule_HMAC)
{
    std::string IPAddress = getIPAddress(commandModule_HMAC);
    int checkingCounter = 0;

    if (IPAddress == "")
    {
        pingNetwork();

        std::cout << "Waiting for Command Module to get IP" << std::endl;
        while ((IPAddress == "") && (!stopVisionBool))
        {
            if (checkingCounter == 8)
            {
                std::cout << "[findCommandModule][Debug] Dint find Server exiting the process" << std::endl;
                break;
            }
            usleep(1000000);
            IPAddress = getIPAddress(commandModule_HMAC);
            checkingCounter++;
        }
    }
    if (IPAddress != "")
    {
        commandModule_IP = IPAddress;
        return true;
    }
    else
    {
        return false;
    }
}

int main(int argc, char const *argv[])
{
    initSigHandler();

    int visionStatus;
    std::string visionFolderPath;
    bool IPAddressStatus = false;
    bool defectCaptureFlag = false;
    bool screenShotStatus = false;
    SMemory shm;
    double primaryCameraFrameCount = 0;
    double secondaryCameraFrameCount = 0;
    std::string videoFolderPath = "";
    shm.create_str("ScreenShotPath", 512);
    // shm.create("DefectCapture", 512);

    if (IPAddressStatus = findCommandModule(commandModule_HMAC))
    {
        std::cout << "IPStatus: " << IPAddressStatus << std::endl;
        std::cout << "Found Command Module IP: " << commandModule_IP << std::endl;
    }
    if (IPAddressStatus)
    {
        remote::session session;
        session.start(remote::make_tcp_binding(commandModule_IP, 8888));
        if (session.wait_for_ready() != remote::session::started)
        {
            std::cout << "[Remote Vision Client][Main]Remote Vision Server Dint Find - Exiting the Remote Client" << std::endl;
            return -1;
        }
        boost::shared_ptr<remoteVision> vision = session.get<remoteVision>("remoteVisionserverInstance");
        vision->startVision();
        sleep(3);
        remote::future<bool> f = vision->getVisionStatus();
        visionStatus = f.get();
        if (visionStatus)
        {
            shm.create_str("videoFolderPath", 1024);
            shm.create("frame_count", 1024, 2);
            vision->initializeScreenShot();
            std::cout << "Vision Started in the Server" << std::endl;
        }
        //Wait Remote Client to Exit
        while ((!stopVisionBool) && visionStatus)
        {
            usleep(500000);
            remote::future<bool> f = vision->getVisionStatus();
            visionStatus = f.get();
            videoFolderPath = vision->getVisionFolderPath();
            primaryCameraFrameCount = vision->primaryCameraFrameCount();
            secondaryCameraFrameCount = vision->secondaryCameraFrameCount();

            shm.update_str("videoFolderPath", videoFolderPath);
            shm.update("frame_count", 1, primaryCameraFrameCount);
            shm.update("frame_count", 2, secondaryCameraFrameCount);
            if (shm.fetch("DefectCapture") == 1.0)
            {
                std::cout << "Screenshot is triggered" << std::endl;
                vision->triggerScreenShot();
                remote::future<bool> screenshot = vision->fetchScreenShot();
                screenShotStatus = screenshot.get();
                while ((!stopVisionBool) && screenShotStatus)
                {
                    remote::future<bool> screenshot = vision->fetchScreenShot();
                    screenShotStatus = screenshot.get();
                    usleep(1000);
                }
                shm.update_str("ScreenShotPath", vision->getscreenShotPath());
                shm.update("DefectCapture", 0.0);
            }
        }

        vision->stopVision();

        //wait remote Server
        while (visionStatus)
        {
            usleep(500000);
            remote::future<bool> f = vision->getVisionStatus();
            visionStatus = f.get();
        }
        shm.update_str("videoFolderPath", "");
        shm.update_str("ScreenShotPath", "");

        std::cout << "Vision in Command module is Closed" << std::endl;
    }

    return 0;
}
