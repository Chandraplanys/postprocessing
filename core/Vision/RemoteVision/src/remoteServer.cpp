#include "videoServer.hpp"
#include "remoteVision.hpp"

#include <remote/server.hpp>
#include <remote/make_tcp_binding.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <iostream>

#ifdef REMOTE_USER_REGISTER_ARCHIVE
#include <remote/register_archive.ipp>
#endif

bool REMOTESERVER_FLAG = true;

void shutDown()
{
    if (REMOTESERVER_FLAG)
    {
        REMOTESERVER_FLAG = false;
        std::string visionKill = "sudo kill -2 $(pidof vision)";
        int ret = system(visionKill.c_str());

        std::cout << "Exiting Remote Server Vision" << std::endl;
    }
}
void handler(int s)
{

    printf("Interupt signal Received --->%d<---- Vision Remote Server\n", s);

    // Broken pipe from a client
    if (s == SIGPIPE)
    {

        // Just ignore, sock class will close the client the next time
    }
    // Catch ctrl-c, segmentation fault, abort signal
    if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTERM)
    {

        shutDown();
    }
}
void initSigHandler()
{

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGPIPE, &sigIntHandler, NULL);
    sigaction(SIGABRT, &sigIntHandler, NULL);
    sigaction(SIGSEGV, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);
}

int main()
{
    VideoServer remoteVisionServer;

    initSigHandler();
    std::cout << "RemoteTCP Server Starting" << std::endl;
    remote::server server;
    server.bind<remoteVision>(&remoteVisionServer, "remoteVisionserverInstance");
    server.start(remote::make_tcp_binding(8888));

    while (REMOTESERVER_FLAG)
    {
        sleep(1);
    }

    (void)pthread_join(remoteVisionServer.wsClient_thread, NULL);
    (void)pthread_join(remoteVisionServer.vision_thread, NULL);
    return 0;
}
