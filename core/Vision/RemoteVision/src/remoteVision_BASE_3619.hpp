#ifndef REMOTEVISION_HPP
#define REMOTEVISION_HPP

#include <remote/idl.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#define REMOTE_CLASS                                   \
    REMOTE_CLASS_BEGIN(remoteVision)                   \
    REMOTE_ASYNC_M0(bool, getVisionStatus)             \
    REMOTE_METHOD_M0(void, startVision)                \
    REMOTE_METHOD_M0(void, stopVision)                 \
    REMOTE_METHOD_M0(std::string, getVisionFolderPath) \
    REMOTE_METHOD_M0(doubke, primaryCameraFrameCount)     \
    REMOTE_METHOD_M0(double, secondaryCameraFrameCount)   \
    REMOTE_CLASS_END
#include <remote/idl/class.hpp>

#define REMOTE_REGISTER_CLASS remoteVision
#include <remote/idl/register_class.hpp>

#endif