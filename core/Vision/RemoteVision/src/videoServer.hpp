#ifndef VIDEOSERVER_H
#define VIDEOSERVER_H

#include <boost/lexical_cast.hpp>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "../../../Logger/DirectoryCreator.h"
#include "../../../Logger/Logger.h"
#include "../../../SharedMemoryService/shared_memory.h"
#include "../../../SupportLibs/JsonParser/jsonParser.h"
#include "../../../WebSocketCPP/client_ws.hpp"

using namespace std;

class VideoServer
{
  SMemory shm;

public:
  pthread_t vision_thread;
  pthread_t wsClient_thread;
  std::string ROVIP;
  std::string ROV_HWMMAC = "94:c6:91:1a:81:c6";

  static void *vision(void *threadarg)
  {
    std::cout << "Starting Vision Thread" << std::endl;
    int ret = system("./../../bin/vision");
  }

  static void *wsClient(void *threadargs)
  {
    VideoServer *VideoServer_Instannce = (VideoServer *)threadargs;
    using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;
    std::string wsROV_ServerEndPoint;
    bool visionStatus = false;
    std::string videoFolderPath_log = "";
    std::string keyArray[] = {"frontcameraFrameCount",
                              "bottomcameraFrameCount",
                              "depth",
                              "altitude",
                              "thickness",
                              "heading",
                              "pitch",
                              "roll",
                              "gps_lat",
                              "gps_lng",
                              "gps_acc",
                              "gps_state"};

    Logger dataLogger;
    JsonParser dataParser;

    visionStatus = VideoServer_Instannce->checkVisionSHM();

    if (visionStatus)
    {

      std::cout << "Vision Started and Starting Logger IF" << std::endl;
      videoFolderPath_log = VideoServer_Instannce->getVisionFolderPath();
      std::cout << "Video Folder Path: " << videoFolderPath_log << std::endl;

      dataLogger.initialize(videoFolderPath_log,
                            "../../../Logger/Logger.properties", "vision");
    }
    else
    {
      while (!visionStatus)
      {
        visionStatus = VideoServer_Instannce->checkVisionSHM();
        if (visionStatus)
        {
          std::cout << "Vision Started and Starting Logger Else" << std::endl;
          videoFolderPath_log = VideoServer_Instannce->getVisionFolderPath();
          std::cout << "Video Folder Path: " << videoFolderPath_log
                    << std::endl;

          dataLogger.initialize(videoFolderPath_log,
                                "../../../Logger/Logger.properties", "vision");
        }
        usleep(200000);
      }
    }

    VideoServer_Instannce->findROV(VideoServer_Instannce->ROV_HWMMAC);

    if (VideoServer_Instannce->ROVIP != "")
    {
      wsROV_ServerEndPoint = (VideoServer_Instannce->ROVIP) + ":8089/echo";
      WsClient client(wsROV_ServerEndPoint);

      client.on_message = [&VideoServer_Instannce, &visionStatus, &dataLogger,
                           &dataParser, &keyArray,
                           &client](shared_ptr<WsClient::Connection> connection,
                                    shared_ptr<WsClient::Message> message) {
        // cout << "Client: Message received: \"" << message->string() << "\""
        // << endl;

        auto shakeHand_message = "Hello";
        std::string csvString = "";

        // cout << "Client: Sending message: \"" << shakeHand_message << "\"" <<
        // endl;

        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << shakeHand_message;
        if (visionStatus)
        {
          csvString = dataParser.getcsv(message->string(), keyArray, 12);
          dataLogger.info(csvString);
          connection->send(send_stream);
          visionStatus = VideoServer_Instannce->getVisionStatus();
        }
        else
        {
          connection->send_close(1000);
        }
      };

      client.on_open = [](shared_ptr<WsClient::Connection> connection) {
        cout << "Client: Opened connection" << endl;

        std::string message = "Hello";
        cout << "Client: Sending message: \"" << message << "\"" << endl;

        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << message;
        connection->send(send_stream);
      };

      client.on_close = [](shared_ptr<WsClient::Connection> /*connection*/,
                           int status, const string & /*reason*/) {
        cout << "Client: Closed connection with status code " << status << endl;
      };

      // See
      // http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html,
      // Error Codes for error code meanings
      client.on_error = [](shared_ptr<WsClient::Connection> /*connection*/,
                           const SimpleWeb::error_code &ec) {
        cout << "Client: Error: " << ec << ", error message: " << ec.message()
             << endl;
      };

      client.start();
    }
  }

  void startVision()
  {
    pthread_create(&this->vision_thread, NULL, vision, NULL);
    pthread_create(&this->wsClient_thread, NULL, wsClient, this);
  }

  void stopVision()
  {
    std::cout << "[Video Server][StopVision]" << std::endl;
    std::string programName = "vision";

    int PID = getPID(programName);

    if (PID)
    {
      std::string pid = boost::lexical_cast<std::string>(PID);
      std::string killProgram_Command = "kill -2 " + pid;

      int ret = system(killProgram_Command.c_str());
      std::cout << "[Warning!!!][Vision Server][Kill Program] " << programName
                << " Terminated" << std::endl
                << std::endl;
    }
    else
    {
      std::cout << "PID vision Not Found" << std::endl;
    }
  }

  bool getVisionStatus()
  {
    int PID = getPID("vision");
    if (PID)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  int getPID(std::string programName)
  {
    int PID = 0;
    char buf[512];
    std::string extractPID_Command;

    extractPID_Command = "pidof " + programName;
    // std::cout << "PID Command: " << extractPID_Command << std::endl;

    FILE *cmd_pipe = popen(extractPID_Command.c_str(), "r");

    char *returnChar = fgets(buf, 512, cmd_pipe);

    pclose(cmd_pipe);
    if (returnChar != NULL)
    {
      PID = atoi(buf);
      // std::cout << "PID Value: " << PID << std::endl;

      if (PID)
      {
        return PID;
      }
      else
      {
        return 0;
      }
    }
    else
    {
      return 0;
    }
  }

  std::string getVisionFolderPath()
  {
    std::string vpath = "";

    try
    {
      shm.create_str("videoFolderPath", 1024);
      vpath = shm.fetch_str("videoFolderPath");
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][Get Folder Path] " << e.what()
                << std::endl;
    }

    return vpath;
  }

  bool checkVisionSHM()
  {
    bool cVSHM = false;
    try
    {
      shm.create_bool("vision_status_flag", 1024);

      cVSHM = shm.fetch_bool("vision_status_flag");
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][Check Vision SHM] "
                << e.what() << std::endl;
    }

    return cVSHM;
  }

  double primaryCameraFrameCount()
  {
    double pCameraFC = 0;
    try
    {
      shm.create("frame_count", 1024, 2);
      pCameraFC = (double)shm.fetch("frame_count", 1);
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][primaryCameraFrameCount] "
                << e.what() << std::endl;
    }
    return pCameraFC;
  }

  double secondaryCameraFrameCount()
  {
    double sCameraFC = 0;
    try
    {
      shm.create("frame_count", 1024, 2);
      sCameraFC = (int)shm.fetch("frame_count", 2);
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][secondaryCameraFrameCount] "
                << e.what() << std::endl;
    }
    return sCameraFC;
  }

  std::string GetStdoutFromCommand(std::string cmd)
  {

    std::string data;
    FILE *stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");
    if (stream)
    {
      while (!feof(stream))
        if (fgets(buffer, max_buffer, stream) != NULL)
          data.append(buffer);
      pclose(stream);
    }
    return data;
  }

  std::string getIPAddress(std::string macAddress)
  {
    if (macAddress == "")
    {
      std::cout << "MAC Address is empty please check" << std::endl;
      return "";
    }
    else
    {

      // TO DO : Run ping for all IPs before arp -an command - ping -c 1
      // 192.168.0.X
      std::string IPAddress;
      std::string getIPCommand =
          "arp -an | grep \"" + macAddress + "\" | cut -d ' ' -f 2";
      std::cout << "Get IP Command " << getIPCommand << std::endl;
      IPAddress = GetStdoutFromCommand(getIPCommand);
      if (IPAddress == "")
      {
        std::cout << "No IP address found for the MAC address" << std::endl;
      }
      else if (IPAddress.length() > 20)
      {
        std::cout << "IP Address found if Ambigious" << std::endl;
        std::cout << "IP Address: " << IPAddress << std::endl;
      }
      else
      {
        IPAddress = IPAddress.substr(1, IPAddress.size() - 3);
        std::cout << "IP Address: " << IPAddress << std::endl;

        return IPAddress;
      }
    }
    return "";
  }

  std::string removeNewLineTrails(std::string line)
  {
    line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
    return line;
  }

  std::string getGatewayDefault()
  {
    std::string gatewayIP;
    // std::string gateway_cmd = "route | grep gateway  | grep 'UG[ \t]' | awk
    // '{print $2}'";
    std::string gateway_cmd = "ip route | awk '/default/ { print $3 }'";

    gatewayIP = removeNewLineTrails(GetStdoutFromCommand(gateway_cmd));
    std::cout << "Gateway: " << gatewayIP << std::endl;

    return gatewayIP;
  }

  void pingNetwork()
  {

    std::string ping_cmd;

    std::string IPGateway = getGatewayDefault();
    if (IPGateway != "")
    {
      std::cout << "Pinging Network Hosts" << std::endl;

      // Clear all the arp cache before populating
      int ret = system("sudo ip -s -s neigh flush all >>/dev/null 2>>/dev/null");
      ping_cmd =
          "nmap -sP " + IPGateway + "/24 -n --send-ip >>/dev/null 2>>/dev/null";
      // std::cout << "Ping Network Command: " << ping_cmd << std::endl;
      ret = system(ping_cmd.c_str());
      // std::cout << "Arp Running" << std::endl;
      ret = system("arp -an  >>/dev/null 2>>/dev/null");
    }
  }

  bool findROV(std::string ROV_HWMMAC)
  {
    std::string IPAddress = getIPAddress(ROV_HWMMAC);
    int checkingCounter = 0;

    if (IPAddress == "")
    {
      pingNetwork();

      std::cout << "Waiting for ROV IP" << std::endl;
      while (IPAddress == "")
      {
        if (checkingCounter == 8)
        {
          std::cout << "[findROV][Debug] Dint find ROV exiting the process"
                    << std::endl;
          break;
        }
        usleep(1000000);
        IPAddress = getIPAddress(ROV_HWMMAC);
        checkingCounter++;
      }
    }
    if (IPAddress != "")
    {
      this->ROVIP = IPAddress;
      return true;
    }
    else
    {
      return false;
    }
  }
  void triggerScreenShot()
  {
    try
    {
      shm.update("DefectCaptureFlag", 1.0);
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][takeScreenShot] " << e.what()
                << std::endl;
    }
  }

  bool fetchScreenShot()
  {

    if (shm.fetch("DefectCaptureFlag") == 1.0)
    {
      return true;
    }
    return false;
  }

  std::string getscreenShotPath()
  {
    std::string spath = "";
    try
    {
      spath = shm.fetch_str("ScreenShotPath");
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][getscreenShotPath] " << e.what()
                << std::endl;
    }
    return spath;
  }
  void initializeScreenShot()
  {
    try
    {
      shm.create_str("ScreenShotPath", 512);
      shm.create("DefectCaptureFlag", 512);
    }
    catch (std::exception &e)
    {
      std::cout << "[Error][Vision Remote Server][initializeScreenshot] " << e.what()
                << std::endl;
    }
  }
};

#endif