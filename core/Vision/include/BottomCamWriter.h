#ifndef BOTTOMCAMWRITER_H
#define BOTTOMCAMWRITER_H

#include <iostream>
#include <linux/videodev2.h>

//FFMPEG LIBRARIES
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavcodec/avfft.h"

#include "libavdevice/avdevice.h"

#include "libavfilter/avfilter.h"
#include "libavfilter/avfiltergraph.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"

#include "libavformat/avformat.h"
#include "libavformat/avio.h"

// libav resample

#include "libavutil/opt.h"
#include "libavutil/common.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavutil/time.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/file.h"

// lib swresample

#include "libswscale/swscale.h"

}


//#include "Config.h"
//#include "Logger.h"
//#include "Utility.h"

#define VIDEOWRITER_STATUS_DEFAULT	0
#define VIDEOWRITER_STATUS_OK		1
#define VIDEOWRITER_STATUS_ERR		2
#define VIDEOWRITER_STATUS_CLOSED	3

class BottomCamWriter
{
private:
	char *source;
	char fileName[512];
	const char *output_file;
	int unscaled_width;
	int unscaled_height;
	int scaled_width;
	int scaled_height;
	int bitrate;
	int fps;
	int gopsize;
	int maxbframes;
	char logStr[200];
	//Logger logger;
	FILE *fp;
	int status;
	unsigned int camformat;
	AVPixelFormat pixFmt;

	/* encoder variables */
	AVCodecID codecId;
	AVFrame *frame;
	AVPacket pkt;
	AVCodecContext *context;
	AVCodec *codec;

	/* container variables */
	int value;
	AVFormatContext *outAVFormatContext;
	AVOutputFormat *output_format;
	AVStream *video_st;

	/* decoder variables */
	AVCodec *dec_codec;
	AVCodecContext *dec_context;
	AVFrame *dec_frame;
	AVPacket dec_pkt;

	/* conversion variables */
	SwsContext *conversionContext;

	/* scale variables */
	AVFrame *scaled_frame;
	SwsContext *scaleContext;
	bool scale;

	//void log(char[], int);
	void bottomcam_writeDelayedFrames();
	void bottomcam_destroy();
	void bottomcam_initEncoder();

	void bottomcam_initContainer();
	void bottomcam_makeContainer();

	void bottomcam_initDecoder(AVCodecID, AVPixelFormat);
	void bottomcam_initConverter();
	void bottomcam_initScaler();
	void bottomcam_decodeMJPEG(unsigned char *, int);
	void bottomcam_encodeFrame(AVFrame *);

public:
	int inputSize;
	long long frameCount;

	BottomCamWriter();
	~BottomCamWriter();
	void bottomcam_flow_continue();

	void bottomcam_initialize(char[], AVCodecID, int, int, int, int, int, int, int, AVPixelFormat, int, unsigned int);
	void bottomcam_initialize(char[], AVCodecID, int, int, int, int, int, int, int, AVPixelFormat, int, unsigned int, char[]);
	void bottomcam_writeFrame(unsigned char *, int);
	/* conversion variables */

  void bottomcam_dumpTrailer();
	void bottomcam_close();

};

#endif
