#ifndef IPCAMERAWRITER_H
#define IPCAMERAWRITER_H

#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include "opencv2/opencv.hpp"
#include <vector>
#include "../../SharedMemoryService/shared_memory.h"
#include "Support.h"

/***
 * Log type defines
 * 
 */
#define DEFAULT 1
#define FILE_LOG 1
#define CONSOLE_LOG 2

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavdevice/avdevice.h>
#include <libavutil/timestamp.h>
#include <libswscale/swscale.h>
}

class IPCameraWriter
{
private:
  int vidx = 0, aidx = 0; // Video, Audio Index
  std::string fileName;
  bool stopStreamRead = false;
  int FPS;
  double BITRATE;

  AVFormatContext *input_ctx;
  AVDictionary *dicts;
  AVFormatContext *output_ctx;
  AVPacket readBufferPacket;
  AVStream *outPutVideoStream;
  AVCodec *in_codec = NULL;
  AVCodecContext *avctx = NULL;
  AVPacket avpkt;

public:
  SMemory shmData;
  Support support;
  char cam[512];
  struct SwsContext *ctx;
  int LOGTYPE;
  long frameCount;
  int defectFrameCount = 0;
  bool defectFrameFlag = false;
  bool defectFrameThreadFlag = true;

  bool CAMERAFOUND = false;

  pthread_t writer_Thread;
  pthread_t defectFrame_Thread;

  IPCameraWriter();

  int fetchStreamVariables(std::string streamPath);

  int setupContainerParameters(int FPS, double BITRATE);
  int initiateContainer(std::string fileName);

  void writeFrames();
  static void *defectFrame(void *arguments);

  static void *readFrames(void *arguments);
  void getFolder();
  void dumpTrailer();

  bool getWriterStatus();
  void stopWriter();

  template <class T>
  void LOG(T logData);
};

#endif