#ifndef PARSE_IMPLEMENTATION
#define PARSE_IMPLEMENTATION
#include <iostream>
#include <string>
#include <list>

#include "Support.h"

const int SIZE = 10;

using namespace std;

class Node
{
  public:
    Node();
    //Terse Constructor(Node) Definition
    Node(string n, string v) : key(n), value(v){};
    string key, value;
};

class HashMap
{
  private:
    list<Node *> data[SIZE];

  public:
    HashMap();
    ~HashMap();
    Node *get(string key);
    void put(string key, string value);
    void split_fun(HashMap *); //This constructs HashMap and parses the data and inserts into HashMap

    Support support;
    bool ENABLE_DEVMODE = false;
    void findCurrentMode();
};

#endif
