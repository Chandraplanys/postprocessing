#ifndef PARSE_SUPPORT_H
#define PARSE_SUPPORT_H

#include <iostream>
#include <string>
#include "ParseConfigure.h"
#include "ParseConfigSupport.h"

using namespace std;

class ParseConfigure
{

  HashMap HashMap_object;

public:
  ParseConfigure();
  ~ParseConfigure();

  std::string CAMERATYPE;

  std::string PRIMARYCAMERA_MAC;
  std::string SECONDARYCAMERA_MAC;

  std::string getCameraType = "CameraType";
  std::string getPrimaryCamera_MAC = "PRIMARYCAMERA_MAC";
  std::string getSecondaryCamera_MAC = "SECONDARYCAMERA_MAC";

  /* Defining configuration for Bottom Camera */
  int PARSE_BOTTOM_WIDTH;
  int PARSE_BOTTOM_HEIGHT;
  int PARSE_BOTTOM_FPS;
  int PARSE_BOTTOM_GOP_STREAMER;
  int PARSE_BOTTOM_GOP_WRITER;
  int PARSE_BOTTOM_MAX_B_FRAMES_WRITER;
  int PARSE_BOTTOM_MAX_B_FRAMES_STREAMER;
  int PARSE_BOTTOM_BITRATE_STREAMER;
  int PARSE_BOTTOM_BITRATE_WRITER;
  int PARSE_BOTTOM_PORT_NO;

  string getBottomWidh = "BOTTOM_WIDTH";
  string getBottomHeight = "BOTTOM_HEIGHT";

  string getBottomGOPstreamer = "BOTTOM_GOP_STREAMER";
  string getBottomGOPwriter = "BOTTOM_GOP_WRITER";

  string getBottomMAXBframesStreamer = "BOTTOM_MAX_B_FRAMES_WRITER";
  string getBottomMAXBframesWriter = "BOTTOM_MAX_B_FRAMES_STREAMER";

  string getBottomBitrateStream = "BOTTOM_BITRATE_STREAMER";
  string getBottomBitrateWrite = "BOTTOM_BITRATE_WRITER";

  string getBottomPortNo = "BOTTOM_PORT_NO";
  string getBottomFPS = "BOTTOM_FPS";

  /* Defining configuration for FRONT Camera */

  int PARSE_FRONT_WIDTH;
  int PARSE_FRONT_HEIGHT;
  int PARSE_FRONT_FPS;
  int PARSE_FRONT_GOP_STREAMER;
  int PARSE_FRONT_GOP_WRITER;
  int PARSE_FRONT_MAX_B_FRAMES_WRITER;
  int PARSE_FRONT_MAX_B_FRAMES_STREAMER;
  int PARSE_FRONT_BITRATE_STREAMER;
  int PARSE_FRONT_BITRATE_WRITER;
  int PARSE_FRONT_PORT_NO;
  std::string primaryCamera_UserName;
  std::string primaryCamera_Password;
  std::string secondaryCamera_UserName;
  std::string secondaryCamera_Password;

  string getFrontWidh = "FRONT_WIDTH";
  string getFrontHeight = "FRONT_HEIGHT";

  string getFrontGOPstreamer = "FRONT_GOP_STREAMER";
  string getFrontGOPwriter = "FRONT_GOP_WRITER";

  string getFrontMAXBframesStreamer = "FRONT_MAX_B_FRAMES_WRITER";
  string getFrontMAXBframesWriter = "FRONT_MAX_B_FRAMES_STREAMER";

  string getFrontBitrateStream = "FRONT_BITRATE_STREAMER";
  string getFrontBitrateWrite = "FRONT_BITRATE_WRITER";

  string getFrontPortNo = "FRONT_PORT_NO";
  string getFrontFPS = "FRONT_FPS";

  std::string getPrimaryCamera_UserName = "PRIMARYCAMERA_USERNAME";
  std::string getPrimaryCamera_Password = "PRIMARYCAMERA_PASSWORD";

  std::string getSecondaryCamera_UserName = "SECONDARYCAMERA_USERNAME";
  std::string getSecondaryCamera_Password = "SECONDARYCAMERA_PASSWORD";

  void initBottomConfigData();
  void initFrontConfigData();
  void initHashObject();
};

#endif
