#ifndef SUPPORT_H
#define SUPPORT_H

#include <iostream>
#include <sstream>
#include <cstring>
#include <signal.h>
#include <cstdlib>
#include <sys/stat.h>
#include <cstdio>
#include <time.h>
#include <unistd.h>

class Support
{

private:
public:
	bool subfolderFlag;
	/* global variables to make file name */
	char curr_time[80];
	char curr_file_bottom[100];
	char curr_file_front[100];
	char directoryName[100];

	/* global variables to make folder name */
	char ret_folder_path[512];
	char curr_date[80];
	char sub_folder_path_name[512];
	char final_folder_path[512];
	char current_folder_path[512];

	char glb_sub_folder_path_name[512];

	char sub_folder_path_name_bottom[512];
	char sub_folder_path_name_front[512];

	Support();
	~Support();

	/*  an utility function to replace a char from ':' to '_' */
	char *char_replace(char *ch);

	/* return system current time hh:mm:ss */
	char *currentTime();

	/* an utility function to get folder path without '\n' character */
	char *remove_nextLine(char *temp);

	/*  get current date, format is DD_MM_YYYY */
	char *currentDate();

	void makeFolderintoPWD(const char *folderName);

	void makeFolder(const char *dictPath);
	/* analyse the current working directory command and return the home and current user directory -> "/home/abdullah/" */
	void popen_parser(char popen_buff[]);

	/* make sub folders range from 1 .. n  */
	char makeSubFolder();

	/* make a new main directory based on system date if directory not exist */
	void makeDateFolder();

	/* create each video file name based on timing */
	char *getFileName();

	/* call fucntions which make files names and folders , returns global_subfolder_path */
	std::string folder_file_maker();

	/* check camera connectivity */
	bool Bottom_CameraConnectivity();

	/* check front camera connectivity */
	bool Front_CameraConnectivity();
};

#endif
