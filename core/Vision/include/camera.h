#ifndef CAMERA_H
#define CAMERA_H

#include <iostream>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include <cmath>
#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>


using namespace std;

#define CLEAR(x) 		memset (&(x), 0, sizeof (x))
#define U32TOFOURCC(x) (char)(x>>0),(char)(x>>8),(char)(x>>16),(char)(x>>24)

enum io_method {
        IO_METHOD_READ,
        IO_METHOD_MMAP,
        IO_METHOD_USERPTR,
};

static int xioctl(int fh, int request, void *arg)
{
        int r;

        do {
                r = ioctl(fh, request, arg);
        } while (-1 == r && EINTR == errno);

        return r;
}

struct DiscreteVideoFormat
{
	/* Format description */
	int fmtdesc_idx;
	unsigned int fmtdesc_pxfmt;
	char fmtdesc_desc[40];
	char fmtdesc_fourcc[8];

	/* Frame description*/
	int frmsize_idx;
	int frmsize_width;
	int frmsize_height;

	/* Frame interval description */
	int frmval_idx;
	int frmval_num;
	int frmval_den;
};

struct CameraControlMenuItem
{
	int menuIdx;
	char name[30];
};

struct CameraControl
{
	unsigned int id;
	char name[50];
	int value;
	int defaultValue;
	int step;
	unsigned int flags;
	int min;
	int max;
	unsigned int type;
	CameraControlMenuItem *menuItems;
	int numMenuItems;
};

struct FrameBuffer
{
	void* start;
	size_t length;
};

class camera
{

private:
      int fd;
      int io_method;
      int num_data_buffers;
      string deviceName;
      string driver;
      string card;
      char logStr[200];
      //Logger logger;
      unsigned long frame_id;
      unsigned int pixelformat;
      int width, height;
      fd_set fds;
      struct timeval devTimeOut;
      struct timeval beforeFetchTime, afterFetchTime;
      float currFetchTimeMillis;
      float defaultFps, currentFps;  // default fps at current image format
      unsigned char *currentFrameData;
      unsigned int bytesused;
      unsigned int contentLength;
      int num_formats, num_controls;
      struct DiscreteVideoFormat *allVideoFormats, currentVideoFormat;
      struct CameraControl *cameraControls;

      struct v4l2_capability v4l2_cap;
      struct v4l2_format v4l2_fmt;
      struct v4l2_requestbuffers reqbuf;
      struct v4l2_buffer v4l2_buf;
      struct FrameBuffer *frameBuff;
      enum v4l2_buf_type type;
      struct v4l2_queryctrl queryctrl;
      struct v4l2_control control;
      struct v4l2_fmtdesc fmtdesc;
      struct v4l2_frmsizeenum fszenum;
      struct v4l2_frmivalenum fvalenum;
      struct v4l2_streamparm streamparm;
      struct v4l2_querymenu querymenu;

      int openDevice();
      int closeDevice();
      void checkV4L2Compatibility();
      void initialize();
      void log(char[], int);
      void initializeReadIO(int);
      void initializeMmapIO();
      void initializeUserptrIO(int);
      void initializeCapture();
      void stopCapture();
      void uninitialize();
      bool fetchFrameData();


public:
       camera();
       ~camera();

       void changeControl(unsigned int, int);
     	 void connect(string, int, int, int, unsigned int);
     	 void disconnect();
     	 unsigned char *readFrame();
     	 void populateFormats();
     	 void listFormats();
     	 void populateControls();
     	 void listControls();
     	 void setFrameRate(int, int);
     	 void setFrameSize(int, int, unsigned int);
     	 unsigned long getFrameCount();
     	 unsigned int getBytesUsed();

};

#endif
