#ifndef MPGVIDEO_H
#define MPGVIDEO_H

#include<iostream>

//FFMPEG LIBRARIES
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavcodec/avfft.h"

#include "libavdevice/avdevice.h"

#include "libavfilter/avfilter.h"
#include "libavfilter/avfiltergraph.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"

#include "libavformat/avformat.h"
#include "libavformat/avio.h"

// libav resample

#include "libavutil/opt.h"
#include "libavutil/common.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavutil/time.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/file.h"

// lib swresample

#include "libswscale/swscale.h"

}

#define VIDEOWRITER_STATUS_DEFAULT	0
#define VIDEOWRITER_STATUS_OK		1
#define VIDEOWRITER_STATUS_ERR		2
#define VIDEOWRITER_STATUS_CLOSED	3

class mpgvideo
{
private:
	char *source;
	char fileName[60];
	const char *output_file;
	int frameCount;
	int unscaled_width;
	int unscaled_height;
	int scaled_width;
	int scaled_height;
	int bitrate;
	int fps;
	int gopsize;
	int maxbframes;
	char logStr[200];
	//Logger logger;
	FILE *fp;
	int status;
	unsigned int camformat;
	AVPixelFormat pixFmt;

	/* encoder variables */
	AVCodecID codecId;
	AVFrame *frame;
	AVPacket pkt;
	AVCodecContext *context;
	AVCodec *codec;

	/* container variables */
	int value;
	AVFormatContext *outAVFormatContext;
	AVOutputFormat *output_format;
	AVStream *video_st;

	/* decoder variables */
	AVCodec *dec_codec;
	AVCodecContext *dec_context;
	AVFrame *dec_frame;
	AVPacket dec_pkt;

	/* conversion variables */
	SwsContext *conversionContext;

	/* scale variables */
	AVFrame *scaled_frame;
	SwsContext *scaleContext;
	bool scale;

	//void log(char[], int);
	void mpg_writeDelayedFrames();
	void mpg_destroy();
	void mpg_initEncoder();

	void mpg_initContainer();
	void mpg_makeContainer();

	void mpg_initDecoder(AVCodecID, AVPixelFormat);
	void mpg_initConverter();
	void mpg_initScaler();
	void mpg_decodeMJPEG(unsigned char *, int);
	void mpg_encodeFrame(AVFrame *);

public:
	int inputSize;

	mpgvideo();
	~mpgvideo();
	void flow_continue();

	void mpg_initialize(char[], AVCodecID, int, int, int, int, int, int, int, AVPixelFormat, int, unsigned int);
	void mpg_initialize(char[], AVCodecID, int, int, int, int, int, int, int, AVPixelFormat, int, unsigned int, char[]);
	void mpg_writeFrame(unsigned char *, int);
	/* conversion variables */
//	void mpg_dumpTrailer();

	void mpg_close();

};

#endif
