#ifndef REMOTEVISION_HPP
#define REMOTEVISION_HPP

#include <remote/idl.hpp>

#define REMOTE_CLASS                    \
    REMOTE_CLASS_BEGIN(remoteVision)    \
    REMOTE_METHOD_M0(void, startVision) \
    REMOTE_METHOD_M0(void, stopVision)  \
    REMOTE_CLASS_END
#include <remote/idl/class.hpp>

#define REMOTE_REGISTER_CLASS remoteVision
#include <remote/idl/register_class.hpp>

#endif