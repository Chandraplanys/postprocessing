#ifndef VISION_H
#define VISION_H

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/thread.hpp>
//#include "Logger.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <fstream>

#include "Support.h"
#include "camera.h"
#include "mpgvideo.h"
#include "streamer.h"
#include "writer.h"

#include "../../SharedMemoryService/shared_memory.h"
#include "ParseConfigSupport.h"
#include "ParseConfigure.h"

#include "BottomCamWriter.h"
#include "IPCameraWriter.h"

//FFMPEG LIBRARIES
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavcodec/avfft.h"

#include "libavdevice/avdevice.h"

#include "libavfilter/avfilter.h"
#include "libavfilter/avfiltergraph.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"

#include "libavformat/avformat.h"
#include "libavformat/avio.h"

  // libav resample

#include "libavutil/opt.h"
#include "libavutil/common.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavutil/time.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/file.h"

  // lib swresample

#include "libswscale/swscale.h"
}

//#define BOTTOM_CAMERA_PORT	"/dev/video1"
//#define FRONT_CAMERA_PORT	"/dev/video0"

//#define BOTTOM_CAMERA_PORT "/dev/Logitech_cam_Bottom"
#define BOTTOM_CAMERA_PORT "/dev/Logitech_cam_Front"
#define FRONT_CAMERA_PORT "/dev/SonyFCB"

SMemory s_memory; /**< s_memory used for communicating with GUI - response
                     update flag*/

bool FRONT_CAMERA_ENABLE_WRITE = true;
bool BOTTOM_CAMERA_ENABLE_WRITE = true;

bool FRONT_CAMERA_ENABLE_STREAM = false;
bool BOTTOM_CAMERA_ENABLE_STREAM = false;

bool FRONT_CAMERA_ENABLE = true;
bool BOTTOM_CAMERA_ENABLE = true;
char opt;

bool USBCAMERA = false;

bool SERVICE_STATUS = false;

// #define BOTTOM_CAMERA_ENABLE true
// #define FRONT_CAMERA_ENABLE false

int BOTTOM_WIDTH;
int BOTTOM_HEIGHT;
int BOTTOM_FPS;
int BOTTOM_GOP_STREAMER;
int BOTTOM_GOP_WRITER;
int BOTTOM_MAX_B_FRAMES_WRITER;
int BOTTOM_MAX_B_FRAMES_STREAMER;
int BOTTOM_BITRATE_STREAMER;
int BOTTOM_BITRATE_WRITER;
int BOTTOM_PORT_NO;

int FRONT_WIDTH;
int FRONT_HEIGHT;
int FRONT_FPS;
int FRONT_GOP_WRITER;
int FRONT_GOP_STREAMER;
int FRONT_MAX_B_FRAMES_STREAMER;
int FRONT_MAX_B_FRAMES_WRITER;
int FRONT_BITRATE_STREAMER;
int FRONT_BITRATE_WRITER;
int FRONT_PORT_NO;

std::string CAMERATYPE;

std::string primaryCamera_MAC;
std::string secondaryCamera_MAC;

/* Logitech LED lights control values */
#define V4L2_CID_LED1_MODE 168062213
#define LED_LIGHTS_OFF 0
#define LED_LIGHTS_ON 1

#define DELAY 20000

class Vision
{

public:
  std::string tmp_IP, primaryCamera_IP, secondaryCamera_IP;
  std::string primaryCamera_UserName, primaryCamera_Password, secondaryCamera_UserName, secondaryCamera_Password;
  std::string primaryCamera_URL, secondaryCamera_URL;

  Vision();
  ~Vision();
  bool preProcess();
  void saveVideo();
  void streamVideo();
  void initiateProcessing();

  void videoBreak();
  void videoRec();
  void visionLog(std::string str);

  bool validateVideoFiles(char *front_camera_file, char *bottom_camera_file);
  bool validateVideos(char *camera_file, AVFormatContext **avformatCtx, AVCodecContext **avcodecCtx);
  void deleteTempFiles(char *tempfiles);
  std::string convertCharToString(char *charArray);
  bool Bottom_CameraConnectivity();
  bool Front_CameraConnectivity();
  bool cameraConnectivity();

  static void *timeKeeper(void *threadarg);
  static void *bottomCamWorker(void *threadarg);
  static void *frontCamWorker(void *threadarg);
  void FolderFilesData();
  void getMPGvideoFileName();
  void initConfigData();
  void EnableCameraAccess();
  bool checkIPCameraConnection(std::string macAddress);
};

#endif
