
if( process.argv.length < 3 ) {
	console.log(
		'Usage: \n' +
		'node stream-server.js <secret> [<stream-port> <websocket-port>]'
	);
	process.exit();
}

var STREAM_SECRET_FRONT = process.argv[2],
	STREAM_PORT_FRONT = process.argv[3] || 8082,
	WEBSOCKET_PORT_FRONT = process.argv[4] || 8084,
	STREAM_MAGIC_BYTES_FRONT = 'jsmp'; // Must be 4 bytes

var STREAM_SECRET_bottom = process.argv[2],
	STREAM_PORT_bottom = process.argv[3] || 8083,
	WEBSOCKET_PORT_bottom = process.argv[4] || 8085,
	STREAM_MAGIC_BYTES_bottom = 'jsmp'; // Must be 4 bytes


var front_width = 1280,
	front_height = 720;

var bottom_width = 800,
	bottom_height = 448;

// Websocket Server
var socketServer_FRONT = new (require('ws').Server)({port: WEBSOCKET_PORT_FRONT});
socketServer_FRONT.on('connection', function(socket) {
	// Send magic bytes and video size to the newly connected socket
	// struct { char magic[4]; unsigned short width, height;}
	var streamHeader = new Buffer(8);
	streamHeader.write(STREAM_MAGIC_BYTES_FRONT);
	streamHeader.writeUInt16BE(front_width, 4);
	streamHeader.writeUInt16BE(front_height, 6);
	socket.send(streamHeader, {binary:true});

	console.log( 'New WebSocket Connection ('+socketServer_FRONT.clients.length+' total)' );

	socket.on('close', function(code, message){
		console.log( 'Disconnected WebSocket ('+socketServer_FRONT.clients.length+' total)' );
	});
});

var socketServer_bottom = new (require('ws').Server)({port: WEBSOCKET_PORT_bottom});
socketServer_bottom.on('connection', function(socket) {
	// Send magic bytes and video size to the newly connected socket
	// struct { char magic[4]; unsigned short width, height;}
	var streamHeader = new Buffer(8);
	streamHeader.write(STREAM_MAGIC_BYTES_bottom);
	streamHeader.writeUInt16BE(bottom_width, 4);
	streamHeader.writeUInt16BE(bottom_height, 6);
	socket.send(streamHeader, {binary:true});

	console.log( 'New WebSocket Connection ('+socketServer_bottom.clients.length+' total)' );

	socket.on('close', function(code, message){
		console.log( 'Disconnected WebSocket ('+socketServer_bottom.clients.length+' total)' );
	});
});


socketServer_FRONT.broadcast = function(data, opts) {
	for( var i in this.clients ) {
		if (this.clients[i].readyState == 1) {
			this.clients[i].send(data, opts);
		}
		else {
			console.log( 'Error: Client ('+i+') not connected.' );
		}
	}
};

socketServer_bottom.broadcast = function(data, opts) {
	for( var i in this.clients ) {
		if (this.clients[i].readyState == 1) {
			this.clients[i].send(data, opts);
		}
		else {
			console.log( 'Error: Client ('+i+') not connected.' );
		}
	}
};


// HTTP Server to accept incomming MPEG Stream
var streamServer_FRONT = require('http').createServer( function(request, response) {
	var params = request.url.substr(1).split('/');

	if( params[0] == STREAM_SECRET_FRONT) {
		response.connection.setTimeout(0);

		 var width = (params[1] || 320)|0;
		var height = (params[2] || 240)|0;

		console.log(
			'Stream Connected: ' + request.socket.remoteAddress +
			':' + request.socket.remotePort + ' size: ' + width + 'x' + height
		);
		request.on('data', function(data){
			socketServer_FRONT.broadcast(data, {binary:true});
		});
	}
	else {
		console.log(
			'Failed Stream Connection: '+ request.socket.remoteAddress +
			request.socket.remotePort + ' - wrong secret.'
		);
		response.end();
	}
}).listen(STREAM_PORT_FRONT);


var streamServer_bottom = require('http').createServer( function(request, response) {
	var params = request.url.substr(1).split('/');

	if( params[0] == STREAM_SECRET_bottom) {
		response.connection.setTimeout(0);

		var width = (params[1] || 320)|0;
		var height = (params[2] || 240)|0;

		console.log(
			'Stream Connected: ' + request.socket.remoteAddress +
			':' + request.socket.remotePort + ' size: ' + width + 'x' + height
		);
		request.on('data', function(data){
			socketServer_bottom.broadcast(data, {binary:true});
		});
	}
	else {
		console.log(
			'Failed Stream Connection: '+ request.socket.remoteAddress +
			request.socket.remotePort + ' - wrong secret.'
		);
		response.end();
	}
}).listen(STREAM_PORT_bottom);

console.log('Listening for MPEG Stream on http://127.0.0.1:'+STREAM_PORT_FRONT+'/<secret>/<width>/<height>');
console.log('Awaiting WebSocket connections on ws://127.0.0.1:'+WEBSOCKET_PORT_FRONT+'/');
