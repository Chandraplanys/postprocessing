#include "../include/IPCameraWriter.h"

IPCameraWriter::IPCameraWriter()
{
    frameCount = 0;

    av_register_all();
    avcodec_register_all();
    avformat_network_init();
    this->input_ctx = NULL;
    this->dicts = NULL;
    this->output_ctx = NULL;

    this->LOGTYPE = CONSOLE_LOG;
}

int IPCameraWriter::fetchStreamVariables(std::string streamPath)
{
    int rc = av_dict_set(&this->dicts, "rtsp_transport", "tcp", 0); // default udp. Set tcp interleaved mode
    if (rc < 0)
    {
        LOG("[IPCameraWriter][fetchStreamVariables][ERROR]Could not set av dictionary to tcp");
        return 0;
    }
    this->input_ctx = avformat_alloc_context();

    //open rtsp
    if (avformat_open_input(&this->input_ctx, streamPath.c_str(), NULL, &this->dicts) != 0)
    {

        LOG("[IPCameraWriter][fetchStreamVariables][ERROR]Could not RTSP Stream Path");
        return 0;
    }
    // get context
    if (avformat_find_stream_info(input_ctx, NULL) < 0)
    {

        LOG("[IPCameraWriter][fetchStreamVariables][ERROR]Could not find stream info");
        return 0;
    }
    this->CAMERAFOUND = true;
    return 1;
}

int IPCameraWriter::setupContainerParameters(int FPS, double BITRATE)
{
    if (this->CAMERAFOUND)
    {
        if (FPS > 0 && BITRATE > 0)
        {
            this->FPS = FPS;
            this->BITRATE = BITRATE;

            return 1;
        }
        else
        {
            LOG("[IPCameraWriter][setupContainerParameters][ERROR]Please Check Input arguments");
            return 0;
        }
    }
}

int IPCameraWriter::initiateContainer(std::string fileName)
{
    if (this->CAMERAFOUND)
    {
        if (fileName == "")
        {
            LOG("[IPCameraWriter][initiateContainer][ERROR]File Name for Container is Empty");
            return 0;
        }
        else
        {
            this->fileName = fileName;
            std::cout << "File Name: " << fileName << std::endl;
            if ((avformat_alloc_output_context2(&this->output_ctx, NULL, NULL, this->fileName.c_str())) >= 0)
            {
                std::cout << "Context Allocation Success" << std::endl;
            }

            // search video stream , audio stream
            std::cout << "NB Stream: " << this->input_ctx->nb_streams << std::endl;
            for (int i = 0; i < this->input_ctx->nb_streams; i++)
            {
                if (this->input_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
                {
                    //video index for video storing
                    this->vidx = i;
                    // codeccontext for screenshot capture
                    this->avctx = this->input_ctx->streams[i]->codec;
                    this->in_codec = avcodec_find_decoder(avctx->codec_id);
                    if (!in_codec)
                    {
                        fprintf(stderr, "in codec not found\n");
                    }
                    break;
                    // std::cout << "Found Stream Index: " << vidx << std::endl;
                }
            }
            if (avcodec_open2(avctx, in_codec, NULL) < 0)
            {
                fprintf(stderr, "[IPCameraWriter][initiateContainer] Not processed ");
            }
            //open output file
            if (this->output_ctx == NULL)
            {
                std::cout << "[IPCameraWriter][initiateContainer][ERROR]Out put file is not set" << std::endl;
                LOG("[IPCameraWriter][initiateContainer][ERROR]Out put file is not set");
                return 0;
            }
            //Assign codec from input to output
            this->outPutVideoStream = avformat_new_stream(this->output_ctx, this->input_ctx->streams[vidx]->codec->codec);

            //Copy Codec contex of input to output
            avcodec_copy_context(this->outPutVideoStream->codec, this->input_ctx->streams[vidx]->codec);

            this->outPutVideoStream->time_base = (AVRational){1, FPS};
            this->outPutVideoStream->sample_aspect_ratio = this->input_ctx->streams[vidx]->codec->sample_aspect_ratio;
            this->output_ctx->bit_rate = this->BITRATE;

            avio_open(&this->output_ctx->pb, this->fileName.c_str(), AVIO_FLAG_WRITE);
            avformat_write_header(this->output_ctx, NULL);
        }
    }
}

void IPCameraWriter::writeFrames()
{
    if (this->CAMERAFOUND)
    {
        pthread_create(&this->writer_Thread, NULL, this->readFrames, (void *)this);
        usleep(10000);
        pthread_create(&this->defectFrame_Thread, NULL, this->defectFrame, (void *)this);
    }
}

void *IPCameraWriter::readFrames(void *arguments)
{
    int ts = 0;
    int nRecvPacket = 0;

    IPCameraWriter *IPCamObject = (IPCameraWriter *)arguments;
    av_init_packet(&IPCamObject->readBufferPacket);

    while (!IPCamObject->stopStreamRead)
    {
        if (IPCamObject->CAMERAFOUND)
        {
            nRecvPacket = av_read_frame(IPCamObject->input_ctx, &IPCamObject->readBufferPacket);

            if (nRecvPacket >= 0)
            {
                if (IPCamObject->readBufferPacket.stream_index == IPCamObject->vidx) // video frame
                {
                    // packet.stream_index = vidx;

                    IPCamObject->readBufferPacket.pts = ts;
                    IPCamObject->readBufferPacket.dts = ts;

                    IPCamObject->outPutVideoStream->id = IPCamObject->vidx;
                    if (IPCamObject->readBufferPacket.pts != AV_NOPTS_VALUE)
                    {
                        IPCamObject->readBufferPacket.duration = av_rescale_q(IPCamObject->readBufferPacket.duration, IPCamObject->input_ctx->streams[IPCamObject->readBufferPacket.stream_index]->time_base, IPCamObject->outPutVideoStream->time_base);
                        ts += IPCamObject->readBufferPacket.duration;

                        IPCamObject->readBufferPacket.pos = -1;
                    }
                    IPCamObject->frameCount++;

                    if (IPCamObject->defectFrameFlag)
                    {
                        AVFrame *frame, *framergb;
                        int bytes, frameFinished;
                        uint8_t *buffer, *inbuf;
                        frame = avcodec_alloc_frame();
                        if (!frame)
                        {
                            fprintf(stderr, "Could not allocate video frame\n");
                        }

                        framergb = avcodec_alloc_frame();
                        if (!framergb)
                        {
                            fprintf(stderr, "Could not allocate video frame\n");
                        }

                        bytes = avpicture_get_size(PIX_FMT_RGB24, IPCamObject->avctx->width, IPCamObject->avctx->height);
                        buffer = (uint8_t *)av_malloc(bytes * sizeof(uint8_t));
                        avpicture_fill((AVPicture *)framergb, buffer, AV_PIX_FMT_BGR24,
                                       IPCamObject->avctx->width, IPCamObject->avctx->height);
                        // av_free(buffer);

                        std::vector<int> compression_params;
                        compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
                        compression_params.push_back(100);

                        avcodec_decode_video2(IPCamObject->avctx, frame, &frameFinished, &IPCamObject->readBufferPacket);
                        // Did we get a video frame?
                        if (frameFinished)
                        {
                            // Convert the image from its native format to RGB
                            IPCamObject->ctx = sws_getContext(IPCamObject->avctx->width, IPCamObject->avctx->height, IPCamObject->avctx->pix_fmt, IPCamObject->avctx->width,
                                                              IPCamObject->avctx->height, AV_PIX_FMT_BGR24, SWS_BICUBIC, NULL, NULL, NULL);
                            sws_scale(IPCamObject->ctx, frame->data, frame->linesize, 0, IPCamObject->avctx->height,
                                      framergb->data, framergb->linesize);
                            cv::Mat img(IPCamObject->avctx->height, IPCamObject->avctx->width, CV_8UC3, framergb->data[0], framergb->linesize[0]);
                            //cv::putText(img, "Image " + std::to_string(static_cast<long long>(IPCamObject->defectFrameCount++)), cv::Point(5, 500), cv::FONT_HERSHEY_DUPLEX, 5, cv::Scalar(0, 143, 143), 2);
                            IPCamObject->getFolder();
                            std::string screenshotName=  std::string(IPCamObject->cam) + "_" + IPCamObject->support.currentTime();;
                            std::string finalScreenShotFolderpath= IPCamObject->shmData.fetch_str("ScreenShotFolderPath") + std::string(IPCamObject->cam);
                            IPCamObject->support.makeFolder(finalScreenShotFolderpath.c_str());
                            finalScreenShotFolderpath+="/";
                            IPCamObject->shmData.update_str("ScreenShotFolderPath", finalScreenShotFolderpath);
                            std::string filePath = finalScreenShotFolderpath + screenshotName + ".jpg";
                            imwrite(filePath, img, compression_params);
                            sws_freeContext(IPCamObject->ctx);
                            IPCamObject->defectFrameFlag = false;
                            IPCamObject->shmData.update("DefectCaptureFlag", 0.0);
                        }
                    }

                    // std::cout << "Frame Number: " << IPCamObject->frameCount++ << std::endl;

                    if ((av_write_frame(IPCamObject->output_ctx, &IPCamObject->readBufferPacket)) < 0)
                    {
                        av_packet_unref(&IPCamObject->readBufferPacket);
                        break;
                    }
                }

                av_packet_unref(&IPCamObject->readBufferPacket);
            }
            else
            {
                // std::string errorString = av_err2str(nRecvPacket);
                std::cout << "av_read_frame ERROR: " << nRecvPacket << std::endl;
                // av_packet_unref(&IPCamObject->readBufferPacket);
                // av_init_packet(&IPCamObject->readBufferPacket);

                // std::cout << "No Frame Received" << std::endl;
            }
        }
    }
}

void IPCameraWriter::dumpTrailer()
{
    if (this->CAMERAFOUND)
    {
        av_write_trailer(this->output_ctx);
        avio_close(this->output_ctx->pb);
        avformat_free_context(this->output_ctx);
        avformat_free_context(this->input_ctx);
        av_dict_free(&this->dicts);
    }
}

bool IPCameraWriter::getWriterStatus()
{
    return this->stopStreamRead;
}

void IPCameraWriter::stopWriter()
{
    if (!this->stopStreamRead)
    {
        this->stopStreamRead = true;
    }
}
template <class T>
void IPCameraWriter::LOG(T logData)
{
    if (this->LOGTYPE = CONSOLE_LOG)
    {
        std::cout << logData << std::endl;
    }
    else
    {
    }
}

void *IPCameraWriter::defectFrame(void *arguments)
{
    IPCameraWriter *Defect_Instance = (IPCameraWriter *)arguments;
    Defect_Instance->shmData.create_str("ScreenShotFolderPath", 512);
    Defect_Instance->shmData.create("DefectCaptureFlag", 512);
    while (Defect_Instance->defectFrameThreadFlag)
    {
        if (Defect_Instance->shmData.fetch("DefectCaptureFlag") == 1.0)
        {
            Defect_Instance->defectFrameFlag = true;
            while (Defect_Instance->defectFrameFlag)
            {
                usleep(10000);
            }
        }
        else
        {
            Defect_Instance->defectFrameFlag = false;
        }
        usleep(10000);
    }
    pthread_exit(NULL);
}
void IPCameraWriter::getFolder()
{
    // std::cout << "[getFolder]Inside" << std::endl;
    std::string vpath = this->shmData.fetch_str("videoFolderPath");
    std::string folder = "";
    for (int i = vpath.size() - 1; i >= 0; i--)
    {
        if (vpath[i] != '/')
        {
            folder += vpath[i];
        }
        else
        {
            break;
        }
    }
    if (!(folder.size() > 0))
        return;
    std::reverse(folder.begin(), folder.end());
    char sub_folder_path[512];
    strcpy(sub_folder_path, this->shmData.fetch_str("ScreenShotPath").c_str());
    strcat(sub_folder_path, folder.c_str());
    this->support.makeFolder(sub_folder_path);
    strcat(sub_folder_path, "/");
    this->shmData.update_str("ScreenShotFolderPath", sub_folder_path);
    return;
}