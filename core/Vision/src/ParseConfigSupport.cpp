#include "../include/ParseConfigSupport.h"
#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>
#include <string>

int loop_counter;

string test[10];

HashMap::HashMap() { ENABLE_DEVMODE = false; }

Node *HashMap::get(string key)
{
  string hashValue = key;
  int bucket = 15 % SIZE;
  list<Node *>::iterator iterator_object = data[bucket].begin();

  while (iterator_object != data[bucket].end())
  {
    Node **d = &(*iterator_object);
    if ((*d)->key == key)
    {
      return *d;
    }
    iterator_object++;
  }
  return NULL;
}

void HashMap::put(string key, string value)
{
  string hashValue = key;
  int bucket = 15 % SIZE;
  Node *node = this->get(key);
  if (node == NULL)
  {
    data[bucket].push_front(new Node(key, value));
  }
}

HashMap::~HashMap()
{
  for (int i = 0; i < SIZE; ++i)
  {
    list<Node *> &val = data[i];
    for (list<Node *>::iterator iterator_object = val.begin();
         iterator_object != val.end(); iterator_object++)
    {
      Node *n = *iterator_object;
      delete n;
    }
  }
}

void HashMap::findCurrentMode()
{

  ENABLE_DEVMODE = false;
}

void HashMap::split_fun(HashMap *HashMap_object)
{
  fstream fp;
  char str[100];
  int i = 0;
  string token;
  int count = 0;

  findCurrentMode();

  if (ENABLE_DEVMODE)
  {
    fp.open("DEVMODE_configure.config", ios::out | ios::in);
    if (!fp.is_open())
    {
      cout << "\n\nError : DEVMODE_Configure file not Found !\n\n";
      exit(EXIT_FAILURE);
    }
  }
  else
  {
    fp.open("../../configure.config", ios::out | ios::in);
    if (!fp.is_open())
    {
      cout << "\n\nError : Configure file not Found !\n\n";
      exit(EXIT_FAILURE);
    }
  }

  while (!fp.eof()) // The input file is processed until end
  {
    fp.getline(str, 100); // The first line is extracted and processed
    stringstream ss(str);
    while (getline(ss, token, '='))
    {
      // The String Before the "=" is stored at test[0] and the next string is
      // stored at test[1]
      test[loop_counter] = token;
      loop_counter++;
    }
    // The n+1 th line becomes nth line
    loop_counter = 0;
    // The commenting lines present in config file are ignored here
    if (test[0] == "#" | test[0] == "//")
      continue;
    HashMap_object->put(test[0], test[1]);
    test[0] = "";
    test[1] = "";
  }
  fp.close();
}
