#include "../include/ParseConfigure.h"

ParseConfigure::ParseConfigure() {}

ParseConfigure::~ParseConfigure() {}

void ParseConfigure::initHashObject()
{

  HashMap_object.split_fun(&HashMap_object);

  if (HashMap_object.get(getCameraType)) // Validates the input.Else It Will
                                         // Cause Segmentation Fault Error
  {
    CAMERATYPE = HashMap_object.get(getCameraType)->value;
  }
  if (HashMap_object.get(getPrimaryCamera_MAC)) // Validates the input.Else It Will
                                                // Cause Segmentation Fault Error
  {
    PRIMARYCAMERA_MAC = HashMap_object.get(getPrimaryCamera_MAC)->value;
  }
  if (HashMap_object.get(getSecondaryCamera_MAC)) // Validates the input.Else It Will
                                                  // Cause Segmentation Fault Error
  {
    SECONDARYCAMERA_MAC = HashMap_object.get(getSecondaryCamera_MAC)->value;
  }

  if (HashMap_object.get(getPrimaryCamera_UserName)) // Get Primary Camera User Name
  {
    primaryCamera_UserName = HashMap_object.get(getPrimaryCamera_UserName)->value;
  }

  if (HashMap_object.get(getPrimaryCamera_Password)) // Get Primary Camera Password
  {
    primaryCamera_Password = HashMap_object.get(getPrimaryCamera_Password)->value;
  }

  if (HashMap_object.get(getSecondaryCamera_UserName)) // Get Secondary Camera User Name
  {
    secondaryCamera_UserName = HashMap_object.get(getSecondaryCamera_UserName)->value;
  }

  if (HashMap_object.get(getSecondaryCamera_Password)) // Get Secondary Camera Password
  {
    secondaryCamera_Password = HashMap_object.get(getSecondaryCamera_Password)->value;
  }
}

void ParseConfigure::initBottomConfigData()
{

  if (HashMap_object.get(getBottomWidh)) // Validates the input.Else It Will
                                         // Cause Segmentation Fault Error
  {
    PARSE_BOTTOM_WIDTH = stoi(HashMap_object.get(getBottomWidh)->value);
  }
  if (HashMap_object.get(getBottomHeight))
  {
    PARSE_BOTTOM_HEIGHT = stoi(HashMap_object.get(getBottomHeight)->value);
  }

  if (HashMap_object.get(getBottomGOPstreamer))
  {
    PARSE_BOTTOM_GOP_STREAMER =
        stoi(HashMap_object.get(getBottomGOPstreamer)->value);
  }
  if (HashMap_object.get(getBottomGOPwriter))
  {
    PARSE_BOTTOM_GOP_WRITER =
        stoi(HashMap_object.get(getBottomGOPwriter)->value);
  }

  if (HashMap_object.get(getBottomMAXBframesStreamer))
  {
    PARSE_BOTTOM_MAX_B_FRAMES_STREAMER =
        stoi(HashMap_object.get(getBottomMAXBframesStreamer)->value);
  }
  if (HashMap_object.get(getBottomMAXBframesWriter))
  {
    PARSE_BOTTOM_MAX_B_FRAMES_WRITER =
        stoi(HashMap_object.get(getBottomMAXBframesWriter)->value);
  }

  if (HashMap_object.get(getBottomBitrateStream))
  {
    PARSE_BOTTOM_BITRATE_STREAMER =
        stoi(HashMap_object.get(getBottomBitrateStream)->value);
  }
  if (HashMap_object.get(getBottomBitrateWrite))
  {
    PARSE_BOTTOM_BITRATE_WRITER =
        stoi(HashMap_object.get(getBottomBitrateWrite)->value);
  }

  if (HashMap_object.get(getBottomPortNo))
  {
    PARSE_BOTTOM_PORT_NO = stoi(HashMap_object.get(getBottomPortNo)->value);
  }
  if (HashMap_object.get(getBottomFPS))
  {
    PARSE_BOTTOM_FPS = stoi(HashMap_object.get(getBottomFPS)->value);
  }
  /*
     cout<<" width"<<PARSE_BOTTOM_WIDTH<<endl;
     cout<<" height"<<PARSE_BOTTOM_HEIGHT<<endl;
     cout<<" gop streamer"<<PARSE_BOTTOM_GOP_STREAMER<<endl;
     cout<<" gop writer"<<PARSE_BOTTOM_GOP_WRITER<<endl;
     cout<<" max-b-writer"<<PARSE_BOTTOM_MAX_B_FRAMES_WRITER<<endl;

     cout<<" max-b-stream"<<PARSE_BOTTOM_MAX_B_FRAMES_STREAMER<<endl;
     cout<<" bitrate_writer"<<PARSE_BOTTOM_BITRATE_WRITER<<endl;
     cout<<" bitrate_stream"<<PARSE_BOTTOM_BITRATE_STREAMER<<endl;
     cout<<" portNo"<<PARSE_BOTTOM_PORT_NO<<endl;
     cout<<" fps"<<PARSE_BOTTOM_FPS<<endl;
    */
}

void ParseConfigure::initFrontConfigData()
{

  if (HashMap_object.get(getFrontWidh)) // Validates the input.Else It Will
                                        // Cause Segmentation Fault Error
  {
    PARSE_FRONT_WIDTH = stoi(HashMap_object.get(getFrontWidh)->value);
  }
  if (HashMap_object.get(getFrontHeight))
  {
    PARSE_FRONT_HEIGHT = stoi(HashMap_object.get(getFrontHeight)->value);
  }

  if (HashMap_object.get(getFrontGOPstreamer))
  {
    PARSE_FRONT_GOP_STREAMER =
        stoi(HashMap_object.get(getFrontGOPstreamer)->value);
  }
  if (HashMap_object.get(getFrontGOPwriter))
  {
    PARSE_FRONT_GOP_WRITER = stoi(HashMap_object.get(getFrontGOPwriter)->value);
  }

  if (HashMap_object.get(getFrontMAXBframesStreamer))
  {
    PARSE_FRONT_MAX_B_FRAMES_STREAMER =
        stoi(HashMap_object.get(getFrontMAXBframesStreamer)->value);
  }
  if (HashMap_object.get(getFrontMAXBframesWriter))
  {
    PARSE_FRONT_MAX_B_FRAMES_WRITER =
        stoi(HashMap_object.get(getFrontMAXBframesWriter)->value);
  }

  if (HashMap_object.get(getFrontBitrateStream))
  {
    PARSE_FRONT_BITRATE_STREAMER =
        stoi(HashMap_object.get(getFrontBitrateStream)->value);
  }
  if (HashMap_object.get(getFrontBitrateWrite))
  {
    PARSE_FRONT_BITRATE_WRITER =
        stoi(HashMap_object.get(getFrontBitrateWrite)->value);
  }

  if (HashMap_object.get(getFrontPortNo))
  {
    PARSE_FRONT_PORT_NO = stoi(HashMap_object.get(getFrontPortNo)->value);
  }
  if (HashMap_object.get(getFrontFPS))
  {
    PARSE_FRONT_FPS = stoi(HashMap_object.get(getFrontFPS)->value);
  }
  /*
    cout << " width" << PARSE_FRONT_WIDTH << endl;
    cout << " height" << PARSE_FRONT_HEIGHT << endl;
    cout << " gop streamer" << PARSE_FRONT_GOP_STREAMER << endl;
    cout << " gop writer" << PARSE_FRONT_GOP_WRITER << endl;
    cout << " max-b-writer" << PARSE_FRONT_MAX_B_FRAMES_WRITER << endl;

    cout << " max-b-stream" << PARSE_FRONT_MAX_B_FRAMES_STREAMER << endl;
    cout << " bitrate_writer" << PARSE_FRONT_BITRATE_WRITER << endl;
    cout << " bitrate_stream" << PARSE_FRONT_BITRATE_STREAMER << endl;
    cout << " portNo" << PARSE_FRONT_PORT_NO << endl;
    cout << " fps" << PARSE_FRONT_FPS << endl;
          */
}
