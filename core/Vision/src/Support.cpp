#include "../include/Support.h"

using namespace std;

Support::Support() {}

Support::~Support() {}

/*  an utility function to replace a char from ':' to '_' */
char *Support::char_replace(char *ch)
{

  if (ch[0] == '\0')
  {
    // exit here
  }

  int i = 0;
  while (ch[i] != '\0')
  {
    if (ch[i] == ':')
    {
      ch[i] = '_';
    }
    i++;
  }

  return ch;
}

/* return system current time hh:mm:ss */
char *Support::currentTime()
{
  time_t now = time(0);
  struct tm tstruct;
  char buf[80];
  tstruct = *localtime(&now);
  strftime(buf, sizeof(buf), "%X", &tstruct);

  strcpy(curr_time, char_replace(buf));
  return curr_time;
}

/* an utility function to get folder path without '\n' character */
char *Support::remove_nextLine(char *temp)
{

  int i, j = 0;
  int size = strlen(temp);
  char tmp;

  for (i = 0; i < size; i++)
  {
    if (temp[i] != '\n')
    {
      tmp = temp[i];
      ret_folder_path[j] = tmp;
      j++;
    }
  }
  ret_folder_path[j] = '\0';

  return ret_folder_path;
}

/*  get current date, format is DD_MM_YYYY */
char *Support::currentDate()
{
  time_t now = time(0);
  struct tm tstruct;

  tstruct = *localtime(&now);
  strftime(curr_date, sizeof(curr_date), "%d_%m_%Y", &tstruct);

  return curr_date;
}

/* analyse the current working directory command and return the home and current
 * user directory -> "/home/abdullah/" */
void Support::popen_parser(char popen_buff[])
{

  if (popen_buff[0] == '\0')
  {
    cout << "\n\nError...";
    return;
  }

  int i = 0;
  int count_slash = 0;
  char str[50];

  while (popen_buff[i] != '\0')
  {
    if (count_slash == 3)
    {
      break;
    }
    if (popen_buff[i] == '/')
    {
      count_slash++;
    }

    str[i] = popen_buff[i];
    i++;
  }

  strcpy(current_folder_path, str);
}

/* make sub folders range from 1 .. n  */
char Support::makeSubFolder()
{

  usleep(200000);
  //  FILE *fin;
  char sub_folder_path[512];
  strcpy(sub_folder_path, final_folder_path);
  strcat(sub_folder_path, "/");

  //  FOLDER MAKING CODE
  struct stat sb;
  char sub_folder_name[512]; // 1..n
  int i = 1;

  while (1)
  {
    strcpy(sub_folder_path_name, sub_folder_path);
    sprintf(sub_folder_name, "%d", i);
    strcat(sub_folder_path_name, sub_folder_name);

    // cout<<"\n\n<<<<"<<sub_folder_path_name<<">>>>\n\n";

    /* make a new char variable to make a folder inside the datefolder */

    /* To check the presence of directory */
    if (stat(sub_folder_path_name, &sb) == 0 && S_ISDIR(sb.st_mode))
    {
      //			cout<<"\n\nsub_folder_name : directory already
      // exists -- "<<sub_folder_name;
    }
    else
    {

      const int dir_err =
          mkdir(sub_folder_path_name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      // commonly used permissions set for user

      if (-1 == dir_err)
      {
        cout << "\nError creating directory\n";
        exit(EXIT_FAILURE);
      }
      else
      {
        strcpy(glb_sub_folder_path_name, sub_folder_path_name);
        break; // loop breaking point
      }
    }

    ++i;
  } // end-while loop
}
void Support::makeFolder(const char *dictPath)
{
  struct stat sb;
  char sub_folder_path[512];
  strcpy(sub_folder_path, dictPath);
  if (stat(sub_folder_path, &sb) == 0 && S_ISDIR(sb.st_mode))
  {
    //			cout<<"\n\nsub_folder_name : directory already
    // exists -- "<<sub_folder_name;
  }
  else
  {

    const int dir_err =
        mkdir(sub_folder_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    // commonly used permissions set for user

    if (-1 == dir_err)
    {
      std::cout << "\n[makeFolder]Error creating directory\n";
      exit(EXIT_FAILURE);
    }
  }
  return;
}

void Support::makeFolderintoPWD(const char *folderName)
{
  FILE *in;
  char popen_buff[512];

  // gives current home directory path.
  if (!(in = popen("pwd", "r")))
  {
    exit(EXIT_FAILURE);
  }

  while (fgets(popen_buff, sizeof(popen_buff), in) != NULL)
  {
    // cout<<popen_buff;
  }
  pclose(in);

  popen_parser(popen_buff);
  strcat(current_folder_path, folderName);
  makeFolder(current_folder_path);
}

/* make a new main directory based on system date if directory not exist */
void Support::makeDateFolder()
{

  FILE *in;
  char popen_buff[512];

  // gives current home directory path.
  if (!(in = popen("pwd", "r")))
  {
    exit(EXIT_FAILURE);
  }

  while (fgets(popen_buff, sizeof(popen_buff), in) != NULL)
  {
    // cout<<popen_buff;
  }
  pclose(in);

  popen_parser(popen_buff);

  struct stat sb;

  char folder_name[512]; // system date as folder name

  strcpy(folder_name, currentDate());

  strcat(current_folder_path, directoryName);
  strcat(current_folder_path, folder_name);

  strcpy(final_folder_path, remove_nextLine(current_folder_path));

  if (stat(final_folder_path, &sb) == 0 && S_ISDIR(sb.st_mode))
  {
  }
  else
  {
    // cout<<"NO\n";
    const int dir_err =
        mkdir(final_folder_path,
              S_IRWXU | S_IRWXG | S_IROTH |
                  S_IXOTH); // commonly used permissions set for user
    if (-1 == dir_err)
    {
      cout << "\n[makeDateFolder]Error creating directory\n";
      exit(EXIT_FAILURE);
    }
    else
    {
      cout << "\nDirectory created successfully!";
    }
  }
  if (subfolderFlag)
  {
    makeSubFolder();
  }
  else
  {
    strcpy(glb_sub_folder_path_name, final_folder_path);
    strcat(glb_sub_folder_path_name, "/");
  }
}
/* create each video file name based on timing */
char *Support::getFileName()
{

  char curr_time[100];
  char format[] = ".avi";
  char bottom_cam[] = "bottomcam_";
  char front_cam[] = "frontcam_";

  strcpy(curr_time, currentTime());

  strcpy(curr_file_bottom, bottom_cam);
  strcat(curr_file_bottom, curr_time);
  strcat(curr_file_bottom, format);

  strcpy(curr_file_front, front_cam);
  strcat(curr_file_front, curr_time);
  strcat(curr_file_front, format);
}

std::string Support::folder_file_maker()
{

  makeDateFolder();
  usleep(20000); // uSLEEP
  getFileName();

  if ((curr_file_bottom[0] != '\0') && (curr_file_front[0] != '\0'))
  {

    strcat(sub_folder_path_name, "/");

    strcpy(sub_folder_path_name_bottom, sub_folder_path_name);
    strcpy(sub_folder_path_name_front, sub_folder_path_name);

    strcat(sub_folder_path_name_bottom, curr_file_bottom);
    strcat(sub_folder_path_name_front, curr_file_front);
  }

  char global_subfolder_path[100];
  strcpy(global_subfolder_path, glb_sub_folder_path_name);
  strcat(global_subfolder_path, "/");
  cout << "#############" << global_subfolder_path << "###################";

  return std::string(global_subfolder_path);
}
