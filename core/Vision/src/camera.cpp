#include "../include/camera.h"

using namespace std;

camera::camera()
{
}

camera::~camera()
{
}

void camera::disconnect()
{

	stopCapture();
	uninitialize();

	if (closeDevice() == -1)
	{
		////LOG(ERROR) << " cannot close device ";
		std::cout << "cannot close device\n";
		exit(EXIT_FAILURE);
	}
	else
	{
     		//LOG(INFO) << " device closed successfully ";
	}
}

int camera::closeDevice()
{
	return close(fd);
}

void camera::uninitialize()
{

	switch (io_method)
	{
		case IO_METHOD_READ:
			free(frameBuff[0].start);
			break;
		case IO_METHOD_MMAP:
			for (int i = 0; i < num_data_buffers; i++)
			{
				if (munmap(frameBuff[i].start, frameBuff[i].length) == -1)
				{
					//sprintf(logStr, "munmap error, %d, %s", errno, strerror(errno));
					std::cout << "munmap error\n";
					////LOG(logStr, LOG_ERROR); // ++++++++++++++++++
          				//LOG(ERROR)<<" munmap error ";
					exit(EXIT_FAILURE);
				}
			}
			break;
		case IO_METHOD_USERPTR:
			for (int i = 0; i < num_data_buffers; i++)
			{
				free(frameBuff[i].start);
			}
			break;
	}

	free(frameBuff);
}

void camera::stopCapture()
{

	switch (io_method)
	{
		case IO_METHOD_READ:
			break;
		case IO_METHOD_MMAP:
		case IO_METHOD_USERPTR:
			type =  V4L2_BUF_TYPE_VIDEO_CAPTURE;

			if (xioctl(fd, VIDIOC_STREAMOFF, &type) == -1)
			{
				//sprintf(logStr, "VIDIOC_STREAMOFF error, %d, %s", errno, strerror(errno));
				////LOG(logStr, LOG_ERROR);
         			//LOG(INFO) << " IDIOC_STREAMOFF error "; // +++++++++++++++++++++
				exit(EXIT_FAILURE);
			}
			break;
	}
}

int camera::openDevice()
{

	fd = open(deviceName.c_str(), O_RDWR | O_NONBLOCK, 0); // O_RDWR for read/write.

	return fd;
/*
O_NONBLOCK
    When opening a FIFO with O_RDONLY or O_WRONLY set: If O_NONBLOCK is set:

        An open() for reading only will return without delay. An open() for writing only will return an error if no process currently has the file open for reading.
*/

}


void camera::changeControl(unsigned int opt_id , int opt_value)
{

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
//cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";
cout<<"Inside change control "<<endl<<endl;

	CLEAR(queryctrl);
	queryctrl.id = opt_id;

	if (-1 == ioctl (fd, VIDIOC_QUERYCTRL, &queryctrl))
	{
		if (errno != EINVAL)
		{
			//sprintf(logStr, "VIDIOC_QUERYCTRL error, %d, %s", errno, strerror(errno));
			//log(logStr, LOG_ERROR);
			return;
		}
		else
		{
			//sprintf(logStr, "%s is not supported", controlIdToString(opt_id).c_str());
			//log(logStr, LOG_ERROR);
			return;
		}
	}
	else
	{
		if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
		{
			//sprintf(logStr, "%s is disabled", controlIdToString(opt_id).c_str());
			//log(logStr, LOG_ERROR);
			return;
		}
		if (queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE)
		{
			//sprintf(logStr, "%s is inactive", controlIdToString(opt_id).c_str());
			//log(logStr, LOG_ERROR);
			return;
		}
		if (queryctrl.flags & V4L2_CTRL_FLAG_READ_ONLY)
		{
			//sprintf(logStr, "%s is read only", controlIdToString(opt_id).c_str());
			//log(logStr, LOG_ERROR);
			return;
		}
		if (queryctrl.flags & V4L2_CTRL_FLAG_GRABBED)
		{
			//sprintf(logStr, "%s is temporarily unavailable", controlIdToString(opt_id).c_str());
			//log(logStr, LOG_ERROR);
			return;
		}

		/* Set control value */
		CLEAR(control);
		control.id 		= opt_id;
		control.value	= opt_value;

		if (0 == ioctl (fd, VIDIOC_S_CTRL, &control))
		{
		//	sprintf(logStr, "control %s value changed to %d for requested value %d",
		//			controlIdToString(control.id).c_str(), control.value, opt_value);
		//	if (control.value == opt_value)// log(logStr, LOG_SUCCESS);
		//	else 						//	log(logStr, LOG_WARNING);
			return;
		}
		else
		{
			if (errno != EINVAL)
			{
				//sprintf(logStr, "VIDIOC_S_CTRL error, %d, %s", errno, strerror(errno));
				//log(logStr, LOG_ERROR);
				return;
			}
		}

	}

}


unsigned char *camera::readFrame()
{

	for(;;)
	{
		gettimeofday(&beforeFetchTime, NULL);

		// File descriptor input watch setings
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		devTimeOut.tv_sec = 1;
		devTimeOut.tv_usec = 0;

		int r = select(fd + 1, &fds, NULL, NULL, &devTimeOut);

		if (r == -1)
		{
			if (errno == EINTR)	continue;
       			//LOG(ERROR) << " select error ";
			//sprintf(logStr, "select error, %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR); // +++++++++++++++++++++
			std::cout<<" \x1B[31m [ERROR][Camera Class][Read Frame] select error with r=-1 \x1B[0m"<<std::endl;
			exit(EXIT_FAILURE);
		}

		if (r == 0)
		{
				std::cout<<" \x1B[31m [ERROR][Camera Class][Read Frame] select error with r=0 \x1B[0m"<<std::endl;
      			//LOG(ERROR) << " select timeout ";
						return NULL;
			//exit(EXIT_FAILURE);
		}


		if (fetchFrameData())
		{
			// Frame count
			frame_id++;

			// Calculate fetch time and fps
			gettimeofday(&afterFetchTime, NULL);
			currFetchTimeMillis = (afterFetchTime.tv_sec - beforeFetchTime.tv_sec)*1000 +
									(float)(afterFetchTime.tv_usec - beforeFetchTime.tv_usec)/1000;
			currentFps = (int)1000/ceil(currFetchTimeMillis);
			currentFps = currentFps == 1000 ? 0 : currentFps;

		// Log
		//sprintf(logStr,"frame #%lu, bytesused: %d, content-length: %d, current fps: %2.1f @ %d x %d, fetch time: %2.1f ms",
		//frame_id, bytesused, contentLength, currentFps, width, height, currFetchTimeMillis);
		////LOG(logStr, LOG_INFO);
////LOG(INFO) << "  ";    // ++++++++++++++
			return currentFrameData;

			break;
		}

	}

}

bool camera::fetchFrameData()
{

	switch (io_method)
	{
		case IO_METHOD_READ:
			if (read(fd, frameBuff[0].start, frameBuff[0].length) == -1)
			{
				switch (errno)
				{
					case EAGAIN:
						return 0;
						break;
					case EIO:
           //LOG(ERROR) << " fetchFrameData: I/O error ";

						break;
					default:
           //LOG(INFO) << " fetchFrameData: read ";  // +++++++++++++++++++++
						//sprintf(logStr, "fetchFrameData: read, %d, %s", errno, strerror(errno));
						break;
				}
				////LOG(logStr, LOG_ERROR);
					std::cout<<" \x1B[31m [ERROR][Camera Class][Fetch Frame Data] Read frame error with r=-1 \x1B[0m"<<std::endl;
				exit(EXIT_FAILURE);
			}

			currentFrameData = (unsigned char*) frameBuff[0].start;
			break;
		case IO_METHOD_MMAP:
			// Dequeue buffer
			if (xioctl(fd, VIDIOC_DQBUF, &v4l2_buf) == -1)
			{
				switch (errno)
				{
					case EAGAIN:
						return 0;
						break;
					case EIO:
           //LOG(ERROR) << " fetchFrameData: I/O error ";

						break;
					default:
           //LOG(ERROR) << " VIDIOC_DQBUF ";
						//sprintf(logStr, "VIDIOC_DQBUF, %d, %s", errno, strerror(errno)); // +++++++++++++++++++++
						break;
				}
				////LOG(logStr, LOG_ERROR);
				std::cout<<" \x1B[31m [ERROR][Camera Class][Fetch Frame Data] Case: IO_METHOD_MMAP deqeue xioctl error with -1 \x1B[0m"<<std::endl;

				exit(EXIT_FAILURE);
			}

			assert(v4l2_buf.index < num_data_buffers);

			bytesused		= v4l2_buf.bytesused;
			contentLength 		= frameBuff[v4l2_buf.index].length;
			currentFrameData 	= (unsigned char*) frameBuff[0].start;

			// Re-enqueue the buffer
			if (xioctl(fd, VIDIOC_QBUF, &v4l2_buf) == -1)
			{

         //LOG(ERROR) << " VIDIOC_QBUF "; // +++++++++++++++++++++
				//sprintf(logStr, "VIDIOC_QBUF, %d, %s", errno, strerror(errno));
				////LOG(logStr, LOG_ERROR);
				std::cout<<" \x1B[31m [ERROR][Camera Class][Fetch Frame Data] Case: IO_METHOD_MMAP re enqeue xioctl error with -1 \x1B[0m"<<std::endl;

				exit(EXIT_FAILURE);
			}

			break;
		case IO_METHOD_USERPTR:
			// Dequeue buffer
			if (xioctl(fd, VIDIOC_DQBUF, &v4l2_buf) == -1)
			{
				switch (errno) {
					case EAGAIN:
						return 0;
						break;
					case EIO:
           //LOG(ERROR) << " fetchFrameData: I/O error ";

						break;
					default:
           //LOG(ERROR) << " VIDIOC_DQBUF "; // +++++++++++++++++++++
						//sprintf(logStr, "VIDIOC_DQBUF, %d, %s", errno, strerror(errno));
						break;
				}
				////LOG(logStr, LOG_ERROR);
				std::cout<<" \x1B[31m [ERROR][Camera Class][Fetch Frame Data] case IO_METHOD_USERPTR: deqeue xioctl error with -1 \x1B[0m"<<std::endl;

				exit(EXIT_FAILURE);
			}

			// Find the index of the dequeued buffer
			int i;
			for (i = 0; i < num_data_buffers; i++)
			{
				if (v4l2_buf.m.userptr == (unsigned long) frameBuff[i].start && v4l2_buf.length == frameBuff[i].length)
					break;
			}

			assert(i < num_data_buffers);

			bytesused		= v4l2_buf.bytesused;
			contentLength 		= v4l2_buf.length;
			currentFrameData 	= (unsigned char*) v4l2_buf.m.userptr;

			// Re-enqueue the buffer
			if (xioctl(fd, VIDIOC_QBUF, &v4l2_buf) == -1)
			{

      //LOG(ERROR) << " VIDIOC_QBUF "; // +++++++++++++++++++++
				//sprintf(logStr, "VIDIOC_QBUF, %d, %s", errno, strerror(errno));
				////LOG(logStr, LOG_ERROR);
				std::cout<<" \x1B[31m [ERROR][Camera Class][Fetch Frame Data] case IO_METHOD_USERPTR: re enqeue xioctl error with -1 \x1B[0m"<<std::endl;

				exit(EXIT_FAILURE);
			}
			break;
	}

/*Frame data fetched successfully*/
	return 1;

}


void camera::connect(string dev, int opt_io_method, int opt_width, int opt_height, unsigned int opt_pixelformat)
{
// 	 Device configuration
  deviceName 			= dev;
  io_method  			= opt_io_method;
  width				= opt_width;
  height			= opt_height;
  pixelformat			= opt_pixelformat;
  frameBuff			= 0;
  num_data_buffers		= 1;
  frame_id			= 0;


	struct stat st;

	if (stat(deviceName.c_str(), &st) == -1)
	{
	  //sprintf(logStr, "cannot identify device: %d, %s", errno, strerror(errno));
	  ////LOG(logStr, LOG_ERROR); // +++++++++++++++++++++
		std::cout<<" \x1B[31m [ERROR][Camera Class][Connect] device check fail \x1B[0m"<<std::endl;

	  exit(EXIT_FAILURE);
	}
	if (!S_ISCHR(st.st_mode))
	{
	    //LOG(ERROR) << " not a device ";
			std::cout<<" \x1B[31m [ERROR][Camera Class][Connect] Not a device \x1B[0m"<<std::endl;

	    exit(EXIT_FAILURE);
	}

	if (openDevice() == -1)
	{
	  //sprintf(logStr, "cannot open device: %d, %s", errno, strerror(errno));
	  ////LOG(logStr, LOG_ERROR);
   //LOG(ERROR) << " cannot open device ";  // +++++++++++++++++++++
	 std::cout<<" \x1B[31m [ERROR][Camera Class][Connect] Cannot open device  \x1B[0m"<<std::endl;
	  exit(EXIT_FAILURE);
	}
	else
	{
		//LOG(INFO) << " device Opened successfully ";
	}

	checkV4L2Compatibility();

}

void camera::checkV4L2Compatibility()
{

	CLEAR(v4l2_cap);

	/* Check V4L2 capability */
	if (xioctl(fd, VIDIOC_QUERYCAP, &v4l2_cap) == -1)
	{
		if (EINVAL == errno)
		{

      //LOG(ERROR) << " not compatible with v4l2 selection ";
			//exit(EXIT_FAILURE);
		}
		else
		{
			//sprintf(logStr, "VIDIOC_QUERYCAP error %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR);
       //LOG(ERROR) << " VIDIOC_QUERYCAP error "; // +++++++++++++++++++++
			//exit(EXIT_FAILURE);
		}
	}
	else
	{
		driver = string((char*)v4l2_cap.driver);
		card = string((char*)v4l2_cap.card);
		//sprintf(logStr, "driver: %s, card: %s", driver.c_str(), card.c_str());

		cout<<"\n\ndriver :"<<driver.c_str()<<"\ncard :"<<card.c_str();
 //LOG(INFO) << " driver : ";
		////LOG(logStr, LOG_INFO);
	}

	/* Check video capture capability */
	if (!(v4l2_cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
	{

     //LOG(ERROR) << " not a video capture device ";
		//exit(EXIT_FAILURE);
	}

	/* Check IO comaptibility */
	switch(io_method)
	{
		case IO_METHOD_READ:
			if (!(v4l2_cap.capabilities & V4L2_CAP_READWRITE))
			{

        //LOG(ERROR) << " does not support read i/o ";
				//exit(EXIT_FAILURE);
			}
			break;
		case IO_METHOD_MMAP:
			if (!(v4l2_cap.capabilities & V4L2_CAP_STREAMING))
			{

         //LOG(ERROR) << " does not support mmap i/o ";
				//exit(EXIT_FAILURE);
			}
			break;
		case IO_METHOD_USERPTR:
			if (!(v4l2_cap.capabilities & V4L2_CAP_STREAMING))
			{

        //LOG(ERROR) << " does not support userptr i/o ";
				//exit(EXIT_FAILURE);
			}
			break;
			default:

			break;
	}

	/* Initialize device */
	initialize();

}

void camera::initialize()
{

	/* Set image format */
	CLEAR(v4l2_fmt);

v4l2_fmt.type 				= V4L2_BUF_TYPE_VIDEO_CAPTURE;
v4l2_fmt.fmt.pix.width 			= width;
v4l2_fmt.fmt.pix.height 		= height;
v4l2_fmt.fmt.pix.pixelformat		= V4L2_PIX_FMT_YUV420;
v4l2_fmt.fmt.pix.field 			= V4L2_FIELD_INTERLACED;

//cout << "**************** 	WIDTH : "<< v4l2_fmt.fmt.pix.width << "\nHEIGHT : "<< v4l2_fmt.fmt.pix.height << endl;
//cout << "**************** width : "<< width << "\nheight : "<< height << endl;

	if (xioctl(fd, VIDIOC_S_FMT, &v4l2_fmt) == -1)
	{
		//sprintf(logStr, "VIDIOC_S_FMT error, %d, %s", errno, strerror(errno));
		////LOG(logStr, LOG_ERROR);
    //LOG(ERROR) << " VIDIOC_S_FMT error "; //++++++++++++++++
		std::cout<<" \x1B[31m [ERROR][Camera Class][Initialize] VIDIOC_S_FMT error \x1B[0m"<<std::endl;

		exit(EXIT_FAILURE);
	}

	//cout << "****************BEFORE IMAGE SET width : "<< width << "\nheight : "<< height << endl;

	if (width != v4l2_fmt.fmt.pix.width)
	{
cout<<"\nImage width set";
		//sprintf(logStr, "Image width set to %d by device for requested width %d, pixelformat %s",
		//		v4l2_fmt.fmt.pix.width, width, pixelformatToString(pixelformat).c_str());
		width = v4l2_fmt.fmt.pix.width;
		//cout << "\n############### IMG_HEIGHT_SET " << width << endl;
     //LOG(INFO) << " Image width set "; // +++++++++++++++++++++++++
		////LOG(logStr, LOG_WARNING);
	}

	//cout << "\n****************BEFORE IMAGE SET width : "<< width << "\nheight : "<< height << endl;

	if (height != v4l2_fmt.fmt.pix.height)
	{
cout<<"\nImage height set";
		//sprintf(logStr, "Image height set to %d by device for requested height %d, pixelformat %s",
		//	v4l2_fmt.fmt.pix.height, height, pixelformatToString(pixelformat).c_str());
		height = v4l2_fmt.fmt.pix.height;

		//cout << "\n############# IMG_HEIGHT_SET " << height << endl;

     //LOG(INFO) << " Image height set ";  //++++++++++++++++++++++++++++++
		////LOG(logStr, LOG_WARNING);
	}

	cout << "\n**************** width : "<< width << "\nheight : "<< height << endl;

	/* Query frame rate at current image format */
	CLEAR(streamparm);
	streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (0 == xioctl(fd, VIDIOC_G_PARM, &streamparm))
	{
		defaultFps = (float)streamparm.parm.capture.timeperframe.denominator/streamparm.parm.capture.timeperframe.numerator;
	}
	else
	{
		if (errno != EINVAL)
		{
			//sprintf(logStr, "VIDIOC_G_PARM error, %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR);
       //LOG(ERROR) << " VIDIOC_G_PARM error ";
			return;
		}
	}

cout<<"\n\n---> using image resoution "<<width<<"x"<<height<<"\n\n---> pixelformat : V4L2_PIX_FMT_YUV420 \n\n---> Fps :"<<defaultFps;
	//sprintf(logStr, "using image resoution %d x %d and pixelformat %s at fps %2.1f",
	//		 width, height, pixelformatToString(pixelformat).c_str(), defaultFps);
	////LOG(logStr, LOG_INFO);
   //LOG(INFO) << " using image resoution "; // ++++++++++++++++++++

	/* Initialize i/o method */
	switch (io_method)
	{
		case IO_METHOD_READ:
			initializeReadIO(v4l2_fmt.fmt.pix.sizeimage);

			break;
		case IO_METHOD_MMAP:
			initializeMmapIO();

			break;
		case IO_METHOD_USERPTR:
			initializeUserptrIO(v4l2_fmt.fmt.pix.sizeimage);

			break;
	}

	/* Initialize capture */

	initializeCapture();

}

void camera::initializeCapture()
{

	switch (io_method)
	{
		case IO_METHOD_READ:
			break;
		case IO_METHOD_MMAP:
			/* Enqueue all mapped buffers */
			for (int i = 0; i < num_data_buffers; i++)
			{
				v4l2_buf.index = i;


				if (xioctl(fd, VIDIOC_QBUF, &v4l2_buf) == -1)
				{//sprintf(logStr, "VIDIOC_QBUF error, %d, %s", errno, strerror(errno));
					////LOG(logStr, LOG_ERROR);
           //LOG(INFO) << " VIDIOC_QBUF error ";
					 std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeCapture] 	case IO_METHOD_MMAP: VIDIOC_QBUF error \x1B[0m"<<std::endl;

					exit(EXIT_FAILURE);
				}
			}

			/* Start capturing */
			type = V4L2_BUF_TYPE_VIDEO_CAPTURE;


			if (xioctl(fd, VIDIOC_STREAMON, &type) == -1)
			{
				//sprintf(logStr, "VIDIOC_STREAMON error, %d, %s", errno, strerror(errno));
				////LOG(logStr, LOG_ERROR);
         //LOG(INFO) << " VIDIOC_STREAMON error ";
				 std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeCapture] 	case IO_METHOD_MMAP: VIDIOC_STREAMON error  \x1B[0m"<<std::endl;

				exit(EXIT_FAILURE);
			}
			break;
		case IO_METHOD_USERPTR:
			/* Enqueue all mapped buffers */
			for (int i = 0; i < num_data_buffers; i++)
			{
				CLEAR(v4l2_buf);

				v4l2_buf.type 		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
				v4l2_buf.memory 	= V4L2_MEMORY_USERPTR;
				v4l2_buf.index		= i;
				v4l2_buf.m.userptr 	= (unsigned long) frameBuff[i].start;
				v4l2_buf.length 	= frameBuff[i].length;


				if (xioctl(fd, VIDIOC_QBUF, &v4l2_buf) == -1)
				{
					//sprintf(logStr, "VIDIOC_QBUF error, %d, %s", errno, strerror(errno));
					////LOG(logStr, LOG_ERROR);
          //LOG(ERROR) << " VIDIOC_QBUF error ";
					std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeCapture] case IO_METHOD_USERPTR: VIDIOC_QBUF error  \x1B[0m"<<std::endl;

					exit(EXIT_FAILURE);
				}
			}

			/* Start capturing */
			type = V4L2_BUF_TYPE_VIDEO_CAPTURE;


			if (xioctl(fd, VIDIOC_STREAMON, &type) == -1)
			{
				cout<<"\n\nVIDIOC_STREAMON error";
				//sprintf(logStr, "VIDIOC_STREAMON error, %d, %s", errno, strerror(errno));
				////LOG(logStr, LOG_ERROR);
        //LOG(ERROR) << "  ";
				std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeCapture] case IO_METHOD_USERPTR: VIDIOC_STREAMON error \x1B[0m"<<std::endl;

				exit(EXIT_FAILURE);
			}
			break;
	}


}

void camera::initializeReadIO(int size)
{



	frameBuff = (FrameBuffer *)calloc(1, sizeof(*frameBuff));

	if (!frameBuff)
	{
		//sprintf(logStr, "initializeReadIO: out of memory");
		////LOG(logStr, LOG_ERROR);
     //LOG(ERROR) << "  ";
		 std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeReadIO] initializeReadIO: out of memory \x1B[0m"<<std::endl;

		exit(EXIT_FAILURE);
	}

	frameBuff[0].length = size;
	frameBuff[0].start 	= malloc(size);

	if	(!frameBuff[0].start)
	{
		//sprintf(logStr, "initializeReadIO: out of memory");
		////LOG(logStr, LOG_ERROR);
     //LOG(ERROR) << " initializeReadIO: out of memory ";
		 std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeReadIO]  frameBuff start initializeReadIO: out of memory \x1B[0m"<<std::endl;

		exit(EXIT_FAILURE);
	}

	//sprintf(logStr,"using i/o method read-write");
	//cout<<"\n\nusing i/o method read-write";
	////LOG(logStr, LOG_INFO);
 //LOG(INFO) << " using i/o method read-write ";

}

void camera::initializeMmapIO()
{


	/* Request buffers */
	CLEAR(reqbuf);

	reqbuf.count 	= num_data_buffers;
	reqbuf.type 	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	reqbuf.memory 	= V4L2_MEMORY_MMAP;

	if (xioctl(fd, VIDIOC_REQBUFS, &reqbuf) == -1)
	{
		if (errno == EINVAL)
		{
			//sprintf(logStr, "initializeMmapIO: device does not support memory mapping");
			////LOG(logStr, LOG_ERROR);
      //LOG(ERROR) << " initializeMmapIO: device does not support memory mapping ";
			std::cout<<" \x1B[31m [ERROR][Camera Class][InitializeReadIO]  frameBuff start initializeReadIO: out of memory \x1B[0m"<<std::endl;

			exit(EXIT_FAILURE);
		}
		else
		{
			//sprintf(logStr, "VIDIOC_REQBUFS error, %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR);
      //LOG(ERROR) << " VIDIOC_REQBUFS error ";
		}
	}

	if (reqbuf.count < 2)
	{
			//sprintf(logStr, "initializeMmapIO: insufficient buffer memory");
			////LOG(logStr, LOG_ERROR);
      //LOG(ERROR) << " initializeMmapIO: insufficient buffer memory ";
			exit(EXIT_FAILURE);
	}

	/* Allocate memory */
	frameBuff = (FrameBuffer *)calloc(reqbuf.count, sizeof(*frameBuff));

	if (!frameBuff)
	{
		//sprintf(logStr, "initializeMmapIO: out of memory");
		////LOG(logStr, LOG_ERROR);
    //LOG(ERROR) << " initializeMmapIO: out of memory ";
		exit(EXIT_FAILURE);
	}

	/* Map buffers */
	for (int n_buffer = 0; n_buffer < reqbuf.count; n_buffer++)
	{
		CLEAR(v4l2_buf);

		v4l2_buf.type 	= reqbuf.type;
		v4l2_buf.memory = V4L2_MEMORY_MMAP;
		v4l2_buf.index 	= n_buffer;

		/* Allocate buffer */
		if (xioctl(fd, VIDIOC_QUERYBUF, &v4l2_buf) == -1)
		{
			//sprintf(logStr, "VIDIOC_QUERYBUF error, %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR);
       //LOG(ERROR) << " VIDIOC_QUERYBUF error ";
			exit(EXIT_FAILURE);
		}

		/* Map memory */
		frameBuff[n_buffer].length 	= v4l2_buf.length;
		frameBuff[n_buffer].start 	= mmap(0, v4l2_buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, v4l2_buf.m.offset);

		if (frameBuff[n_buffer].start == MAP_FAILED)
		{
			//sprintf(logStr, "mmap error, %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR);
       //LOG(ERROR) << " mmap error ";
			exit(EXIT_FAILURE);
		}
	}


   //LOG(INFO) << " using i/o method mmap ";
}


void camera::initializeUserptrIO(int size)
{

	/* Request buffers */
	CLEAR(reqbuf);

	unsigned int page_size;

	page_size 	= getpagesize();
	size 		= (size + page_size - 1) & ~(page_size - 1); // Rounding to the nearest page_size (4096) byte page size

	reqbuf.count 	= num_data_buffers;
	reqbuf.type 	= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	reqbuf.memory 	= V4L2_MEMORY_USERPTR;

	if (xioctl(fd, VIDIOC_REQBUFS, &reqbuf) == -1)
	{
		if (errno == EINVAL) {


       //LOG(ERROR) << " initializeUserptrIO: device does not support user pointer i/o ";
			exit(EXIT_FAILURE);
		}
		else
		{
			//sprintf(logStr, "VIDIOC_REQBUFS error, %d, %s", errno, strerror(errno));
			////LOG(logStr, LOG_ERROR);
      //LOG(ERROR) << " VIDIOC_REQBUFS error ";
		}
	}

	/* Allocate memory */
	frameBuff = (FrameBuffer *)calloc(reqbuf.count, sizeof(*frameBuff));

	if (!frameBuff)
	{

     //LOG(ERROR) << " initializeUserptrIO: out of memory ";
		exit(EXIT_FAILURE);
	}

	/* Allocate buffers */
	for (int n_buffer = 0; n_buffer < reqbuf.count; n_buffer++)
	{
			frameBuff[n_buffer].length 	= size;
			cout << "width : "<< v4l2_fmt.fmt.pix.width << "\nheight : "<< v4l2_fmt.fmt.pix.height << endl;

			frameBuff[n_buffer].start 	= memalign(page_size, size);

			if (!frameBuff[n_buffer].start)
			{

        //LOG(ERROR) << " initializeUserptrIO: out of memory ";
				exit(EXIT_FAILURE);
			}
	}

   //LOG(INFO) << " using i/o method userptr ";
}

unsigned int camera::getBytesUsed()
{
	return bytesused;
}
