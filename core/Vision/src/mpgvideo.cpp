#include "../include/mpgvideo.h"
#include <linux/videodev2.h>

using namespace std;

mpgvideo::mpgvideo() {
  frameCount = 0;
  context = NULL;
  source = NULL;
  scale = false;
  status = VIDEOWRITER_STATUS_DEFAULT;
  av_register_all();
  avcodec_register_all();
  avdevice_register_all();
}

mpgvideo::~mpgvideo() {}

void mpgvideo::mpg_initialize(char *opt_fname, AVCodecID opt_cid,
                              int opt_usc_wd, int opt_usc_ht, int opt_sc_wd,
                              int opt_sc_ht, int opt_fps, int opt_br,
                              int opt_gop, AVPixelFormat opt_pFmt, int opt_mbf,
                              unsigned int opt_camfmt) {
  sprintf(fileName, "%s", opt_fname);
  codecId = opt_cid;
  fps = opt_fps;
  bitrate = opt_br;
  gopsize = opt_gop;
  pixFmt = opt_pFmt;
  maxbframes = opt_mbf;
  camformat = opt_camfmt;

  if ((opt_usc_wd != opt_sc_wd) || (opt_usc_ht != opt_sc_ht)) {
    unscaled_width = opt_usc_wd;
    unscaled_height = opt_usc_ht;

    scaled_width = opt_sc_wd;
    scaled_height = opt_sc_ht;

    mpg_initScaler();
  } else {
    unscaled_width = opt_sc_wd;
    unscaled_height = opt_sc_ht;

    scaled_width = unscaled_width;
    scaled_height = unscaled_height;
  }

  // sprintf(logStr, "encoding video file");
  if (source) {
    // sprintf(logStr, "%s from source %s", logStr, source);
  }
  // log(logStr, LOG_INFO);

  mpg_initEncoder();

  mpg_initDecoder(AV_CODEC_ID_MJPEG, AV_PIX_FMT_YUV422P);

  mpg_initConverter();
}

void mpgvideo::mpg_initEncoder() {
  /*
   * Initialize encoder
   */

  codec = avcodec_find_encoder(codecId);
  if (!codec) {
    // sprintf(logStr, "codec not found");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  context = avcodec_alloc_context3(codec);
  if (!context) {
    // sprintf(logStr, "could not allocate video codec context");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  context->bit_rate = bitrate;
  context->width = scaled_width;
  context->height = scaled_height;
  context->time_base = (AVRational){1, fps};
  context->gop_size = gopsize;
  context->pix_fmt = pixFmt;

  if (maxbframes != NULL)
    context->max_b_frames = maxbframes;

  if (codecId == AV_CODEC_ID_H264) {
    av_opt_set(context->priv_data, "preset", "slow", AV_OPT_SEARCH_CHILDREN);
    // av_opt_set(context->priv_data, "profile", 	"baseline",
    // AV_OPT_SEARCH_CHILDREN);
    // av_opt_set(context->priv_data, "level", 		"3.0",
    // AV_OPT_SEARCH_CHILDREN);
    // av_opt_set(context->priv_data, "crf",  		"18",
    // AV_OPT_SEARCH_CHILDREN);
  }

  if (avcodec_open2(context, codec, NULL) < 0) {
    // sprintf(logStr, "could not open codec");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  fp = fopen(fileName, "wb");

  if (!fp) {
    // sprintf(logStr, "could not open video file on disk");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  frame = av_frame_alloc();

  if (!frame) {
    // sprintf(logStr, "could not allocate video frame");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  if (av_image_alloc(frame->data, frame->linesize, unscaled_width,
                     unscaled_height, context->pix_fmt, 32) < 0) {
    // sprintf(logStr, "could not allocate raw picture buffer");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  status = VIDEOWRITER_STATUS_OK;
}

void mpgvideo::mpg_initDecoder(AVCodecID opt_decId, AVPixelFormat opt_pxfmt) {
  /*
   * Initialize decoder
   */

  dec_codec = avcodec_find_decoder(opt_decId);
  if (!dec_codec) {
    // sprintf(logStr, "decoder not found");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  dec_context = avcodec_alloc_context3(dec_codec);
  if (!dec_context) {
    // sprintf(logStr, "could not allocate video decoder context");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  avcodec_get_context_defaults3(dec_context, dec_codec);

  dec_context->width = unscaled_width;
  dec_context->height = unscaled_height;
  dec_context->pix_fmt = opt_pxfmt;

  if (avcodec_open2(dec_context, dec_codec, NULL) < 0) {
    // sprintf(logStr, "could not open decoder");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  dec_frame = av_frame_alloc();

  if (!dec_frame) {
    // sprintf(logStr, "could not allocate decoder video frame");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  if (av_image_alloc(dec_frame->data, dec_frame->linesize, dec_context->width,
                     dec_context->height, dec_context->pix_fmt, 32) < 0) {
    // sprintf(logStr, "could not allocate decoder raw picture buffer");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  if (dec_codec->capabilities & CODEC_CAP_TRUNCATED) {
    dec_context->flags |= CODEC_FLAG_TRUNCATED;
  }
}

void mpgvideo::mpg_initConverter() {
  /*
   * Initialize conversion context
   */
  conversionContext = sws_getContext(
      unscaled_width, unscaled_height, AV_PIX_FMT_YUYV422, unscaled_width,
      unscaled_height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
}

void mpgvideo::mpg_initScaler() {
  /*
   * Initialize scaler context
   */
  scaleContext = sws_getContext(unscaled_width, unscaled_height, pixFmt,
                                scaled_width, scaled_height, pixFmt,
                                SWS_FAST_BILINEAR, NULL, NULL, NULL);

  /*
   * Allocate frame data
   */
  scaled_frame = av_frame_alloc();

  if (!scaled_frame) {
    // sprintf(logStr, "could not allocate scaler video frame");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  int scaled_size = avpicture_get_size(pixFmt, scaled_width, scaled_height);
  unsigned char *temp_buffer =
      (unsigned char *)av_malloc(scaled_size * sizeof(uint8_t));
  avpicture_fill((AVPicture *)scaled_frame, temp_buffer, pixFmt, scaled_width,
                 scaled_height);

  // sprintf(logStr, "initialized scaler to scale from %d x %d to %d x %d",
  // unscaled_width, unscaled_height, scaled_width, scaled_height);
  // log(logStr, LOG_INFO);

  scale = true;
}

void mpgvideo::mpg_writeFrame(unsigned char *data, int len) {

  if (status == VIDEOWRITER_STATUS_OK) {
    if (camformat == V4L2_PIX_FMT_YUYV) {
      /* YUYV422 to YUV420P: Method 1
       * takes nearly 10ms per frame
       */
      // YUV422toYUV420(data, frame, width, height);

      /* YUYV422 to YUV420P: Method 2
       * takes nearly 1 ms per frame
       */
      uint8_t *inData[1] = {data};
      const int inLinesize[1] = {2 * unscaled_width};
      sws_scale(conversionContext, inData, inLinesize, 0, unscaled_height,
                frame->data, frame->linesize);
    } else if (camformat == V4L2_PIX_FMT_MJPEG) {
      /* Decode JPEG data to YUV420P in frame */

      // struct timeval tv1, tv2;
      // gettimeofday(&tv1, NULL);

      mpg_decodeMJPEG(data, len);

      /*
      int sz = 1843200;
      unsigned char *t = (unsigned char *)malloc(sz);
      MJPEGtoYUV420P(t, data, width, height, sz);
      frame->data[0] = t;
      frame->data[1] = frame->data[0] + width * height;
  frame->data[2] = frame->data[1] + (width * height) / 4;
      */

      // gettimeofday(&tv2, NULL);
      // printf("decode time: %lu\n", tv2.tv_usec - tv1.tv_usec);
    }

    if (scale) {
      /* Scale frame */
      sws_scale(scaleContext, frame->data, frame->linesize, 0, unscaled_height,
                scaled_frame->data, scaled_frame->linesize);

      /* Write frame data */
      mpg_encodeFrame(scaled_frame);
    } else {
      /* Write frame data */
      mpg_encodeFrame(frame);
    }

    // free(t);
  } else {
    if (status == VIDEOWRITER_STATUS_DEFAULT) {
      // sprintf(logStr, "writeFrame: VideoWriter unintialized");
    } else if (status == VIDEOWRITER_STATUS_ERR) {
      // sprintf(logStr, "writeFrame: VideoWriter in error state");
    } else if (status == VIDEOWRITER_STATUS_CLOSED) {
      // sprintf(logStr, "writeFrame: VideoWriter in closed state");
    }

    // log(logStr, LOG_ERROR);
  }
}

void mpgvideo::mpg_encodeFrame(AVFrame *opt_frame) {
  int got_output = 0;
  av_init_packet(&pkt);
  pkt.data = NULL;
  pkt.size = 0;

  opt_frame->width = scaled_width;
  opt_frame->height = scaled_height;
  opt_frame->pts = frameCount;
  opt_frame->format = AV_PIX_FMT_YUV420P;

  if (avcodec_encode_video2(context, &pkt, opt_frame, &got_output) < 0) {
    // sprintf(logStr, "error encoding frame");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
  }

  if (got_output) {
    // sprintf(logStr, "encoding frame #%d", frameCount+1);
    // log(logStr, LOG_INFO);
    fwrite(pkt.data, 1, pkt.size, fp);
    av_free_packet(&pkt);
  }

  frameCount++;
}

void mpgvideo::mpg_writeDelayedFrames() {
  if (frameCount > 0) // everything was good and some frames were written
  {
    int got_output;

    for (got_output = 1; got_output; frameCount++) {
      if (avcodec_encode_video2(context, &pkt, NULL, &got_output) < 0) {
        // sprintf(logStr, "error encoding frame");
        // log(logStr, LOG_ERROR);
        status = VIDEOWRITER_STATUS_ERR;
      }
      if (got_output) {
        // sprintf(logStr, "write delayed frame %3d (size=%5d)", frameCount,
        // pkt.size);
        // log(logStr, LOG_INFO);
        fwrite(pkt.data, 1, pkt.size, fp);
        av_free_packet(&pkt);
      }
    }
  }
}

void mpgvideo::mpg_decodeMJPEG(unsigned char *data, int len) {
  int got_picture = 0;

  av_init_packet(&dec_pkt);
  dec_pkt.size = len;
  dec_pkt.data = data;

  dec_frame->width = unscaled_width;
  dec_frame->height = unscaled_height;
  dec_frame->format = AV_PIX_FMT_YUV422P;

  if (avcodec_decode_video2(dec_context, dec_frame, &got_picture, &dec_pkt) <
      0) {
    // sprintf(logStr, "error decoding frame");
    // log(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
  }

  if (got_picture) {
    /* Convert YUVJ422P to YUV420P */
    conversionContext =
        sws_getContext(unscaled_width, unscaled_height, AV_PIX_FMT_YUV422P,
                       unscaled_width, unscaled_height, AV_PIX_FMT_YUV420P,
                       SWS_FAST_BILINEAR, NULL, NULL, NULL);
    sws_scale(conversionContext, dec_frame->data, dec_frame->linesize, 0,
              unscaled_height, frame->data, frame->linesize);
  }
}

void mpgvideo::mpg_initialize(char *opt_fname, AVCodecID opt_cid,
                              int opt_usc_wd, int opt_usc_ht, int opt_sc_wd,
                              int opt_sc_ht, int opt_fps, int opt_br,
                              int opt_gop, AVPixelFormat opt_pFmt, int opt_mbf,
                              unsigned int opt_camfmt, char *opt_src) {
  source = (char *)malloc(sizeof(opt_src));
  sprintf(source, "%s", opt_src);
  mpg_initialize(opt_fname, opt_cid, opt_usc_wd, opt_usc_ht, opt_sc_wd,
                 opt_sc_ht, opt_fps, opt_br, opt_gop, opt_pFmt, opt_mbf,
                 opt_camfmt);
}

void mpgvideo::mpg_close() {
  mpg_writeDelayedFrames();

  mpg_destroy();
}

/*
void mpgvideo::mpg_slog(char *str, int type)
{
        char tmp[sizeof(logStr)];
        //sprintf(tmp, "%s[%s]%s %s", ANSI_BOLD_ON, fileName, ANSI_BOLD_OFF,
str);
        //logger.log(tmp, type);
}
*/

void mpgvideo::mpg_destroy() {
  if (status == VIDEOWRITER_STATUS_OK) {
    // sprintf(logStr, "releasing video encoder, decoder, converter and scaler
    // resources");
    // log(logStr, LOG_INFO);

    /* release encoder resources */
    uint8_t endcode[] = {0x00, 0x00, 0x01, 0xb7};
    fwrite(endcode, 1, sizeof(endcode), fp);
    fclose(fp);
    avcodec_close(context);
    av_free(context);
    av_freep(&frame->data[0]);
    av_frame_free(&frame);

    /* release decoder resources */
    avcodec_close(dec_context);
    av_free(dec_context);
    av_frame_free(&dec_frame);

    /* release converter resources */
    sws_freeContext(conversionContext);

    /* release scaler resources */
    if (scale) {
      sws_freeContext(scaleContext);
      av_frame_free(&scaled_frame);
    }

    status = VIDEOWRITER_STATUS_CLOSED;
  }
}
