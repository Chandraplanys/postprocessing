/** @brief Script to run both node server and vision module
 *
 *  This script first starts a nodejs server which recieves the input from the
 *  vision module and pushes to frontend over websockets.
 */
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "../include/colors.h"
#include "../include/vision.h"

using namespace std;

char ret_line[512];
char build_folder_path[512];
char build_folder_path_jsmpeg[512];

// bool ENABLE_DEVMODE = false;

/* an utility function to get folder path without '\n' character */
char *remove_nextLine(char *temp) {
  std::string charToString(temp);
  charToString.erase(
      std::remove(charToString.begin(), charToString.end(), '\n'),
      charToString.end());
  char *returnValue = new char[charToString.length()];
  strcpy(returnValue, charToString.c_str());

  return returnValue;
}

/**
 * Method extracts the path to the JSMPEG library
 */
void PathFinder() {

  try {
    FILE *in;
    char popen_buff[512];
    struct stat sb;
    std::string jsmpegPath_RelativeToGUI = "../Vision/libs/jsmpeg";

    if (!(in = popen("pwd", "r"))) { /**< Extracting the executable file path */
      throw "PWD command execution error";
    }
    while (fgets(popen_buff, sizeof(popen_buff), in) != NULL) {
    }
    pclose(in);

    strcpy(build_folder_path,
           popen_buff); /**< Copying the Executable file path*/

    /**
    * Check if the Jsmpeg folder path Exists
    */
    if (stat(jsmpegPath_RelativeToGUI.c_str(), &sb) == 0 &&
        S_ISDIR(sb.st_mode)) {
      // If required print success statement

      /**
      * Creating the path to Jsmpeg node server
      */
      strcpy(build_folder_path_jsmpeg, build_folder_path);
      strcat(build_folder_path_jsmpeg, jsmpegPath_RelativeToGUI.c_str());
    } else {
      throw " Couldnt find the Jsmpeg Folder ";
    }

  } catch (char const *e) {
    std::cerr << KRED << "[Error][Vision Server][PathFinder] " << e << RST
              << std::endl;
  } catch (std::exception &e) {
    std::cerr << "[Error][Vision Server][PathFinder] " << e.what() << std::endl;
  }
}

/* starts the jsmpeg server */
void *server(void *arg_server) {

  FILE *in;

  char server_path[512] = {};

  /* to make server path */
  strcpy(server_path, "nodejs ");
  strcat(server_path, build_folder_path_jsmpeg);

  strcat(server_path, " dual_stream-server.js q");
  strcpy(server_path, remove_nextLine(server_path));

  std::cerr << KBLU << "[Info][Vision Server][Server] " << server_path << RST
            << std::endl;

  char buff[512];

  if (!(in = popen(server_path, "r"))) {
    cout << "\nBash command failed !\n";
    exit(EXIT_FAILURE);
  }

  while (fgets(buff, sizeof(buff), in) != NULL) {
    cout << "Success : " << buff;
  }
  pclose(in);
}

/* run the vision module */
void *vision(void *arg_vision) {
  char stream_path[512];

  strcpy(stream_path, build_folder_path);
  strcat(stream_path, "/../Vision/vision");
  strcpy(stream_path, remove_nextLine(stream_path));

  cout << "\n\n\n\n\n------------------------------->" << stream_path;
  usleep(20000);
  system(stream_path);
}

int main() {

  PathFinder();

  pthread_t thread_server, thread_vision;

  pthread_create(&thread_server, NULL, server, NULL);
  pthread_create(&thread_vision, NULL, vision, NULL);

  pthread_join(thread_vision, NULL);

  usleep(100000);
  int PID;

  char buf[512];
  FILE *cmd_pipe = popen("pidof nodejs", "r");

  fgets(buf, 512, cmd_pipe);

  pclose(cmd_pipe);
  std::cout << "Nodejs: " << std::string(buf) << std::endl;

  PID = atoi(buf);

  std::string killPID = "kill -9 " + std::to_string(PID);
  std::cout << "String: " << killPID << std::endl;
  system(killPID.c_str());

  pthread_kill(thread_server, SIGKILL);
  usleep(20000);
  pthread_join(thread_server, NULL);

  //	if(ENABLE_DEVMODE)
  //	{
  //	pthread_join(thread_front_browser , NULL);
  //	pthread_join(thread_top_browser , NULL);
  //	}

  usleep(20000);

  cout << "\n\n++++++++PROGRAM_ENDS+++++++++\n\n";
  return 0;
}
