/** @brief Script to run both node server and vision module
 *
 *  This script first starts a nodejs server which recieves the input from the
 *  vision module and pushes to frontend over websockets.
 */
#include <algorithm>
#include <boost/thread.hpp>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "../include/colors.h"
#include "../include/vision.h"

using namespace std;

bool noderServerKillStatus = false;
std::string build_folder_path, build_folder_path_jsmpeg;

/**
  * Kill Any program using the pid of the respective function
  *
  *@param programName is the Program unique name that has to be killed
  */
void killProgram(std::string programName)
{

  try
  {
    int PID;
    char buf[512];
    std::string extractPID_Command, killProgram_Command;

    extractPID_Command = "pidof " + programName;

    FILE *cmd_pipe = popen(extractPID_Command.c_str(), "r");

    fgets(buf, 512, cmd_pipe);

    pclose(cmd_pipe);
    std::cout << KMAG
              << "[Warning!!!][Vision Server][Kill Program] Attempting to Kill "
              << programName << RST << std::endl
              << std::endl;

    PID = atoi(buf);
    if (PID)
    {
      killProgram_Command = "kill -9 " + std::to_string(PID);

      system(killProgram_Command.c_str());
      std::cout << KMAG << "[Warning!!!][Vision Server][Kill Program] "
                << programName << " Terminated" << RST << std::endl
                << std::endl;
    }
    else
    {
      throw " NOT_FOUND ";
    }
  }
  catch (char const *e)
  {
    std::cout << KRED << "[Error][Vision Server][Kill Program] " << programName
              << " " << e << RST << std::endl
              << std::endl;
  }
  catch (std::exception &e)
  {
    std::cout << "[Error][Vision Server][Kill Program] " << e.what()
              << std::endl
              << std::endl;
  }
}

/**
 * Convert Char* to std::string from all popen returns
 */
std::string convertCharToString(char *charArray)
{
  std::string tempString(charArray, charArray + strlen(charArray));
  return tempString;
}

/**
  * A utility function to get folder path without '\n' character
  */
std::string remove_nextLine(std::string inputString)
{

  inputString.erase(std::remove(inputString.begin(), inputString.end(), '\n'),
                    inputString.end());

  return inputString;
}

/**
 * Method extracts the path to the JSMPEG library and the vision executable
 */
void PathFinder()
{

  try
  {
    FILE *in;
    char popen_buff[512];
    struct stat sb;
    std::string jsmpegPath_RelativeToGUI = "../Vision/libs/jsmpeg";

    if (!(in = popen("pwd", "r")))
    { /**< Extracting the executable file path */
      throw "PWD command execution error";
    }
    while (fgets(popen_buff, sizeof(popen_buff), in) != NULL)
    {
    }
    pclose(in);

    build_folder_path = remove_nextLine(convertCharToString(
        popen_buff)); /**< Copying the Executable file path*/

    /**
    * Check if the Jsmpeg folder path Exists
    */
    if (stat(jsmpegPath_RelativeToGUI.c_str(), &sb) == 0 &&
        S_ISDIR(sb.st_mode))
    {
      // If required print success statement

      /**
      * Creating the path to Jsmpeg node server
      */
      build_folder_path_jsmpeg =
          build_folder_path + "/" + jsmpegPath_RelativeToGUI;
    }
    else
    {
      throw " Couldnt find the Jsmpeg Folder ";
    }
  }
  catch (char const *e)
  {
    std::cout << KRED << "[Error][Vision Server][PathFinder] " << e << RST
              << std::endl
              << std::endl;
  }
  catch (std::exception &e)
  {
    std::cout << "[Error][Vision Server][PathFinder] " << e.what() << std::endl
              << std::endl;
  }
}

/**
  * Node Server Thread Function
  */

void *server(void *arg_server)
{

  std::string nodeServerCommand;

  nodeServerCommand =
      "nodejs " + build_folder_path_jsmpeg + "/dual_stream-server.js q";

  try
  {
    FILE *in;

    if (!(in = popen(nodeServerCommand.c_str(), "r")))
    {
      throw "Bash nodeServerCommand Failed to Execute";
    }
    else
    {

      std::cerr << KBLU
                << "[Info][Vision Server][Server] Node Stream Server Started "
                << RST << std::endl
                << std::endl;

      while (true)
      {
        if (noderServerKillStatus)
        {
          std::cerr
              << KBLU
              << "[Info][Vision Server][Server] Closing the Node Stream Popen"
              << RST << std::endl
              << std::endl;
          killProgram("nodejs"); /**< Kill the Node Server of Video Streaming*/
          pclose(in);
          break;
        }
        boost::this_thread::sleep(boost::posix_time::microseconds(1000));
      }
    }
  }
  catch (char const *e)
  {
    std::cerr << KRED << "[Error][Vision Server][Server] " << e << RST
              << std::endl;
    pthread_exit(NULL);
  }
  catch (std::exception &e)
  {
    std::cerr << "[Error][Vision Server][Server] " << e.what() << std::endl;
    pthread_exit(NULL);
  }
  std::cerr
      << KBLU
      << "[Info][Vision Server][server thread] Exiting the Node Server Thread "
      << RST << std::endl
      << std::endl;
  pthread_exit(NULL);
}

/**
  * Vision thread Function
  */

void *vision(void *arg_vision)
{

  std::string visionExecutablePath;

  visionExecutablePath = build_folder_path + "/../Vision/bin/vision";

  std::cerr << KBLU << "[Info][Vision Server][vision] Vision Started" << RST
            << std::endl
            << std::endl;
  usleep(500000);
  system(visionExecutablePath.c_str());
  std::cerr << KBLU
            << "[Info][Vision Server][vision thread] Exiting the vision thread "
            << RST << std::endl
            << std::endl;
  pthread_exit(NULL);
}

int main()
{

  PathFinder(); /**< Extract all the paths required for the Program*/

  pthread_t thread_server,
      thread_vision; /**< Create Threads for Node server and Vision Server*/

  /**
    * @param server is the Node Server Thread Function
    * @param thread_server is the thread to run the server thread function
    */
  pthread_create(&thread_server, NULL, server, NULL);
  /**
    * @param vision is the Node Server Thread Function
    * @param thread_vision is the thread to run the server thread function
    */
  pthread_create(&thread_vision, NULL, vision, NULL);

  /**
    *Logic: You have to wait till the vision thread is closed then kill the
    *dedicated node server for vision stream
    */
  pthread_join(thread_vision, NULL);
  usleep(100000);

  // killProgram("nodejs"); /**< Kill the Node Server of Video Streaming*/
  noderServerKillStatus = true;
  usleep(20000);
  // pthread_kill(thread_server, SIGKILL); /**< Kill the thread_server thread*/
  pthread_join(thread_server, NULL);

  usleep(20000);

  std::cout << KBLU
            << "\n\n[Info][Vision Server][Main] ++++++++PROGRAM_ENDS+++++++++"
            << RST << std::endl;
  return 0;
}
