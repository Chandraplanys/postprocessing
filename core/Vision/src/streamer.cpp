#include "../include/streamer.h"
#include <linux/videodev2.h>

using namespace std;

// el::Logger* defaultLogger_streamer = el::Loggers::getLogger("default");

streamer::streamer()
{
  frameCount = 0;
  context = NULL;
  source = NULL;
  scale = false;
  status = VIDEOWRITER_STATUS_DEFAULT;
  av_register_all();
  avcodec_register_all();
  avdevice_register_all();
  avformat_network_init();
  ////LOG(INFO) << "streamer : registered with FFMPEG libs";
}

streamer::~streamer()
{
  if (outAVFormatContext != NULL)
  {
    value = av_write_trailer(outAVFormatContext);
  }
  if (value < 0)
  {
    // LOG(ERROR)<<" av_write_trailer() ";
  }
  else
  {
    // LOG(INFO) << "av_write_trailer()";
  }
}
// //LOG(INFO)<<"  ";
void streamer::stream_writeFrame(unsigned char *data, int len)
{

  if (status == VIDEOWRITER_STATUS_OK)
  {
    if (camformat == V4L2_PIX_FMT_YUYV)
    {
      // YUYV422 to YUV420P: Method 1
      // takes nearly 10ms per frame
      //
      // YUV422toYUV420(data, frame, width, height);

      // YUYV422 to YUV420P: Method 2
      // takes nearly 1 ms per frame

      uint8_t *inData[1] = {data};
      const int inLinesize[1] = {2 * unscaled_width};
      sws_scale(conversionContext, inData, inLinesize, 0, unscaled_height,
                frame->data, frame->linesize);
    }
    else if (camformat == V4L2_PIX_FMT_MJPEG)
    {
      // Decode JPEG data to YUV420P in frame

      // struct timeval tv1, tv2;
      // gettimeofday(&tv1, NULL);

      stream_decodeMJPEG(data, len);

      // int sz = 1843200;
      // unsigned char *t = (unsigned char *)malloc(sz);
      // MJPEGtoYUV420P(t, data, width, height, sz);
      // frame->data[0] = t;
      // frame->data[1] = frame->data[0] + width * height;
      // frame->data[2] = frame->data[1] + (width * height) / 4;
      //

      // gettimeofday(&tv2, NULL);
      // printf("decode time: %lu\n", tv2.tv_usec - tv1.tv_usec);
    }

    if (scale)
    {
      // Scale frame
      sws_scale(scaleContext, frame->data, frame->linesize, 0, unscaled_height,
                scaled_frame->data, scaled_frame->linesize);

      // Write frame data
      stream_encodeFrame(scaled_frame);
    }
    else
    {
      // Write frame data
      stream_encodeFrame(frame);
    }

    // free(t);
  }
  else
  {
    if (status == VIDEOWRITER_STATUS_DEFAULT)
    {
      // LOG(INFO)<<" writeFrame: VideoWriter unintialized ";
      // sprintf(logStr, "writeFrame: VideoWriter unintialized");
    }
    else if (status == VIDEOWRITER_STATUS_ERR)
    {
      // LOG(INFO)<<" writeFrame: VideoWriter in error state ";
      // sprintf(logStr, "writeFrame: VideoWriter in error state");
    }
    else if (status == VIDEOWRITER_STATUS_CLOSED)
    {
      // LOG(INFO)<<" writeFrame: VideoWriter in closed state ";
      // sprintf(logStr, "writeFrame: VideoWriter in closed state");
    }

    ////LOG(logStr, LOG_ERROR);
  }
}

void streamer::stream_destroy()
{

  if (status == VIDEOWRITER_STATUS_OK)
  {

    // LOG(INFO)<<" releasing video encoder, decoder, converter and scaler
    // resources ";

    /* release encoder resources */
    uint8_t endcode[] = {0x00, 0x00, 0x01, 0xb7};

    avcodec_close(context);
    av_free(context);
    av_freep(&frame->data[0]);
    av_frame_free(&frame);

    /* release decoder resources */
    avcodec_close(dec_context);
    av_free(dec_context);
    av_frame_free(&dec_frame);

    /* release converter resources */
    sws_freeContext(conversionContext);

    /* release scaler resources */
    if (scale)
    {
      sws_freeContext(scaleContext);
      av_frame_free(&scaled_frame);
    }

    status = VIDEOWRITER_STATUS_CLOSED;
  }
}

void streamer::stream_close() { stream_destroy(); }

void streamer::stream_encodeFrame(AVFrame *opt_frame)
{

  int got_output = 0;
  av_init_packet(&pkt);
  pkt.data = NULL;
  pkt.size = 0;

  opt_frame->width = scaled_width;
  opt_frame->height = scaled_height;
  opt_frame->pts = frameCount;
  opt_frame->format = AV_PIX_FMT_YUV420P;

  if (avcodec_encode_video2(context, &pkt, opt_frame, &got_output) < 0)
  {
    // LOG(ERROR)<<" error encoding frame ";
    // sprintf(logStr, "error encoding frame");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
  }

  if (got_output)
  {
    // sprintf(logStr, "encoding frame #%d", frameCount+1);
    ////LOG(logStr, LOG_INFO);
    // defaultLogger_streamer->info("Stream Frame : %v size %v", frameCount+1 ,
    // pkt.size/1000 );

    if (pkt.pts != AV_NOPTS_VALUE)
      pkt.pts = av_rescale_q(pkt.pts, video_st->codec->time_base,
                             video_st->time_base);
    if (pkt.dts != AV_NOPTS_VALUE)
      pkt.dts = av_rescale_q(pkt.dts, video_st->codec->time_base,
                             video_st->time_base);

    // av_write_frame Returns < 0 on error, = 0 if OK, 1 if flushed and there is
    // no more data to flush
    if (av_write_frame(outAVFormatContext, &pkt) != 0)
    {
      // //LOG(ERROR)<<" av_write_frame() ";  // Flushing into the sockets.
      // hence produces value < 0 .
    }

    // fwrite(pkt.data, 1, pkt.size, fp);
    av_packet_unref(&pkt);
  }

  frameCount++;
}

void streamer::stream_decodeMJPEG(unsigned char *data, int len)
{

  int got_picture = 0;

  av_init_packet(&dec_pkt);
  dec_pkt.size = len;
  dec_pkt.data = data;

  dec_frame->width = unscaled_width;
  dec_frame->height = unscaled_height;
  dec_frame->format = AV_PIX_FMT_YUV422P;

  if (avcodec_decode_video2(dec_context, dec_frame, &got_picture, &dec_pkt) <
      0)
  {
    // LOG(INFO)<<" error decoding frame ";
    status = VIDEOWRITER_STATUS_ERR;
  }

  if (got_picture)
  {
    /* Convert YUVJ422P to YUV420P */
    conversionContext =
        sws_getContext(unscaled_width, unscaled_height, AV_PIX_FMT_YUV422P,
                       unscaled_width, unscaled_height, AV_PIX_FMT_YUV420P,
                       SWS_FAST_BILINEAR, NULL, NULL, NULL);
    sws_scale(conversionContext, dec_frame->data, dec_frame->linesize, 0,
              unscaled_height, frame->data, frame->linesize);
  }
}

void streamer::stream_initialize(char *opt_fname, AVCodecID opt_cid,
                                 int opt_usc_wd, int opt_usc_ht, int opt_sc_wd,
                                 int opt_sc_ht, int opt_fps, int opt_br,
                                 int opt_gop, AVPixelFormat opt_pFmt,
                                 int opt_mbf, unsigned int opt_camfmt,
                                 char *opt_src)
{

  source = (char *)malloc(sizeof(opt_src));
  sprintf(source, "%s", opt_src);
  stream_initialize(opt_fname, opt_cid, opt_usc_wd, opt_usc_ht, opt_sc_wd,
                    opt_sc_ht, opt_fps, opt_br, opt_gop, opt_pFmt, opt_mbf,
                    opt_camfmt);
}

void streamer::stream_initialize(char *opt_fname, AVCodecID opt_cid,
                                 int opt_usc_wd, int opt_usc_ht, int opt_sc_wd,
                                 int opt_sc_ht, int opt_fps, int opt_br,
                                 int opt_gop, AVPixelFormat opt_pFmt,
                                 int opt_mbf, unsigned int opt_camfmt)
{
  sprintf(fileName, "%s", opt_fname);
  codecId = opt_cid;
  fps = opt_fps;
  bitrate = opt_br;
  gopsize = opt_gop;
  pixFmt = opt_pFmt;
  maxbframes = opt_mbf;
  camformat = opt_camfmt;

  if ((opt_usc_wd != opt_sc_wd) || (opt_usc_ht != opt_sc_ht))
  {
    unscaled_width = opt_usc_wd;
    unscaled_height = opt_usc_ht;

    scaled_width = opt_sc_wd;
    scaled_height = opt_sc_ht;

    stream_initScaler();
  }
  else
  {
    unscaled_width = opt_sc_wd;
    unscaled_height = opt_sc_ht;

    scaled_width = unscaled_width;
    scaled_height = unscaled_height;
  }

  // LOG(INFO)<<" encoding video file ";
  // sprintf(logStr, "encoding video file");
  if (source)
  {
    // sprintf(logStr, "%s from source %s", logStr, source);
    // LOG(INFO)<<" from source ";
  }

  stream_initEncoder();
  stream_initDecoder(AV_CODEC_ID_MJPEG, AV_PIX_FMT_YUV422P);
  stream_initConverter();
}

void streamer::stream_initConverter()
{
  /*
   * Initialize conversion context
   */
  conversionContext = sws_getContext(
      unscaled_width, unscaled_height, AV_PIX_FMT_YUYV422, unscaled_width,
      unscaled_height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
}

void streamer::stream_initDecoder(AVCodecID opt_decId,
                                  AVPixelFormat opt_pxfmt)
{
  /*
   * Initialize decoder
   */
  dec_codec = avcodec_find_decoder(opt_decId);
  if (!dec_codec)
  {
    // LOG(INFO)<<" decoder not found ";
    // sprintf(logStr, "decoder not found");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  dec_context = avcodec_alloc_context3(dec_codec);
  if (!dec_context)
  {
    // LOG(INFO)<<" stream could not allocate video decoder context ";
    // sprintf(logStr, "could not allocate video decoder context");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  avcodec_get_context_defaults3(dec_context, dec_codec);
  /*
  avcodec_get_context_defaults3 :
  Set the fields of the given AVCodecContext to default values corresponding to
  the given codec (defaults may be codec-dependent).
  */
  dec_context->width = unscaled_width;
  dec_context->height = unscaled_height;
  dec_context->pix_fmt = opt_pxfmt;

  if (avcodec_open2(dec_context, dec_codec, NULL) < 0)
  {

    // LOG(INFO)<<" stream could not open decoder ";
    // sprintf(logStr, "could not open decoder");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  dec_frame = av_frame_alloc();

  if (!dec_frame)
  {

    // LOG(INFO)<<" stream_ could not allocate decoder video frame ";
    // sprintf(logStr, "could not allocate decoder video frame");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  if (av_image_alloc(dec_frame->data, dec_frame->linesize, dec_context->width,
                     dec_context->height, dec_context->pix_fmt, 32) < 0)
  {

    // LOG(INFO)<<" stream could not allocate decoder raw picture buffer ";
    // sprintf(logStr, "could not allocate decoder raw picture buffer");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  if (dec_codec->capabilities & CODEC_CAP_TRUNCATED)
  {
    dec_context->flags |= CODEC_FLAG_TRUNCATED;
  }
}

void streamer::stream_initContainer()
{

  // LOG(INFO)<<"  ";

  outAVFormatContext = NULL;

  avformat_alloc_output_context2(&outAVFormatContext, NULL, "mpeg1video",
                                 fileName);
  if (!outAVFormatContext)
  {
    // LOG(ERROR)<<" stream : avformat_alloc_output_context2() ";
    return;
  }
  else
  {
    // LOG(INFO)<<" stream : avformat_alloc_output_context2() ";
  }

  output_format = outAVFormatContext->oformat;

  video_st = avformat_new_stream(outAVFormatContext, NULL);
  if (!video_st)
  {
    // LOG(ERROR)<<"  ";
    // return -1;
  }
  else
  {
    // LOG(INFO)<<" stream : avformat_new_stream() ";
  }
}

void streamer::stream_makeContainer()
{

  if (!(outAVFormatContext->flags & AVFMT_NOFILE))
  {
    if (avio_open2(&outAVFormatContext->pb, fileName, AVIO_FLAG_WRITE, NULL,
                   NULL) < 0)
    {
      // LOG(ERROR)<<" stream : avio_open2() ";
    }
    else
    {
      // LOG(INFO)<<"stream : avio_open2()  ";
    }
  }

  if (!outAVFormatContext->nb_streams)
  {
    cout << "\n\nstream_Error : Output file dose not contain any stream";
    // return -1;
    // LOG(ERROR)<<" stream : Output file dose not contain any stream ";
  }

  value = avformat_write_header(outAVFormatContext, NULL);
  if (value < 0)
  {
    // return -1;
    // LOG(ERROR)<<" stream : avformat_write_header() ";
  }
  else
  {
    // LOG(INFO)<<" avformat_write_header() ";
  }

  //	cout<<"\n\n++++++++++++++Output file information++++++++++++++\n\n";
  // av_dump_format(outAVFormatContext , 0 ,output_file ,1);
}

void streamer::stream_initEncoder()
{
  /*
   * Initialize encoder
   */
  stream_initContainer();

  codec = avcodec_find_encoder(codecId);
  if (!codec)
  {

    // LOG(ERROR)<<" stream codec not found ";
    // sprintf(logStr, "codec not found");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }
  else
  {
    // LOG(INFO)<<" stream Success : avcodec_find_encoder() ";
  }

  context = avcodec_alloc_context3(codec);
  if (!context)
  {
    // LOG(ERROR)<<" stream could not allocate video codec context ";
    // sprintf(logStr, "could not allocate video codec context");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }
  else
  {
    // LOG(INFO)<<" stream Success : avcodec_alloc_context3() ";
  }
  context = video_st->codec;
  context->bit_rate = bitrate;
  context->width = scaled_width;
  context->height = scaled_height;
  context->time_base = (AVRational){1, fps};
  context->gop_size = gopsize;
  context->pix_fmt = pixFmt;

  if (maxbframes != NULL)
    context->max_b_frames = maxbframes;

  if (codecId == AV_CODEC_ID_H264)
  {
    av_opt_set(context->priv_data, "preset", "slow", AV_OPT_SEARCH_CHILDREN);
    // av_opt_set(context->priv_data, "profile", 	"baseline",
    // AV_OPT_SEARCH_CHILDREN);
    // av_opt_set(context->priv_data, "level", 		"3.0",
    // AV_OPT_SEARCH_CHILDREN);
    // av_opt_set(context->priv_data, "crf",  		"18",
    // AV_OPT_SEARCH_CHILDREN);
  }

  if (outAVFormatContext->oformat->flags & AVFMT_GLOBALHEADER)
  {
    context->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
  }

  if (avcodec_open2(context, codec, NULL) < 0)
  {
    // LOG(ERROR)<<" stream could not open codec ";
    // sprintf(logStr, "could not open codec");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }
  else
  {
    // LOG(INFO)<<" stream_Success : avcodec_open2() ";
  }

  stream_makeContainer();

  frame = av_frame_alloc();

  if (!frame)
  {
    // LOG(ERROR)<<" stream_ could not allocate video frame ";
    // sprintf(logStr, "could not allocate video frame");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  if (av_image_alloc(frame->data, frame->linesize, unscaled_width,
                     unscaled_height, context->pix_fmt, 32) < 0)
  {
    // LOG(ERROR)<<" stream could not allocate raw picture buffer ";
    // sprintf(logStr, "could not allocate raw picture buffer");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  status = VIDEOWRITER_STATUS_OK;
}

void streamer::stream_initScaler()
{

  /*
   * Initialize scaler context
   */
  scaleContext = sws_getContext(unscaled_width, unscaled_height, pixFmt,
                                scaled_width, scaled_height, pixFmt,
                                SWS_FAST_BILINEAR, NULL, NULL, NULL);

  /*
   * Allocate frame data
   */
  scaled_frame = av_frame_alloc();

  if (!scaled_frame)
  {

    // LOG(ERROR)<<" stream could not allocate scaler video frame ";
    // sprintf(logStr, "could not allocate scaler video frame");
    ////LOG(logStr, LOG_ERROR);
    status = VIDEOWRITER_STATUS_ERR;
    return;
  }

  // int scaled_size = avpicture_get_size(pixFmt, scaled_width, scaled_height);
  int scaled_size =
      av_image_get_buffer_size(pixFmt, scaled_width, scaled_height, 1);
  unsigned char *temp_buffer =
      (unsigned char *)av_malloc(scaled_size * sizeof(uint8_t));
  // avpicture_fill((AVPicture *)scaled_frame, temp_buffer, pixFmt,
  // scaled_width, scaled_height);

  uint8_t *video_outbuf = (uint8_t *)av_malloc(scaled_size);
  if (video_outbuf == NULL)
  {
    // LOG(ERROR)<<" stream_Error : av_malloc() ";
  }
  else
  {
    // LOG(INFO)<<" stream_Success : av_malloc() ";
  }

  value =
      av_image_fill_arrays(scaled_frame->data, scaled_frame->linesize,
                           video_outbuf, pixFmt, scaled_height, scaled_width,
                           16); // returns : the size in bytes required for src

  // sprintf(logStr, "initialized scaler to scale from %d x %d to %d x %d",
  // unscaled_width, unscaled_height, scaled_width, scaled_height);
  cout << "stream_initialized scaler to scale from " << unscaled_width << " to "
       << scaled_width << " and " << unscaled_height << " to " << scaled_height;
  ////LOG(logStr, LOG_INFO);
  // LOG(INFO)<<"  ";

  scale = true;
}
