/** @brief Main File of Vision Module
 *
 *  The Complete logic of the vision module
 *  is structured in this file.
 *
 */
#include "../include/vision.h"
#include "../../SharedMemoryService/shared_memory.h"
#include "../include/colors.h"

/**
 * Global variables of camera frame counts that will be shared through Shared
 * Memory to other
 * process for logging
 */
long long frontcamCount;  /**<  Sony FCB frame count */
long long bottomcamCount; /**<  Logitech frame count */

/**
 * Time keeper counters used in splitting of the videos.
 * "limit_seconds" is set to the required split video file duration
 */
static int seconds = 0;
static int limit_seconds = 10800;

/**
 * Class object declarations.
 * Front camera and Bottom camera writers are separated as both cameras
 * behaviour is different
 */
camera frnt_camera, bottom_camera;                           //USB Camera initialisation
IPCameraWriter primaryCamera_Writer, secondaryCamera_Writer; //IP Camera Writer Initialisation
writer frnt_wrtr;                                            //USB Camera Writer initialisation
BottomCamWriter bottom_cam_writer;
streamer frnt_strm, bottom_strm;
mpgvideo frnt_mpgvideo, bottom_mpgvideo;
Support support;

Vision VisionServer;

ParseConfigure parseconfig;

/**
 * Threads for saving and streaming
 */
pthread_t thread_bottom;
pthread_t thread_frnt;
/**
 * Thread for time keeping incase of splitting
 */
pthread_t thread_time;

bool thread_bottom_do_work; /**< Thread Control Flag for Bottom Camera */
bool thread_frnt_do_work;   /**< Thread Control Flag for Front Sony FCB Camera */

bool thread_time_do_work; /**< Thread Control Flag for Time Keep */

char front_camera_file[100];
char bottom_camera_file[100];
char front_camera_backup_file[100];

char mpg_front_camera_file[100];
char mpg_bottom_camera_file[100];

/** @brief Method called to close all the process before closing the program
 *
 *  shutdown is triggered at the instance of either at system exception
 *  signal or when a stop command is recieved
 */
void shutDown();

// /** @brief Break the video once the splitting time reaches its limit
//  *
//  *  VideoBreak is used in video splitting process once the time keeper thread
//  *  reached its max count mentioned in global time keeper variables
//  */

Vision::Vision()
{
}

Vision::~Vision()
{
}

bool Vision::preProcess()
{
  bool cameraConnectionFlag = true;
  initConfigData();

  /* check camera connectivity */
  cameraConnectionFlag = cameraConnectivity();
  if (cameraConnectionFlag == false)
  {
    std::cout << "Camera Connectivvity is False" << std::endl;
    return false;
  }

  s_memory.create("frame_count", 1024, 2);
  s_memory.update("frame_count", 1, 0);
  s_memory.update("frame_count", 2, 0);
  s_memory.create_bool("vision_status_flag", 1024);

  s_memory.create_str("videoFolderPath", 1024);

  //Creating Folder for Storage of Video Files and share the path over shared memory;

  if (FRONT_CAMERA_ENABLE_WRITE || BOTTOM_CAMERA_ENABLE_WRITE)
  {
    support.subfolderFlag = true;
    strcpy(support.directoryName, "Videos/");
    FolderFilesData();
    s_memory.update_str("videoFolderPath",
                        std::string(support.glb_sub_folder_path_name));
    getMPGvideoFileName();

    support.makeFolderintoPWD("Pictures/Observations");
    support.subfolderFlag = false;
    strcpy(support.directoryName, "Pictures/Observations/");
    support.makeDateFolder();
    s_memory.update_str("ScreenShotPath", std::string(support.glb_sub_folder_path_name));
  }
  if (CAMERATYPE == "USB")
  {
    if (BOTTOM_CAMERA_ENABLE)
    {
      bottom_camera.connect(BOTTOM_CAMERA_PORT, IO_METHOD_USERPTR, BOTTOM_WIDTH,
                            BOTTOM_HEIGHT, V4L2_PIX_FMT_YUYV);
    }

    if (FRONT_CAMERA_ENABLE)
    {
      frnt_camera.connect(FRONT_CAMERA_PORT, IO_METHOD_USERPTR, FRONT_WIDTH,
                          FRONT_HEIGHT, V4L2_PIX_FMT_YUYV);
    }

    /* control LED lights */
    if (BOTTOM_CAMERA_ENABLE)
    {
      bottom_camera.changeControl(V4L2_CID_LED1_MODE, LED_LIGHTS_OFF);
      usleep(2000);
    }
  }
  else if (CAMERATYPE == "IP")
  {
    if (FRONT_CAMERA_ENABLE)
    {
      primaryCamera_Writer.fetchStreamVariables(this->primaryCamera_URL);
    }
    if (BOTTOM_CAMERA_ENABLE)
    {
      secondaryCamera_Writer.fetchStreamVariables(this->secondaryCamera_URL);
    }
  }
  return true;
}

void Vision::saveVideo()
{

  if (BOTTOM_CAMERA_ENABLE_WRITE)
  {
    if (CAMERATYPE == "USB")
    {
      bottom_cam_writer.bottomcam_initialize(
          bottom_camera_file, AV_CODEC_ID_MPEG4, BOTTOM_WIDTH, BOTTOM_HEIGHT,
          BOTTOM_WIDTH, BOTTOM_HEIGHT, BOTTOM_FPS, BOTTOM_BITRATE_WRITER,
          BOTTOM_GOP_WRITER, AV_PIX_FMT_YUV420P, BOTTOM_MAX_B_FRAMES_WRITER,
          V4L2_PIX_FMT_YUYV, BOTTOM_CAMERA_PORT);

      bottom_mpgvideo.mpg_initialize(
          mpg_bottom_camera_file, AV_CODEC_ID_MPEG1VIDEO, BOTTOM_WIDTH,
          BOTTOM_HEIGHT, BOTTOM_WIDTH, BOTTOM_HEIGHT, BOTTOM_FPS,
          BOTTOM_BITRATE_WRITER, BOTTOM_GOP_WRITER, AV_PIX_FMT_YUV420P,
          BOTTOM_MAX_B_FRAMES_WRITER, V4L2_PIX_FMT_YUYV, BOTTOM_CAMERA_PORT);
    }
    else if (CAMERATYPE == "IP")
    {
      if (BOTTOM_CAMERA_ENABLE)
      {
        secondaryCamera_Writer.setupContainerParameters(BOTTOM_FPS, BOTTOM_BITRATE_WRITER);
        secondaryCamera_Writer.initiateContainer(std::string(bottom_camera_file));
      }
    }
  }

  if (FRONT_CAMERA_ENABLE_WRITE)
  {
    if (CAMERATYPE == "USB")
    {
      frnt_wrtr.initialize(front_camera_file, AV_CODEC_ID_MPEG4, FRONT_WIDTH,
                           FRONT_HEIGHT, FRONT_WIDTH, FRONT_HEIGHT, FRONT_FPS,
                           FRONT_BITRATE_WRITER, FRONT_GOP_WRITER,
                           AV_PIX_FMT_YUV420P, FRONT_MAX_B_FRAMES_WRITER,
                           V4L2_PIX_FMT_YUYV, FRONT_CAMERA_PORT);

      frnt_mpgvideo.mpg_initialize(
          mpg_front_camera_file, AV_CODEC_ID_MPEG1VIDEO, FRONT_WIDTH,
          FRONT_HEIGHT, FRONT_WIDTH, FRONT_HEIGHT, FRONT_FPS,
          FRONT_BITRATE_WRITER, FRONT_GOP_WRITER, AV_PIX_FMT_YUV420P,
          FRONT_MAX_B_FRAMES_WRITER, V4L2_PIX_FMT_YUYV, FRONT_CAMERA_PORT);
    }
    else if (CAMERATYPE == "IP")
    {
      if (FRONT_CAMERA_ENABLE)
      {
        primaryCamera_Writer.setupContainerParameters(FRONT_FPS, FRONT_BITRATE_WRITER);
        primaryCamera_Writer.initiateContainer(std::string(front_camera_file));
      }
    }
  }

  std::cout << "FRONT_FPS :" << FRONT_FPS << "\nFRONT_WIDTH : " << FRONT_WIDTH << "\nFRONT_HEIGHT :" << FRONT_HEIGHT << std::endl;
  //sleep(100);
}

void Vision::streamVideo()
{
  if (FRONT_CAMERA_ENABLE_STREAM)
  {
    string BOTTOM_URL_STREAM;
    string FRONT_URL_STREAM;

    char url_bottom[100];
    char url_front[100];

    /* output stream class to operate on string */
    ostringstream buffer_bottom;
    ostringstream buffer_front;

    buffer_bottom << "http://localhost:" << BOTTOM_PORT_NO << "/"
                  << "q"
                  << "/" << BOTTOM_WIDTH << "/" << BOTTOM_HEIGHT;

    BOTTOM_URL_STREAM = buffer_bottom.str();
    strcpy(url_bottom, BOTTOM_URL_STREAM.c_str());

    if (BOTTOM_CAMERA_ENABLE_STREAM)
    {
      bottom_strm.stream_initialize(
          url_bottom, AV_CODEC_ID_MPEG1VIDEO, BOTTOM_WIDTH, BOTTOM_HEIGHT,
          BOTTOM_WIDTH, BOTTOM_HEIGHT, BOTTOM_FPS, BOTTOM_BITRATE_STREAMER,
          BOTTOM_GOP_STREAMER, AV_PIX_FMT_YUV420P, BOTTOM_MAX_B_FRAMES_STREAMER,
          V4L2_PIX_FMT_YUYV, BOTTOM_CAMERA_PORT);
    }

    buffer_front << "http://localhost:" << FRONT_PORT_NO << "/"
                 << "q"
                 << "/" << FRONT_WIDTH << "/" << FRONT_HEIGHT;

    FRONT_URL_STREAM = buffer_front.str();
    strcpy(url_front, FRONT_URL_STREAM.c_str());

    if (FRONT_CAMERA_ENABLE_STREAM)
    {
      frnt_strm.stream_initialize(
          url_front, AV_CODEC_ID_MPEG1VIDEO, FRONT_WIDTH, FRONT_HEIGHT,
          FRONT_WIDTH, FRONT_HEIGHT, FRONT_FPS, FRONT_BITRATE_STREAMER,
          FRONT_GOP_STREAMER, AV_PIX_FMT_YUV420P, FRONT_MAX_B_FRAMES_STREAMER,
          V4L2_PIX_FMT_YUYV, FRONT_CAMERA_PORT);
    }
  }
}

void Vision::initiateProcessing()
{
  if ((CAMERATYPE == "USB") || FRONT_CAMERA_ENABLE_STREAM || BOTTOM_CAMERA_ENABLE_STREAM)
  {
    if (BOTTOM_CAMERA_ENABLE)
    {
      thread_bottom_do_work = true;
      pthread_create(&thread_bottom, NULL, bottomCamWorker, (void *)NULL);
    }

    if (FRONT_CAMERA_ENABLE)
    {
      thread_frnt_do_work = true;
      pthread_create(&thread_frnt, NULL, frontCamWorker, (void *)NULL);
    }

    if (FRONT_CAMERA_ENABLE || BOTTOM_CAMERA_ENABLE)
    {
      thread_time_do_work = true;
      pthread_create(&thread_time, NULL, timeKeeper, (void *)NULL);
    }
  }
  else if (CAMERATYPE == "IP")
  {
    if (FRONT_CAMERA_ENABLE)
    {
      strcpy(primaryCamera_Writer.cam, "FrontCam");
      primaryCamera_Writer.writeFrames();
    }
    if (BOTTOM_CAMERA_ENABLE)
    {
      strcpy(secondaryCamera_Writer.cam, "BottomCam");
      secondaryCamera_Writer.writeFrames();
    }
  }

  SERVICE_STATUS = true;
  long int count = 0;

  /*****Updating the vision status*****/
  s_memory.update_bool("vision_status_flag", true);
  s_memory.create_bool("Vision_response_flag", 1024);
  s_memory.update_bool("Vision_response_flag", true);
}

void Vision::visionLog(std::string str)
{
  ofstream ofile;
  char visionlog_path[100];
  strcpy(visionlog_path, support.glb_sub_folder_path_name);
  strcat(visionlog_path, "/visionlog.txt");

  cout << "********* " << visionlog_path << "************" << endl;
  ofile.open(visionlog_path, std::ios_base::app);
  ofile << str << endl;

  ofile.close();
}

void Vision::deleteTempFiles(char *tempfiles)
{

  // std::cout << "deleting temp files " << std::endl;
  if (remove(tempfiles) != 0)
  {
    std::cout << "Unable to delete the video file !\n"
              << std::endl;
  }
  else
  {
    std::cout << "Video file has been deleted !\n"
              << std::endl;
  }
}

bool Vision::validateVideos(char *camera_file, AVFormatContext **avformatCtx, AVCodecContext **avcodecCtx)
{
  cout << " inside validate videos " << endl;
  int retval;
  bool filestatus = true;
  string retstr;

  if (retval = avformat_open_input(avformatCtx, camera_file, NULL, NULL) < 0)
  {
    avformat_close_input(avformatCtx);
    retstr = "unable to open the file\n";
    visionLog(retstr);
    return false;
  }
  else
  {
    retstr = "file opened successfully\n";
  }

  if (retval = avformat_find_stream_info(*avformatCtx, NULL) < 0)
  {
    avformat_close_input(avformatCtx);
    string retstr = "unable to find the media stream info\n";
    visionLog(retstr);
    return false;
  }
  else
  {
    retstr = "media stream information found\n";
    visionLog(retstr);
  }

  std::cout << "showing file information for " << camera_file << std::endl;
  av_dump_format(*avformatCtx, 0, camera_file, 0);

  retstr = "deleting the temp files\n";

  visionLog(retstr);
  avformat_close_input(avformatCtx);
  return true;
}

bool Vision::validateVideoFiles(char *front_camera_file, char *bottom_camera_file)
{

  sleep(2);
  std::cout << front_camera_file << std::endl
            << bottom_camera_file << std::endl;
  AVFormatContext *front_camera_file_avformatctx = NULL, *bottom_camera_file_avformatctx = NULL;
  AVCodecContext *front_camera_file_codecctx = NULL, *bottom_camera_file_codecctx = NULL;

  bool front_status = validateVideos(front_camera_file, &front_camera_file_avformatctx, &front_camera_file_codecctx);
  bool bottom_status = validateVideos(bottom_camera_file, &bottom_camera_file_avformatctx, &bottom_camera_file_codecctx);

  return front_status && bottom_status;
}

/**
 * Convert Char* to std::string from all popen returns
 */
std::string Vision::convertCharToString(char *charArray)
{
  std::string tempString(charArray, charArray + strlen(charArray));
  return tempString;
}

std::string remove_nextLine(std::string inputString)
{

  inputString.erase(std::remove(inputString.begin(), inputString.end(), '\n'),
                    inputString.end());

  return inputString;
}

/**
 * Signal handler function
 */
void recv_signal(int s)
{

  std::cout << FRED("[Info][Vision][Signal Handler] Signal Recieved is ") << s
            << std::endl;
  if (s == SIGPIPE)
  {
    // Just ignore, sock class will close the client the next time
  }
  // Catch ctrl-c, segmentation fault, abort signal
  if (s == SIGINT || s == SIGSEGV || s == SIGABRT || s == SIGTSTP ||
      s == SIGKILL || s == SIGTERM || s == SIGHUP)
  {
    shutDown();
  }
}

void shutDown()
{

  // usleep(DELAY);
  SERVICE_STATUS = false;
  usleep(DELAY);

  /**
   * Stopping the Front Camera threads and Releasing the Resources
   */

  if (CAMERATYPE == "USB")
  {
    if (FRONT_CAMERA_ENABLE)
    {
      thread_time_do_work =
          false; /**< Toggle the Time keeper thread flag to stop*/
      thread_frnt_do_work =
          false; /**< Toggle the Time keeper thread falg to stop*/
      (void)pthread_join(thread_frnt, NULL);
      usleep(DELAY);
      frnt_camera.disconnect(); /**< Disconect the Front Camera*/
      usleep(DELAY);
      if (FRONT_CAMERA_ENABLE_STREAM)
      {
        frnt_strm.stream_close();
      }
      usleep(DELAY);

      if (FRONT_CAMERA_ENABLE_WRITE)
      {
        frnt_wrtr.dumpTrailer();
        frnt_wrtr.close();
      }
    }
  }
  else if (CAMERATYPE == "IP")
  {

    primaryCamera_Writer.stopWriter();
    usleep(500000);

    if (FRONT_CAMERA_ENABLE_WRITE)
    {
      primaryCamera_Writer.dumpTrailer();
      usleep(DELAY);
    }
  }

  /**
   * Stopping the Bottom Camera threads and Releasing the Resources
   */

  if (CAMERATYPE == "USB")
  {
    if (BOTTOM_CAMERA_ENABLE)
    {
      thread_time_do_work =
          false; /**< Toggle the Time keeper thread flag to stop*/
      thread_bottom_do_work =
          false; /**< Toggle the bottom Camera thread falg to stop*/
      (void)pthread_join(thread_bottom, NULL);
      usleep(DELAY);

      if (CAMERATYPE == "USB")
      {
        bottom_camera.disconnect(); /**< Disconect the Bottom Camera*/
        usleep(DELAY);

        if (BOTTOM_CAMERA_ENABLE_STREAM)
        {
          bottom_strm.stream_close();
        }
        usleep(DELAY);
      }

      if (BOTTOM_CAMERA_ENABLE_WRITE)
      {
        bottom_cam_writer.bottomcam_dumpTrailer();
        bottom_cam_writer.bottomcam_close();
      }
    }
  }
  else if (CAMERATYPE == "IP")
  {

    secondaryCamera_Writer.stopWriter();
    usleep(500000);

    if (BOTTOM_CAMERA_ENABLE_WRITE)
    {
      secondaryCamera_Writer.dumpTrailer();
      usleep(DELAY);
    }
  }

  s_memory.update("frame_count", 1, 0);
  s_memory.update("frame_count", 2, 0);
  printf("\nReleased Bottom camera resources\n");

  usleep(DELAY);
  if (CAMERATYPE == "USB")
  {
    bool filestatus = VisionServer.validateVideoFiles(front_camera_file, bottom_camera_file);
    if (filestatus)
    {
      VisionServer.deleteTempFiles(mpg_front_camera_file);
      VisionServer.deleteTempFiles(mpg_bottom_camera_file);
    }
    (void)pthread_join(thread_time, NULL);
  }

  if (s_memory.fetch_bool("vision_status_flag"))
  {
    s_memory.update_bool("vision_status_flag", false);
  }

  if (s_memory.fetch_bool("Vision_response_flag"))
  {
    s_memory.update_bool("Vision_response_flag", true);
  }

  s_memory.update_str("videoFolderPath",
                      "");
  usleep(1000000);

  if (CAMERATYPE == "IP")
  {

    if (primaryCamera_Writer.CAMERAFOUND)
    {

      pthread_join(primaryCamera_Writer.writer_Thread, NULL);
    }
    if (secondaryCamera_Writer.CAMERAFOUND)
    {

      pthread_join(secondaryCamera_Writer.writer_Thread, NULL);
    }
  }

  exit(0);
}

/**
  * Signal handler
  */
void initSigHandler()
{

  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = recv_signal;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  sigaction(SIGTSTP, &sigIntHandler, NULL);
  sigaction(SIGPIPE, &sigIntHandler, NULL);
  sigaction(SIGABRT, &sigIntHandler, NULL);
  sigaction(SIGSEGV, &sigIntHandler, NULL);
  sigaction(SIGKILL, &sigIntHandler, NULL);
  sigaction(SIGTERM, &sigIntHandler, NULL);
  sigaction(SIGHUP, &sigIntHandler, NULL);
}

// std::string GetStdoutFromCommand(std::string cmd)
// {

//   std::string data;
//   FILE *stream;
//   const int max_buffer = 256;
//   char buffer[max_buffer];
//   cmd.append(" 2>&1");

//   stream = popen(cmd.c_str(), "r");
//   if (stream)
//   {
//     while (!feof(stream))
//       if (fgets(buffer, max_buffer, stream) != NULL)
//         data.append(buffer);
//     pclose(stream);
//   }
//   return data;
// }
std::string resolveIpAddress(std::string cmd)
{
  char buffer[1024];
  std::string result = "";
  FILE *pipe = popen(cmd.c_str(), "r");
  if (!pipe)
    throw std::runtime_error("popen() failed!");
  try
  {
    if (fgets(buffer, sizeof buffer, pipe) != NULL)
    {
      pclose(pipe);
      return buffer;
    }
    else
    {
      pclose(pipe);
      return "NULL";
    }
  }
  catch (...)
  {
    pclose(pipe);
    throw;
  }
}
std::string GetStdoutFromCommand(std::string cmd)
{

  std::string data;
  FILE *stream;
  const int max_buffer = 256;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream)
  {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL)
        data.append(buffer);
    pclose(stream);
  }
  return data;
}
std::string GetIpAddress(std::string cmd)
{

  std::string data;
  FILE *stream;
  const int max_buffer = 256;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream)
  {
    while (!feof(stream))
    {
      if (fgets(buffer, max_buffer, stream) != NULL)
      {
        if (buffer != "\n")
        {
          std::string resolve_cmd = "avahi-resolve -a " + std::string(buffer).substr(1, 13);
          std::string ip = resolveIpAddress(resolve_cmd);
          if (ip != "NULL")
          {
            pclose(stream);
            return std::string(buffer).substr(1, 13);
          }
        }
      }
    }
  }
  return "";
}

std::string getGatewayDefault()
{
  std::string gatewayIP;
  // std::string gateway_cmd = "route | grep gateway  | grep 'UG[ \t]' | awk '{print $2}'";
  std::string gateway_cmd = "ip route | awk '/default/ { print $3 }'";

  gatewayIP = remove_nextLine(GetStdoutFromCommand(gateway_cmd));
  std::cout << "Gateway: " << gatewayIP << std::endl;

  return gatewayIP;
}

void pingNetwork()
{

  std::string ping_cmd;

  std::string IPGateway = getGatewayDefault();
  if (IPGateway != "")
  {
    std::cout << "Pinging Network Hosts" << std::endl;

    ping_cmd = "nmap -sP " + IPGateway + "/24 -n --send-ip >>/dev/null 2>>/dev/null";
    // std::cout << "Ping Network Command: " << ping_cmd << std::endl;
    int ret = system(ping_cmd.c_str());
    std::cout << "Arp Running" << std::endl;
    ret = system("arp -an >>/dev/null 2>>/dev/null");
  }
}

// bool Vision::checkIPCameraConnection(std::string macAddress)
// {
//   if (macAddress == "")
//   {
//     std::cout << "MAC Address is empty please check" << std::endl;
//     return false;
//   }
//   else
//   {
//     std::string IPAddress;
//     std::string getIPCommand = "arp -an | grep \"" + macAddress + "\" | cut -d ' ' -f 2";
//     std::cout << "Get IP Command " << getIPCommand << std::endl;
//     IPAddress = GetStdoutFromCommand(getIPCommand);
//     if (IPAddress == "")
//     {
//       std::cout << "No IP address found for the MAC address" << std::endl;
//     }
//     else
//     {
//       IPAddress = IPAddress.substr(1, IPAddress.size() - 3);
//       this->tmp_IP = IPAddress;
//       std::cout << "IP Address: " << IPAddress << std::endl;

//       return true;
//     }
//   }
//   return false;
// }
bool Vision::checkIPCameraConnection(std::string macAddress)
{
  if (macAddress == "")
  {
    std::cout << "MAC Address is empty please check" << std::endl;
    return false;
  }
  else
  {
    std::string IPAddress;
    std::string getIPCommand = "arp -an | grep \"" + macAddress + "\" | cut -d ' ' -f 2";
    std::cout << "Get IP Command " << getIPCommand << std::endl;
    IPAddress = GetIpAddress(getIPCommand);
    if (IPAddress == "")
    {
      std::cout << "No IP address found for the MAC address" << std::endl;
    }
    else
    {
      this->tmp_IP = IPAddress;
      std::cout << "IP Address: " << IPAddress << std::endl;

      return true;
    }
  }
  return false;
}
/**
  * Check Bottom camera connectivity
  */
bool Vision::Bottom_CameraConnectivity()
{
  FILE *bottom_cam_file;
  bool cameraStatus = true;
  try
  {

    bottom_cam_file = fopen(BOTTOM_CAMERA_PORT, "r");

    if (bottom_cam_file == NULL)
    {
      cameraStatus = false;
      throw "[Error][Vision][BottomCamerConnectivity] Bottom Camera Not "
            "Connected";
    }
  }
  catch (char const *e)
  {
    std::cout << KRED << e << RST << std::endl
              << std::endl;
  }
  catch (std::exception &e)
  {
    std::cout << "[Error][Vision Server][BottomCamerConnectivity] " << e.what()
              << std::endl;
  }
  if (cameraStatus)
  {
    fclose(bottom_cam_file); /**< Close only if the fopen returns success*/
  }
  return cameraStatus;
}

/**
  * Check Front camera connectivity
  */
bool Vision::Front_CameraConnectivity()
{
  FILE *front_cam_file;
  bool cameraStatus = true;

  try
  {

    front_cam_file = fopen(FRONT_CAMERA_PORT, "r");
    if (front_cam_file == NULL)
    {
      cameraStatus = false;
      throw "[Error][Vision][FrontCamerConnectivity] Front Camera Not "
            "Connected";
    }
  }
  catch (char const *e)
  {
    std::cout << KRED << e << RST << std::endl
              << std::endl;
  }
  catch (std::exception &e)
  {
    std::cout << "[Error][Vision Server][FrontCamerConnectivity] " << e.what()
              << std::endl;
  }
  if (cameraStatus)
  {
    fclose(front_cam_file); /**< Close only if the fopen returns success*/
  }

  return cameraStatus;
}

/**
  * Checks the Cameras Connection Status
  */
bool Vision::cameraConnectivity()
{
  if (CAMERATYPE == "USB")
  {
    EnableCameraAccess();

    FRONT_CAMERA_ENABLE = Front_CameraConnectivity();

    BOTTOM_CAMERA_ENABLE = Bottom_CameraConnectivity();

    //if both the cameras a not connected stop the process
    if ((FRONT_CAMERA_ENABLE == false) && (BOTTOM_CAMERA_ENABLE == false))
    {
      return false;
    }
  }
  else if (CAMERATYPE == "IP")
  {
    FRONT_CAMERA_ENABLE = checkIPCameraConnection(primaryCamera_MAC);
    if (!FRONT_CAMERA_ENABLE)
    {
      pingNetwork();
    }
    FRONT_CAMERA_ENABLE = checkIPCameraConnection(primaryCamera_MAC);

    if (FRONT_CAMERA_ENABLE)
    {
      this->primaryCamera_IP = this->tmp_IP;
      this->primaryCamera_URL = "rtsp://" + this->primaryCamera_UserName + ":" + this->primaryCamera_Password + "@" + this->primaryCamera_IP + ":554";
      std::cout << "URL: " << this->primaryCamera_URL << std::endl;
    }
    else
    {
      std::cout << "[Camera Connectivity][ERROR]Primary Camera is NOT CONNECTED" << std::endl;
    }

    BOTTOM_CAMERA_ENABLE = checkIPCameraConnection(secondaryCamera_MAC);
    // BOTTOM_CAMERA_ENABLE = false;

    if (BOTTOM_CAMERA_ENABLE)
    {
      this->secondaryCamera_IP = this->tmp_IP;
      this->secondaryCamera_URL = "rtsp://" + this->secondaryCamera_UserName + ":" + this->secondaryCamera_Password + "@" + this->secondaryCamera_IP + ":554";
      std::cout << "URL: " << this->secondaryCamera_URL << std::endl;
    }
    else
    {
      std::cout << "[Camera Connectivity][ERROR]Secondary Camera is NOT CONNECTED" << std::endl;
    }

    //if both the cameras a not connected stop the process
    if ((FRONT_CAMERA_ENABLE == false) && (BOTTOM_CAMERA_ENABLE == false))
    {
      return false;
    }
  }
  return true;
}

/** @brief Keep the Track of Video Splitting time
  *
  * Once the Time keeper reaches the set limit time(Varibale name: limit_seconds
  * ) it breaks the video and
  * makes a new split video
  */
void *Vision::timeKeeper(void *threadarg)
{
  try
  {
    while (thread_time_do_work)
    {
      seconds++;
      sleep(1);

      if (seconds == limit_seconds)
      {
        support.getFileName();
        VisionServer.videoBreak();
        VisionServer.videoRec();
        seconds = 0;
      }
    }
  }
  catch (char const *e)
  {
    std::cout << KRED << e << RST << std::endl
              << std::endl;
  }
  catch (std::exception &e)
  {
    std::cout << "[Error][Vision Server][timeKeeper] " << e.what() << std::endl;
  }

  pthread_exit(NULL);
}

void *Vision::bottomCamWorker(void *threadarg)
{
  while (thread_bottom_do_work)
  {
    unsigned char *img = bottom_camera.readFrame();
    if (img == NULL)
    {
      std::cout << "[Error][Vision][bottomCamWorker] Read Frame timedout and returned null" << std::endl;
    }
    else
    {
      bottom_strm.stream_writeFrame(img, bottom_camera.getBytesUsed());
      if (seconds < limit_seconds)
      {
        bottom_cam_writer.bottomcam_writeFrame(img, bottom_camera.getBytesUsed());
        bottom_mpgvideo.mpg_writeFrame(img, bottom_camera.getBytesUsed());
      }
    }
  }

  pthread_exit(NULL);
}

void *Vision::frontCamWorker(void *threadarg)
{

  while (thread_frnt_do_work)
  {
    unsigned char *img = frnt_camera.readFrame();
    if (img == NULL)
    {
      std::cout << "[Error][Vision][frontCamWorker] Read Frame timedout and returned null" << std::endl;
    }
    else
    {
      frnt_strm.stream_writeFrame(img, frnt_camera.getBytesUsed());
      if (seconds < limit_seconds)
      {
        frnt_wrtr.writeFrame(img, frnt_camera.getBytesUsed());
        frnt_mpgvideo.mpg_writeFrame(img, frnt_camera.getBytesUsed());
      }
    }
  }

  pthread_exit(NULL);
}

void Vision::videoRec()
{

  strcpy(front_camera_file, support.glb_sub_folder_path_name);
  strcpy(bottom_camera_file, support.glb_sub_folder_path_name);

  strcat(front_camera_file, "/");
  strcat(bottom_camera_file, "/");

  strcat(front_camera_file, support.curr_file_front);
  strcat(bottom_camera_file, support.curr_file_bottom);

  if (BOTTOM_CAMERA_ENABLE_WRITE)
  {
    bottom_cam_writer.bottomcam_initialize(
        bottom_camera_file, AV_CODEC_ID_MPEG4, BOTTOM_WIDTH, BOTTOM_HEIGHT,
        BOTTOM_WIDTH, BOTTOM_HEIGHT, BOTTOM_FPS, BOTTOM_BITRATE_WRITER,
        BOTTOM_GOP_WRITER, AV_PIX_FMT_YUV420P, BOTTOM_MAX_B_FRAMES_WRITER,
        V4L2_PIX_FMT_YUYV, BOTTOM_CAMERA_PORT);

    bottom_mpgvideo.mpg_initialize(
        mpg_bottom_camera_file, AV_CODEC_ID_MPEG1VIDEO, BOTTOM_WIDTH,
        BOTTOM_HEIGHT, BOTTOM_WIDTH, BOTTOM_HEIGHT, BOTTOM_FPS,
        BOTTOM_BITRATE_WRITER, BOTTOM_GOP_WRITER, AV_PIX_FMT_YUV420P,
        BOTTOM_MAX_B_FRAMES_WRITER, V4L2_PIX_FMT_YUYV, BOTTOM_CAMERA_PORT);
  }

  if (FRONT_CAMERA_ENABLE_WRITE)
  {

    // FRONT_BITRATE_WRITER = 1900000;
    frnt_wrtr.initialize(front_camera_file, AV_CODEC_ID_MPEG4, FRONT_WIDTH,
                         FRONT_HEIGHT, FRONT_WIDTH, FRONT_HEIGHT, FRONT_FPS,
                         FRONT_BITRATE_WRITER, FRONT_GOP_WRITER,
                         AV_PIX_FMT_YUV420P, FRONT_MAX_B_FRAMES_WRITER,
                         V4L2_PIX_FMT_YUYV, FRONT_CAMERA_PORT);

    frnt_mpgvideo.mpg_initialize(
        mpg_front_camera_file, AV_CODEC_ID_MPEG1VIDEO, FRONT_WIDTH,
        FRONT_HEIGHT, FRONT_WIDTH, FRONT_HEIGHT, FRONT_FPS,
        FRONT_BITRATE_WRITER, FRONT_GOP_WRITER, AV_PIX_FMT_YUV420P,
        FRONT_MAX_B_FRAMES_WRITER, V4L2_PIX_FMT_YUYV, FRONT_CAMERA_PORT);
  }

  return;
}

void Vision::videoBreak()
{
  if (FRONT_CAMERA_ENABLE)
  {

    if (FRONT_CAMERA_ENABLE_WRITE)
    {
      frnt_wrtr.dumpTrailer();
      frnt_wrtr.close();
      frnt_mpgvideo.mpg_close();
    }
    std::cerr << KBLU << "[Info][Vision Server][videoBreak] Front video "
                         "footage break success !"
              << RST << std::endl;
  }

  if (BOTTOM_CAMERA_ENABLE)
  {

    if (BOTTOM_CAMERA_ENABLE_WRITE)
    {
      bottom_cam_writer.bottomcam_dumpTrailer();
      bottom_cam_writer.bottomcam_close();
      bottom_mpgvideo.mpg_close();
    }
    std::cerr << KBLU << "[Info][Vision Server][videoBreak] Bottom video "
                         "footage break success !"
              << RST << std::endl;
  }
}

/* Make folder at Ubuntu's Videos and return folder path and file names for
 * front and bottom camera */
void Vision::FolderFilesData()
{
  support.folder_file_maker();
  strcpy(front_camera_file,
         support.sub_folder_path_name_front); // get front camera file name
  strcpy(bottom_camera_file,
         support.sub_folder_path_name_bottom); // get bottom camera file name

  //	cout<<"\n\n\nFolderFilesData : front :: "<<front_camera_file;
  //	cout<<"\n\n\nFolderFilesData : bottom :: "<<bottom_camera_file;
}

/* provide mpgvideo file names along with path */
void Vision::getMPGvideoFileName()
{
  strcpy(mpg_front_camera_file, support.glb_sub_folder_path_name);
  strcpy(mpg_bottom_camera_file, support.glb_sub_folder_path_name);

  strcat(mpg_front_camera_file, "/TEMP_front.mpg");
  strcat(mpg_bottom_camera_file, "/TEMP_bottom.mpg");
}

void Vision::initConfigData()
{

  parseconfig.initHashObject();
  parseconfig.initBottomConfigData();
  parseconfig.initFrontConfigData();

  sleep(1);

  CAMERATYPE = parseconfig.CAMERATYPE;
  if (CAMERATYPE == "USB")
  {
    USBCAMERA = true;
  }
  primaryCamera_MAC = parseconfig.PRIMARYCAMERA_MAC;
  std::cout << "Primary MAC: " << primaryCamera_MAC << std::endl;
  secondaryCamera_MAC = parseconfig.SECONDARYCAMERA_MAC;
  std::cout << "Secondary MAC: " << secondaryCamera_MAC << std::endl;
  ;

  BOTTOM_WIDTH = parseconfig.PARSE_BOTTOM_WIDTH;
  BOTTOM_HEIGHT = parseconfig.PARSE_BOTTOM_HEIGHT;
  BOTTOM_FPS = parseconfig.PARSE_BOTTOM_FPS;

  BOTTOM_GOP_STREAMER = parseconfig.PARSE_BOTTOM_GOP_STREAMER;
  BOTTOM_GOP_WRITER = parseconfig.PARSE_BOTTOM_GOP_WRITER;

  BOTTOM_MAX_B_FRAMES_WRITER = parseconfig.PARSE_BOTTOM_MAX_B_FRAMES_WRITER;
  BOTTOM_MAX_B_FRAMES_STREAMER = parseconfig.PARSE_BOTTOM_MAX_B_FRAMES_STREAMER;

  BOTTOM_BITRATE_STREAMER = parseconfig.PARSE_BOTTOM_BITRATE_STREAMER;
  BOTTOM_BITRATE_WRITER = parseconfig.PARSE_BOTTOM_BITRATE_WRITER;
  BOTTOM_PORT_NO = parseconfig.PARSE_BOTTOM_PORT_NO;

  FRONT_WIDTH = parseconfig.PARSE_FRONT_WIDTH;
  FRONT_HEIGHT = parseconfig.PARSE_FRONT_HEIGHT;

  FRONT_FPS = parseconfig.PARSE_FRONT_FPS;
  FRONT_GOP_WRITER = parseconfig.PARSE_FRONT_GOP_WRITER;
  FRONT_GOP_STREAMER = parseconfig.PARSE_FRONT_GOP_STREAMER;

  FRONT_MAX_B_FRAMES_STREAMER = parseconfig.PARSE_FRONT_MAX_B_FRAMES_STREAMER;
  FRONT_MAX_B_FRAMES_WRITER = parseconfig.PARSE_FRONT_MAX_B_FRAMES_WRITER;

  FRONT_BITRATE_STREAMER = parseconfig.PARSE_FRONT_BITRATE_STREAMER;
  FRONT_BITRATE_WRITER = parseconfig.PARSE_FRONT_BITRATE_WRITER;
  FRONT_PORT_NO = parseconfig.PARSE_FRONT_PORT_NO;

  primaryCamera_UserName = parseconfig.primaryCamera_UserName;
  primaryCamera_Password = parseconfig.primaryCamera_Password;

  secondaryCamera_UserName = parseconfig.secondaryCamera_UserName;
  secondaryCamera_Password = parseconfig.secondaryCamera_Password;

  sleep(1);
}

void Vision::EnableCameraAccess()
{

  /* Get ROV user name */
  FILE *in;
  char UserName[512];

  if (!(in = popen("cd /home && ls", "r")))
  {
    cout << "\ncommand Error!\n";
    return;
  }

  while (fgets(UserName, sizeof(UserName), in) != NULL)
  {
  }
  pclose(in);

  std::string ownCamerasToLocalUser_Command;

  ownCamerasToLocalUser_Command =
      "chown " + remove_nextLine(convertCharToString(UserName)) + " /dev/vid*";

  usleep(10);

  /* Enable camera access permissions */
  system(ownCamerasToLocalUser_Command.c_str());
}

int main()
{
  int frontCameraCount_past = 0, bottomCameraCount_past = 0, frameNumberCheckingIterator = 0;
  bool cameraConnectionFlag;
  initSigHandler();

  cameraConnectionFlag = VisionServer.preProcess();
  if (cameraConnectionFlag)
  {
    VisionServer.saveVideo();
    usleep(DELAY);

    // VisionServer.streamVideo();

    VisionServer.initiateProcessing();
    SERVICE_STATUS = true;

    while (SERVICE_STATUS == true)
    {
      // std::cerr << KRED << "[DEBUG][Vision][Main] in the while loop !"
      //           << RST << std::endl;
      if (CAMERATYPE == "USB")
      {
        frontcamCount = frnt_wrtr.frameCount;
        bottomcamCount = bottom_cam_writer.frameCount;
      }
      else if (CAMERATYPE == "IP")
      {
        frontcamCount = primaryCamera_Writer.frameCount;
        bottomcamCount = secondaryCamera_Writer.frameCount;
        if (frameNumberCheckingIterator == 0)
        {
          frontCameraCount_past = frontcamCount;
          bottomCameraCount_past = bottomcamCount;
        }
        else if (frameNumberCheckingIterator == 20)
        {
          if (frontCameraCount_past == frontcamCount)
          {
            SERVICE_STATUS = false;
            std::cout << "[Error][Vision] Front Camera Object not receiving frames" << std::endl;
          }
          else if (bottomCameraCount_past == bottomcamCount)
          {
            SERVICE_STATUS = false;
            std::cout << "[Error][Vision] Bottom Camera Object not receiving frames" << std::endl;
          }
          frameNumberCheckingIterator = 0;
        }
        frameNumberCheckingIterator++;
      }

      std::cout << "fcount: " << frontcamCount << " bcount: " << bottomcamCount
                << std::endl;
      s_memory.update("frame_count", 1, frontcamCount);
      s_memory.update("frame_count", 2, bottomcamCount);
      SERVICE_STATUS = s_memory.fetch_bool("vision_status_flag");
      usleep(15000);
    }
    if (!SERVICE_STATUS)
    {
      std::cerr << KYEL << "[Warning!!!][Vision][Main] Recieved shutDown Signal from GUI !"
                << RST << std::endl;
    }

    // ValidateVideoFiles(front_camera_file, bottom_camera_file);

    shutDown();

    if (CAMERATYPE == "IP")
    {
      if (primaryCamera_Writer.CAMERAFOUND)
      {

        pthread_join(primaryCamera_Writer.writer_Thread, NULL);
      }
      if (secondaryCamera_Writer.CAMERAFOUND)
      {

        pthread_join(secondaryCamera_Writer.writer_Thread, NULL);
      }
    }
  }
  else
  {
    std::cout << "[Warning][Vision][Main]No Cameras found Exiting the Vision Storage Process" << std::endl;
  }

  cout << "\n\n-----EXIT_SUCCESS-----\n\n";
  return 0;
}
