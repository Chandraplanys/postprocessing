#include<iostream>
#include"../SharedMemoryService/shared_memory.h"

using namespace std;

int main()
{

SMemory s_test;
s_test.create_bool("vision_status_flag",1024);


if(s_test.fetch_bool("vision_status_flag"))
{
s_test.update_bool("vision_status_flag",false);
}
else
{
s_test.update_bool("vision_status_flag",true);
}

return 0;
}
