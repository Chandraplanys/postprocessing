#include "test.h"

int message_change_number = 0;

//################ Overriding Private onmessage Function ######################
// Sending 'successful' message  when there is messge from client side
// on private channel
void MyWebsocketApi::PrivateOnmessage(
    std::shared_ptr<WsServer::Connection> connection, std::string msg) {
  std::cout << "[Override Func][Private][Message Recieved] " << msg
            << std::endl;
  if (msg == "Hand Shake") {
    this->PrivateSend(connection, "connection established successfully");
  } else {
    this->PrivateSend(connection, "successful");
  }
}

//############### Overriding Broadcast onmessage Function ######################
// Changing the broadcast message when there is message from client side
// on broadcast channel
void MyWebsocketApi::BroadcastOnmessage(
    std::shared_ptr<WsServer::Connection> connection, std::string msg) {
  std::cout << "[Override Func][Broadcast][Message Recieved] " << msg
            << std::endl;
  message_change_number++;
  std::string set_message =
      "Message No: " + std::to_string(message_change_number);
  this->SetBroadcastMessage(set_message);
}

int main(int argc, char const *argv[]) {
  // Create obj with server setup and start server
  MyWebsocketApi wa(8090);
  wa.StartServer();

  // For NO terminal outputs of message logs from api
  wa.terminal_ouputs = true;

  // changing broadcast message
  sleep(1);
  wa.SetBroadcastMessage("Changed Message");


  pthread_join(wa.broadcast_thread,NULL);
  pthread_join(wa.server_thread, NULL);

  return 0;
}
