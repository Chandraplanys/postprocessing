#ifndef TEST_H
#define TEST_H

#include "websocket_api.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

class MyWebsocketApi : public WebsocketApi {
public:
  MyWebsocketApi(unsigned int port = 8000,
                 unsigned int broadcast_delay_in_micro_sec = 500000,
                 bool require_broadcast = true, bool require_private = true)
      : WebsocketApi(port, broadcast_delay_in_micro_sec, require_broadcast,
                     require_private) {}
  void BroadcastOnmessage(std::shared_ptr<WsServer::Connection> connection,
                          std::string msg);

  void PrivateOnmessage(std::shared_ptr<WsServer::Connection> connection,
                        std::string msg);
};

#endif // TEST_H
