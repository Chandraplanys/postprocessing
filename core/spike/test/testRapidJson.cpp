#include <iostream>
#include "rapidjson/document.h"

using namespace rapidjson;

int main(int argc, char const *argv[])
{

    std::string jsonString = "{\"helo\": \"world\"}";

    Document data;
    data.Parse(jsonString.c_str());
    assert(data.HasMember("hello"));
    assert(data["hello"].IsString());
    // printf("hello = %s\n", data["hello"].GetString());
    std::cout << "Hello: " << data["hello"].GetString() << std::endl;
    return 0;
}
