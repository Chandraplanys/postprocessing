#include "opencv2/opencv.hpp"
extern "C" {
#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
}
#include <iostream>
#include <string>

using namespace cv;
/*
This functions opens a video file and extracts the frames and put them into a vector of Mat(its the class for representing an img)
*/
void extract_frames(const std::string &videoFilePath,std::vector<Mat>& frames){
	
	try{
		//open the video file
  	VideoCapture cap(videoFilePath); // open the video file
  	if(!cap.isOpened())  // check if we succeeded
  		CV_Error(CV_StsError, "Can not open Video file");
  	//cap.get(CV_CAP_PROP_FRAME_COUNT) contains the number of frames in the video;
  	for(int frameNum = 0; frameNum < cap.get(CV_CAP_PROP_FRAME_COUNT);frameNum++)
  	{
  		Mat frame;
  		cap >> frame; // get the next frame from video
  		frames.push_back(frame);
  	}
  }
  catch( cv::Exception& e ){
    std::cerr << e.msg << std::endl;
    exit(1);
  }
	
}
/*
It saves a vector of frames into jpg images into the outputDir as 1.jpg,2.jpg etc where 1,2 etc represents the frame number
*/
void save_frames(std::vector<Mat> &frames, const std::string& outputDir)
{
	int frameNumber=0;
	std::vector<int> compression_params;
	compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
	compression_params.push_back(100);
	for(std::vector<Mat>::iterator frame = frames.begin(); frame != frames.end(); ++frame){
	  std::string filePath = outputDir + std::to_string(static_cast<long long>(++frameNumber))+ ".jpg";
	  imwrite(filePath,*frame,compression_params);
	}
}
/*
It put text on image and saves the image
*/
void textonImage(std::vector<Mat> &images,const std::string &outputDir)
{
	std::vector<Mat> textimages;
	int frameNumber=0;
	for(int i = 0; i < images.size(); i++)
	{
		if(!images[i].data)
		std::cerr<<"Error loading image" << std::endl;
		putText(images[i], "Image "+std::to_string(static_cast<long long>(++frameNumber)), Point(5,500), FONT_HERSHEY_DUPLEX, 5, Scalar(0,143,143), 2);
		textimages.push_back(images[i]);
	}
	save_frames(textimages,outputDir);
}

std::vector<Mat> getframes(const char * filename)
{
	FILE *f;
	AVCodec *in_codec = NULL;
	AVCodecContext* avctx = NULL;
	AVFormatContext *ifmt_ctx=NULL;
	AVFrame *frame,*framergb;
	AVPacket avpkt;
	int bytes,framecount=0,ret,frameFinished;
	uint8_t *buffer,*inbuf;
	std::vector<Mat> images;
	av_register_all();
	if (avformat_open_input(&ifmt_ctx, filename,NULL,NULL)!=0)
	{
		fprintf(stderr, "Could not open input file '%s'", filename);
		exit(1);
	}
	// Extract streams description
	if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0)
	{
		fprintf(stderr, "Failed to retrieve input stream information");
		exit(1);
	}
	// Print detailed information about the input or output format,
	// such as duration, bitrate, streams, container, programs, metadata, side data, codec and time base.
	av_dump_format(ifmt_ctx, 0, filename, 0);
	int videoStream = -1;
	for (int i = 0; i < ifmt_ctx->nb_streams; i++)
	{
		if (ifmt_ctx->streams[i]->codec->coder_type == AVMEDIA_TYPE_VIDEO)
		{
			videoStream = i;
			avctx = ifmt_ctx->streams[i]->codec;
			in_codec = avcodec_find_decoder(avctx->codec_id);
			if (!in_codec)
			{
				fprintf(stderr, "in codec not found\n");
				exit(1);
			}
			break;
		}
	}
	if(avcodec_open2(avctx,in_codec,NULL) < 0) exit(1);
	frame = avcodec_alloc_frame();
	if (!frame) {
			fprintf(stderr, "Could not allocate video frame\n");
			exit(1);
	}

	framergb = avcodec_alloc_frame();
	if (!framergb) {
			fprintf(stderr, "Could not allocate video frame\n");
			exit(1);
	}

	bytes=avpicture_get_size(PIX_FMT_RGB24, avctx->width, avctx->height);
	buffer=(uint8_t *)av_malloc(bytes*sizeof(uint8_t));
	avpicture_fill((AVPicture *)framergb, buffer, AV_PIX_FMT_BGR24,
									avctx->width, avctx->height);
	// av_free(buffer);
	std::vector<int> compression_params;
	compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
	compression_params.push_back(100);
	while (av_read_frame(ifmt_ctx, &avpkt) >= 0)
	{
		// Is this a packet from the video stream?
		if (avpkt.stream_index == videoStream)
		{

			// Decode video frame
			avcodec_decode_video2(avctx, frame, &frameFinished, &avpkt);
			// Did we get a video frame?
			if (frameFinished)
			{
				// Convert the image from its native format to RGB
				struct SwsContext *ctx = sws_getContext(avctx->width, avctx->height, avctx->pix_fmt, avctx->width,
						avctx->height, AV_PIX_FMT_BGR24, SWS_BICUBIC, NULL, NULL, NULL);
				sws_scale(ctx, frame->data, frame->linesize, 0, avctx->height,
						framergb->data, framergb->linesize);
				cv::Mat img(avctx->height, avctx->width, CV_8UC3, framergb->data[0], framergb->linesize[0]);
				images.push_back(img);
				putText(img, "Image "+std::to_string(static_cast<long long>(framecount)), Point(5,500), FONT_HERSHEY_DUPLEX, 5, Scalar(0,143,143), 2);
				std::string filePath = "/home/chandra/Videos/Images/" + std::to_string(static_cast<long long>(framecount))+ ".jpg";
				imwrite(filePath,img,compression_params);
				framecount++;
				sws_freeContext(ctx);
			}
		}
		av_packet_unref(&avpkt);
		// Free the packet that was allocated by av_read_frame
		av_free_packet(&avpkt);
	}
	printf("Total Frames: %d",framecount);
	// Free the RGB image
	av_free(framergb);
	av_free(frame);
	avcodec_close(avctx);
	// Close the video file
	avformat_close_input(&ifmt_ctx);
	return images;
}


int main(int argc, char** argv)
{
	const char* filename = "/home/chandra/Videos/2.mp4";
	const std::string outputPath = "/home/chandra/Videos/Images/";
	std::vector<Mat> frames = getframes(filename);
	// save_frames(frames,outputPath);
	// textonImage(frames,outputPath);
	return 0;
}